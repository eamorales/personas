package BeanRespuesta;

import java.util.LinkedHashMap;
import java.util.Map;

import com.mx.beans.BeanGenerico;
/**
 * @Autor: Gustavo Bujano Guzm�n
 * @Fecha: 06/06/2017
 * @version: 1.0
 * */
public class RespuestaBase extends BeanGenerico{
	
	String mensaje;
	String codigoError;
	String idSesion;
	Map<String,String> catalogoUsado = new LinkedHashMap<>();

	public String getCodigoError() {
		return codigoError;
	}

	public void setCodigoError(String codigoError) {
		this.codigoError = codigoError;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public void setRespuestaBase(String codigoError2, String mensaje2) {
		this.codigoError = codigoError2;
		this.mensaje = mensaje2;
	}

	public Map<String,String> getCatalogoUsado() {
		return catalogoUsado;
	}

	public void setCatalogoUsado(Map<String,String> catalogoUsado) {
		this.catalogoUsado = catalogoUsado;
	}

	public String getIdSesion() {
		return idSesion;
	}

	public void setIdSesion(String idSesion) {
		this.idSesion = idSesion;
	}
	
	
	
	
}
