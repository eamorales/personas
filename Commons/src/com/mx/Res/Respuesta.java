package com.mx.Res;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import com.mx.beans.BeanAlertas;
import com.mx.beans.BeanAutorizacionesPendientes;
import com.mx.beans.BeanCatalogosDatos;
import com.mx.beans.BeanDireUbic;
import com.mx.beans.BeanDireccion;
import com.mx.beans.BeanDomicilios;
import com.mx.beans.BeanDomiciliosHistorico;
import com.mx.beans.BeanExpedientes;
import com.mx.beans.BeanIdentificacion;
import com.mx.beans.BeanImagenAlfresco;
import com.mx.beans.BeanInvolucradosLista;
import com.mx.beans.BeanInvolucradosPrincipal;
import com.mx.beans.BeanPosicionGlobalPrincipalBean;
import com.mx.beans.BeanTelefonos;
import com.mx.beans.BeanTipoNumeracion;
import com.mx.beans.BeanTipoTelefono;
import com.mx.beans.BeanTipoUbicacion;
import com.mx.beans.ErrorDTO;
import com.mx.beans.Busqueda.BeanCliente;

public class Respuesta {

	private ErrorDTO error;
	private boolean valido;
	private List<BeanAlertas> alertas;
	private List<BeanTipoTelefono> listatipoTelefonos;
	private List<BeanTipoTelefono> listaCompTelefonos;

	private BeanTelefonos telefonos;
	private List<BeanTelefonos> listaTelefonos;

	/** Respuestas Domicilios */
	private BeanDireccion direccion;
	private List<BeanDomiciliosHistorico> direcciones;
	private BeanDireUbic direUbic;
	private List<BeanDireUbic> direUbics;
	private BeanTipoUbicacion tipoUbicacion;
	private List<BeanTipoUbicacion> listatipoUbicaciones;

	private LinkedHashMap<String, String> catalogoTipoIdentificacion;
	private LinkedHashMap<String, String> catalogoTipoIdentificacionExpresiones;

	private LinkedHashMap<String, String> tablaIdentificacion;
	// Atributos de respuesta Identificaciones
	private BeanIdentificacion identificaciones;
	private List<BeanIdentificacion> listaIdentificacion;
	private List<BeanDomicilios> listaColonias;
	private BeanImagenAlfresco imagenAlfresco;
	private BeanTipoNumeracion tipoNumeracion;
	private List<BeanTipoNumeracion> listaTipoNumeraciones;
	/** Involucrados */
	private List<BeanInvolucradosLista> listaEdoCivil;
	private List<BeanInvolucradosLista> listaOcupacion;
	private List<BeanInvolucradosPrincipal> listaInvolucrados;
	private List<BeanInvolucradosLista> listaNacionalidad;
	private List<BeanInvolucradosLista> listaNacimiento;

	/** Lista de Autorizaciones Pendientes */
	private List<BeanAutorizacionesPendientes> listaAutorizacionesDao;

	/** posicion global respuesta */
	private BeanPosicionGlobalPrincipalBean posicionGlobalRespuesta;

	/** Involucrados respuesta **/
	private BeanInvolucradosPrincipal beanInvolucradosPrincipal;
	private Map<String, String> catalogoTipoDocumento;
	private List<BeanInvolucradosLista> ListInvolucrados;

	/** Datos Response **/
	private LinkedHashMap<String, String> catalogoEstadoCivil;
	private Map<String, String> listaActividad;
	private List<BeanCatalogosDatos> listaGrado;
	private Map<String, String> listaSector;
	private Map<String, String> listaEstablecimiento;
	private Map<String, String> listaGiro;
	private Map<String, String> listaPuesto;
	private Map<String, String> listaFacultad;
	private Map<String, String> listaCorreo;
	private Map<String, String> listaSituacion;
	private Map<String, String> listaSociedad;
	private Map<String, String> listaTipoDocumentosPers;
	private Map<String, String> listaDocumentosPers;
	private Map<String, String> listaMunicipios;
	private Map<String, String> listaLocalidades;
	private Map<String, String> listaClaveDocumento;






	/** Expedientes **/
	private List<BeanExpedientes> listaExpedientes;
	private List<BeanCatalogosDatos> listaCatalogos;
	private Map<String, String> listObjects;

	/** Clientes **/
	private BeanCatalogosDatos BeanCatalogoDatos;
	private BeanCliente cliente;

	public ErrorDTO getError() {
		return error;
	}

	public List<BeanInvolucradosLista> getListaNacimiento() {
		return listaNacimiento;
	}

	public void setListaNacimiento(List<BeanInvolucradosLista> listaNacimiento) {
		this.listaNacimiento = listaNacimiento;
	}

	public List<BeanInvolucradosLista> getListaNacionalidad() {
		return listaNacionalidad;
	}

	public void setListaNacionalidad(List<BeanInvolucradosLista> listaNacionalidad) {
		this.listaNacionalidad = listaNacionalidad;
	}

	public List<BeanInvolucradosLista> getListaOcupacion() {
		return listaOcupacion;
	}

	public void setListaOcupacion(List<BeanInvolucradosLista> listaOcupacion) {
		this.listaOcupacion = listaOcupacion;
	}

	public List<BeanInvolucradosLista> getListaEdoCivil() {
		return listaEdoCivil;
	}

	public void setListaEdoCivil(List<BeanInvolucradosLista> listaEdoCivil) {
		this.listaEdoCivil = listaEdoCivil;
	}

	public List<BeanTipoTelefono> getListaCompTelefonos() {
		return listaCompTelefonos;
	}

	public void setListaCompTelefonos(List<BeanTipoTelefono> listaCompTelefonos) {
		this.listaCompTelefonos = listaCompTelefonos;
	}

	public void setError(ErrorDTO error) {
		this.error = error;
	}

	public List<BeanInvolucradosLista> getListInvolucrados() {
		return ListInvolucrados;
	}

	public void setListInvolucrados(List<BeanInvolucradosLista> listInvolucrados) {
		ListInvolucrados = listInvolucrados;
	}

	public BeanTelefonos getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(BeanTelefonos telefonos) {
		this.telefonos = telefonos;
	}

	public List<BeanTipoTelefono> getListatipoTelefonos() {
		return listatipoTelefonos;
	}

	public void setListatipoTelefonos(List<BeanTipoTelefono> listatipoTelefonos) {
		this.listatipoTelefonos = listatipoTelefonos;
	}

	public List<BeanTelefonos> getListaTelefonos() {
		return listaTelefonos;
	}

	public void setListaTelefonos(List<BeanTelefonos> listaTelefonos) {
		this.listaTelefonos = listaTelefonos;
	}

	public List<BeanDomicilios> getListaColonias() {
		return listaColonias;
	}

	public void setListaColonias(List<BeanDomicilios> listaColonias) {
		this.listaColonias = listaColonias;
	}

	/**
	 * @return the direccion
	 */
	public BeanDireccion getDireccion() {
		return direccion;
	}

	/**
	 * @param direccion
	 *            the direccion to set
	 */
	public void setDireccion(BeanDireccion direccion) {
		this.direccion = direccion;
	}

	/**
	 * @return the direcciones
	 */
	public List<BeanDomiciliosHistorico> getDirecciones() {
		return direcciones;
	}

	/**
	 * @param direcciones
	 *            the direcciones to set
	 */
	public void setDirecciones(List<BeanDomiciliosHistorico> direcciones) {
		this.direcciones = direcciones;
	}

	/**
	 * @return the direUbic
	 */
	public BeanDireUbic getDireUbic() {
		return direUbic;
	}

	/**
	 * @param direUbic
	 *            the direUbic to set
	 */
	public void setDireUbic(BeanDireUbic direUbic) {
		this.direUbic = direUbic;
	}

	/**
	 * @return the direUbics
	 */
	public List<BeanDireUbic> getDireUbics() {
		return direUbics;
	}

	/**
	 * @param direUbics
	 *            the direUbics to set
	 */
	public void setDireUbics(List<BeanDireUbic> direUbics) {
		this.direUbics = direUbics;
	}

	/**
	 * @return the tipoUbicacion
	 */
	public BeanTipoUbicacion getTipoUbicacion() {
		return tipoUbicacion;
	}

	/**
	 * @param tipoUbicacion
	 *            the tipoUbicacion to set
	 */
	public void setTipoUbicacion(BeanTipoUbicacion tipoUbicacion) {
		this.tipoUbicacion = tipoUbicacion;
	}

	public List<BeanTipoUbicacion> getListatipoUbicaciones() {
		return listatipoUbicaciones;
	}

	public void setListatipoUbicaciones(List<BeanTipoUbicacion> listatipoUbicaciones) {
		this.listatipoUbicaciones = listatipoUbicaciones;
	}

	public LinkedHashMap<String, String> getCatalogoTipoIdentificacion() {
		return catalogoTipoIdentificacion;
	}

	public void setCatalogoTipoIdentificacion(LinkedHashMap<String, String> catalogoTipoIdentificacion) {
		this.catalogoTipoIdentificacion = catalogoTipoIdentificacion;
	}

	/**
	 * @return the identificaciones
	 */
	public BeanIdentificacion getIdentificaciones() {
		return identificaciones;
	}

	/**
	 * @param identificaciones
	 *            the identificaciones to set
	 */
	public void setIdentificaciones(BeanIdentificacion identificaciones) {
		this.identificaciones = identificaciones;
	}

	/**
	 * @return the listaIdentificacion
	 */
	public List<BeanIdentificacion> getListaIdentificacion() {
		return listaIdentificacion;
	}

	/**
	 * @param listaIdentificacion
	 *            the listaIdentificacion to set
	 */
	public void setListaIdentificacion(List<BeanIdentificacion> listaIdentificacion) {
		this.listaIdentificacion = listaIdentificacion;
	}

	public BeanImagenAlfresco getImagenAlfresco() {
		return imagenAlfresco;
	}

	public void setImagenAlfresco(BeanImagenAlfresco imagenAlfresco) {
		this.imagenAlfresco = imagenAlfresco;
	}

	public LinkedHashMap<String, String> getTablaIdentificacion() {
		return tablaIdentificacion;
	}

	public void setTablaIdentificacion(LinkedHashMap<String, String> tablaIdentificacion) {
		this.tablaIdentificacion = tablaIdentificacion;
	}

	/**
	 * @return the tipoNumeracion
	 */
	public BeanTipoNumeracion getTipoNumeracion() {
		return tipoNumeracion;
	}

	/**
	 * @param tipoNumeracion
	 *            the tipoNumeracion to set
	 */
	public void setTipoNumeracion(BeanTipoNumeracion tipoNumeracion) {
		this.tipoNumeracion = tipoNumeracion;
	}

	public List<BeanTipoNumeracion> getListaTipoNumeraciones() {
		return listaTipoNumeraciones;
	}

	public void setListaTipoNumeraciones(List<BeanTipoNumeracion> listaTipoNumeraciones) {
		this.listaTipoNumeraciones = listaTipoNumeraciones;
	}

	/**
	 * @return the listaAutorizacionesDao
	 */
	public List<BeanAutorizacionesPendientes> getListaAutorizacionesDao() {
		return listaAutorizacionesDao;
	}

	/**
	 * @param listaAutorizacionesDao
	 *            the listaAutorizacionesDao to set
	 */
	public void setListaAutorizacionesDao(List<BeanAutorizacionesPendientes> listaAutorizacionesDao) {
		this.listaAutorizacionesDao = listaAutorizacionesDao;
	}

	public BeanPosicionGlobalPrincipalBean getPosicionGlobalRespuesta() {
		return posicionGlobalRespuesta;
	}

	public void setPosicionGlobalRespuesta(BeanPosicionGlobalPrincipalBean posicionGlobalRespuesta) {
		this.posicionGlobalRespuesta = posicionGlobalRespuesta;
	}

	public BeanInvolucradosPrincipal getBeanInvolucradosPrincipal() {
		return beanInvolucradosPrincipal;
	}

	public void setBeanInvolucradosPrincipal(BeanInvolucradosPrincipal beanInvolucradosPrincipal) {
		this.beanInvolucradosPrincipal = beanInvolucradosPrincipal;
	}

	public List<BeanInvolucradosPrincipal> getListaInvolucrados() {
		return listaInvolucrados;
	}

	public void setListaInvolucrados(List<BeanInvolucradosPrincipal> listaInvolucrados) {
		this.listaInvolucrados = listaInvolucrados;
	}

	public void setCatalogoTipoDocumento(Map<String, String> elementsCombo) {
		this.catalogoTipoDocumento = elementsCombo;
	}

	public Map<String, String> getCatalogoTipoDocumento() {
		return catalogoTipoDocumento;
	}

	public List<BeanExpedientes> getListaExpedientes() {
		return listaExpedientes;
	}

	public void setListaExpedientes(List<BeanExpedientes> listaExpedientes) {
		this.listaExpedientes = listaExpedientes;
	}

	public BeanCliente getCliente() {
		return cliente;
	}

	public void setCliente(BeanCliente cliente) {
		this.cliente = cliente;
	}

	public LinkedHashMap<String, String> getCatalogoTipoIdentificacionExpresiones() {
		return catalogoTipoIdentificacionExpresiones;
	}

	public void setCatalogoTipoIdentificacionExpresiones(
			LinkedHashMap<String, String> catalogoTipoIdentificacionExpresiones) {
		this.catalogoTipoIdentificacionExpresiones = catalogoTipoIdentificacionExpresiones;
	}

	public LinkedHashMap<String, String> getCatalogoEstadoCivil() {
		return catalogoEstadoCivil;
	}

	public void setCatalogoEstadoCivil(LinkedHashMap<String, String> catalogoEstadoCivil) {
		this.catalogoEstadoCivil = catalogoEstadoCivil;
	}

	public List<BeanCatalogosDatos> getListaGrado() {
		return listaGrado;
	}

	public void setListaGrado(List<BeanCatalogosDatos> listaGrado) {
		this.listaGrado = listaGrado;
	}

	public List<BeanCatalogosDatos> getListaCatalogos() {
		return listaCatalogos;
	}

	public void setListaCatalogos(List<BeanCatalogosDatos> listaCatalogos) {
		this.listaCatalogos = listaCatalogos;
	}

	public Map<String, String> getListObjects() {
		return listObjects;
	}

	public void setListObjects(Map<String, String> listObjects) {
		this.listObjects = listObjects;
	}

	public Map<String, String> getListaSector() {
		return listaSector;
	}

	public void setListaSector(Map<String, String> listaSector) {
		this.listaSector = listaSector;
	}

	public Map<String, String> getListaEstablecimiento() {
		return listaEstablecimiento;
	}

	public void setListaEstablecimiento(Map<String, String> listaEstablecimiento) {
		this.listaEstablecimiento = listaEstablecimiento;
	}

	public Map<String, String> getListaGiro() {
		return listaGiro;
	}

	public void setListaGiro(Map<String, String> listaGiro) {
		this.listaGiro = listaGiro;
	}

	public Map<String, String> getListaPuesto() {
		return listaPuesto;
	}

	public void setListaPuesto(Map<String, String> listaPuesto) {
		this.listaPuesto = listaPuesto;
	}

	public Map<String, String> getListaFacultad() {
		return listaFacultad;
	}

	public void setListaFacultad(Map<String, String> listaFacultad) {
		this.listaFacultad = listaFacultad;
	}

	public Map<String, String> getListaCorreo() {
		return listaCorreo;
	}

	public void setListaCorreo(Map<String, String> listaCorreo) {
		this.listaCorreo = listaCorreo;
	}

	public Map<String, String> getListaSituacion() {
		return listaSituacion;
	}

	public void setListaSituacion(Map<String, String> listaSituacion) {
		this.listaSituacion = listaSituacion;
	}

	public Map<String, String> getListaSociedad() {
		return listaSociedad;
	}

	public void setListaSociedad(Map<String, String> listaSociedad) {
		this.listaSociedad = listaSociedad;
	}

	public Map<String, String> getListaActividad() {
		return listaActividad;
	}

	public void setListaActividad(Map<String, String> listaActividad) {
		this.listaActividad = listaActividad;
	}

	public Map<String, String> getListaTipoDocumentosPers() {
		return listaTipoDocumentosPers;
	}

	public void setListaTipoDocumentosPers(Map<String, String> listaTipoDocumentosPers) {
		this.listaTipoDocumentosPers = listaTipoDocumentosPers;
	}

	public Map<String, String> getListaDocumentosPers() {
		return listaDocumentosPers;
	}

	public void setListaDocumentosPers(Map<String, String> listaDocumentosPers) {
		this.listaDocumentosPers = listaDocumentosPers;
	}

	public Map<String, String> getListaMunicipios() {
		return listaMunicipios;
	}

	public void setListaMunicipios(Map<String, String> listaMunicipios) {
		this.listaMunicipios = listaMunicipios;
	}

	public Map<String, String> getListaLocalidades() {
		return listaLocalidades;
	}

	public void setListaLocalidades(Map<String, String> listaLocalidades) {
		this.listaLocalidades = listaLocalidades;
	}

	public List<BeanAlertas> getAlertas() {
		return alertas;
	}

	public void setAlertas(List<BeanAlertas> alertas) {
		this.alertas = alertas;
	}


	public boolean isValido() {
		return valido;
	}

	public void setValido(boolean valido) {
		this.valido = valido;
	}

	public Map<String, String> getListaClaveDocumento() {
		return listaClaveDocumento;
	}

	public void setListaClaveDocumento(Map<String, String> listaClaveDocumento) {
		this.listaClaveDocumento = listaClaveDocumento;
	}

	public BeanCatalogosDatos getBeanCatalogoDatos() {
		return BeanCatalogoDatos;
	}

	public void setBeanCatalogoDatos(BeanCatalogosDatos beanCatalogoDatos) {
		BeanCatalogoDatos = beanCatalogoDatos;
	}

	
	
	
}
