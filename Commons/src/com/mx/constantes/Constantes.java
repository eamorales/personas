package com.mx.constantes;
/**
 * @author GBG
 * @version 1.0
 * @fecha 06/06/2017
 * @descripci�n: aqui estar�n todos los datos de variables que se puedan utilizar en la pantalla
 * */

import org.springframework.stereotype.Service;


@Service
public class Constantes {
		
	/**
	 * 
	 * Genericos
	 * 
	 * */
	public static int STATUS_ALTA = 1;
	

	private final int  STATUS_BAJA = 2;
	public static String BLANK="";
	public static int OCHO = 8;
	public static int SIETE= 7;
	public static int CERO = 0;
	public static String REGISTRODEFAULT= "0";
	public static String ERROR ="ERROR";
	public static String EXITO ="EXITO";
	public static String CLAVE_CORRECTA ="0";
	public static String ERROR_EXECUTE = "1";
	public static String EXITO_EXECUTE = "0";
	
	/*telefonos*/
	public static int  LADAMX = 55;
	public static int DATOS_TELEFONO = 0;
	public static int DIRECTORIO_TELEFONO = 1;


	public static String telefonos_Datostelefono_pantalla="1"; /*Pantalla de datos telefonos*/
	public static String telefonos_Directoriotelefonico_pantalla="2"; /*Pantalla de directorio telefonos*/
	/*Identificaciones*/
	public static String identificacion_datos_pantalla="3";
	public static String identificacion_Directorio_pantalla="4";
	
	// Tipos de Header
	public  static String HEADER_STATUS="COD_STATUS";
	public static String HEADER_IDMESSAGE="IDMESSAGE";
	public static String HEADER_MESSAGE="MESSAGE";
	public static String HEADER_SESSION="SESSION_PARAM";
	public static String HEADER_NAVEGADOR="user-agent";
	public static String HEADER_CONTET="Content-Type";
	public static String HEADER_ACCEPT="Accept";
	public static String COD_STATUS_OK="OK";
	public static String COD_STATUS_ERROR="ERROR";
	public static String CONTENT_TYPE="application/json;charset=UTF-8";
	
	public static String DOMICILIOSTAB01="1";
	public static String DOMICILIOSTAB02="2";
	public static String DOMICILIOSTAB03="3";
	
	
	

	public static String ESTATUS_ACTIVO = "1";

	public static String ESTATUS_BAJA = "2";

	public static String PATH_ESTADONAC = "/estados";
	public static String PATH_CORREOS = "/correos";
	public static String PATH_OCUPACIONES = "/ocupaciones";
	public static String PATH_TIPO_TELEFONO = "/tipoTelefono";
	public static String PATH_COMPANIA_TELEFONO = "/companiaTel";
	public static String PATH_COLONIA_DOMICILIO="/colonias";
	public static String PATH_AREAS="/areas";
	public static String PATH_BANCOS="/bancos";
	public static String PATH_CIUDADES="/ciudades";
	public static String PATH_COMOENTERO="/comoEntero";
	public static String PATH_COMPANIATEL="/companiaTel";
	public static String PATH_EDOCIVIL="/estadoCivil";
	public static String PATH_GRADOESTUDIOS="/gradoEstudios";
	public static String PATH_LOCALIDADES="/localidades";
	public static String PATH_MUNICIPIOS="/municipios";
	public static String PATH_PAISES="/paises";
	public static String PATH_SUCURSALES="/paises";
	public static String PATH_ZONAS="/paises";
	public static String PATH_ALERTAS="/alertas";
	public static String ERROR_CONSULTA_TABLA_AUTORIZACIONESPENDIENTES = "NO SE ENCONTRARON AUTORIZACIONES PENDIENTES DE LA PERSONA SELECCIONADA";
	public static String ERROR_POSICIONGLOBAL = " NO SE ENCONTRARON REGISTROS PARA ESTA CONSULTA";

	

	/**
	 * 
	 * DOMICILIOS
	 * */
	
	/**
	 * Secci�n de Error en las tablas
	 * */
	

	public static String getBLANK() {
		return BLANK;
	}

	public static void setBLANK(String bLANK) {
		BLANK = bLANK;
	}


	public static int getOCHO() {
		return OCHO;
	}

	public static void setOCHO(int oCHO) {
		OCHO = oCHO;
	}

	public static int getSIETE() {
		return SIETE;
	}

	public static void setSIETE(int sIETE) {
		SIETE = sIETE;
	}

	public static int getCERO() {
		return CERO;
	}

	public static void setCERO(int cERO) {
		CERO = cERO;
	}

	public static String getREGISTRODEFAULT() {
		return REGISTRODEFAULT;
	}

	public static void setREGISTRODEFAULT(String rEGISTRODEFAULT) {
		REGISTRODEFAULT = rEGISTRODEFAULT;
	}

	public static String getERROR() {
		return ERROR;
	}

	public static void setERROR(String eRROR) {
		ERROR = eRROR;
	}

	public static String getEXITO() {
		return EXITO;
	}

	public static void setEXITO(String eXITO) {
		EXITO = eXITO;
	}

	public static String getCLAVE_CORRECTA() {
		return CLAVE_CORRECTA;
	}

	public static void setCLAVE_CORRECTA(String cLAVE_CORRECTA) {
		CLAVE_CORRECTA = cLAVE_CORRECTA;
	}

	public static int getLADAMX() {
		return LADAMX;
	}

	public static void setLADAMX(int lADAMX) {
		LADAMX = lADAMX;
	}

	public static int getDATOS_TELEFONO() {
		return DATOS_TELEFONO;
	}

	public static void setDATOS_TELEFONO(int dATOS_TELEFONO) {
		DATOS_TELEFONO = dATOS_TELEFONO;
	}

	public static int getDIRECTORIO_TELEFONO() {
		return DIRECTORIO_TELEFONO;
	}

	public static void setDIRECTORIO_TELEFONO(int dIRECTORIO_TELEFONO) {
		DIRECTORIO_TELEFONO = dIRECTORIO_TELEFONO;
	}

	public static String getTelefonos_Datostelefono_pantalla() {
		return telefonos_Datostelefono_pantalla;
	}

	public static void setTelefonos_Datostelefono_pantalla(String telefonos_Datostelefono_pantalla) {
		Constantes.telefonos_Datostelefono_pantalla = telefonos_Datostelefono_pantalla;
	}

	public static String getTelefonos_Directoriotelefonico_pantalla() {
		return telefonos_Directoriotelefonico_pantalla;
	}

	public static void setTelefonos_Directoriotelefonico_pantalla(String telefonos_Directoriotelefonico_pantalla) {
		Constantes.telefonos_Directoriotelefonico_pantalla = telefonos_Directoriotelefonico_pantalla;
	}

	public static String getIdentificacion_datos_pantalla() {
		return identificacion_datos_pantalla;
	}

	public static void setIdentificacion_datos_pantalla(String identificacion_datos_pantalla) {
		Constantes.identificacion_datos_pantalla = identificacion_datos_pantalla;
	}

	public static String getIdentificacion_Directorio_pantalla() {
		return identificacion_Directorio_pantalla;
	}

	public static void setIdentificacion_Directorio_pantalla(String identificacion_Directorio_pantalla) {
		Constantes.identificacion_Directorio_pantalla = identificacion_Directorio_pantalla;
	}

	public static String getHEADER_STATUS() {
		return HEADER_STATUS;
	}

	public static void setHEADER_STATUS(String hEADER_STATUS) {
		HEADER_STATUS = hEADER_STATUS;
	}

	public static String getHEADER_IDMESSAGE() {
		return HEADER_IDMESSAGE;
	}

	public static void setHEADER_IDMESSAGE(String hEADER_IDMESSAGE) {
		HEADER_IDMESSAGE = hEADER_IDMESSAGE;
	}

	public static String getHEADER_MESSAGE() {
		return HEADER_MESSAGE;
	}

	public static void setHEADER_MESSAGE(String hEADER_MESSAGE) {
		HEADER_MESSAGE = hEADER_MESSAGE;
	}

	public static String getHEADER_SESSION() {
		return HEADER_SESSION;
	}

	public static void setHEADER_SESSION(String hEADER_SESSION) {
		HEADER_SESSION = hEADER_SESSION;
	}

	public static String getHEADER_NAVEGADOR() {
		return HEADER_NAVEGADOR;
	}

	public static void setHEADER_NAVEGADOR(String hEADER_NAVEGADOR) {
		HEADER_NAVEGADOR = hEADER_NAVEGADOR;
	}

	public static String getHEADER_CONTET() {
		return HEADER_CONTET;
	}

	public static void setHEADER_CONTET(String hEADER_CONTET) {
		HEADER_CONTET = hEADER_CONTET;
	}

	public static String getHEADER_ACCEPT() {
		return HEADER_ACCEPT;
	}

	public static void setHEADER_ACCEPT(String hEADER_ACCEPT) {
		HEADER_ACCEPT = hEADER_ACCEPT;
	}

	public static String getCOD_STATUS_OK() {
		return COD_STATUS_OK;
	}

	public static void setCOD_STATUS_OK(String cOD_STATUS_OK) {
		COD_STATUS_OK = cOD_STATUS_OK;
	}

	public static String getCOD_STATUS_ERROR() {
		return COD_STATUS_ERROR;
	}

	public static void setCOD_STATUS_ERROR(String cOD_STATUS_ERROR) {
		COD_STATUS_ERROR = cOD_STATUS_ERROR;
	}

	public static String getCONTENT_TYPE() {
		return CONTENT_TYPE;
	}

	public static void setCONTENT_TYPE(String cONTENT_TYPE) {
		CONTENT_TYPE = cONTENT_TYPE;
	}

	public static String getDOMICILIOSTAB01() {
		return DOMICILIOSTAB01;
	}

	public static void setDOMICILIOSTAB01(String dOMICILIOSTAB01) {
		DOMICILIOSTAB01 = dOMICILIOSTAB01;
	}

	public static String getDOMICILIOSTAB02() {
		return DOMICILIOSTAB02;
	}

	public static void setDOMICILIOSTAB02(String dOMICILIOSTAB02) {
		DOMICILIOSTAB02 = dOMICILIOSTAB02;
	}

	public static String getDOMICILIOSTAB03() {
		return DOMICILIOSTAB03;
	}

	public static void setDOMICILIOSTAB03(String dOMICILIOSTAB03) {
		DOMICILIOSTAB03 = dOMICILIOSTAB03;
	}

	public static String getESTATUS_ACTIVO() {
		return ESTATUS_ACTIVO;
	}

	public static void setESTATUS_ACTIVO(String eSTATUS_ACTIVO) {
		ESTATUS_ACTIVO = eSTATUS_ACTIVO;
	}

	public static String getESTATUS_BAJA() {
		return ESTATUS_BAJA;
	}

	public static void setESTATUS_BAJA(String eSTATUS_BAJA) {
		ESTATUS_BAJA = eSTATUS_BAJA;
	}

	public static String getPATH_TIPO_TELEFONO() {
		return PATH_TIPO_TELEFONO;
	}

	public static void setPATH_TIPO_TELEFONO(String pATH_TIPO_TELEFONO) {
		PATH_TIPO_TELEFONO = pATH_TIPO_TELEFONO;
	}

	public static String getPATH_COMPANIA_TELEFONO() {
		return PATH_COMPANIA_TELEFONO;
	}

	public static void setPATH_COMPANIA_TELEFONO(String pATH_COMPANIA_TELEFONO) {
		PATH_COMPANIA_TELEFONO = pATH_COMPANIA_TELEFONO;
	}

	public static String getPATH_COLONIA_DOMICILIO() {
		return PATH_COLONIA_DOMICILIO;
	}

	public static void setPATH_COLONIA_DOMICILIO(String pATH_COLONIA_DOMICILIO) {
		PATH_COLONIA_DOMICILIO = pATH_COLONIA_DOMICILIO;
	}

	public static String getPATH_AREAS() {
		return PATH_AREAS;
	}

	public static void setPATH_AREAS(String pATH_AREAS) {
		PATH_AREAS = pATH_AREAS;
	}

	public static String getPATH_BANCOS() {
		return PATH_BANCOS;
	}

	public static void setPATH_BANCOS(String pATH_BANCOS) {
		PATH_BANCOS = pATH_BANCOS;
	}

	public static String getPATH_CIUDADES() {
		return PATH_CIUDADES;
	}

	public static void setPATH_CIUDADES(String pATH_CIUDADES) {
		PATH_CIUDADES = pATH_CIUDADES;
	}

	public static String getPATH_COMOENTERO() {
		return PATH_COMOENTERO;
	}

	public static void setPATH_COMOENTERO(String pATH_COMOENTERO) {
		PATH_COMOENTERO = pATH_COMOENTERO;
	}

	public static String getPATH_COMPANIATEL() {
		return PATH_COMPANIATEL;
	}

	public static void setPATH_COMPANIATEL(String pATH_COMPANIATEL) {
		PATH_COMPANIATEL = pATH_COMPANIATEL;
	}

	public static String getPATH_EDOCIVIL() {
		return PATH_EDOCIVIL;
	}

	public static void setPATH_EDOCIVIL(String pATH_EDOCIVIL) {
		PATH_EDOCIVIL = pATH_EDOCIVIL;
	}

	public static String getPATH_GRADOESTUDIOS() {
		return PATH_GRADOESTUDIOS;
	}

	public static void setPATH_GRADOESTUDIOS(String pATH_GRADOESTUDIOS) {
		PATH_GRADOESTUDIOS = pATH_GRADOESTUDIOS;
	}

	public static String getPATH_LOCALIDADES() {
		return PATH_LOCALIDADES;
	}

	public static void setPATH_LOCALIDADES(String pATH_LOCALIDADES) {
		PATH_LOCALIDADES = pATH_LOCALIDADES;
	}

	public static String getPATH_MUNICIPIOS() {
		return PATH_MUNICIPIOS;
	}

	public static void setPATH_MUNICIPIOS(String pATH_MUNICIPIOS) {
		PATH_MUNICIPIOS = pATH_MUNICIPIOS;
	}

	public static String getPATH_PAISES() {
		return PATH_PAISES;
	}

	public static void setPATH_PAISES(String pATH_PAISES) {
		PATH_PAISES = pATH_PAISES;
	}

	public static String getPATH_SUCURSALES() {
		return PATH_SUCURSALES;
	}

	public static void setPATH_SUCURSALES(String pATH_SUCURSALES) {
		PATH_SUCURSALES = pATH_SUCURSALES;
	}

	public static String getPATH_ZONAS() {
		return PATH_ZONAS;
	}

	public static void setPATH_ZONAS(String pATH_ZONAS) {
		PATH_ZONAS = pATH_ZONAS;
	}

	public static String getPATH_ALERTAS() {
		return PATH_ALERTAS;
	}

	public static void setPATH_ALERTAS(String pATH_ALERTAS) {
		PATH_ALERTAS = pATH_ALERTAS;
	}

	public static int getSTATUS_ALTA() {
		return STATUS_ALTA;
	}
	
	public int getSTATUS_BAJA() {
		return STATUS_BAJA;
	}

}
