package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;
/**
 * @author JMata
 * @date 11/06/2017
 * 
 * */

public class BeanPosicionGlobalPrincipalBean extends RespuestaBase {

	
	List<BeanPosicionGlobalAhorro> listaAhorro;
	List<BeanPosicionGlobalCreditos> listaCreditos;
	List<BeanPosicionGlobalInversion> listaInversion;
	List<BeanPosicionGlobalSeguros> listaSeguros;
	
	private BeanPosicionGlobalAhorro ahorro;
	private BeanPosicionGlobalCreditos creditos;
	private BeanPosicionGlobalInversion inversion;
	private BeanPosicionGlobalSeguros seguros;
	
	
	public BeanPosicionGlobalAhorro getAhorro() {
		return ahorro;
	}
	public void setAhorro(BeanPosicionGlobalAhorro ahorro) {
		this.ahorro = ahorro;
	}
	public BeanPosicionGlobalCreditos getCreditos() {
		return creditos;
	}
	public void setCreditos(BeanPosicionGlobalCreditos creditos) {
		this.creditos = creditos;
	}
	public BeanPosicionGlobalInversion getInversion() {
		return inversion;
	}
	public void setInversion(BeanPosicionGlobalInversion inversion) {
		this.inversion = inversion;
	}
	public BeanPosicionGlobalSeguros getSeguros() {
		return seguros;
	}
	public void setSeguros(BeanPosicionGlobalSeguros seguros) {
		this.seguros = seguros;
	}
	public List<BeanPosicionGlobalAhorro> getListaAhorro() {
		return listaAhorro;
	}
	public void setListaAhorro(List<BeanPosicionGlobalAhorro> listaAhorro) {
		this.listaAhorro = listaAhorro;
	}
	public List<BeanPosicionGlobalCreditos> getListaCreditos() {
		return listaCreditos;
	}
	public void setListaCreditos(List<BeanPosicionGlobalCreditos> listaCreditos) {
		this.listaCreditos = listaCreditos;
	}
	public List<BeanPosicionGlobalInversion> getListaInversion() {
		return listaInversion;
	}
	public void setListaInversion(List<BeanPosicionGlobalInversion> listaInversion) {
		this.listaInversion = listaInversion;
	}
	public List<BeanPosicionGlobalSeguros> getListaSeguros() {
		return listaSeguros;
	}
	public void setListaSeguros(List<BeanPosicionGlobalSeguros> listaSeguros) {
		this.listaSeguros = listaSeguros;
	}
	
	
}
