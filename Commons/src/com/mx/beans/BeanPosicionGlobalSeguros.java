package com.mx.beans;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class BeanPosicionGlobalSeguros extends RespuestaBase {

	String estatus;
	String cuenta;
	String seguro;
	String producto;
	String idSeg;
	String poiza;
	String suma;
	String fechaVig;
	
	
	public String getIdSeg() {
		return idSeg;
	}
	public void setIdSeg(String idSeg) {
		this.idSeg = idSeg;
	}
	public String getPoiza() {
		return poiza;
	}
	public void setPoiza(String poiza) {
		this.poiza = poiza;
	}
	public String getSuma() {
		return suma;
	}
	public void setSuma(String suma) {
		this.suma = suma;
	}
	public String getFechaVig() {
		return fechaVig;
	}
	public void setFechaVig(String fechaVig) {
		this.fechaVig = fechaVig;
	}
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getSeguro() {
		return seguro;
	}
	public void setSeguro(String seguro) {
		this.seguro = seguro;
	}
	String sumaAsegurada;
	String vigencia;
	
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getSumaAsegurada() {
		return sumaAsegurada;
	}
	public void setSumaAsegurada(String sumaAsegurada) {
		this.sumaAsegurada = sumaAsegurada;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	
}
