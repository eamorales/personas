package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;

/**
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class BeanInvolucradosDatos extends RespuestaBase {
	String tipoInvolucrado; /*representante lagal ||accionista/socio*/
	String primerNombre;
	String segundoNombre;
	String primerApellido;
	String segundoApellido;
	boolean sexo;
	String curp;
	String rfc;
	List <String> estadoCivil;
	List <String> ocupacion;
	String TelefonoMasc1;
	String TelefonoMAsc2;
	List<String>tipoTelefono;
	List<String>compania;
	List<String>nacionalidad;
	List<String>EstadoNacimiento;
	String FechaNacimiento;
	String CorreoElectronico;
	String nombramiento;
	int acciones;
	int codigoPostal;
	boolean principal;
	List<String>colonia;
	String Foto;
	List<String>ubicacion1;
	String descripcionU1;
	List<String>ubicacion2;
	String descripcionU2;
	List<String>ubicacion3;
	String descripcionU3;
	List<String>numeracion1;
	String descripcionNum1;
	List<String>numeracion2;
	String descripcionNum2;
	String latitud;
	String longitud;
	String referencia;
	String direccion;
	
	
	public String getTipoInvolucrado() {
		return tipoInvolucrado;
	}
	public void setTipoInvolucrado(String tipoInvolucrado) {
		this.tipoInvolucrado = tipoInvolucrado;
	}
	public String getPrimerNombre() {
		return primerNombre;
	}
	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}
	public String getSegundoNombre() {
		return segundoNombre;
	}
	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}
	public String getPrimerApellido() {
		return primerApellido;
	}
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	public String getSegundoApellido() {
		return segundoApellido;
	}
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	public boolean isSexo() {
		return sexo;
	}
	public void setSexo(boolean sexo) {
		this.sexo = sexo;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public List<String> getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(List<String> estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public List<String> getOcupacion() {
		return ocupacion;
	}
	public void setOcupacion(List<String> ocupacion) {
		this.ocupacion = ocupacion;
	}
	public String getTelefonoMasc1() {
		return TelefonoMasc1;
	}
	public void setTelefonoMasc1(String telefonoMasc1) {
		TelefonoMasc1 = telefonoMasc1;
	}
	public String getTelefonoMAsc2() {
		return TelefonoMAsc2;
	}
	public void setTelefonoMAsc2(String telefonoMAsc2) {
		TelefonoMAsc2 = telefonoMAsc2;
	}
	public List<String> getTipoTelefono() {
		return tipoTelefono;
	}
	public void setTipoTelefono(List<String> tipoTelefono) {
		this.tipoTelefono = tipoTelefono;
	}
	public List<String> getCompania() {
		return compania;
	}
	public void setCompania(List<String> compania) {
		this.compania = compania;
	}
	public List<String> getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(List<String> nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public List<String> getEstadoNacimiento() {
		return EstadoNacimiento;
	}
	public void setEstadoNacimiento(List<String> estadoNacimiento) {
		EstadoNacimiento = estadoNacimiento;
	}
	public String getFechaNacimiento() {
		return FechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		FechaNacimiento = fechaNacimiento;
	}
	public String getCorreoElectronico() {
		return CorreoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		CorreoElectronico = correoElectronico;
	}
	public String getNombramiento() {
		return nombramiento;
	}
	public void setNombramiento(String nombramiento) {
		this.nombramiento = nombramiento;
	}
	public int getAcciones() {
		return acciones;
	}
	public void setAcciones(int acciones) {
		this.acciones = acciones;
	}
	public int getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(int codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public boolean isPrincipal() {
		return principal;
	}
	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}
	public List<String> getColonia() {
		return colonia;
	}
	public void setColonia(List<String> colonia) {
		this.colonia = colonia;
	}
	public String getFoto() {
		return Foto;
	}
	public void setFoto(String foto) {
		Foto = foto;
	}
	public List<String> getUbicacion1() {
		return ubicacion1;
	}
	public void setUbicacion1(List<String> ubicacion1) {
		this.ubicacion1 = ubicacion1;
	}
	public String getDescripcionU1() {
		return descripcionU1;
	}
	public void setDescripcionU1(String descripcionU1) {
		this.descripcionU1 = descripcionU1;
	}
	public List<String> getUbicacion2() {
		return ubicacion2;
	}
	public void setUbicacion2(List<String> ubicacion2) {
		this.ubicacion2 = ubicacion2;
	}
	public String getDescripcionU2() {
		return descripcionU2;
	}
	public void setDescripcionU2(String descripcionU2) {
		this.descripcionU2 = descripcionU2;
	}
	public List<String> getUbicacion3() {
		return ubicacion3;
	}
	public void setUbicacion3(List<String> ubicacion3) {
		this.ubicacion3 = ubicacion3;
	}
	public String getDescripcionU3() {
		return descripcionU3;
	}
	public void setDescripcionU3(String descripcionU3) {
		this.descripcionU3 = descripcionU3;
	}
	public List<String> getNumeracion1() {
		return numeracion1;
	}
	public void setNumeracion1(List<String> numeracion1) {
		this.numeracion1 = numeracion1;
	}
	public String getDescripcionNum1() {
		return descripcionNum1;
	}
	public void setDescripcionNum1(String descripcionNum1) {
		this.descripcionNum1 = descripcionNum1;
	}
	public List<String> getNumeracion2() {
		return numeracion2;
	}
	public void setNumeracion2(List<String> numeracion2) {
		this.numeracion2 = numeracion2;
	}
	public String getDescripcionNum2() {
		return descripcionNum2;
	}
	public void setDescripcionNum2(String descripcionNum2) {
		this.descripcionNum2 = descripcionNum2;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	
	
	
}
