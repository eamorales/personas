/**
 * 
 */
package com.mx.beans;

import BeanRespuesta.RespuestaBase;

/**
 * @author G
 *
 */
public class BeanAutReglasNegocio extends RespuestaBase {

	/** Id Autprozacon pendiente */
	private String id;
	/** modulo */
	private String modulo;
	/** regla */
	private String regla;
	/** expresion validacion */
	private String expresion_val;
	/** expresion autorizacion */
	private String expresion_aut;
	/** estatus */
	private String estatus;
	/** descripcion */
	private String descripcion;
	/** mensaje */
	private String mensaje;
	/** enviar capturar */
	private String envia_captura;
	/** etapa */
	private String etapa;
	/** producto */
	private String producto;
	/** usuario creacion */
	private String usuario_creacion;
	/** fecha creacion */
	private String fecha_creacion;
	/** usuario modificacion */
	private String usuario_modificacion;
	/** fecha modificacion */
	private String fecha_modificacion;
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the modulo
	 */
	public String getModulo() {
		return modulo;
	}
	/**
	 * @param modulo the modulo to set
	 */
	public void setModulo(String modulo) {
		this.modulo = modulo;
	}
	/**
	 * @return the regla
	 */
	public String getRegla() {
		return regla;
	}
	/**
	 * @param regla the regla to set
	 */
	public void setRegla(String regla) {
		this.regla = regla;
	}
	/**
	 * @return the expresion_val
	 */
	public String getExpresion_val() {
		return expresion_val;
	}
	/**
	 * @param expresion_val the expresion_val to set
	 */
	public void setExpresion_val(String expresion_val) {
		this.expresion_val = expresion_val;
	}
	/**
	 * @return the expresion_aut
	 */
	public String getExpresion_aut() {
		return expresion_aut;
	}
	/**
	 * @param expresion_aut the expresion_aut to set
	 */
	public void setExpresion_aut(String expresion_aut) {
		this.expresion_aut = expresion_aut;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the mensaje
	 */
	public String getMensaje() {
		return mensaje;
	}
	/**
	 * @param mensaje the mensaje to set
	 */
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	/**
	 * @return the envia_captura
	 */
	public String getEnvia_captura() {
		return envia_captura;
	}
	/**
	 * @param envia_captura the envia_captura to set
	 */
	public void setEnvia_captura(String envia_captura) {
		this.envia_captura = envia_captura;
	}
	/**
	 * @return the etapa
	 */
	public String getEtapa() {
		return etapa;
	}
	/**
	 * @param etapa the etapa to set
	 */
	public void setEtapa(String etapa) {
		this.etapa = etapa;
	}
	/**
	 * @return the producto
	 */
	public String getProducto() {
		return producto;
	}
	/**
	 * @param producto the producto to set
	 */
	public void setProducto(String producto) {
		this.producto = producto;
	}
	/**
	 * @return the usuario_creacion
	 */
	public String getUsuario_creacion() {
		return usuario_creacion;
	}
	/**
	 * @param usuario_creacion the usuario_creacion to set
	 */
	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}
	/**
	 * @return the fecha_creacion
	 */
	public String getFecha_creacion() {
		return fecha_creacion;
	}
	/**
	 * @param fecha_creacion the fecha_creacion to set
	 */
	public void setFecha_creacion(String fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}
	/**
	 * @return the usuario_modificacion
	 */
	public String getUsuario_modificacion() {
		return usuario_modificacion;
	}
	/**
	 * @param usuario_modificacion the usuario_modificacion to set
	 */
	public void setUsuario_modificacion(String usuario_modificacion) {
		this.usuario_modificacion = usuario_modificacion;
	}
	/**
	 * @return the fecha_modificacion
	 */
	public String getFecha_modificacion() {
		return fecha_modificacion;
	}
	/**
	 * @param fecha_modificacion the fecha_modificacion to set
	 */
	public void setFecha_modificacion(String fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}
}
