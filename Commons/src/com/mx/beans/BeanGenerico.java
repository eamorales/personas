package com.mx.beans;

public class BeanGenerico {
	private String pantalla;
	private String disabledcomboBox;
	
	public String getDisabledcomboBox() {
		return disabledcomboBox;
	}

	public void setDisabledcomboBox(String disabledcomboBox) {
		this.disabledcomboBox = disabledcomboBox;
	}

	public String getPantalla() {
		return pantalla;
	}

	public void setPantalla(String pantalla) {
		this.pantalla = pantalla;
	}
	
	
	
}
