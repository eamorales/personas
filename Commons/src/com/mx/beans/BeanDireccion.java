package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;

public class BeanDireccion extends RespuestaBase {

	/** ID de la Direccion */
	private Long id;
	/** Id de la persona en Direccion */
	private Long idPersona;
	/** Id de la Colonia en Direccion */
	private Long idColonia;
	/** Principal en Direccion */
	private boolean principal;
	/** fechaCreacion en Direccion */
	private String fechaCreacion;
	/** fechaModificacion en Direccion */
	private String fechaModificacion;
	/** usuarioCreacion en Direccion */
	private Long usuarioCreacion;
	/** usuarioModificacion en Direccion */
	private Long usuarioModificacion;
	/** referencia en Direccion */
	private String referencia;
	/** direccion en Direccion */
	private String direccion;
	/** latitud en Direccion */
	private String latitud;
	/** longitud en Direccion */
	private String longitud;
	/** nombreImg en Direccion */
	private String nombreImg;
	/** estatus en Direccion */
	private Long estatus;
	/** rutaAlfresco en Direccion */
	private String rutaAlfresco;
	
	private List<BeanPeDireUbic> peDireUbic;
	

	public List<BeanPeDireUbic> getPeDireUbic() {
		return peDireUbic;
	}
	public void setPeDireUbic(List<BeanPeDireUbic> peDireUbic) {
		this.peDireUbic = peDireUbic;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the idPersona
	 */
	public Long getIdPersona() {
		return idPersona;
	}
	/**
	 * @param idPersona the idPersona to set
	 */
	public void setIdPersona(Long idPersona) {
		this.idPersona = idPersona;
	}
	/**
	 * @return the idColonia
	 */
	public Long getIdColonia() {
		return idColonia;
	}
	/**
	 * @param idColonia the idColonia to set
	 */
	public void setIdColonia(Long idColonia) {
		this.idColonia = idColonia;
	}
	/**
	 * @return the principal
	 */
	public boolean isPrincipal() {
		return principal;
	}
	/**
	 * @param principal the principal to set
	 */
	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}
	/**
	 * @return the fechaCreacion
	 */
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	/**
	 * @return the fechaModificacion
	 */
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	/**
	 * @return the usuarioCreacion
	 */
	public Long getUsuarioCreacion() {
		return usuarioCreacion;
	}
	/**
	 * @param usuarioCreacion the usuarioCreacion to set
	 */
	public void setUsuarioCreacion(Long usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	/**
	 * @return the usuarioModificacion
	 */
	public Long getUsuarioModificacion() {
		return usuarioModificacion;
	}
	/**
	 * @param usuarioModificacion the usuarioModificacion to set
	 */
	public void setUsuarioModificacion(Long usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	/**
	 * @return the referencia
	 */
	public String getReferencia() {
		return referencia;
	}
	/**
	 * @param referencia the referencia to set
	 */
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	/**
	 * @return the direccion
	 */
	public String getDireccion() {
		return direccion;
	}
	/**
	 * @param direccion the direccion to set
	 */
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	/**
	 * @return the latitud
	 */
	public String getLatitud() {
		return latitud;
	}
	/**
	 * @param latitud the latitud to set
	 */
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	/**
	 * @return the longitud
	 */
	public String getLongitud() {
		return longitud;
	}
	/**
	 * @param longitud the longitud to set
	 */
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	/**
	 * @return the nombreImg
	 */
	public String getNombreImg() {
		return nombreImg;
	}
	/**
	 * @param nombreImg the nombreImg to set
	 */
	public void setNombreImg(String nombreImg) {
		this.nombreImg = nombreImg;
	}
	/**
	 * @return the estatus
	 */
	public Long getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Long estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the rutaAlfresco
	 */
	public String getRutaAlfresco() {
		return rutaAlfresco;
	}
	/**
	 * @param rutaAlfresco the rutaAlfresco to set
	 */
	public void setRutaAlfresco(String rutaAlfresco) {
		this.rutaAlfresco = rutaAlfresco;
	}	
}