/**
 * 
 */
package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;

/**
 * @author G
 *
 */
public class BeanAutorizacionesPendientes extends RespuestaBase {

	/** Id Autprozacon pendiente */
	private String id;
	/** Id regla del negicio */
	private String id_regla_negocio;
	/** estatus */
	private String estatus;
	/** dato general */
	private String dato_gen;
	/** usuario autizo */
	private String usuario_aut;
	/** id usuario autorizo */
	private String usuario_id_aut;
	/** expirado*/
	private String expirado;
	/** usuario creacion */
	private String usuario_creacion;
	/** fecha creacion */
	private String fecha_creacion;
	/** usuario modificacion */
	private String usuario_modificacion;
	/** fecha modificacion */
	private String fecha_modificacion;
	/** Lista de Autorizaciones */
	private List<BeanAutorizacionesPendientes> listaAutorizacionesVista;
	/** Descripcion de la regal */
	private String descripcion;
	/** estatusVista */
	private Boolean estatusVista = false;

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the id_regla_negocio
	 */
	public String getId_regla_negocio() {
		return id_regla_negocio;
	}
	/**
	 * @param id_regla_negocio the id_regla_negocio to set
	 */
	public void setId_regla_negocio(String id_regla_negocio) {
		this.id_regla_negocio = id_regla_negocio;
	}
	/**
	 * @return the estatus
	 */
	public String getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the dato_gen
	 */
	public String getDato_gen() {
		return dato_gen;
	}
	/**
	 * @param dato_gen the dato_gen to set
	 */
	public void setDato_gen(String dato_gen) {
		this.dato_gen = dato_gen;
	}
	/**
	 * @return the usuario_aut
	 */
	public String getUsuario_aut() {
		return usuario_aut;
	}
	/**
	 * @param usuario_aut the usuario_aut to set
	 */
	public void setUsuario_aut(String usuario_aut) {
		this.usuario_aut = usuario_aut;
	}
	/**
	 * @return the usuario_id_aut
	 */
	public String getUsuario_id_aut() {
		return usuario_id_aut;
	}
	/**
	 * @param usuario_id_aut the usuario_id_aut to set
	 */
	public void setUsuario_id_aut(String usuario_id_aut) {
		this.usuario_id_aut = usuario_id_aut;
	}
	/**
	 * @return the expirado
	 */
	public String getExpirado() {
		return expirado;
	}
	/**
	 * @param expirado the expirado to set
	 */
	public void setExpirado(String expirado) {
		this.expirado = expirado;
	}
	/**
	 * @return the usuario_creacion
	 */
	public String getUsuario_creacion() {
		return usuario_creacion;
	}
	/**
	 * @param usuario_creacion the usuario_creacion to set
	 */
	public void setUsuario_creacion(String usuario_creacion) {
		this.usuario_creacion = usuario_creacion;
	}
	/**
	 * @return the fecha_creacion
	 */
	public String getFecha_creacion() {
		return fecha_creacion;
	}
	/**
	 * @param fecha_creacion the fecha_creacion to set
	 */
	public void setFecha_creacion(String fecha_creacion) {
		this.fecha_creacion = fecha_creacion;
	}
	/**
	 * @return the usuario_modificacion
	 */
	public String getUsuario_modificacion() {
		return usuario_modificacion;
	}
	/**
	 * @param usuario_modificacion the usuario_modificacion to set
	 */
	public void setUsuario_modificacion(String usuario_modificacion) {
		this.usuario_modificacion = usuario_modificacion;
	}
	/**
	 * @return the fecha_modificacion
	 */
	public String getFecha_modificacion() {
		return fecha_modificacion;
	}
	/**
	 * @param fecha_modificacion the fecha_modificacion to set
	 */
	public void setFecha_modificacion(String fecha_modificacion) {
		this.fecha_modificacion = fecha_modificacion;
	}
	/**
	 * @return the listaAutorizacionesVista
	 */
	public List<BeanAutorizacionesPendientes> getListaAutorizacionesVista() {
		return listaAutorizacionesVista;
	}
	/**
	 * @param listaAutorizacionesVista the listaAutorizacionesVista to set
	 */
	public void setListaAutorizacionesVista(
			List<BeanAutorizacionesPendientes> listaAutorizacionesVista) {
		this.listaAutorizacionesVista = listaAutorizacionesVista;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	/**
	 * @return the estatusVista
	 */
	public Boolean getEstatusVista() {
		return estatusVista;
	}
	/**
	 * @param estatusVista the estatusVista to set
	 */
	public void setEstatusVista(Boolean estatusVista) {
		this.estatusVista = estatusVista;
	}
}