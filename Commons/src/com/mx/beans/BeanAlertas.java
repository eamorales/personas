package com.mx.beans;

public class BeanAlertas {

	
	private int id;
	private String	alerta;
	private String estatus;
	private  long	fechades; // en milisegundos
 	private long fechaHasta;  // en milisegundos
	private String sucursal;
	
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getAlerta() {
		return alerta;
	}
	public void setAlerta(String alerta) {
		this.alerta = alerta;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public long getFechades() {
		return fechades;
	}
	public void setFechades(long fechades) {
		this.fechades = fechades;
	}
	public long getFechaHasta() {
		return fechaHasta;
	}
	public void setFechaHasta(long fechaHasta) {
		this.fechaHasta = fechaHasta;
	}
	public String getSucursal() {
		return sucursal;
	}
	public void setSucursal(String sucursal) {
		this.sucursal = sucursal;
	}

	
	 
}
