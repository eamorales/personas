package com.mx.beans;

import java.util.Date;

import com.mx.beans.Busqueda.BeanCliente;
import com.mx.beans.Busqueda.BeanClienteEntrada;

import BeanRespuesta.RespuestaBase;

public class BeanCatalogosDatos extends RespuestaBase  {
	// Atributos para Catalogos de Datos
	private String estatus;
	private String id;
	private String usuarioCreacion;
	private String usuarioModificacion;
	private String tipoCorreo;
	private String fechaCreacion;
	private String fechaModificacion;
	private String descripcion;
	private String actividad;
	private String clave;
	private String giro;
	private String sector;
	private String riesgo;
	private String grado;
	private String numHijos;
	private String companiaTelefonica;
	private String tipoTelefono;
	private String estado;
	private String nacionalidad; 
	private String ocupacion;
	private String tipoSociedad;
	private String situacion;
	private String colonia;
	private String municipio;
	private String localidad;
	private String facultad;
	private String tipoDocumentoPers;
	private String documentoPers;
	private String puesto; 
	private String federatario;
	private String establecimiento;
	private String vip = "";
	private Date fechaNacimiento;
	
	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	BeanClienteEntrada beanClienteEntrada = new BeanClienteEntrada();
	BeanCliente beanCliente = new BeanCliente();
	public BeanClienteEntrada getBeanClienteEntrada() {
		return beanClienteEntrada;
	}
	public void setBeanClienteEntrada(BeanClienteEntrada beanClienteEntrada) {
		this.beanClienteEntrada = beanClienteEntrada;
	}
	public BeanCliente getBeanCliente() {
		return beanCliente;
	}
	public void setBeanCliente(BeanCliente beanCliente) {
		this.beanCliente = beanCliente;
	}
	

	public String getRiesgo() {
		return riesgo;
	}

	public void setRiesgo(String riesgo) {
		this.riesgo = riesgo;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getEstatus() {
		return estatus;
	}

	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}

	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}

	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}

	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}

	public String getTipoCorreo() {
		return tipoCorreo;
	}

	public void setTipoCorreo(String tipoCorreo) {
		this.tipoCorreo = tipoCorreo;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}

	public String getFechaModificacion() {
		return fechaModificacion;
	}

	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getActividad() {
		return actividad;
	}

	public void setActividad(String actividad) {
		this.actividad = actividad;
	}

	public String getGrado() {
		return grado;
	}

	public void setGrado(String grado) {
		this.grado = grado;
	}

	public String getNumHijos() {
		return numHijos;
	}

	public void setNumHijos(String numHijos) {
		this.numHijos = numHijos;
	}

	public String getGiro() {
		return giro;
	}

	public void setGiro(String giro) {
		this.giro = giro;
	}

	public String getSector() {
		return sector;
	}

	public void setSector(String sector) {
		this.sector = sector;
	}
	public String getCompaniaTelefonica() {
		return companiaTelefonica;
	}

	public void setCompaniaTelefonica(String companiaTelefonica) {
		this.companiaTelefonica = companiaTelefonica;
	}

	public String getTipoTelefono() {
		return tipoTelefono;
	}

	public void setTipoTelefono(String tipoTelefono) {
		this.tipoTelefono = tipoTelefono;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public String getOcupacion() {
		return ocupacion;
	}

	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}

	public String getTipoSociedad() {
		return tipoSociedad;
	}

	public void setTipoSociedad(String tipoSociedad) {
		this.tipoSociedad = tipoSociedad;
	}

	public String getSituacion() {
		return situacion;
	}

	public void setSituacion(String situacion) {
		this.situacion = situacion;
	}

	public String getColonia() {
		return colonia;
	}

	public void setColonia(String colonia) {
		this.colonia = colonia;
	}
	
	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getLocalidad() {
		return localidad;
	}

	public void setLocalidad(String localidad) {
		this.localidad = localidad;
	}

	public String getFacultad() {
		return facultad;
	}

	public void setFacultad(String facultad) {
		this.facultad = facultad;
	}

	public String getTipoDocumentoPers() {
		return tipoDocumentoPers;
	}

	public void setTipoDocumentoPers(String tipoDocumentoPers) {
		this.tipoDocumentoPers = tipoDocumentoPers;
	}

	public String getDocumentoPers() {
		return documentoPers;
	}

	public void setDocumentoPers(String documentoPers) {
		this.documentoPers = documentoPers;
	}

	public String getPuesto() {
		return puesto;
	}

	public void setPuesto(String puesto) {
		this.puesto = puesto;
	}

	public String getFederatario() {
		return federatario;
	}

	public void setFederatario(String federatario) {
		this.federatario = federatario;
	}

	public String getEstablecimiento() {
		return establecimiento;
	}

	public void setEstablecimiento(String establecimiento) {
		this.establecimiento = establecimiento;
	}
	public String getVip() {
		return vip;
	}
	public void setVip(String vip) {
		this.vip = vip;
	}
	@Override
	public String toString() {
		return "BeanCatalogosDatos [estatus=" + estatus + ", id=" + id + ", usuarioCreacion=" + usuarioCreacion
				+ ", usuarioModificacion=" + usuarioModificacion + ", tipoCorreo=" + tipoCorreo + ", fechaCreacion="
				+ fechaCreacion + ", fechaModificacion=" + fechaModificacion + ", descripcion=" + descripcion
				+ ", actividad=" + actividad + ", clave=" + clave + ", giro=" + giro + ", sector=" + sector
				+ ", riesgo=" + riesgo + ", grado=" + grado + ", numHijos=" + numHijos + ", companiaTelefonica="
				+ companiaTelefonica + ", tipoTelefono=" + tipoTelefono + ", estado=" + estado + ", nacionalidad="
				+ nacionalidad + ", ocupacion=" + ocupacion + ", tipoSociedad=" + tipoSociedad + ", situacion="
				+ situacion + ", colonia=" + colonia + ", municipio=" + municipio + ", localidad=" + localidad
				+ ", facultad=" + facultad + ", tipoDocumentoPers=" + tipoDocumentoPers + ", documentoPers="
				+ documentoPers + ", puesto=" + puesto + ", federatario=" + federatario + ", establecimiento="
				+ establecimiento + ", vip=" + vip + ", beanClienteEntrada=" + beanClienteEntrada + ", beanCliente="
				+ beanCliente + "]";
	}
	
	
	
	
}
