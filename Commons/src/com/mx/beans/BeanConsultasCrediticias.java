package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class BeanConsultasCrediticias extends RespuestaBase {
	
	String entidadFinanciera;/*M= buro de credito|| F consulta a circulo*/
	String antecedentes;
	String resultado;
	private List<BeanConsultasCrediticias> listaRealizaConsulta;//tabla
	
	//Consultas Anteriores
	private String folio;
	private String autoriza;
	private String fecha;
	private String observaciones;
	private String PDF;
	
	public String getPDF() {
		return PDF;
	}
	public void setPDF(String pDF) {
		PDF = pDF;
	}
	private List<BeanConsultasCrediticias> listaConsultaAnterior;//tabla
	
	
	public String getEntidadFinanciera() {
		return entidadFinanciera;
	}
	public void setEntidadFinanciera(String entidadFinanciera) {
		this.entidadFinanciera = entidadFinanciera;
	}
	public String getAntecedentes() {
		return antecedentes;
	}
	public void setAntecedentes(String antecedentes) {
		this.antecedentes = antecedentes;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	public List<BeanConsultasCrediticias> getListaRealizaConsulta() {
		return listaRealizaConsulta;
	}
	public void setListaRealizaConsulta(List<BeanConsultasCrediticias> listaRealizaConsulta) {
		this.listaRealizaConsulta = listaRealizaConsulta;
	}
	public String getFolio() {
		return folio;
	}
	public void setFolio(String folio) {
		this.folio = folio;
	}
	public String getAutoriza() {
		return autoriza;
	}
	public void setAutoriza(String autoriza) {
		this.autoriza = autoriza;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public List<BeanConsultasCrediticias> getListaConsultaAnterior() {
		return listaConsultaAnterior;
	}
	public void setListaConsultaAnterior(List<BeanConsultasCrediticias> listaConsultaAnterior) {
		this.listaConsultaAnterior = listaConsultaAnterior;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	
	
}
