package com.mx.beans.Busqueda;

import com.mx.beans.BeanInformacion;
import com.mx.beans.BeanDatosAdicionales;
import com.mx.beans.BeanDatosGenerales;

public class BeanCliente {
	public BeanDatosGenerales datosGenerales = new BeanDatosGenerales();
	public BeanInformacion BeanInformacion = new BeanInformacion();
	public BeanDatosAdicionales beanDatosAdicionales = new BeanDatosAdicionales();

	public BeanDatosGenerales getDatosGenerales() {
		return datosGenerales;
	}
	public void setDatosGenerales(BeanDatosGenerales datosGenerales) {
		this.datosGenerales = datosGenerales;
	}
	public BeanInformacion getBeanInformacion() {
		return BeanInformacion;
	}
	public void setBeanInformacion(BeanInformacion beanInformacion) {
		BeanInformacion = beanInformacion;
	}
	public BeanDatosAdicionales getBeanDatosAdicionales() {
		return beanDatosAdicionales;
	}
	public void setBeanDatosAdicionales(BeanDatosAdicionales beanDatosAdicionales) {
		this.beanDatosAdicionales = beanDatosAdicionales;
	}
	@Override
	public String toString() {
		return "BeanCliente [datosGenerales=" + datosGenerales + ", BeanInformacion=" + BeanInformacion
				+ ", beanDatosAdicionales=" + beanDatosAdicionales + "]";
	}
	
	
	
}
