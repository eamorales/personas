package com.mx.beans;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class BeanInformacion extends RespuestaBase {
	/*informacion*/
	long noCliente;
	String nombre;
	String tipocliente;
	String direccion;
	String sucursalOrigen;
	String personalidad = "1";
	String celular;	
	String correo;
	String sexo;
	int edad;
	
	
	public long getNoCliente() {
		return noCliente;
	}
	public void setNoCliente(long noCliente) {
		this.noCliente = noCliente;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTipocliente() {
		return tipocliente;
	}
	public void setTipocliente(String tipocliente) {
		this.tipocliente = tipocliente;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getSucursalOrigen() {
		return sucursalOrigen;
	}
	public void setSucursalOrigen(String sucursalOrigen) {
		this.sucursalOrigen = sucursalOrigen;
	}
	public String getPersonalidad() {
		return personalidad;
	}
	public void setPersonalidad(String personalidad) {
		this.personalidad = personalidad;
	}
	public String getCelular() {
		return celular;
	}
	public void setCelular(String celular) {
		this.celular = celular;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public int getEdad() {
		return edad;
	}
	public void setEdad(int edad) {
		this.edad = edad;
	}
	@Override
	public String toString() {
		return "BeanInformacion [noCliente=" + noCliente + ", nombre=" + nombre + ", tipocliente=" + tipocliente
				+ ", direccion=" + direccion + ", sucursalOrigen=" + sucursalOrigen + ", personalidad=" + personalidad
				+ ", celular=" + celular + ", correo=" + correo + ", sexo=" + sexo + ", edad=" + edad + "]";
	}
	
	
}
