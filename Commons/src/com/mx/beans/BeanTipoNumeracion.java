/**
 * 
 */
package com.mx.beans;

import BeanRespuesta.RespuestaBase;

/**
 * @author G
 *
 */
public class BeanTipoNumeracion extends RespuestaBase {

	/** id de Tipo Numeracion*/
	private Long id;
	/** estatus de Numeracion*/
	private Integer estatus;
	/** usuario creo de Numeracion*/
	private String usuarioCreacion;
	/** usuario modifico de Numeracion*/
	private String usuarioModificacion;
	/** fecha de creacion de Numeracion*/
	private String fechaCreacion;
	/** fecha de modificacion de Numeracion*/
	private String fechaModificacion;
	/** tipo de Numeracion*/
	private String tipoNumeracion;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the estatus
	 */
	public Integer getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the usuarioCreacion
	 */
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	/**
	 * @param usuarioCreacion the usuarioCreacion to set
	 */
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	/**
	 * @return the usuarioModificacion
	 */
	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}
	/**
	 * @param usuarioModificacion the usuarioModificacion to set
	 */
	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	/**
	 * @return the fechaCreacion
	 */
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	/**
	 * @return the tipoUbicacion
	 */
	public String getTipoNumeracion() {
		return tipoNumeracion;
	}
	/**
	 * @param tipoUbicacion the tipoUbicacion to set
	 */
	public void setTipoNumeracion(String tipoNumeracion) {
		this.tipoNumeracion = tipoNumeracion;
	}
	/**
	 * @return the fechaModificacion
	 */
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}	
}