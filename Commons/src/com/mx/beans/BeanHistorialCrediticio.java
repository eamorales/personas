package com.mx.beans;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class BeanHistorialCrediticio extends RespuestaBase {
	
	int folio;
	String buro;
	String autorizo;
	String fecha;
	String resultado;
	
	
	public int getFolio() {
		return folio;
	}
	public void setFolio(int folio) {
		this.folio = folio;
	}
	public String getBuro() {
		return buro;
	}
	public void setBuro(String buro) {
		this.buro = buro;
	}
	public String getAutorizo() {
		return autorizo;
	}
	public void setAutorizo(String autorizo) {
		this.autorizo = autorizo;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	
}
