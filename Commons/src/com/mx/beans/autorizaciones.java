package com.mx.beans;

import BeanRespuesta.RespuestaBase;

public class autorizaciones extends RespuestaBase {
	
	String descripcion;
	boolean autorizar;
	
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public boolean isAutorizar() {
		return autorizar;
	}
	public void setAutorizar(boolean autorizar) {
		this.autorizar = autorizar;
	}
	
	
}
