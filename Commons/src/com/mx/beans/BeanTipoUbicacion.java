/**
 * 
 */
package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;

/**
 * @author Gus
 *
 */
public class BeanTipoUbicacion extends RespuestaBase {

	/** id de Tipo Ubicacion*/
	private Long id;
	/** estatus de Ubicacion*/
	private Integer estatus;
	/** usuario creo de Ubicacion*/
	private String usuarioCreacion;
	/** usuario modifico de Ubicacion*/
	private String usuarioModificacion;
	/** fecha de creacion de Ubicacion*/
	private String fechaCreacion;
	/** fecha de modificacion de Colonia*/
	private String fechaModificacion;
	/** tipo de Ubicacion*/
	private String tipoUbicacion;
	
	private List<BeanTipoUbicacion> listaUbicacion;
	
	public List<BeanTipoUbicacion> getListaUbicacion() {
		return listaUbicacion;
	}
	public void setListaUbicacion(List<BeanTipoUbicacion> listaUbicacion) {
		this.listaUbicacion = listaUbicacion;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the estatus
	 */
	public Integer getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the usuarioCreacion
	 */
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	/**
	 * @param usuarioCreacion the usuarioCreacion to set
	 */
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	/**
	 * @return the usuarioModificacion
	 */
	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}
	/**
	 * @param usuarioModificacion the usuarioModificacion to set
	 */
	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	/**
	 * @return the fechaCreacion
	 */
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	/**
	 * @return the tipoUbicacion
	 */
	public String getTipoUbicacion() {
		return tipoUbicacion;
	}
	/**
	 * @param tipoUbicacion the tipoUbicacion to set
	 */
	public void setTipoUbicacion(String tipoUbicacion) {
		this.tipoUbicacion = tipoUbicacion;
	}
	/**
	 * @return the fechaModificacion
	 */
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}	
}