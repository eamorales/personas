package com.mx.beans;

import java.util.Date;
import java.util.List;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class BeanExpedientes extends RespuestaBase {
	private Date fecha;
	private int idPersona;  
	private String tipoDocumento;
	private List<String> tipoDocumentos;
	private String fechaExpedicion;
	private String estatus;
	private String vigencia;
	private String observaciones;
	private String fechaCreacion;
	private String ruta;
	private String nombreArchivo;
	private boolean seleccionado;
	private int idExpediente;
	private int indiceseleccionado;
	private int idExpedienteSeleccionado;
	
	public int getIdExpedienteSeleccionado() {
		return idExpedienteSeleccionado;
	}
	public void setIdExpedienteSeleccionado(int idExpedienteSeleccionado) {
		this.idExpedienteSeleccionado = idExpedienteSeleccionado;
	}
	private List<BeanExpedientes> listaExpedientes;//tabla
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public boolean isSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	public int getIdExpediente() {
		return idExpediente;
	}
	public void setIdExpediente(int idExpediente) {
		this.idExpediente = idExpediente;
	}
	public List<BeanExpedientes> getListaExpedientes() {
		return listaExpedientes;
	}
	public void setListaExpedientes(List<BeanExpedientes> listaExpedientes) {
		this.listaExpedientes = listaExpedientes;
	}
	public List<String> getTipoDocumentos() {
		return tipoDocumentos;
	}
	public void setTipoDocumentos(List<String> tipoDocumentos) {
		this.tipoDocumentos = tipoDocumentos;
	}
	public int getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}
	public String getVigencia() {
		return vigencia;
	}
	public void setVigencia(String vigencia) {
		this.vigencia = vigencia;
	}
	public String getFechaExpedicion() {
		return fechaExpedicion;
	}
	public void setFechaExpedicion(String fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}
	public Date getFecha() {
		return fecha;
	}
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	public int getIndiceseleccionado() {
		return indiceseleccionado;
	}
	public void setIndiceseleccionado(int indiceseleccionado) {
		this.indiceseleccionado = indiceseleccionado;
	}
	
	
}
