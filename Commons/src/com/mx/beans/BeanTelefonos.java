package com.mx.beans;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import BeanRespuesta.RespuestaBase;

/**
 * @author JonaMata 
 * @Fecha: 23/05/2017
 * @category Telefonos Bean
 * @Reviso> Gustavo
 * */
public class BeanTelefonos extends RespuestaBase {
	
	private Integer maxchar;
	/*Id del registro del telefono de la personas*/
	private String idTel;
	/*Tipo de telefono*/
	private String tipoTelefono;
	/*Compa�ia telefonica*/
	private String compania;
	/*lada del telefono*/
	private String lada;
	/*numero telefonico*/
	private String numero;
	/*extensi�n del telefono*/
	private String  extension;
	/*Observaciones*/
	private String observacion;
	/*revisa si es el telefono principal*/
	private boolean principal;
	
	private String estatus;
	/*Numero del cliente que se esta buscando informaci�n*/
	private String numCliente;
	/*Listado para obtener si es telefono de casa, cel, etc. (para llenar los catalogos)*/
	private List<BeanTipoTelefono> listaTipoTelefono;
	/*Listado para obtener si es telefono de casa, cel, etc. (para llenar los catalogos)*/
	private List<BeanTipoTelefono> listaCompaniaTelefono;
    
	/*Id seleccioado de la tabla*/
	private String idTelSeleccionado;

	/*Listado para llenar la tabla*/
	private List<BeanTelefonos> listaDirectorio; // Tabla
	/*Listado para llenar la tabla seleccionad*/
	private BeanTelefonos listaDirectorioSelected; // Tabla
	
	private String disabled;
	
	
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getDisabled() {
		return disabled;
	}
	public void setDisabled(String disabled) {
		this.disabled = disabled;
	}
	public BeanTelefonos getListaDirectorioSelected() {
		return listaDirectorioSelected;
	}
	public void setListaDirectorioSelected(BeanTelefonos listaDirectorioSelected) {
		this.listaDirectorioSelected = listaDirectorioSelected;
	}
	public void setMaxchar(Integer maxchar) {
		this.maxchar = maxchar;
	}
	public void setIdTel(String idTel) {
		this.idTel = idTel;
	}
	public void setExtension(String extension) {
		this.extension = extension;
	}


	public String getTipoTelefono() {
		return tipoTelefono;
	}

	public void setTipoTelefono(String tipoTelefono) {
		this.tipoTelefono = tipoTelefono;
	}

	public String getIdTel() {
		return idTel;
	}


	public String getCompania() {
		return compania;
	}

	public void setCompania(String compania) {
		this.compania = compania;
	}

	public String getLada() {
		return lada;
	}

	public void setLada(String lada) {
		this.lada = lada;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getExtension() {
		return extension;
	}


	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public boolean isPrincipal() {
		return principal;
	}

	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}

	public String getNumCliente() {
		return numCliente;
	}

	public void setNumCliente(String numCliente) {
		this.numCliente = numCliente;
	}

	public List<BeanTipoTelefono> getListaTipoTelefono() {
		return listaTipoTelefono;
	}

	public void setListaTipoTelefono(List<BeanTipoTelefono> listaTipoTelefono) {
		this.listaTipoTelefono = listaTipoTelefono;
	}

	public List<BeanTelefonos> getListaDirectorio() {
		return listaDirectorio;
	}

	public void setListaDirectorio(List<BeanTelefonos> listaDirectorio) {
		this.listaDirectorio = listaDirectorio;
	}

	public Integer getMaxchar() {
		return maxchar;
	}

	public String getIdTelSeleccionado() {
		return idTelSeleccionado;
	}
	
	public void setIdTelSeleccionado(String idTelSeleccionado) {
		this.idTelSeleccionado = idTelSeleccionado;
	}
	public List<BeanTipoTelefono> getListaCompaniaTelefono() {
		return listaCompaniaTelefono;
	}
	public void setListaCompaniaTelefono(List<BeanTipoTelefono> listaCompaniaTelefono) {
		this.listaCompaniaTelefono = listaCompaniaTelefono;
	}
	
	

}
