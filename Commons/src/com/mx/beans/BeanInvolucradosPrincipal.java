package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;

public class BeanInvolucradosPrincipal extends RespuestaBase {

	
	private List<BeanInvolucradosLista> listaInv;
	private List<BeanInvolucradosDatos> datosInv;
	private String tipoInvolucrado;
	private String tipoEstadoCivil;

	
	public String getTipoEstadoCivil() {
		return tipoEstadoCivil;
	}

	public void setTipoEstadoCivil(String tipoEstadoCivil) {
		this.tipoEstadoCivil = tipoEstadoCivil;
	}

	public String getTipoInvolucrado() {
		return tipoInvolucrado;
	}

	public void setTipoInvolucrado(String tipoInvolucrado) {
		this.tipoInvolucrado = tipoInvolucrado;
	}

	public List<BeanInvolucradosLista> getListaInv() {
		return listaInv;
	}

	public void setListaInv(List<BeanInvolucradosLista> listaInv) {
		this.listaInv = listaInv;
	}

	public List<BeanInvolucradosDatos> getDatosInv() {
		return datosInv;
	}

	public void setDatosInv(List<BeanInvolucradosDatos> datosInv) {
		this.datosInv = datosInv;
	}
}
