package com.mx.beans;

import BeanRespuesta.RespuestaBase;
/**
 * @author Gustavo Bujano
 * @fecha: 06/06/2017
 * @version 1.0
 * @descripci�n: sirve para cargar el catalogo de telefonos
 **/
public class BeanTipoTelefono extends RespuestaBase {
	String idTipoTelefono;
	String tipoTelefono;
	String TipoCompania;
	String idTipoCompania;
	String validaExtension;
	String estatus;
	public String getTipoCompania() {
		return TipoCompania;
	}
	public void setTipoCompania(String tipoCompania) {
		TipoCompania = tipoCompania;
	}
	public String getIdTipoCompania() {
		return idTipoCompania;
	}
	public void setIdTipoCompania(String idTipoCompania) {
		this.idTipoCompania = idTipoCompania;
	}
	public String getTipoTelefono() {
		return tipoTelefono;
	}
	public void setTipoTelefono(String tipoTelefono) {
		this.tipoTelefono = tipoTelefono;
	}
	public String getIdTipoTelefono() {
		return idTipoTelefono;
	}
	public void setIdTipoTelefono(String idTipoTelefono) {
		this.idTipoTelefono = idTipoTelefono;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getValidaExtension() {
		return validaExtension;
	}
	public void setValidaExtension(String validaExtension) {
		this.validaExtension = validaExtension;
	}
	
	
	

	
	
}
