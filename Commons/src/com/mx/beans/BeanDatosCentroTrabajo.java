package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class BeanDatosCentroTrabajo extends RespuestaBase {
	String nombre;
	List<String>giro;
	List<String>actividad;
	List<String>sector;
	List<String>operacion;
	String ingresoAnual;
	String ingresoMensual;
	String ubicacion;
	int codigoPostal;
	List<String>colonia;
	List<String>localidad;
	List<String>municipio;
	List<String>establecimientoPropio;
	List<String>puesto;
	BeanTelefonos telefonosBean;
	BeanDatosGenerales beanDatosGrals;
	BeanDomicilios beanDomicilio;
	//Datos Establecimiento
	List<String>situacion;
	List<String>tipoEstablecimiento;
	
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public List<String> getGiro() {
		return giro;
	}
	public void setGiro(List<String> giro) {
		this.giro = giro;
	}
	public List<String> getActividad() {
		return actividad;
	}
	public void setActividad(List<String> actividad) {
		this.actividad = actividad;
	}
	public List<String> getSector() {
		return sector;
	}
	public void setSector(List<String> sector) {
		this.sector = sector;
	}
	public String getUbicacion() {
		return ubicacion;
	}
	public void setUbicacion(String ubicacion) {
		this.ubicacion = ubicacion;
	}
	public int getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(int codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public List<String> getColonia() {
		return colonia;
	}
	public void setColonia(List<String> colonia) {
		this.colonia = colonia;
	}
	public List<String> getLocalidad() {
		return localidad;
	}
	public void setLocalidad(List<String> localidad) {
		this.localidad = localidad;
	}
	public List<String> getMunicipio() {
		return municipio;
	}
	public void setMunicipio(List<String> municipio) {
		this.municipio = municipio;
	}
	public List<String> getEstablecimientoPropio() {
		return establecimientoPropio;
	}
	public void setEstablecimientoPropio(List<String> establecimientoPropio) {
		this.establecimientoPropio = establecimientoPropio;
	}
	public List<String> getSituacion() {
		return situacion;
	}
	public void setSituacion(List<String> situacion) {
		this.situacion = situacion;
	}
	public List<String> getTipoEstablecimiento() {
		return tipoEstablecimiento;
	}
	public void setTipoEstablecimiento(List<String> tipoEstablecimiento) {
		this.tipoEstablecimiento = tipoEstablecimiento;
	}
	public List<String> getPuesto() {
		return puesto;
	}
	public void setPuesto(List<String> puesto) {
		this.puesto = puesto;
	}
	public BeanTelefonos getTelefonosBean() {
		return telefonosBean;
	}
	public void setTelefonosBean(BeanTelefonos telefonosBean) {
		this.telefonosBean = telefonosBean;
	}
	public List<String> getOperacion() {
		return operacion;
	}
	public void setOperacion(List<String> operacion) {
		this.operacion = operacion;
	}
	public String getIngresoAnual() {
		return ingresoAnual;
	}
	public void setIngresoAnual(String ingresoAnual) {
		this.ingresoAnual = ingresoAnual;
	}
	public String getIngresoMensual() {
		return ingresoMensual;
	}
	public void setIngresoMensual(String ingresoMensual) {
		this.ingresoMensual = ingresoMensual;
	}
	public BeanDatosGenerales getBeanDatosGrals() {
		return beanDatosGrals;
	}
	public void setBeanDatosGrals(BeanDatosGenerales beanDatosGrals) {
		this.beanDatosGrals = beanDatosGrals;
	}
	public BeanDomicilios getBeanDomicilio() {
		return beanDomicilio;
	}
	public void setBeanDomicilio(BeanDomicilios beanDomicilio) {
		this.beanDomicilio = beanDomicilio;
	}
	
	
}
