package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class BeanInvolucradosLista extends RespuestaBase {

	private String nombre;
	private String rol;
	private String estatus;
	
	
	/********DATOS PERSONALES********/
	
	String primerNombre;
	String segundoNombre;
	String primerApellido;
	String segundoApellido;
	String sexo;
	String curp;
	String rfc;
	String estadoCivil;
	String ocupacion;
	String Telefono;
	String tipoTelefono;
	List<String> compania;
	String nacionalidad;
	String estadoNac;
	String lugarNac;
	String acciones;
	String correo;
	List<String> tipoCorreo;
	String id;
	String idTipoInvolucrado;
	String pais;
	String zona;
	
	public String getOcupacion() {
		return ocupacion;
	}
	public void setOcupacion(String ocupacion) {
		this.ocupacion = ocupacion;
	}
	
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(String estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public String getIdTipoInvolucrado() {
		return idTipoInvolucrado;
	}
	public void setIdTipoInvolucrado(String idTipoInvolucrado) {
		this.idTipoInvolucrado = idTipoInvolucrado;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getRfc() {
		return rfc;
	}
	
	public String getZona() {
		return zona;
	}
	public void setZona(String zona) {
		this.zona = zona;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}

	public String getPrimerNombre() {
		return primerNombre;
	}
	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}
	public String getSegundoNombre() {
		return segundoNombre;
	}
	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}
	public String getPrimerApellido() {
		return primerApellido;
	}
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	public String getSegundoApellido() {
		return segundoApellido;
	}
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	public String getSexo() {
		return sexo;
	}
	public void setSexo(String sexo) {
		this.sexo = sexo;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	
	public String getTelefono() {
		return Telefono;
	}
	public void setTelefono(String telefono) {
		Telefono = telefono;
	}
	public String getTipoTelefono() {
		return tipoTelefono;
	}
	public void setTipoTelefono(String tipoTelefono) {
		this.tipoTelefono = tipoTelefono;
	}
	public List<String> getCompania() {
		return compania;
	}
	public void setCompania(List<String> compania) {
		this.compania = compania;
	}
	
	public String getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(String nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	
	public String getEstadoNac() {
		return estadoNac;
	}
	public void setEstadoNac(String estadoNac) {
		this.estadoNac = estadoNac;
	}
	public String getPais() {
		return pais;
	}
	public void setPais(String pais) {
		this.pais = pais;
	}
	public String getLugarNac() {
		return lugarNac;
	}
	public void setLugarNac(String lugarNac) {
		this.lugarNac = lugarNac;
	}
	public String getAcciones() {
		return acciones;
	}
	public void setAcciones(String acciones) {
		this.acciones = acciones;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public List<String> getTipoCorreo() {
		return tipoCorreo;
	}
	public void setTipoCorreo(List<String> tipoCorreo) {
		this.tipoCorreo = tipoCorreo;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getRol() {
		return rol;
	}
	public void setRol(String rol) {
		this.rol = rol;
	}


	
}
