/**
 * 
 */
package com.mx.beans;

import BeanRespuesta.RespuestaBase;

/**
 * @author Gus
 *
 */
public class BeanDireUbic extends RespuestaBase {

	/** id de Tipo DireUbic*/
	private Long id;
	/** idDireccion de Tipo DireUbic*/
	private Long idDireccion;
	/** idTipoUbicacion de Tipo DireUbic*/
	private Long idTipoUbicacion;
	/** estatus de DireUbic*/
	private Integer estatus;
	/** idTipoNumeracion de DireUbic*/
	private Long idTipoNumeracion;
	/** usuario creo de DireUbic*/
	private String usuarioCreacion;
	/** usuario modifico de DireUbic*/
	private String usuarioModificacion;
	/** fecha de creacion de DireUbic*/
	private String fechaCreacion;
	/** fecha de modificacion de DireUbic*/
	private String fechaModificacion;
	/** detalle de Ubicacion*/
	private String detalle;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the idDireccion
	 */
	public Long getIdDireccion() {
		return idDireccion;
	}
	/**
	 * @param idDireccion the idDireccion to set
	 */
	public void setIdDireccion(Long idDireccion) {
		this.idDireccion = idDireccion;
	}
	/**
	 * @return the idTipoUbicacion
	 */
	public Long getIdTipoUbicacion() {
		return idTipoUbicacion;
	}
	/**
	 * @param idTipoUbicacion the idTipoUbicacion to set
	 */
	public void setIdTipoUbicacion(Long idTipoUbicacion) {
		this.idTipoUbicacion = idTipoUbicacion;
	}
	/**
	 * @return the estatus
	 */
	public Integer getEstatus() {
		return estatus;
	}
	/**
	 * @param estatus the estatus to set
	 */
	public void setEstatus(Integer estatus) {
		this.estatus = estatus;
	}
	/**
	 * @return the idTipoNumeracion
	 */
	public Long getIdTipoNumeracion() {
		return idTipoNumeracion;
	}
	/**
	 * @param idTipoNumeracion the idTipoNumeracion to set
	 */
	public void setIdTipoNumeracion(Long idTipoNumeracion) {
		this.idTipoNumeracion = idTipoNumeracion;
	}
	/**
	 * @return the usuarioCreacion
	 */
	public String getUsuarioCreacion() {
		return usuarioCreacion;
	}
	/**
	 * @param usuarioCreacion the usuarioCreacion to set
	 */
	public void setUsuarioCreacion(String usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	/**
	 * @return the usuarioModificacion
	 */
	public String getUsuarioModificacion() {
		return usuarioModificacion;
	}
	/**
	 * @param usuarioModificacion the usuarioModificacion to set
	 */
	public void setUsuarioModificacion(String usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	/**
	 * @return the fechaCreacion
	 */
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	/**
	 * @param fechaCreacion the fechaCreacion to set
	 */
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	/**
	 * @return the fechaModificacion
	 */
	public String getFechaModificacion() {
		return fechaModificacion;
	}
	/**
	 * @param fechaModificacion the fechaModificacion to set
	 */
	public void setFechaModificacion(String fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	/**
	 * @return the detalle
	 */
	public String getDetalle() {
		return detalle;
	}
	/**
	 * @param detalle the detalle to set
	 */
	public void setDetalle(String detalle) {
		this.detalle = detalle;
	}	
}