package com.mx.Req;


import com.mx.beans.BeanPosicionGlobalPrincipalBean;

import BeanRespuesta.RespuestaBase;

public class PosicionGlobalReq extends RespuestaBase{
BeanPosicionGlobalPrincipalBean posicionGlobalReq;

public BeanPosicionGlobalPrincipalBean getPosicionGlobalReq() {
	return posicionGlobalReq;
}

public void setPosicionGlobalReq(BeanPosicionGlobalPrincipalBean posicionGlobalReq) {
	this.posicionGlobalReq = posicionGlobalReq;
}
	
}
