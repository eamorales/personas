package com.mx.Req;

import com.mx.beans.BeanInvolucradosDatos;
import com.mx.beans.BeanInvolucradosLista;

import BeanRespuesta.RespuestaBase;

public class InvolucradosReq extends RespuestaBase {
	
	BeanInvolucradosDatos invDatos;
	BeanInvolucradosLista invLista;
	
	
	public BeanInvolucradosDatos getInvDatos() {
		return invDatos;
	}
	public void setInvDatos(BeanInvolucradosDatos invDatos) {
		this.invDatos = invDatos;
	}
	public BeanInvolucradosLista getInvLista() {
		return invLista;
	}
	public void setInvLista(BeanInvolucradosLista invLista) {
		this.invLista = invLista;
	}
	
}
