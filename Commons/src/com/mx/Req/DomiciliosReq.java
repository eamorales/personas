/**
 * 
 */
package com.mx.Req;

import java.util.List;

import BeanRespuesta.RespuestaBase;

import com.mx.beans.BeanDireccion;

/**
 * @author G
 *
 */
public class DomiciliosReq extends RespuestaBase{

	BeanDireccion domicilio;
	private List <BeanDireccion> domicilios;
	/**
	 * @return the domicilio
	 */
	public BeanDireccion getDomicilio() {
		return domicilio;
	}
	/**
	 * @param domicilio the domicilio to set
	 */
	public void setDomicilio(BeanDireccion domicilio) {
		this.domicilio = domicilio;
	}
	/**
	 * @return the domicilios
	 */
	public List<BeanDireccion> getDomicilios() {
		return domicilios;
	}
	/**
	 * @param domicilios the domicilios to set
	 */
	public void setDomicilios(List<BeanDireccion> domicilios) {
		this.domicilios = domicilios;
	}
}