package com.mx.Req;

import java.util.List;
import com.mx.beans.BeanIdentificacion;
import BeanRespuesta.RespuestaBase;

public class IdentificacionesReq extends RespuestaBase {
	
	BeanIdentificacion identificacion;
	private List<BeanIdentificacion> tipoIdentificacion;
	/**
	 * @return the identificacion
	 */
	public BeanIdentificacion getIdentificacion() {
		return identificacion;
	}
	/**
	 * @param identificacion the identificacion to set
	 */
	public void setIdentificacion(BeanIdentificacion identificacion) {
		this.identificacion = identificacion;
	}
	/**
	 * @return the tipoIdentificacion
	 */
	public List<BeanIdentificacion> getTipoIdentificacion() {
		return tipoIdentificacion;
	}
	/**
	 * @param tipoIdentificacion the tipoIdentificacion to set
	 */
	public void setTipoIdentificacion(List<BeanIdentificacion> tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	
}
