package com.mx.Req;

import java.util.List;

import com.mx.beans.BeanConsultasCrediticias;

import BeanRespuesta.RespuestaBase;

public class ConsCredReq extends RespuestaBase {
	
	private List<BeanConsultasCrediticias> consultas;

	public List<BeanConsultasCrediticias> getConsultas() {
		return consultas;
	}

	public void setConsultas(List<BeanConsultasCrediticias> consultas) {
		this.consultas = consultas;
	}

}
