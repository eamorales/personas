package com.mx.Req;

import BeanRespuesta.RespuestaBase;

public class Request extends RespuestaBase {
	
	private String user;
	private String pass;

	
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getPass() {
		return pass;
	}
	public void setPass(String pass) {
		this.pass = pass;
	}
	

}
