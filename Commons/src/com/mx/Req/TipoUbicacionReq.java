package com.mx.Req;

import java.util.List;

import BeanRespuesta.RespuestaBase;

import com.mx.beans.BeanTipoUbicacion;

public class TipoUbicacionReq extends RespuestaBase {
	BeanTipoUbicacion tipoUbicacion;
	private List <BeanTipoUbicacion> tipoUbicaciones;
	/**
	 * @return the tipoUbicacion
	 */
	public BeanTipoUbicacion getTipoUbicacion() {
		return tipoUbicacion;
	}
	/**
	 * @param tipoUbicacion the tipoUbicacion to set
	 */
	public void setTipoUbicacion(BeanTipoUbicacion tipoUbicacion) {
		this.tipoUbicacion = tipoUbicacion;
	}
	/**
	 * @return the tipoUbicaciones
	 */
	public List<BeanTipoUbicacion> getTipoUbicaciones() {
		return tipoUbicaciones;
	}
	/**
	 * @param tipoUbicaciones the tipoUbicaciones to set
	 */
	public void setTipoUbicaciones(List<BeanTipoUbicacion> tipoUbicaciones) {
		this.tipoUbicaciones = tipoUbicaciones;
	}
}