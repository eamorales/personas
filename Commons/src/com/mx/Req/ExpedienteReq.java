package com.mx.Req;

import java.util.List;

import com.mx.beans.BeanExpedientes;

import BeanRespuesta.RespuestaBase;

public class ExpedienteReq extends ImagenAlfrescoReq  {
	private int idPersona; 
	BeanExpedientes expediente; 
	List<BeanExpedientes> listaExpedientes;

	public BeanExpedientes getExpediente() {
		return expediente;
	}
	public void setExpediente(BeanExpedientes expediente) {
		this.expediente = expediente;
	}
	public List<BeanExpedientes> getListaExpedientes() {
		return listaExpedientes;
	}
	public void setListaExpedientes(List<BeanExpedientes> listaExpedientes) {
		this.listaExpedientes = listaExpedientes;
	}
	public int getIdPersona() {
		return idPersona;
	}
	public void setIdPersona(int idPersona) {
		this.idPersona = idPersona;
	}
	
	
	
}
