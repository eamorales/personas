package com.mx.Req;

import java.util.List;

import BeanRespuesta.RespuestaBase;

import com.mx.beans.BeanAutReglasNegocio;
import com.mx.beans.BeanAutorizacionesPendientes;

public class AutorizacionesReq  extends RespuestaBase{

	/** Lista de Autorizaciones */
	private List <BeanAutorizacionesPendientes> listaAutorizaciones;
	/** Autorizacion */
	BeanAutorizacionesPendientes autorizacionesPe;

	/** Lista de regla de Autorizaciones */
	private List <BeanAutReglasNegocio> listaReglasAut;
	/** Regla de Autorizaciones */
	BeanAutReglasNegocio reglasAut;
	/**
	 * @return the listaAutorizaciones
	 */
	public List<BeanAutorizacionesPendientes> getListaAutorizaciones() {
		return listaAutorizaciones;
	}
	/**
	 * @param listaAutorizaciones the listaAutorizaciones to set
	 */
	public void setListaAutorizaciones(
			List<BeanAutorizacionesPendientes> listaAutorizaciones) {
		this.listaAutorizaciones = listaAutorizaciones;
	}
	/**
	 * @return the autorizacionesPe
	 */
	public BeanAutorizacionesPendientes getAutorizacionesPe() {
		return autorizacionesPe;
	}
	/**
	 * @param autorizacionesPe the autorizacionesPe to set
	 */
	public void setAutorizacionesPe(BeanAutorizacionesPendientes autorizacionesPe) {
		this.autorizacionesPe = autorizacionesPe;
	}
	/**
	 * @return the listaReglasAut
	 */
	public List<BeanAutReglasNegocio> getListaReglasAut() {
		return listaReglasAut;
	}
	/**
	 * @param listaReglasAut the listaReglasAut to set
	 */
	public void setListaReglasAut(List<BeanAutReglasNegocio> listaReglasAut) {
		this.listaReglasAut = listaReglasAut;
	}
	/**
	 * @return the reglasAut
	 */
	public BeanAutReglasNegocio getReglasAut() {
		return reglasAut;
	}
	/**
	 * @param reglasAut the reglasAut to set
	 */
	public void setReglasAut(BeanAutReglasNegocio reglasAut) {
		this.reglasAut = reglasAut;
	}
	
	
}
