package com.mx.Req;

import java.util.List;

import com.mx.beans.BeanTelefonos;
import com.mx.beans.BeanTipoTelefono;

import BeanRespuesta.RespuestaBase;

public class TelefonosReq  extends RespuestaBase{

	BeanTelefonos telefonos;
	private List <BeanTipoTelefono> Tipotelefonos;

	public BeanTelefonos getTelefonos() {
		return telefonos;
	}

	public void setTelefonos(BeanTelefonos telefonos) {
		this.telefonos = telefonos;
	}
	
	public List<BeanTipoTelefono> getTipoTelefonos() {
		return Tipotelefonos;
	}
	public void setTipoTelefonos(List<BeanTipoTelefono> Tipotelefonos) {
		this.Tipotelefonos = Tipotelefonos;
	}


	
}
