package com.mx.Req;

import java.util.List;

import BeanRespuesta.RespuestaBase;

import com.mx.beans.BeanTipoNumeracion;

public class TipoNumeracionReq extends RespuestaBase {
	BeanTipoNumeracion tipoNumeracion;
	private List <BeanTipoNumeracion> tipoNumeraciones;
	/**
	 * @return the tipoNumeracion
	 */
	public BeanTipoNumeracion getTipoNumeracion() {
		return tipoNumeracion;
	}
	/**
	 * @param tipoNumeracion the tipoNumeracion to set
	 */
	public void setTipoNumeracion(BeanTipoNumeracion tipoNumeracion) {
		this.tipoNumeracion = tipoNumeracion;
	}
	/**
	 * @return the tipoNumeraciones
	 */
	public List<BeanTipoNumeracion> getTipoNumeraciones() {
		return tipoNumeraciones;
	}
	/**
	 * @param tipoNumeraciones the tipoNumeraciones to set
	 */
	public void setTipoNumeraciones(List<BeanTipoNumeracion> tipoNumeraciones) {
		this.tipoNumeraciones = tipoNumeraciones;
	}

}
