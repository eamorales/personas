package com.mx.url;
public class UrlService {
	public static final String Auditoria="urlAuditoria";
	public static final String InsertAuditoria="insertAuditoria";
	
	public static final String consultarTemplate="Template/Consulta";
	public static final String conexion = "conexion";
	public static final String ConsultarPersonas ="consultarPersonas";
	public static final String ConsultarTelefonos ="consultarTelefonos";
	public static final String consultarTelefono = "Telefono/Consulta";
	public static final String WEBSERVICEAlfresco= "/data";
	
	/*Constantes de Ejemplo*/
	public static final String TELEFONO="/Telefono/";
	public static final String CONSULTA_CATALOGOS_TELEFONO ="/companiaTel/consultarAll";
	public static final String CONSULTA_TELEFONO= "ConsultaTelefono";
	public static final String CONSULTA_DIRECTORIO_TELEFONO = "ConsultaDirectorioTelefono";
	public static final String ACTUALIZAR_TELEFONO= "ActualizarTelefono";
	public static final String ELIMINAR_TELEFONO= "EliminarTelefono";
	public static final String INSERTAR_TELEFONO = "InsertarTelefono";
	public static final String CONSCRED_REALIZAR_CONSULTA="";
	/*Constantes Identificaciones**/
	public static final String IDENTIFICACIONES="/Identificaciones/";
	public static final String INSERTAR_IDENTIFICAICONES = "InsertarIdentificaciones";
	public static final String ACTUALIZAR_IDENTIFICACIONES= "ActualizarIdentificaciones";
	public static final String BAJA_IDENTIFICACIONES= "BajaIdentificaciones";
	public static final String CONSULTAR_CATALOGO_TIPO_IDENTIFICACION = "ConsultarCatalogoTipoIdentificacion";
	public static final String CONSULTAR_CATALOGO_TIPO_IDENTIFICACION_EXPRESIONES = "ConsultarCatalogoTipoIdentificacionExpresiones";
	public static final String CONSULTAR_TABLA_IDENTIFICACIONES = "ConsultarTablaIdentificaciones";
	
	
	/*Consultas Domicilios*/
	public static final String DOMICILIO="/Domicilio/";
	public static final String CONSULTA_COLONIAS = "/Colonias/consultarAll";
	public static final String CONSULTA_COLONIA_ID = "ConsultaColonia";
	public static final String ACTUALIZAR_COLONIA = "ActualizarColonia";
	public static final String ELIMINAR_COLONIA = "EliminarColonia";
	public static final String INSERTAR_COLONIA = "InsertarColonia";
	public static final String CONSULTA_DIRECIONES = "/Direcciones/consultarAll";
	public static final String CONSULTA_DIRECION_ID = "ConsultaDireccion";
	public static final String ACTUALIZAR_DIRECION = "ActualizarDireccion";
	public static final String ELIMINAR_DIRECION = "EliminarDireccion";
	public static final String INSERTAR_DIRECION = "InsertarDireccion";
	public static final String CONSULTA_TIPOUBICACION_ID = "ConsultaTipoUbicacionId";
	public static final String CONSULTA_TIPOUBICACIONES = "ConsultaTipoUbicacion";
	public static final String ACTUALIZAR_TIPOUBICACION = "ActualizarTipoUbicacion";
	public static final String ELIMINAR_TIPOUBICACION = "EliminarTipoUbicacion";
	public static final String INSERTAR_TIPOUBICACION = "InsertarTipoUbicacion";
	public static final String ACTUALIZAR_DIREUBIC = "ActualizarDireUbic";
	public static final String ELIMINAR_DIREUBIC = "EliminarDireUbic";
	public static final String INSERTAR_DIREUBIC = "InsertarDireUbic";
	public static final String SUBIR_IMAGENES = "/SubirImagenes";
	public static final String CONSULTA_IMAGENES = "/BuscaImagen";
	public static final String GUARDA_DOCUMENTO = "/GuardaDocumento";
	public static final String CREAR_CARPETA = "/CrearCarpeta";
	public static final String CONSULTA_DATOS_IDENTIFICACIONES = "ConsultaDatosIdentificaciones";
	public static final String CONSULTA_TABLADIRECCION = "ConsultaTablaDireccion";
	public static final String CONSULTA_TIPONUMERACION_ID = "ConsultaTipoNumeracionId";
	public static final String CONSULTA_TIPONUMERACIONES = "ConsultaTipoNumeracion";
	
	/* Autorizaciones Pendientes */
	public static final String AUTORIZACIONESPENDIENTES = "/Autorizaciones/";
	public static final String CONSULTA_AUTORIZACIONES = "ConsultaAutorizaciones";
	public static final String ACTUALIZA_AUTORIZACIONES = "ActualizaAutorizaciones";
	public static final String URL_POST = "POST";
	
	
	/** Expedientes Braulio 12/07/2017 **/
	public static final String EXPEDIENTES="/Expedientes/";
	public static final String INSERTAR_EXPEDIENTES = "InsertarExpedientes";
	public static final String CONSULTA_EXPEDIENTES = "ConsultaExpedientes";
	public static final String CONSULTAR_CATALOGO_TIPO_DOCUMENTOS = "consultarCatalogoTipoDocumentos";
	public static final String BAJA_EXPEDIENTE =  "darBajaExpediente";
	public static final String ACTUALIZAR_EXPEDIENTE = "actualizarExpedienteService";

	/**POSICION GLOBAL JMATA JUL 2017**/
	public static final String POSICIONGLOBAL="/PosicionGlobal/";
	public static final String CONSULTA_AHORRO = "ConsultaAhorro";
	public static final String CONSULTA_CREDITO = "ConsultaCredito";
	public static final String CONSULTA_INVERSION = "ConsultaInversion";
	public static final String CONSULTA_SEGUROS = "ConsultaSeguro";
	
	/**INVOLUCRADOS JMATA JUL 2017*/
	public static final String INVOLUCRADOS="/Involucrados/";
	public static final String CONSULTAINVLUCRADOSLISTA="ConsultaInvolucradosLista/";
	
	/*Busqueda */ 
	public static final String BUSQUEDA_CLIENTE="/buscaCliente/";
	public static final String BUSQUEDA_CLIENTE_ID = "id";
	public static final String BUSQUEDA_CLIENTE_AVANZADA = "avanzada";
	public static final String BUSQUEDA_CLIENTE_ACT_CORREO = "actCorreo";
	public static final String INSERTA_CLIENTE = "insertaCliente";
	
	/** BRAULIOCHM 24/07/2017 */
	public static final String DATOS = "/Datos/";
	public static final String CONSULTAR_CATALOGO_ACTIVIDAD = "consultarCatalogoActividad";
	public static final String CONSULTAR_CATALOGO_FACULTAD = "consultarCatalogoFacultad";
	public static final String CONSULTAR_CATALOGO_SITUACION = "consultarCatalogoSituacion";
	public static final String CONSULTAR_CATALOGO_CORREO = "consultarCatalogoCorreo";
	public static final String CONSULTAR_CATALOGO_ESTABLECIMIENTO = "consultarCatalogoEstablecimiento";
	public static final String CONSULTAR_CATALOGO_GIRO = "consultarCatalogoGiro";
	public static final String CONSULTAR_CATALOGO_SECTOR = "consultarCatalogoSector";
	public static final String CONSULTAR_CATALOGO_TIPO_DE_SOCIEDAD = "consultarCatalogoTipoSociedad";
	public static final String CONSULTAR_CATALOGO_PUESTO = "consultarCatalogoPuesto";
	public static final String CONSULTAR_CATALOGO_TIPODOCUMENTOPERS = "consultarCatalogoTipoDocumentoPers";
	public static final String CONSULTAR_CATALOGO_DOCUMENTOPERS = "consultarCatalogoDocumentoPers";



	
	
	
	
	
	
}
