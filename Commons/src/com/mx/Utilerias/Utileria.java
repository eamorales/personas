package com.mx.Utilerias;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import org.apache.log4j.Logger;

public class Utileria {
	
	private static final Logger LOG = Logger.getLogger(Utileria.class); // Implementación del LOG
	private static final Date date = Calendar.getInstance().getTime();
	private static final DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	private static String fecha;
	
	public static int differenceofDays(String fechaAnterior) {
		int dias=0;
		if( fechaAnterior ==  null ||fechaAnterior.isEmpty()) 
			return  0;
		try {
			Date  fechaExp = new Date();
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
			Date hoy = getFechaHoy();
			fechaExp = formato.parse(fechaAnterior.trim());
			if(fechaExp.before(hoy)) 
				dias = (int) (( hoy.getTime() - fechaExp.getTime()) / 86400000);
			return dias;
		}catch(ParseException e) {
			LOG.info(e);
			return dias;
		}
	}
	
	 public static Map<String, String> sortByValue(Map<String, String> unsortMap) {
        // 1. Convert Map to List of Map
        List<Entry<String, String>> list = new LinkedList<Map.Entry<String, String>>(unsortMap.entrySet());
        // 2. Sort list with Collections.sort(), provide a custom Comparator
        //    Try switch the o1 o2 position for a different order
        Collections.sort(list, new Comparator<Map.Entry<String, String>>() {
            public int compare(Map.Entry<String, String> o1, Map.Entry<String, String> o2) {
                return (o1.getValue()).compareTo(o2.getValue());
            }
        });
        // 3. Loop the sorted list and put it into a new insertion order Map LinkedHashMap
        Map<String, String> sortedMap = new LinkedHashMap<String, String>();
        for (Map.Entry<String, String> entry : list) {
            sortedMap.put(entry.getKey(), entry.getValue());
        }
        return sortedMap;
	 }
	 
	 public static String cambioMeses(String fechas) {
		 fecha = fechas;
		 LOG.info(fecha);
		 String [] mesesEspaniol= {"ENE","ABR","AGO","DIC"};
		 String [] mesesIngles= {"Jan","Apr","Aug","Dec"};
		 for(int i =0; i < mesesEspaniol.length;i++)
			 fecha = fechas.replace( mesesIngles[i],mesesEspaniol[i]);
		 return fecha.toUpperCase();
	 }

	 public static Date getFechaHoy() {
		return date;
	}

	public static String getFecha() {
		return fecha;
	}
	public static String getFechaActual() {
		return formatter.format(date);
	}


	public static Date convertirFechatoDate(String fecha) {
		Date fechaFormateada = null;
		if(fecha != null){
			try {
				fechaFormateada = formatter.parse(fecha);
		

			}catch(ParseException e ) {
				e.getMessage();
			}
			
		}
		return fechaFormateada;	
	
	}

	public static String convertirFechatoString( Date fecha) {
				if(fecha != null ) {
					return formatter.format(fecha);
				}
			
				return "";
			
			
	
	}
	public static String formatearFecha(String  fecha) {
		if(fecha != null ) {
			return formatter.format(fecha);
		}
	
		return "";
} 
	
	public static int milisegundos2tiempo(int ms)
	{
	    int mili = ms%1000; ms -= mili; ms /= 1000;
	    int segs = ms%60; ms -= segs; ms /= 60;
	    int mins = ms%60; ms -= mins; ms /= 60;
	    int horas = ms;
	    return horas*1000*100*100 + mins*1000*100 + segs*1000 + mili;
	}
	
	public static Date convertirMilisegundoToDate(long miliseconds) {
		return new Date(miliseconds);
		
		
	}
	
	public static void main(String[] args)  {
		
		
	Long in = Long.parseLong("1500423837000");
	Long fin = Long.parseLong("1500423846000");
	System.out.println(convertirFechatoString(convertirMilisegundoToDate(in)));
		System.out.println(convertirFechatoString(convertirMilisegundoToDate(fin)));
 
			System.out.println(validarRangoFechas(convertirMilisegundoToDate(in), convertirMilisegundoToDate(fin)));


			System.out.println(formatearFechaToDate(convertirFechatoDate("17/ago/2017"), "yyyy-MM-dd"));
			System.out.println(obtenerFechaArchivoAlFresco());
	}
	
	public static Date formatearFechaToDate(Date fecha, String formato) {
		if(fecha != null && !formato.isEmpty()) {
			try {
				   DateFormat formateador = new SimpleDateFormat(formato);
				   String fecFormateada = formateador.format(fecha);
				   return formateador.parse(fecFormateada);
			} catch (ParseException e) {
				e.getMessage();
			}
		
		   
		}
		return fecha;
	}

	public static boolean validarRangoFechas(Date inicio, Date fin) {
		
		return ( getFechaHoy().compareTo(inicio) >= 0 && getFechaHoy().compareTo(fin) <=0);
	}
    public static String obtenerFechaArchivoAlFresco() {
    	
        Calendar fecha = Calendar.getInstance();
        String month = String.valueOf(fecha.get(Calendar.MONTH) + 1);
        String day = String.valueOf(fecha.get(Calendar.DAY_OF_MONTH));
        String ano = String.valueOf(fecha.get(Calendar.YEAR));
        String mes = (month.length() > 1 ? month : "0"+month );
        String dia = String.valueOf(day.length() > 1 ? day : "0"+day );
        String hora = String.valueOf(fecha.get(Calendar.HOUR_OF_DAY));
        String minuto = String.valueOf(fecha.get(Calendar.MINUTE));
        String segundo = String.valueOf(fecha.get(Calendar.SECOND));
        String milisegundos = String.valueOf(fecha.get(Calendar.MILLISECOND));
        return new StringBuilder().append(("_"+ano+""+mes+""+dia)+"_"+(hora+""+minuto+""+segundo+""+""+milisegundos+"_")).toString();
    	
    }
	
	
	

}
