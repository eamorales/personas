package mx.com.promesi.exceptions;


public class BusinessException extends Exception{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String id;
	
	public BusinessException() {
		
	}
	public BusinessException(String id) {
		super(id);
		this.id=id;
	}
	public BusinessException(Throwable exc) {
		super(exc);
	}
	public BusinessException(String id,Throwable exc) {
		super(id,exc);
		this.id=id;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	
	
	
}
