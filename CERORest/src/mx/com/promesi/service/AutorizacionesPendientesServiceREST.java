package mx.com.promesi.service;


import mx.com.promesi.persistencia.AutorizacionesPendientesDao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.Req.AutorizacionesReq;
import com.mx.Res.Respuesta;
import com.mx.url.UrlService;

/**
 * @author GBG
 *
 */
@RestController
@RequestMapping(UrlService.AUTORIZACIONESPENDIENTES)
public class AutorizacionesPendientesServiceREST{
	private static final Logger LOG = Logger.getLogger(AutorizacionesPendientesServiceREST.class);

	@Autowired
	private AutorizacionesPendientesDao autorizaciones;

	@RequestMapping(value = UrlService.CONSULTA_AUTORIZACIONES)
	@ResponseBody
	public Respuesta consultaAutorizacionesPendientesLista(@RequestBody AutorizacionesReq bAutPe) {
		Respuesta resListConAut = new Respuesta();
		LOG.info("ConsultaAutorizacionesPendientesREST");
		try {
			resListConAut = autorizaciones.consultaAutorizacionesPendientes(bAutPe.getAutorizacionesPe());
		} catch (Exception e) {
			LOG.info(e);
			LOG.info("ha ocurrido un error al consultar autorizaciones pendientes");
		}
		return resListConAut;
	}

	@RequestMapping(value = UrlService.ACTUALIZA_AUTORIZACIONES)
	@ResponseBody
	public Respuesta actualizaAutorizacionesPendientesLista(@RequestBody AutorizacionesReq bAutPe) {
		Respuesta resListConAut = new Respuesta();
		LOG.info("ActualizaAutorizacionesPendientesREST");
		try {
			resListConAut = autorizaciones.actualizaAutorizacionesPendientes(bAutPe.getListaAutorizaciones());
		} catch (Exception e) {
			LOG.info(e);
			LOG.info("ha ocurrido un error al actualizar autorizaciones pendientes");
		}
		return resListConAut;
	}
}