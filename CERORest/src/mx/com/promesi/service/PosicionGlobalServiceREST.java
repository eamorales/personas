package mx.com.promesi.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.Res.Respuesta;
import com.mx.url.UrlService;

import mx.com.promesi.persistencia.PosicionGlobalDao;

/**
 * @author JMATA JUL 2017
 *Clase para el servicio del posición global
 */
@RestController
@RequestMapping(UrlService.POSICIONGLOBAL)
public class PosicionGlobalServiceREST {
	private static final Logger LOG = Logger.getLogger(DomiciliosServiceREST.class);
	
	@Autowired
	private PosicionGlobalDao posicionDao;
	
	/**
	 * 
	 * Función para consultar Ahorro
	 * */
	@RequestMapping(value =UrlService.CONSULTA_AHORRO)
	public @ResponseBody Respuesta consultaAhorro(){
		Respuesta res = new Respuesta();
		LOG.info("::DENTRO DE REST PG::");
		try {
			res=(posicionDao.consultaAhorro());
		} catch (Exception e) {
			LOG.info("::ERROR POSICION GLOBAL CONSULTA AHORRO SERVICE ::");
		}
		return res;
	}
	/**
	 * 
	 * Función para consultar Credito
	 * */
	@RequestMapping(value =UrlService.CONSULTA_CREDITO)
	public @ResponseBody Respuesta consultaCreditos(){
		Respuesta res = new Respuesta();
		
		try {
			res=posicionDao.consultaCredito();
		} catch (Exception e) {
			LOG.info("::ERROR POSICION GLOBAL CONSULTA CREDITO SERVICE::");
		}
		return res;
	}
	/**
	 * 
	 * Función para consultar Inversión
	 * */
	@RequestMapping(value =UrlService.CONSULTA_INVERSION)
	public @ResponseBody Respuesta consultaInversion(){
		Respuesta res = new Respuesta();
		try {
			res=posicionDao.consultaInversion();
		} catch (Exception e) {
			LOG.info("::ERROR POSICION GLOBAL CONSULTA INVERSION SERVICE::");
		}
		return res;
	}
	/**
	 * 
	 * Función para consultar Seguros
	 * */
	@RequestMapping(value =UrlService.CONSULTA_SEGUROS)
	public @ResponseBody Respuesta consultaSeguros(){
		Respuesta res = new Respuesta();
		try {
			res=posicionDao.consultSeguros();
		} catch (Exception e) {
			LOG.info("::ERROR POSICION GLOBAL CONSULTA SEGUROS SERVICE:");
		}
		return res;
	}

	
}
