/**
 * 
 */
package mx.com.promesi.service;

//import mx.com.promesi.beans.AppSessions;
import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.persistencia.DomiciliosDao;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.mx.Req.DomiciliosReq;
import com.mx.Req.TipoNumeracionReq;
import com.mx.Req.TipoUbicacionReq;
import com.mx.Res.Respuesta;
import com.mx.url.UrlService;

/**
 * @author Gustavo Bujano
 *
 */
@RestController
@RequestMapping(UrlService.DOMICILIO)
public class DomiciliosServiceREST{
	private static final Logger LOG = Logger.getLogger(DomiciliosServiceREST.class);
	private Respuesta res = new Respuesta();
	@Autowired
	private DomiciliosDao domicilio;
	
//	@Autowired
//	private AppSessions sesiones;
	/**
	 * 
	 * Regresa todas los tipos de ubicaciones para la sección de Domicilio
	 * 
	 * */
	@RequestMapping(value = UrlService.CONSULTA_TIPOUBICACIONES)
	@ResponseBody
	public Respuesta consultaTipoUbicaciones(@RequestBody TipoUbicacionReq t,HttpServletRequest request) {
//		String auth=request.getHeader(Constantes.AUTH_HEADER);
//		sesiones.getSesiones().get(auth)
		
		res = new Respuesta();
		LOG.info("consultaTipoUbicacionesREST");
		try {		
			res = domicilio.consultaTipoUbicacionAll();
		} catch (Exception e) {
			LOG.info(e);
		}
		return res;
	}
	
	/**
	 * 
	 * Regresa todos los tipos de Numeración para la sección de Domicilios
	 * 
	 * */
	@RequestMapping(value = UrlService.CONSULTA_TIPONUMERACIONES)
	@ResponseBody
	public Respuesta consultaTipoNumeraciones(@RequestBody TipoNumeracionReq tipoNuReq) {
		res = new Respuesta();
		LOG.info("consultaTipoNumeracionesREST");
		try {		
			res =domicilio.consultaTipoNumeracionAll();
		} catch (Exception e) {
			LOG.info(e);
		}
		return res;
	}

	/**
	 * Funcion para obtener todas los registros de la tabla.
	 * */
	@RequestMapping(value =UrlService.CONSULTA_TABLADIRECCION)
	public  @ResponseBody Respuesta consultaTabla(@RequestBody DomiciliosReq t){
		res = new Respuesta();
//		LOG.info(sesiones.getSesiones().get(t.getIdSesion()));
		LOG.info("::CONSULTA TABLA REST::");
		try {		
			res =(domicilio.consultaTabla());
		} catch (Exception e) {
			LOG.info(e);			
		}
		return res;
	}

	@RequestMapping(value = UrlService.INSERTAR_DIRECION)
	@ResponseBody
	public Respuesta insertaDomicilio(@RequestBody DomiciliosReq domReq) {
		LOG.info("::Insertar Domicilio::");
		res = new Respuesta();
		try {
			res  = domicilio.insertaDomicilio(domReq.getDomicilio());
		} catch (Exception e) {
			LOG.info(e.getMessage() + "A ocurrido en error en Web Service");
		}
		return res;
	}
	@RequestMapping(value = UrlService.ELIMINAR_DIRECION)
	@ResponseBody
	public Respuesta eliminarDomicilio(@RequestBody DomiciliosReq domReq) {
		LOG.info("Service ::::::: Eliminar Domicilio::");

		res = new Respuesta();
		try {
			res  = domicilio.eliminaDomicilio(domReq.getDomicilio());
		} catch (Exception e) {
			LOG.info(e);
		}
		LOG.info("::MENSAJE DE ERROR::");
		return res;
	}
	@RequestMapping(value = UrlService.CONSULTA_DIRECION_ID)
	@ResponseBody
	public Respuesta consultaDimicilioId(@RequestBody DomiciliosReq domReq) {
		 res = new Respuesta();
		LOG.info("ConsultaDireccionIdREST");
		try {
			res= domicilio.consultaDomicilioId(domReq.getDomicilio());
		} catch (Exception e) {
			LOG.info(e);
		}
		return res;
	}
	
	
	@RequestMapping(value = UrlService.ACTUALIZAR_DIRECION)
	@ResponseBody
	public Respuesta actualizarDomicilio(@RequestBody DomiciliosReq domReq) {
		LOG.info("Actulizando  Domicilio");
		res = new Respuesta();
		try {
			res  = domicilio.actualizaDomicilio(domReq.getDomicilio());
		} catch (BusinessException e) {
			LOG.info(e);
		}
		return res;
	}
}