/**
 * 
 */ 
package mx.com.promesi.service;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.Req.IdentificacionesReq;
import com.mx.Req.TelefonosReq;
import com.mx.Res.Respuesta;
import com.mx.url.UrlService;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.persistencia.IdentificacionesDao;

/**
 * @author Mariel
 *
 */
@RestController
@RequestMapping(UrlService.IDENTIFICACIONES)
public class IdentificacionesServiceREST {
	private static final Logger LOG = Logger.getLogger(IdentificacionesServiceREST.class);
	private Respuesta res = new Respuesta();
	@Autowired
	private IdentificacionesDao identificaciones;
	
	/**
	 * Función para insertar las identificaciones
	 */ 
	@RequestMapping(value =UrlService.INSERTAR_IDENTIFICAICONES)
	@ResponseBody
	public Respuesta insertarIdentificacion(@RequestBody IdentificacionesReq objIdentificaciones) {
	 	 res = new Respuesta();		
		LOG.info("Insertar Identificaciones");
		try {
			res =identificaciones.insertarIdentificaciones(objIdentificaciones.getIdentificacion());
		} catch (Exception e) {
			LOG.info(e);
		}
		return res;
	}
	/**
	 * Función para actualizar las identificaciones
	 */ 
	@RequestMapping(value =UrlService.ACTUALIZAR_IDENTIFICACIONES)
	@ResponseBody
	public Respuesta actualizaridentificacion(@RequestBody IdentificacionesReq objIdentificaciones) {
		LOG.info("Actualizar Identificaciones");
		 res = new Respuesta();
		try {
			res  =identificaciones.actualizaridentificaciones(objIdentificaciones.getIdentificacion());
		} catch (BusinessException e) {
			LOG.info(e);
		}
		return res;
	}
	/**
	 * Función para obtener el catalogo de tipo identificaciones
	 */ 
	@RequestMapping(value =UrlService.CONSULTAR_CATALOGO_TIPO_IDENTIFICACION)
	@ResponseBody
	public Respuesta consultarCatalogoTipo(@RequestBody TelefonosReq t) {
	 	 res = new Respuesta();		 	
		LOG.info("Consulta Catalogo Tipo Identificacion");
		try {
			res =identificaciones.consultarCatalogoTipo();
		} catch (Exception e) {
			LOG.info(e);
		}
		return res;
	}
	/**
	 * Función para obtener el catalogo de tipo identificaciones
	 */ 
	@RequestMapping(value =UrlService.CONSULTAR_CATALOGO_TIPO_IDENTIFICACION_EXPRESIONES)
	@ResponseBody
	public Respuesta consultarCatalogoTipoExpresiones(@RequestBody TelefonosReq t) {
	 	 res = new Respuesta();		 	
		LOG.info("Consulta Catalogo Tipo Identificacion Expresiones");
		try {
			res =identificaciones.consultarCatalogoTipoExpresiones();
		} catch (Exception e) {
			LOG.info(e);
		}
		return res;
	}
	
	
	/**
	 * Función para consultar la tabla identificaciones
	 */ 
	@RequestMapping(value =UrlService.CONSULTAR_TABLA_IDENTIFICACIONES)
	@ResponseBody
	public Respuesta consultarTablaIdentificaciones(@RequestBody IdentificacionesReq objIdentificaciones) {
	 	 res = new Respuesta();		 	
		LOG.info("Consulta tabla Identificacion");
		try {
			res =identificaciones.consultarTablaIdentificaciones() ;
		} catch (Exception e) {
			LOG.info(e);
		}
		return res;
	}
	
	
	/**
	 * Función para eliminar una identificacion
	 */ 
	@RequestMapping(value =UrlService.BAJA_IDENTIFICACIONES)
	@ResponseBody
	public Respuesta baja(@RequestBody IdentificacionesReq objIdentificaciones) {
	 	 res = new Respuesta();		 	
		LOG.info("::BAJA IDENTIFICACIONES::");
		try {
			res =identificaciones.eliminarIdentificaciones(objIdentificaciones.getIdentificacion());
		} catch (Exception e) {
			LOG.info(e);
		}
		return res;
	}
	/**
	 * Función para consultar el detalle de la identificacion
	 */ 
@RequestMapping(value =UrlService.CONSULTA_DATOS_IDENTIFICACIONES)
@ResponseBody
public Respuesta consultaIdentificacionDetalle(@RequestBody IdentificacionesReq objIdentificaciones) {
	res = new Respuesta();
	LOG.info("Consulta Datos Identificaciones");
	try {
		res = identificaciones.consultaIdentificacionesDetalle(objIdentificaciones.getIdentificacion());
	} catch (Exception e) {
		LOG.info(e);
	}
	return res;
}
}
