package mx.com.promesi.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.Req.ExpedienteReq;
import com.mx.Res.Respuesta;
import com.mx.Utilerias.Utileria;
import com.mx.beans.BeanExpedientes;
import com.mx.beans.ErrorDTO;
import com.mx.constantes.Constantes;
import com.mx.url.UrlService;

import mx.com.promesi.persistencia.ExpedientesDao;

@RestController
@RequestMapping(UrlService.EXPEDIENTES)
public class ExpedientesServiceREST {
	

	private static final Logger  LOG = Logger.getLogger(ExpedientesServiceREST.class);
	private Respuesta response;
	@Autowired
	private ExpedientesDao expediente;

	@RequestMapping(UrlService.CONSULTAR_CATALOGO_TIPO_DOCUMENTOS)
	@ResponseBody 
	public Respuesta consultarCatalogoTipoDocumentos (@RequestBody ExpedienteReq  request) {
		response = new Respuesta();
		try {
			LOG.info("SERVICE EXPEDIENTES ::::: CONSULTAR TIPO DOCUMENTOS ");
			response = expediente.consultarCatalogoTipoDocumento();
		} catch (Exception e) {
			LOG.error(e.getMessage());
		
		}
		return response;
	}
	
	
	/****
	 * Funcion para guardar Expedientes
	 * 
	 * */
	@RequestMapping(value = UrlService.INSERTAR_EXPEDIENTES)
	@ResponseBody
	public Respuesta insertarExpedientes(@RequestBody ExpedienteReq request) {
		response = new Respuesta();
		ErrorDTO error = new ErrorDTO();
		try {
			LOG.info("SERVICE EXPEDIENTES ::::: INSERTAR EXPEDIENTES ");
			for (BeanExpedientes element : request.getListaExpedientes()) {
				if(element.getFecha() != null && element.getTipoDocumento() != null && element.getObservaciones() != null )
				{
					response = expediente.insertarExpedientes(element);
				}else {
					error.setClave(Constantes.ERROR_EXECUTE);
					error.setMessage("Verifica que se hayan llenado los campos correctamente");
					response.setValido(false);
					response.setError(error);
					break;
				}
					
			
			}
			
		} catch (Exception e) {
			LOG.info(e.getMessage());
			LOG.error("ERROR EN REST NUEVO EXPEDIENTE");
		}
		
		return response;
	}
	@RequestMapping(value = UrlService.CONSULTA_EXPEDIENTES)
	@ResponseBody 
	public Respuesta consultarExpedientes(@RequestBody ExpedienteReq  request) {
		response = new Respuesta();
		try {
			LOG.info("SERVICE EXPEDIENTES ::::: CONSULTAN EXPEDIENTES ");
			response = expediente.consultarExpedientes(request.getIdPersona());
		} catch (Exception e) {
			LOG.error(response.getError().getMessage());
			LOG.error("ERROR EN REST AL CONSULTAR EXPEDIENTES");

		}
		return response;
	}
	@RequestMapping(value = UrlService.BAJA_EXPEDIENTE)
	@ResponseBody 
	public Respuesta bajaExpediente(@RequestBody ExpedienteReq  request) {
		
		response = new Respuesta();
		try {
			for (BeanExpedientes element : request.getListaExpedientes()) {
				if(element.isSeleccionado()) {
					LOG.info("SERVICE EXPEDIENTES ::::: DANDO DE BAJA EXPEDIENTES" + element.getIdExpediente() +" "+ element.isSeleccionado());
					response = expediente.eliminarExpediente(element.getIdExpediente());
				}
			}
		} catch (Exception e) {
			LOG.error(response.getError().getMessage());
			LOG.error("ERROR EN REST BAJA EXPEDIENTE");
		}
		return response;
	}
	
	
	@RequestMapping(value = UrlService.ACTUALIZAR_EXPEDIENTE)
	@ResponseBody 
	public Respuesta actualizarExpedienteService(@RequestBody ExpedienteReq  request) {
		LOG.error("SERVICE WORKING UPDATE EXPEDIENTE");
		response = new Respuesta();
		try {
			for (BeanExpedientes element : request.getListaExpedientes()) {
				if(element.isSeleccionado()) {
					response = expediente.actualizarExpediente(element);
				}
			}
		} catch (Exception e) {
			LOG.error(response.getError().getMessage());
			LOG.error("ERROR EN REST ACTUALIZAR EXPEDIENTE");
		}
		return response;
	}
	
	
		
}
