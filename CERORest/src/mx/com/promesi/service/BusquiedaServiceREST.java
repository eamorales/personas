package mx.com.promesi.service;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.persistencia.ClienteDao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.Res.Respuesta;
import com.mx.beans.BeanCatalogosDatos;
import com.mx.beans.ErrorDTO;
import com.mx.beans.Busqueda.BeanCliente;
import com.mx.beans.Busqueda.BeanClienteEntrada;
import com.mx.constantes.Constantes;
import com.mx.url.UrlService;

/**
 * @author EAMA
 *
 */
@RestController
@RequestMapping(UrlService.BUSQUEDA_CLIENTE)
public class BusquiedaServiceREST {
	private static final Logger logger = Logger.getLogger(BusquiedaServiceREST.class);

	@Autowired
	private ClienteDao clienteDao;

	@RequestMapping(value = UrlService.BUSQUEDA_CLIENTE_ID)
	@ResponseBody
	public Respuesta buscaClientePorId(@RequestBody BeanClienteEntrada noCliente) {
		Respuesta resCliente = new Respuesta();
		ErrorDTO error = new ErrorDTO();
		logger.info("Consulta Busqueda cliente rest");
		try {
			resCliente.setBeanCatalogoDatos(clienteDao.consultaClientePorId(noCliente.getNoCliente()));
			error.setClave(Constantes.CLAVE_CORRECTA);
		} catch (BusinessException e) {
			logger.error("ha ocurrido un error al consultar la busqueda del cliente");
			error.setClave("60");
			error.setMessage("ERROR EN C AL BUSCAR DATOS CLIENTE");
		}
		resCliente.setError(error);
		return resCliente;
	}
	
	@RequestMapping(value = UrlService.INSERTA_CLIENTE)
	@ResponseBody
	public Respuesta insertaCliente(@RequestBody BeanCatalogosDatos datosCliente) {
		Respuesta resCliente = new Respuesta();
		ErrorDTO error = new ErrorDTO();
		logger.info("Inserta cliente rest");
		long id =0;
		BeanCatalogosDatos catalogoDatos = new BeanCatalogosDatos();
		try {
			id = clienteDao.insertaCliente(datosCliente);
			catalogoDatos.getBeanCliente().getBeanInformacion().setNoCliente(id);;
			error.setClave(Constantes.CLAVE_CORRECTA);
			resCliente.setBeanCatalogoDatos(catalogoDatos);
		} catch (BusinessException e) {
			logger.error("ha ocurrido un error al consultar la busqueda del cliente");
			error.setClave("60");
			error.setMessage("ERROR EN C AL BUSCAR DATOS CLIENTE");
		}
		resCliente.setError(error);
		return resCliente;
	}
}