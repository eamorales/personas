package mx.com.promesi.service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class Urls {
	  private static final Logger LOG = Logger.getLogger(Urls.class);

	/**
	 * 
	 * SCHEMAS
	 * 
	 * */
	private static String schemaPersonas;
	private static String schemaNucleoCentral;
	private static String schemaAutorizaciones;
	
	Urls(){
		LOG.info("Cargando URLS.. REST");
		/**Se crear el properties*/
		Properties propiedades = new Properties();
		try {
			/**Cargamos el archivo desde la ruta especificada*/
			propiedades.load(getClass().getResourceAsStream("personas.properties"));
			/**
			 * Se cargan los Schemas
			 * */
			setSchemaPersonas(propiedades.getProperty("schemaPersonas"));
			setSchemaNucleoCentral(propiedades.getProperty("schemaNucleoCentral"));
			setSchemaAutorizaciones(propiedades.getProperty("schemaAutorizaciones"));
		} catch (FileNotFoundException e) {
			LOG.info("Error, El archivo no exite");
		} catch (IOException e) {
			LOG.info("ha ocurrido un error al leer el archivo");
		}
	}
	
	public static String getSchemaPersonas() {
		return schemaPersonas;
	}
	public static void setSchemaPersonas(String schemaPersonas) {
		Urls.schemaPersonas = schemaPersonas;
	}
	public static String getSchemaNucleoCentral() {
		return schemaNucleoCentral;
	}
	public static void setSchemaNucleoCentral(String schemaNucleoCentral) {
		Urls.schemaNucleoCentral = schemaNucleoCentral;
	}
	public static String getSchemaAutorizaciones() {
		return schemaAutorizaciones;
	}
	public static void setSchemaAutorizaciones(String schemaAutorizaciones) {
		Urls.schemaAutorizaciones = schemaAutorizaciones;
	}
	}
