package mx.com.promesi.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.Req.DatosReq;
import com.mx.Res.Respuesta;
import com.mx.url.UrlService;

import mx.com.promesi.persistencia.DatosDao;

@RestController
@RequestMapping(UrlService.DATOS)
public class DatosServiceREST {
	private static final Logger log = Logger.getLogger(DatosServiceREST.class);
	@Autowired
	private DatosDao daoInterface;
	private Respuesta response;
	
	

	@RequestMapping(UrlService.CONSULTAR_CATALOGO_SECTOR)
	@ResponseBody
	public Respuesta consultarCatalogoSector(@RequestBody DatosReq request) {
		response = new Respuesta();
		try {
			log.info("SERVICE WORKING SECTOR" + UrlService.CONSULTAR_CATALOGO_SECTOR);
			response = daoInterface.obtenerCatalogoSector();
		} catch (Exception e) {
			log.error(e.getMessage());

		}
		return response;
	}

	@RequestMapping(UrlService.CONSULTAR_CATALOGO_FACULTAD)
	@ResponseBody
	public Respuesta consultarCatalogoFacultad(@RequestBody DatosReq request) {
		response = new Respuesta();
		try {
			log.info("SERVICE WORKING FACCULTAD " + UrlService.CONSULTAR_CATALOGO_FACULTAD);
			response = daoInterface.obtenerCatalogoFacultad();
		} catch (Exception e) {
			log.error(e.getMessage());

		}
		return response;
	}

	@RequestMapping(UrlService.CONSULTAR_CATALOGO_SITUACION)
	@ResponseBody
	public Respuesta consultarCatalogoSituacion(@RequestBody DatosReq request) {
		response = new Respuesta();
		try {
			log.info("SERVICE WORKING SITUACION" + UrlService.CONSULTAR_CATALOGO_SITUACION);
			response = daoInterface.obtenerCatalogoSituacion();
		} catch (Exception e) {
			log.error(e.getMessage());

		}
		return response;
	}

	@RequestMapping(UrlService.CONSULTAR_CATALOGO_CORREO)
	@ResponseBody
	public Respuesta consultarCatalogoCorreo(@RequestBody DatosReq request) {
		response = new Respuesta();
		try {
			log.info("SERVICE WORKING CORREO::: " + UrlService.CONSULTAR_CATALOGO_CORREO);
			response = daoInterface.obtenerCatalogoCorreo();
		} catch (Exception e) {
			log.error(e.getMessage());

		}
		return response;
	}

	@RequestMapping(UrlService.CONSULTAR_CATALOGO_ESTABLECIMIENTO)
	@ResponseBody
	public Respuesta consultarCatalogoEstablecimiento(@RequestBody DatosReq request) {
		response = new Respuesta();
		try {
			log.info("SERVICE WORKING ESTABLECIMIENTO ::: " + UrlService.CONSULTAR_CATALOGO_ESTABLECIMIENTO);
			response = daoInterface.obtenerCatalogoEstablecimiento();
		} catch (Exception e) {
			log.error(e.getMessage());

		}
		return response;
	}

	@RequestMapping(UrlService.CONSULTAR_CATALOGO_GIRO)
	@ResponseBody
	public Respuesta consultarCatalogoGiro(@RequestBody DatosReq request) {
		response = new Respuesta();
		try {
			log.info("SERVICE WORKING GIRO ::: " + UrlService.CONSULTAR_CATALOGO_GIRO);
			response = daoInterface.obtenerCatalogoGiro();
		} catch (Exception e) {
			log.error(e.getMessage());

		}
		return response;
	}

	@RequestMapping(UrlService.CONSULTAR_CATALOGO_TIPO_DE_SOCIEDAD)
	@ResponseBody
	public Respuesta consultarCatalogoTipoSociedad(@RequestBody DatosReq request) {
		response = new Respuesta();
		try {
			log.info("SERVICE WORKING TIPO SOCIEDAD::: " + UrlService.CONSULTAR_CATALOGO_TIPO_DE_SOCIEDAD);
			response = daoInterface.obtenerCatalogoGiro();
		} catch (Exception e) {
			log.error(e.getMessage());

		}
		return response;
	}

	@RequestMapping(UrlService.CONSULTAR_CATALOGO_PUESTO)
	@ResponseBody
	public Respuesta consultarCatalogoPuesto(@RequestBody DatosReq request) {
		response = new Respuesta();
		try {
			log.info("SERVICE WORKING PUESTO::: " + UrlService.CONSULTAR_CATALOGO_PUESTO);
			response = daoInterface.obtenerCatalogoPuesto();
		} catch (Exception e) {
			log.error(e.getMessage());

		}
		return response;
	}

	
	@RequestMapping(UrlService.CONSULTAR_CATALOGO_ACTIVIDAD)
	@ResponseBody
	public Respuesta consultarCatalogoActividad(@RequestBody DatosReq request) {
		response = new Respuesta();
		try {
			log.info("SERVICE WORKING ACTIVIDAD ::: " + UrlService.CONSULTAR_CATALOGO_ACTIVIDAD);
			response = daoInterface.obtenerCatalogosActividad();
		} catch (Exception e) {
			log.error(e.getMessage());
			
		}
		return response;
	}


	@RequestMapping(UrlService.CONSULTAR_CATALOGO_TIPODOCUMENTOPERS)
	@ResponseBody
	public Respuesta consultarCatalogoTipoDocumentoPers(@RequestBody DatosReq request) {
		response = new Respuesta();
		try {
			log.info("SERVICE WORKING TIPO DE DOCUMENTOS PERSONALIDAD ::: " + UrlService.CONSULTAR_CATALOGO_TIPODOCUMENTOPERS);
			response = daoInterface.obtenerCatalogoTipoDocumentoPers();
		} catch (Exception e) {
			log.error(e.getMessage());
			
		}
		return response;
	}
	
	@RequestMapping(UrlService.CONSULTAR_CATALOGO_DOCUMENTOPERS)
	@ResponseBody
	public Respuesta consultarCatalogoDocumentoPers(@RequestBody DatosReq request) {
		response = new Respuesta();
		try {
			log.info("SERVICE WORKING DOCUMETOS PERSONALIDAD ::: " + UrlService.CONSULTAR_CATALOGO_DOCUMENTOPERS);
			response = daoInterface.obtenerCatalogoDocumentoPers();
		} catch (Exception e) {
			log.error(e.getMessage());
			
		}
		return response;
	}
	
}
