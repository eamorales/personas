package mx.com.promesi.service;


import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.Req.TelefonosReq;
import com.mx.Res.Respuesta;
import com.mx.url.UrlService;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.persistencia.TelefonosDao;

/**
 * @author GBG
 * Clase para el Servicio de Telefonos 
 *
 */
@RestController
@RequestMapping(UrlService.TELEFONO)
public class TelefonosServiceREST{
	  private static final Logger LOG = Logger.getLogger(TelefonosServiceREST.class);
	  Respuesta res = new Respuesta();
	 @Autowired
	 private TelefonosDao telefono;
	 
	 /**
	  * Función para insertar telefonos
	  * */
	 @RequestMapping(value =UrlService.INSERTAR_TELEFONO)
		@ResponseBody
		public Respuesta insertarTelefono(@RequestBody TelefonosReq t) {
		 	res = new Respuesta();		 	
			LOG.info("InsetarTelefonoREST");
			try {
				res =telefono.insertarTelefono(t.getTelefonos());
			} catch (Exception e) {
				LOG.info(e);
				LOG.info("ha ocurrido un error al insertar el detalle del telefono");

			}
			return res;
		}
	 
	 /**
	  * Función para consultar el detalle del telefonos
	  * */
	@RequestMapping(value =UrlService.CONSULTA_TELEFONO)
	@ResponseBody
	public Respuesta consultaTelefonoDetalle(@RequestBody TelefonosReq t) {
		res = new Respuesta();
		LOG.info("ConsultaTelefonoDetalleREST");
		try {
			res = telefono.consultaTelefonoDetalle(t.getTelefonos());
		} catch (Exception e) {
			LOG.info(e);
			LOG.info("ha ocurrido un error al consultar el detalle del telefono");
		}
		return res;
	}
	/**
	  * Función para consultar el catalogo de telefonos
	  * */
	@RequestMapping(value =UrlService.CONSULTA_DIRECTORIO_TELEFONO)
	@ResponseBody
	public Respuesta consultaDirectorioTelefono(@RequestBody TelefonosReq t) {
		res = new Respuesta();
		LOG.info("consultaDirectorioTelefonoREST");
		try {		
			res = telefono.consultaDirectorioTelefono();
		} catch (Exception e) {
			LOG.info(e);
			LOG.info("ha ocurrido un error al consultar el directorio telefonico");

		}		
		return res;
	}
	/**
	  * Función para Actualizar el detalle del telefonos
	  * */
	@RequestMapping(value =UrlService.ACTUALIZAR_TELEFONO)
	@ResponseBody
	public Respuesta actualizarTelefono(@RequestBody TelefonosReq t) {
		LOG.info("Actualizar Telefono");
		res = new Respuesta();
		try {
			res  =telefono.actualizarTelefono(t.getTelefonos());
		} catch (BusinessException e) {
			LOG.info(e);
			LOG.info("ha ocurrido un error al actualizar el detalle del telefono");

		}
		return res;
	}
	/***
	 * 
	 * Función para eliminar el telefono
	 * */
	@RequestMapping(value =UrlService.ELIMINAR_TELEFONO)
	@ResponseBody
	public Respuesta eliminarTelefono(@RequestBody TelefonosReq t) {
		LOG.info("::Eliminar Telefono::");
		res = new Respuesta();
		try {
			res = telefono.eliminarTelefono(t.getTelefonos());
		} catch (Exception e) {
			LOG.info(e);
			LOG.info("ha ocurrido un error al eliminar el detalle del telefono");

		}
		return res;
	}

}
