package mx.com.promesi.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.Res.Respuesta;
import com.mx.url.UrlService;

import mx.com.promesi.persistencia.InvolucradosDao;

@RestController
@RequestMapping(UrlService.INVOLUCRADOS)
public class InvolucradosServiceRest {
	private static final Logger LOG = Logger.getLogger(DomiciliosServiceREST.class);
	
	@Autowired
	private InvolucradosDao involucradosDao;
	
	@RequestMapping (value= UrlService.CONSULTAINVLUCRADOSLISTA)
	public @ResponseBody Respuesta consultaInvolucradosLista(){
		
		LOG.info("::DENTRO DE CONSULTA INVOLUCRADOS LISTA SERVICE::"+ UrlService.CONSULTAINVLUCRADOSLISTA);
		Respuesta res= new Respuesta();
		
		try {
			res=(involucradosDao.consultaListaInv());
		} catch (Exception e) {
			LOG.info("::ERROR AL CONSULTAR LISTA DE INVOLUCRADOS::");
		}
		return res;
		
	}
	
	
	

}
