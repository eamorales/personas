package mx.com.promesi.service;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.mx.Res.Respuesta;
import com.mx.beans.BeanAuditoria;
import com.mx.url.UrlService;

import mx.com.promesi.persistencia.AuditoriaDao;

@RestController
@RequestMapping(UrlService.Auditoria)
public class AuditoriaREST {
	
	private static final Logger log = Logger.getLogger(DatosServiceREST.class);
	@Autowired 
	private AuditoriaDao Audit;
	private Respuesta response;
	
	
	@RequestMapping(UrlService.InsertAuditoria)
	public Respuesta guardarAuditoria() {
		
		BeanAuditoria audit = new BeanAuditoria();
		response = new Respuesta();
		try {
			log.info("SERVICE WORKING SECTOR" + UrlService.CONSULTAR_CATALOGO_SECTOR);
			response = Audit.insertarAuditoria(audit);
		} catch (Exception e) {
			log.error(e.getMessage());

		}
		return response;
		
		
	}
	

}
