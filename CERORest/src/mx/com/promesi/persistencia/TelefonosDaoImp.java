package mx.com.promesi.persistencia;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mx.Res.Respuesta;
import com.mx.Utilerias.Utileria;
import com.mx.beans.BeanTelefonos;
import com.mx.beans.ErrorDTO;
import com.mx.constantes.Constantes;
import com.mx.constantes.HardCode;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.service.Urls;

/**
 * @author Gustavo Bujano
 * @version 1.2
 * @category Implementacion
 */
@Repository
public class TelefonosDaoImp implements TelefonosDao {

	/**
	 * 
	 * Se crean variables de instancia
	 * 
	 */
	private NamedParameterJdbcTemplate namedJdbcTemplate;
	private static final Logger LOG = Logger.getLogger(TelefonosDaoImp.class); // Implementación del LOG
	String today = Utileria.getFechaActual();
	String tablaTelefono = "\"PeTelefono\" ";
	ErrorDTO error = new ErrorDTO();
	Respuesta respuesta = new Respuesta();

	/**
	 * 
	 * Funcion para obtener fecha actual
	 * 
	 * @return String fecha
	 * 
	 */
	public String getToday() {
		return today;
	}

	/**
	 * Funcion para indicar conexion a BD
	 */
	@Resource(name = "DataSource")
	public void setDataSource(DataSource dataSource) {
		this.namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	/**
	 * 
	 * Funcion para guardar telefono
	 * 
	 * @return Respuesta
	 * @exception ErrorDTO
	 * 
	 */
	@Override
	public Respuesta insertarTelefono(BeanTelefonos t) {
		String schema = Urls.getSchemaPersonas();
		respuesta = new Respuesta();
		error = new ErrorDTO();
		BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(t);
		int crearPrincipal = 0;
		try {
			if (t.isPrincipal()){
				changeprincipal(t.getIdTelSeleccionado());
			}else{
			    crearPrincipal  = validaPrincipalExista("0");
			   if(crearPrincipal == 0)
			    t.setPrincipal(true);
			}
			StringBuilder qry = new StringBuilder();
			qry.append("INSERT INTO " + schema + tablaTelefono
					+ "(\"fechaCreacion\", \"fechaModificacion\", \"usuarioCreacion\", \"usuarioModificacion\",\"idPersona\", \"idTipoTel\", \"idCompaniaTel\", telefono,"
					+ " estatus, ");
			if(t.getExtension()!=null && t.getExtension().length()>0){ /*Se valida la extensión*/
				qry.append("ext, ");
			}
			qry.append("observaciones, principal)  VALUES ('" + today + "', '" + today + "'," + 1 + ","
					+ 1 + "," + 1 + "," + t.getTipoTelefono() + ", " + t.getCompania() + "," + t.getLada()
					+ t.getNumero() + ", ");
			if(t.getExtension()!=null && t.getExtension().length()>0){ /*Se valida la extensión*/
				qry.append(t.getExtension()+", ");
			}
			qry.append(t.getEstatus() + ",'" + t.getObservacion() + "',"+ t.isPrincipal() + ");");
			LOG.info("Query::" + qry.toString());
			int result = this.namedJdbcTemplate.update(qry.toString(), namedParameters);
			if (result == 1) {
				error.setClave("0");
				error.setMessage(Constantes.EXITO);
			} else {
				error.setClave("1");
				error.setMessage(HardCode.ERRROR_TELEFONO);
			}
		} catch (DataAccessException e) {
			LOG.error("Error al insertar telefono seleccionado", e);
			error.setClave("1");
			error.setMessage(HardCode.ERRROR_TELEFONO);
		}
		respuesta.setError(error);
		return respuesta;
	}

	/**
	 * 
	 * Funcion consultar telefono
	 * 
	 * @return Respuesta
	 * @exception ErrorDTO
	 * 
	 */
	@Override
	public Respuesta consultaTelefonoDetalle(BeanTelefonos beanTel) throws BusinessException {
		String schema = Urls.getSchemaPersonas();
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		respuesta = new Respuesta();
		error = new ErrorDTO();
		try {
			Long id = Long.parseLong(beanTel.getIdTelSeleccionado());
			namedParameters.addValue("id", id);
			StringBuilder query = new StringBuilder();
			query.append("SELECT  \"idTipoTel\" as tipoTelefono, " + "\"idCompaniaTel\" as compania, telefono as numero, ext AS extension, estatus, id as idTelSeleccionado, \"fechaCreacion\", " +" \"fechaModificacion\", observaciones as observacion, principal " + 
					"  FROM "+schema+tablaTelefono+"");
			if (beanTel.getIdTelSeleccionado().equals(Constantes.REGISTRODEFAULT)) {
				query.append(" WHERE \"principal\" = true");
			} else {
				query.append(" WHERE id = :id;");
			}
			LOG.info("Query ConsultaTelefonoDetalle:::" + query.toString());
			respuesta.setListaTelefonos(this.namedJdbcTemplate.query(query.toString(), namedParameters, new BeanPropertyRowMapper<BeanTelefonos>(BeanTelefonos.class)));
			if (respuesta.getListaTelefonos().isEmpty()) {
				error.setClave("1");
				error.setMessage("");
			} else {
				error.setClave("0");
				error.setMessage(Constantes.EXITO);
			}
		} catch (DataAccessException e) {
			LOG.error("Error al consultar telefono seleccionado", e);
			error.setClave("1");
			error.setMessage("");
		}
		respuesta.setError(error);
		return respuesta;
	}

	/**
	 * 
	 * Funcion para consultar informacion de directorio telefonico
	 * 
	 * @return Respuesta
	 * @exception ErrorDTO
	 * 
	 */

	@Override
	public Respuesta consultaDirectorioTelefono() throws BusinessException {
		String schema = Urls.getSchemaPersonas();
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		StringBuilder query = new StringBuilder();
		respuesta = new Respuesta();
		error = new ErrorDTO();
		try {
			query.append("SELECT " 
					+ "\"idTipoTel\" as tipoTelefono, \"idCompaniaTel\" as compania, telefono as numero, ext as extension, id as idTelSeleccionado, observaciones as observacion, principal as principal FROM "+schema+tablaTelefono+"  where estatus = 1 ");
			LOG.info("::CONSULTA DIRECTORIO::"+query);
			respuesta.setListaTelefonos(this.namedJdbcTemplate.query(query.toString(), namedParameters,	new BeanPropertyRowMapper<BeanTelefonos>(BeanTelefonos.class)));
			if (respuesta.getListaTelefonos().isEmpty()) {
				error.setClave("1");
				error.setMessage(HardCode.ERROR_CONSULTA_TABLA_TELEFONOS);
			} else {
				error.setClave("0");
				error.setMessage(Constantes.EXITO);
			}

		} catch (DataAccessException e) {
			LOG.error("Error al consultar Directorio de telefonos", e);
			error.setClave("1");
			error.setMessage(HardCode.ERROR);
		}
		respuesta.setError(error);
		return respuesta;
	}

	/**
	 * 
	 * Funcion para modificar telefono
	 * 
	 * @return Respuesta
	 * @exception ErrorDTO
	 * 
	 */
	@Override
	public Respuesta actualizarTelefono(BeanTelefonos t) throws BusinessException {
		String schema = Urls.getSchemaPersonas();
		respuesta = new Respuesta();
		error = new ErrorDTO();
		BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(t);
		int cantidadPrincipal = 1;
		int result = 0;
		if (!t.isPrincipal()) {
			cantidadPrincipal = validaPrincipalExista(t.getIdTelSeleccionado());
		}
		try {
			StringBuilder qry = new StringBuilder();
			qry.append("UPDATE  " + schema + tablaTelefono);
			qry.append(" SET  \"idPersona\" = 1 ,  \"idTipoTel\" = " + t.getTipoTelefono() + ", \"idCompaniaTel\" = "
					+ t.getCompania() + ", " + "\"telefono\" = '" + t.getLada() + t.getNumero() + "',");
			if (t.getExtension() != null && !t.getExtension().equals(""))
				qry.append(" \"ext\" = " + t.getExtension() + ",");
			qry.append(" \"observaciones\" = '" + t.getObservacion() + "', \"principal\" = " + t.isPrincipal() + "");
			qry.append(" WHERE id = " + t.getIdTelSeleccionado() + "");
			LOG.info("query::" + qry.toString());
			if (cantidadPrincipal != 0) {
				result = this.namedJdbcTemplate.update(qry.toString(), namedParameters);
			}
			if (result == 1) {
				if (t.isPrincipal())
					changeprincipal(t.getIdTelSeleccionado());
				error.setClave("0");
				error.setMessage(Constantes.EXITO);
			} else {
				error.setClave("1");
				if(cantidadPrincipal==0){
					error.setMessage(HardCode.ERROR_TELEFONO_KEY);
				}else{
					error.setMessage(HardCode.ERRROR_TELEFONO);
				}
			}
		} catch (DataAccessException e) {
			LOG.error("Error actualizar telefono.");
			error.setClave("1");
			error.setMessage(HardCode.ERRROR_TELEFONO);
		}
		respuesta.setError(error);
		return respuesta;
	}

	/**
	 * 
	 * Funcion para eliminar un telefono del directorio telefonico
	 * 
	 * @return Respuesta
	 * @exception ErrorDTO
	 * 
	 */
	@Override
	public Respuesta eliminarTelefono(BeanTelefonos t) throws BusinessException {
		String schema = Urls.getSchemaPersonas();
		LOG.info("::DELETE-TELEFONO::");
		respuesta = new Respuesta();
		error = new ErrorDTO();
		int cantidadPrincipal = 1;
		int result = 0;
		try {
			cantidadPrincipal = validaPrincipalExista(t.getIdTelSeleccionado());
			StringBuilder qry = new StringBuilder();
			qry.append("UPDATE " + schema + tablaTelefono);
			qry.append(" SET estatus= '" + t.getEstatus() + "'");
			qry.append(" WHERE id='" + t.getIdTelSeleccionado() + "'");
			LOG.info("consulta" + qry);
			BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(t);
			if (cantidadPrincipal == 1)
				result = namedJdbcTemplate.update(qry.toString(), namedParameters);
			if (result == 1) {
				if (t.isPrincipal() && cantidadPrincipal == 1)
					changeprincipal(t.getIdTelSeleccionado());
				error.setClave("0");
				error.setMessage(Constantes.EXITO);
			} else {
				error.setClave("1");
				if(cantidadPrincipal!=1)
					error.setMessage(HardCode.ERROR_KEY);
				else
					error.setMessage(HardCode.ERRROR_TELEFONO);
			}

		} catch (DataAccessException e) {
			LOG.error("Error al eliminar telefono.");
			error.setClave("1");
			error.setMessage(HardCode.ERRROR_TELEFONO);
		}
		respuesta.setError(error);
		return respuesta;
	}

	/**
	 * 
	 * Valida que exista otra principal antes de eliminarla
	 * 
	 * @return Entero
	 * 
	 */
	public int validaPrincipalExista(String id) { /** */
		String schema = Urls.getSchemaPersonas();
		StringBuilder qry = new StringBuilder();
		BeanPropertySqlParameterSource namedParameters = null;
		qry.append("SELECT count(*) FROM " + schema + tablaTelefono);
		qry.append(" where principal = true ");
		qry.append(" and id != " + id + "");
		LOG.info("Query changePrincipal " + qry.toString());
		return this.namedJdbcTemplate.queryForObject(qry.toString(), namedParameters, Integer.class);
	}

	/**
	 * 
	 * Funcion cambiar registro principal a registro normal
	 * 
	 * @return Entero
	 * 
	 */
	public void changeprincipal(String id) {
		String schema = Urls.getSchemaPersonas();
		StringBuilder qry = new StringBuilder();
		BeanPropertySqlParameterSource namedParameters = null;
		qry.append("UPDATE  " + schema + tablaTelefono);
		qry.append("SET  principal = false ");
		qry.append(" WHERE id != " + id + "");
		this.namedJdbcTemplate.update(qry.toString(), namedParameters);
	}

}