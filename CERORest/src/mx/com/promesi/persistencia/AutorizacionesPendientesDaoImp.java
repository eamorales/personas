/**
 * 
 */
package mx.com.promesi.persistencia;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.service.Urls;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mx.Res.Respuesta;
import com.mx.beans.BeanAutorizacionesPendientes;
import com.mx.beans.ErrorDTO;
import com.mx.constantes.Constantes;

/**
 * @author G
 *
 */
@Repository
public class AutorizacionesPendientesDaoImp implements AutorizacionesPendientesDao{

	private NamedParameterJdbcTemplate namedJdbcTemplate;
	private static final Logger LOG = Logger.getLogger(AutorizacionesPendientesDaoImp.class); // Implementación del LOG
	Date date = Calendar.getInstance().getTime();
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	String today = formatter.format(date);

	int updateAutoriza = 0;

	/**
	 * @param dataSource
	 */
	@Resource(name="DataSource")
	public void setDataSource(DataSource dataSource){
		this.namedJdbcTemplate=new NamedParameterJdbcTemplate(dataSource);
	}

	/* (non-Javadoc)
	 * @see mx.com.promesi.persistencia.AutorizacionesPendientesDao#consultaAutorizacionesPendientes(com.mx.beans.BeanAutorizacionesPendientes)
	 */
	@Override
	public Respuesta consultaAutorizacionesPendientes(BeanAutorizacionesPendientes bAutPen) throws BusinessException {

		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		Respuesta autPeRes = new Respuesta();
		ErrorDTO autPeError = new ErrorDTO();
		String schema = Urls.getSchemaAutorizaciones();
		try {
			namedParameters.addValue("dato_gen", bAutPen.getDato_gen());
			namedParameters.addValue("estatus", "-1");
			String estatus = "-1";
			StringBuilder queryAutPe = new StringBuilder();

			queryAutPe.append("SELECT a.id, a.id_regla_negocio, a.estatus, a.dato_gen, a.usuario_aut, a.usuario_id_aut, a.expirado, a.usuario_creacion, a.fecha_creacion, a.usuario_modificacion, a.fecha_modificacion, b.descripcion");
			queryAutPe.append(" FROM "+schema+"autautorizacionespendientes a, "+schema+"autreglasnegocio b");
			queryAutPe.append(" WHERE a.dato_gen = '" + bAutPen.getDato_gen() + "'");
			queryAutPe.append(" AND a.estatus = " + estatus);
			queryAutPe.append(" AND a.id_regla_negocio = b.id");
			queryAutPe.append(" AND b.estatus = 1");

			LOG.info("Query consultaAutorizacionesPendientes ::: "+queryAutPe.toString());
			autPeRes.setListaAutorizacionesDao(this.namedJdbcTemplate.query(queryAutPe.toString(), 	namedParameters, new BeanPropertyRowMapper<BeanAutorizacionesPendientes>(BeanAutorizacionesPendientes.class)));

			if(!autPeRes.getListaAutorizacionesDao().isEmpty()){
				autPeError.setClave("0");
				autPeError.setMessage(Constantes.EXITO);
			}else{
				autPeError.setClave("1");
				autPeError.setMessage(Constantes.ERROR_CONSULTA_TABLA_AUTORIZACIONESPENDIENTES);
			}
		} catch (DataAccessException e) {
			LOG.error("Error al consultar autorizaciones pendientes seleccionado", e);
			autPeError.setClave("1");
			autPeError.setMessage(Constantes.ERROR_CONSULTA_TABLA_AUTORIZACIONESPENDIENTES);
		}
		finally {
			autPeRes.setError(autPeError);
		}
		return autPeRes;
	}


	/* (non-Javadoc)
	 * @see mx.com.promesi.persistencia.AutorizacionesPendientesDao#actualizaAutorizacionesPendientes(com.mx.beans.BeanAutorizacionesPendientes)
	 */
	@Override
	public Respuesta actualizaAutorizacionesPendientes(List<BeanAutorizacionesPendientes> listabAutPen) throws BusinessException {
		String schema = Urls.getSchemaAutorizaciones();
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		Respuesta autPeRes = new Respuesta();
		ErrorDTO autPeError = new ErrorDTO();

		try {

			for(int i = 0; i < listabAutPen.size(); i++) {
				StringBuilder qryAct = new StringBuilder();

				if(listabAutPen.get(i).getEstatusVista()) {
					qryAct.append("UPDATE  "+schema+"autautorizacionespendientes");

					qryAct.append(" SET  estatus = 0 ,  ");
					qryAct.append(" fecha_modificacion = '"+ today + "', ");
					qryAct.append(" usuario_aut = '"+ listabAutPen.get(i).getUsuario_aut() +"', ");
					qryAct.append(" usuario_id_aut = "+ listabAutPen.get(i).getUsuario_id_aut() +", ");
					qryAct.append(" usuario_modificacion = '"+ listabAutPen.get(i).getUsuario_modificacion() +"' ");
					qryAct.append(" WHERE id = " + listabAutPen.get(i).getId());
					LOG.info("query::"+qryAct.toString());

					int resultUpdate = this.namedJdbcTemplate.update(qryAct.toString(), namedParameters);
					updateAutoriza = updateAutoriza + resultUpdate;
				}
				else
				{
					LOG.info("Registro no actualizado valor false: " + listabAutPen.get(i).getDato_gen());
				}
			}
			if(updateAutoriza > 0){
				autPeError.setClave("0");
				autPeError.setMessage(Constantes.EXITO);
			}else{
				autPeError.setClave("1");
				autPeError.setMessage(Constantes.ERROR_CONSULTA_TABLA_AUTORIZACIONESPENDIENTES);
			}
		} catch (DataAccessException e) {
			LOG.error("Error actualizar Autorizaciones.");
			autPeError.setClave("1");
			autPeError.setMessage(Constantes.ERROR_CONSULTA_TABLA_AUTORIZACIONESPENDIENTES);			
		}
		autPeRes.setError(autPeError);

		return autPeRes;
	}
}