/**
 * 
 */
package mx.com.promesi.persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.object.SqlUpdate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;

import com.mx.beans.BeanCatalogosDatos;
import com.mx.beans.BeanDatosAdicionales;
import com.mx.beans.BeanDatosGenerales;
import com.mx.beans.BeanInformacion;
import com.mx.beans.Busqueda.BeanCliente;
import com.mx.beans.Busqueda.BeanClienteEntrada;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.service.Urls;


/**
 * @author EAMA
 *
 */
@Repository
public class ClienteDaoImpl implements ClienteDao {

	private NamedParameterJdbcTemplate namedJdbcTemplate;
	private DataSource ds;

	@Resource(name = "DataSource")
	public void setDataSource(DataSource dataSource) {
		ds = dataSource;
		this.namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	static String schema;

	final static Logger logger = Logger.getLogger(ClienteDaoImpl.class);

	private class ClienteMapper implements RowMapper<BeanCatalogosDatos> {

		@Override
		public BeanCatalogosDatos mapRow(ResultSet rs, int rowNum) throws SQLException {
			BeanCliente beanCliente = new BeanCliente();
			BeanCatalogosDatos beanCatalogosDatos = new BeanCatalogosDatos();
			BeanInformacion beanInformacion = new BeanInformacion();
			BeanDatosGenerales beandatosGenerales = new BeanDatosGenerales();
			BeanDatosAdicionales beanDatosAdicionales = new BeanDatosAdicionales();
			beanInformacion.setNoCliente((long) rs.getObject("id"));

			beanInformacion.setNombre(rs.getString("nombre"));

			beandatosGenerales.setPrimerNombre(rs.getString("nombre"));
			beandatosGenerales.setEstadoCivil(rs.getString("idEdoCivil"));

			beandatosGenerales.setSegundoNombre(rs.getString("segundoNombre"));
			logger.debug("Informacion del rs.getString(segundoNombre): " + rs.getString("segundoNombre"));
			beandatosGenerales.setPrimerApellido(rs.getString("apaterno"));
			beandatosGenerales.setSegundoApellido(rs.getString("amaterno"));
			beandatosGenerales.setRfc(rs.getString("rfc"));
			
			beanInformacion.setTipocliente(rs.getString("tipo_cliente"));

			beanInformacion.setDireccion(rs.getString("direccion"));
			
			beanInformacion.setSucursalOrigen(rs.getString("idSucursal"));

			beanInformacion.setPersonalidad(rs.getString("idTipoPersona"));
			beandatosGenerales.setPersonalidad(rs.getString("tipoPersona"));
			//TODO tipo persona esta definido en la tabla con un int y en este caso esta como un boolean
			//beandatosGenerales.setTipoPersona(tipoPersona);

			beanInformacion.setCelular(rs.getString("telefono"));

			beandatosGenerales.setCorreoElectronico(rs.getString("correo"));
			beanInformacion.setCorreo(rs.getString("correo"));

			// TODO Se modifico bean datos generales por que marcaba error con los tipos definidos para sexo
			beanInformacion.setSexo(rs.getString("sexo"));
			beandatosGenerales.setFechaNacimiento(rs.getString("fechaNac"));
			beanCatalogosDatos.setFechaNacimiento(rs.getDate("fechaNac"));
			int edad = calcularEdad(beandatosGenerales.getFechaNacimiento(), "dd/MMM/yyyy");
			beanInformacion.setEdad(edad);

			beandatosGenerales.setCurp(rs.getString("curp"));
			beandatosGenerales.setOcupacion(rs.getString("idOcupacion"));
			beandatosGenerales.setLugarNacimiento(rs.getString("lugarNacimiento"));
			
			beanCliente.setBeanInformacion(beanInformacion);
			beanCliente.setDatosGenerales(beandatosGenerales);
			beanCatalogosDatos.setVip(rs.getString("vip"));
			beanCatalogosDatos.setGrado(rs.getString("idGradoEstudios"));
			beanCatalogosDatos.setNacionalidad(rs.getString("idNacionalidad"));
			beanCatalogosDatos.setEstado(rs.getString("idEstadoNac"));
			
			beanCatalogosDatos.setBeanCliente(beanCliente);
			logger.debug("Informacion del mapper: " + beanCatalogosDatos.toString());
			return beanCatalogosDatos;
		}

		/**
		 * Calcula la edad en base a la fecha de nacimiento
		 * 
		 * @param fechaNacimiento
		 * @param formato
		 *            formato de fecha ej: yyyy-MM-dd
		 * @return int edad en años
		 */
		private int calcularEdad(String fechaNacimiento, String formato) {
			logger.debug("datos entrada fecha nacimiento" + fechaNacimiento + " formato: " + formato);
			DateTimeFormatter fmt = DateTimeFormatter.ofPattern(formato);
			LocalDate fechaNac = LocalDate.parse(fechaNacimiento, fmt);
			LocalDate ahora = LocalDate.now();
			Period periodo = Period.between(fechaNac, ahora);
			int edad = periodo.getYears();
			logger.debug("edad: " + edad);
			return edad;
		}

	}

	@Override
	public long insertaCliente(BeanCatalogosDatos beanCatalogosDatos) throws BusinessException {
		logger.info("Inserta cliente");
		schema = Urls.getSchemaPersonas();
		SqlUpdate insert = new SqlUpdate(ds, "INSERT INTO personas.\"PePersona\" ( nombre, \"idTipoPersona\", rfc, vip ) VALUES ( ?,?,?,? )");
		insert.declareParameter(new SqlParameter(Types.VARCHAR));	
		insert.declareParameter(new SqlParameter(Types.BIGINT));
		insert.declareParameter(new SqlParameter(Types.VARCHAR));
		insert.declareParameter(new SqlParameter(Types.VARCHAR));
				
		insert.setReturnGeneratedKeys(true);
		// assuming auto-generated col is named 'id'
		insert.setGeneratedKeysColumnNames(new String[] {"id"}); 
		insert.compile();
		GeneratedKeyHolder keyHolder = new GeneratedKeyHolder();
		logger.info(insert);
		
		insert.update(new Object[]{beanCatalogosDatos.getBeanCliente().getDatosGenerales().getPrimerNombre()
				+" "+beanCatalogosDatos.getBeanCliente().getDatosGenerales().getSegundoNombre()
				+" "+beanCatalogosDatos.getBeanCliente().getDatosGenerales().getPrimerApellido()
				+" "+beanCatalogosDatos.getBeanCliente().getDatosGenerales().getSegundoApellido(),
				Long.valueOf(beanCatalogosDatos.getBeanCliente().getBeanInformacion().getPersonalidad()),
				beanCatalogosDatos.getBeanCliente().getDatosGenerales().getRfc(),
				beanCatalogosDatos.getVip()
				
				},keyHolder);
		long  id = keyHolder.getKey().longValue();
		logger.info("id :"+id);
		final HashMap<String, Object> namedParameters = new HashMap<>();
		namedParameters.put("id", id);
		Date fechaEntrada = beanCatalogosDatos.getFechaNacimiento();
		logger.info("fecha de entrada: "+fechaEntrada );
				
		namedParameters.put("fechaNac",fechaEntrada );
		StringBuffer sql = new StringBuffer("");
		sql.append("INSERT INTO personas.\"PeDatosPersonales\" (\"idPersona\" , \"fechaNac\" ) VALUES (:id , :fechaNac )");
		namedJdbcTemplate.update(sql.toString(), namedParameters);
		
		return id;
	}

	@Override
	public BeanCliente eliminaCliente(BeanCliente BeanCliente) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BeanCatalogosDatos consultaClientePorId(long numeroCliente) throws BusinessException {
		logger.info("consultaClientePorId");
		schema = Urls.getSchemaPersonas();
		BeanCatalogosDatos cliente = null;
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("id", numeroCliente);
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT a.id, \"idTipoPersona\" ,\"tipoPersona\",nombre, apaterno, amaterno, \"idSucursal\", \"idTipoCliente\",");
		sql.append("  descripcion tipo_cliente, \"segundoNombre\", direccion, telefono, sexo, \"fechaNac\" , correo ");
		
		sql.append(" FROM personas.\"PePersona\" a LEFT JOIN personas.\"PeTipoCliente\" b on a.\"idTipoCliente\" = b.clave ");
		sql.append(" LEFT JOIN personas.\"PeDatosPersonales\" c on a.id = c.\"idPersona\"");
		sql.append(" LEFT JOIN  personas.\"PeDireccion\" d on a.id = d.\"idPersona\"");
		sql.append(" LEFT JOIN  personas.\"PeTelefono\" e on a.id = e.\"idPersona\" ");
		sql.append(" LEFT JOIN  personas.\"PeDatosAdicionales\" f on a.id = f.\"idPersona\"");
		sql.append(" LEFT JOIN  personas.\"PeTipoPersona\" g on a.\"idTipoPersona\" = g.id");
		sql.append(" where  a.id = :id");
		sql.append(" and (e.\"idTipoTel\" = (select id from personas.\"NcTipoTel\" e where \"tipoTelefono\" like ('celular'))");
		sql.append("or not EXISTS ( select  e.\"idTipoTel\" from personas.\"PeTelefono\" where a.id = e.\"idPersona\"  ) )");
		logger.info(sql);
		List<BeanCatalogosDatos> clientes = namedJdbcTemplate.query(sql.toString(), namedParameters, new ClienteMapper());
		if (clientes.size() > 0) {
			cliente = clientes.get(0);
			cliente.getBeanCliente().getBeanInformacion().setCodigoError("0");
		}
		return cliente;
	}

	

	@Override
	public BeanCliente actualizaCliente(BeanCliente BeanCliente) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public int actualizaCorreoCliente(BeanClienteEntrada beEntrada) throws BusinessException {
		logger.info("actualizaCorreoCliente " + beEntrada.toString());
		schema = Urls.getSchemaPersonas();
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("id", beEntrada.getNoCliente());
		namedParameters.addValue("correo", beEntrada.getCorreo());
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE personas.\"PeDatosAdicionales\" a");
		sql.append("  SET correo = :correo");
		sql.append(" WHERE a.\"idPersona\" = :id");
		logger.info(sql);
		int status = namedJdbcTemplate.update(sql.toString(), namedParameters);
		logger.info("status "+status);
		return status;
	}	
	
}
