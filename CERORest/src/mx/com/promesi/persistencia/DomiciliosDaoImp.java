/**
 * 
 */
package mx.com.promesi.persistencia;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mx.Res.Respuesta;
import com.mx.Utilerias.Utileria;
import com.mx.beans.BeanDireccion;
import com.mx.beans.BeanDomiciliosHistorico;
import com.mx.beans.BeanPeDireUbic;
import com.mx.beans.BeanTipoNumeracion;
import com.mx.beans.BeanTipoUbicacion;
import com.mx.beans.ErrorDTO;
import com.mx.constantes.Constantes;
import com.mx.constantes.HardCode;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.service.Urls;

/**
 * @author G
 *
 */
@Repository
public class DomiciliosDaoImp implements DomiciliosDao {

	private NamedParameterJdbcTemplate namedJdbcTemplate;
	private static final Logger LOG = Logger.getLogger(DomiciliosDaoImp.class); // Implementación del LOG
	
	String today = Utileria.getFechaActual();
	Respuesta respuesta = new Respuesta();
	ErrorDTO error = new ErrorDTO();
	String peDireccion = "\"PeDireccion\"";
	String update = "UPDATE ";

	@Resource(name = "DataSource")
	public void setDataSource(DataSource dataSource) {
		this.namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	/**
	 * 
	 * Regres todas los tipos de ubicaciones para la sección de Domicilio
	 * 
	 */
	@Override
	public Respuesta consultaTipoUbicacionAll() throws BusinessException {
		String schema =Urls.getSchemaPersonas();
		 respuesta = new Respuesta();
		 error = new ErrorDTO();
		List<BeanTipoUbicacion> listaUbicacion = new ArrayList<>();
		StringBuilder query = new StringBuilder();
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();

		try {
			query.append("SELECT \"id\", \"tipoUbicacion\" FROM " + schema + "\"PeTipoUbicacion\" where estatus = 1  ORDER BY \"tipoUbicacion\" ASC");
			LOG.info(">>> consultaTipoUbicacionAll() Query: " + query.toString());
			listaUbicacion = this.namedJdbcTemplate.query(query.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanTipoUbicacion>(BeanTipoUbicacion.class));
			if (listaUbicacion.isEmpty()) {
				error.setMessage(Constantes.COD_STATUS_ERROR);
				error.setClave("1");
			} else {
				error.setClave("0");
				error.setMessage(Constantes.EXITO);
				respuesta.setListatipoUbicaciones(listaUbicacion);
			}
		} catch (DataAccessException e) {
			LOG.error("Error al consultar Catalogo Tipo Ubicacion Todo", e);
			error.setClave("1");
			error.setMessage(Constantes.COD_STATUS_ERROR);
		}
		respuesta.setError(error);
		return respuesta;
	}

	@Override
	public Respuesta consultaTipoNumeracionAll() throws BusinessException {
		StringBuilder query = new StringBuilder();
		 respuesta = new Respuesta();
		 error = new ErrorDTO();
		List<BeanTipoNumeracion> listaNumeracion = new ArrayList<>();
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		String schema =Urls.getSchemaPersonas();
		try {
			query.append(
					"SELECT \"id\", \"tipoNumeracion\" FROM " + schema + "\"PeTipoNumeracion\" where estatus = 1  ORDER BY \"tipoNumeracion\" ASC");
			LOG.info(">>> Query: " + query.toString());
			listaNumeracion = this.namedJdbcTemplate.query(query.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanTipoNumeracion>(BeanTipoNumeracion.class));
			if (listaNumeracion.isEmpty()) {
				error.setMessage(Constantes.COD_STATUS_ERROR);
				error.setClave("1");
			} else {
				error.setClave("0");
				error.setMessage(Constantes.EXITO);
				respuesta.setListaTipoNumeraciones(listaNumeracion);
			}
		} catch (DataAccessException e) {
			LOG.error("Error al consultar Catalogo Tipo Ubicacion Todo", e);
			error.setMessage(Constantes.COD_STATUS_ERROR);
			error.setClave("1");
		}
		respuesta.setError(error);
		return respuesta;
	}

	@Override
	public Respuesta consultaTabla() throws BusinessException {
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		String schema =Urls.getSchemaPersonas();
		 respuesta = new Respuesta();
		 error = new ErrorDTO();
		try {
			StringBuilder query = new StringBuilder();
			query.append(
					"SELECT estatus, id as idDom, direccion, principal, to_char( \"fechaCreacion\" , 'dd/Mon/yyyy') as \"fechaCreacion\" , to_char(\"fechaModificacion\", 'dd/Mon/yyyy') as \"fechaModificacion\"    FROM "+schema+peDireccion+" WHERE estatus = 1 ORDER BY \"fechaCreacion\" DESC limit 5 ");
			LOG.info("Query:::" + query.toString());
			respuesta.setDirecciones(this.namedJdbcTemplate.query(query.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanDomiciliosHistorico>(BeanDomiciliosHistorico.class)));
			if (respuesta.getDirecciones().isEmpty()) {
				error.setClave("1");
				error.setMessage(HardCode.ERROR_CONSULTA_TABLA_DOMICILIOS);

			} else {
				error.setClave("0");
				error.setMessage(Constantes.EXITO);
			}
		} catch (DataAccessException e) {
			LOG.error("Error al consultar Listado de direciones", e);
			error.setClave("1");
			error.setMessage(Constantes.ERROR);
		}
		respuesta.setError(error);
		return respuesta;
	}
     
	
	/**
	 * 
	 * Funcion para obtener el registro de un domicilio
	 * 
	 * */
	@Override
	public Respuesta consultaDomicilioId(BeanDireccion beanDir) throws BusinessException {
		 respuesta = new Respuesta();
		 error = new ErrorDTO();
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		String schema =Urls.getSchemaPersonas();
		try {
			LOG.info(":::::::: ID de Direcccion ::::: )" + beanDir.getId());
			namedParameters.addValue("id",beanDir.getId());
			StringBuilder query = new StringBuilder();
			query.append("SELECT \"idColonia\", estatus, id, direccion, principal, referencia, "
					+ "latitud, longitud, \"fechaCreacion\", \"fechaModificacion\", \"nombreImg\",\"usuarioModificacion\", \"usuarioCreacion\", \"idPersona\", \"rutaAlfresco\" FROM "+schema+peDireccion);
			if ((""+beanDir.getId()).equals(Constantes.REGISTRODEFAULT)) {
				query.append(" where \"principal\" = true");
			} else {
				query.append(" where id = :id");
			}
			LOG.info("Query:::" + query.toString());
			List<BeanDireccion> listaRespuesta = new ArrayList<>();
			listaRespuesta = this.namedJdbcTemplate.query(query.toString(), namedParameters, new BeanPropertyRowMapper<BeanDireccion>(BeanDireccion.class));
		    if(listaRespuesta.isEmpty()){
		    	error.setClave("1");
		    	error.setMessage(Constantes.ERROR);
		    }else{
		    	error.setClave("0");
		    	error.setMessage(Constantes.EXITO);
		    	respuesta.setDireccion(listaRespuesta.get(0));
		    	if(respuesta.getDireccion()!=null) {
		    		LOG.info("::: Exito al Obtener datos de PeDirecUbic ::::::: ID a consultar ::: "+ respuesta.getDireccion().getId() );
		    		respuesta.getDireccion().setPeDireUbic(obtenerDatosDomicilios(respuesta.getDireccion().getId()));
		    	}
		    }
		} catch (DataAccessException e) {
			LOG.error("Error al consultar Listado por ID de Direccion", e);
		}
		respuesta.setError(error);
		return respuesta;
		
	}
	
	/**
	 * 
	 * Funcion para consultar informacion de la tabla de  PeDirecUbic
	 * @return  List<BeanPeDireUbic> listaDeResgistros
	 * 
	 * */
	public List<BeanPeDireUbic> obtenerDatosDomicilios(Long idDireccion){
		StringBuilder query = new StringBuilder();
		String schema =Urls.getSchemaPersonas();
		final  MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("id", idDireccion);
		query.append("SELECT \"idDireccion\", \"idTipoUbicacion\", estatus, id, \"idTipoNumeracion\", \r\n" + 
				"       \"fechaCreacion\", \"fechaModificacion\", detalle, \"usuarioCreacion\"," + 
				"       \"usuarioModificacion\", \"ORDEN\" as orden" + 
				"  FROM "+schema+"\"PeDireUbic\" WHERE \"idDireccion\" = :id ORDER BY orden");
		LOG.info("Query tabla PeDireUbic ::::: "+ query);
		return this.namedJdbcTemplate.query(query.toString(), namedParameters, new BeanPropertyRowMapper<BeanPeDireUbic>(BeanPeDireUbic.class));		
	}
		
		
		/**
		 * Funcion para validar que exista un principal diferente al registro a
		 * editar
		 */
		public int validaPrincipalExista(Long long1) {
			StringBuilder qry = new StringBuilder();
			String schema =Urls.getSchemaPersonas();
			BeanPropertySqlParameterSource namedParameters = null;
			qry.append("SELECT count(*) FROM "+schema+"\"PeDireccion\" ");
			qry.append(" where principal = true ");
			qry.append(" and id != " + long1 + "");
			LOG.info("Query Valida que exista otro principal" + qry.toString());
			return this.namedJdbcTemplate.queryForObject(qry.toString(), namedParameters, Integer.class);
		}

	@Override
	public Respuesta insertaDomicilio(BeanDireccion bDir) throws BusinessException {
		respuesta = new Respuesta();
		error = new ErrorDTO();
		BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(bDir);
		String schema =Urls.getSchemaPersonas();
		try {
			StringBuilder qry = new StringBuilder();
			qry.append("INSERT INTO " + schema
					+ "\"PeDireccion\"(\"idColonia\", \"estatus\", \"direccion\", \"principal\", \"referencia\","
					+ "\"latitud\", \"longitud\", \"fechaCreacion\", \"fechaModificacion\", \"nombreImg\", \"usuarioCreacion\", "
					+ "\"usuarioModificacion\",\"idPersona\", \"rutaAlfresco\")" + " VALUES (" + bDir.getIdColonia()
					+ ", " + bDir.getEstatus() + ", '" + bDir.getDireccion() + "', " + bDir.isPrincipal() + ", '"
					+ bDir.getReferencia() + "', " + "'" + bDir.getLatitud() + "', '" + bDir.getLatitud() + "', '"
					+ today + "', '" + today + "', 1 , '1', " + "	1, " + 1 + ", '0') RETURNING id ;");
			LOG.info("Query::" + qry.toString());
			int result = this.namedJdbcTemplate.queryForObject(qry.toString(), namedParameters, Integer.class);
			if (result > 0) {
				int orden = 1;
				for (BeanPeDireUbic element : bDir.getPeDireUbic()) {
					StringBuilder query = new StringBuilder();
					query.append("INSERT INTO " + schema
							+ "\"PeDireUbic\"(\"idDireccion\", \"idTipoUbicacion\", \"estatus\",\"idTipoNumeracion\", "
							+ "\"fechaCreacion\", \"fechaModificacion\", \"detalle\", \"usuarioCreacion\", "
							+ "\"usuarioModificacion\", \"ORDEN\")" + " VALUES (" + result + ",'" + element.getIdTipoUbicacion() + "'," + element.getEstatus()
							+ ",'" + element.getIdTipoNumeracion() + "','" + today + "', '" + today + "','" + element.getDetalle() + "', 1 , 1, "+orden+");");
					LOG.info("Query" + query.toString() + " " + result);
					this.namedJdbcTemplate.update(query.toString(), namedParameters);
					orden ++;
				}
				if (bDir.isPrincipal()) {
					changeStatusPrincipal(Integer.toString(result));
				}
				error.setClave("0");
				error.setMessage(Constantes.EXITO);
			} else {
				error.setClave("1");
				error.setMessage(HardCode.ERROR_DIRECCION);
			}
		} catch (DataAccessException e) {
			LOG.error("Error al insertar Direccion", e);
			error.setClave("1");
			error.setMessage(HardCode.ERROR_DIRECCION);
		}
		respuesta.setError(error);
		return respuesta;
	}
	@Override
	public Respuesta actualizaDomicilio(BeanDireccion bDir) throws BusinessException {
		String schema =Urls.getSchemaPersonas();
		 respuesta = new Respuesta();
		 error = new ErrorDTO();
		try {
			final BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(bDir);
	        LOG.info("ID ::: " + bDir.getId());
			if (!bDir.isPrincipal()) {
				validaPrincipalExista(bDir.getId());
			}
			StringBuilder qry = new StringBuilder();
			qry.append(update+schema+peDireccion +"   SET \"idColonia\"="+ bDir.getIdColonia()+", \"estatus\"="+bDir.getEstatus()+",  \"direccion\"='"+bDir.getDireccion()+"', \"principal\"="+bDir.isPrincipal()
					+", \"referencia\"='"+bDir.getReferencia()+"' ," + 
					" \"latitud\"="+bDir.getLatitud()+", \"longitud\"="+bDir.getLongitud()+",\"fechaModificacion\"='"+Utileria.getFechaActual()+"', " + 
					"       \"nombreImg\"="+bDir.getNombreImg()+", \"usuarioModificacion\"="+bDir.getUsuarioModificacion()+", " + 
					"       \"idPersona\"="+bDir.getIdPersona()+", \"rutaAlfresco\"="+bDir.getRutaAlfresco()+" " + 
					" WHERE id = "+bDir.getId()+"");
			LOG.info("QUERY ::: "+ qry.toString());
			int result = this.namedJdbcTemplate.update(qry.toString(), namedParameters);
			if (result == 1) {
				for (BeanPeDireUbic bean : bDir.getPeDireUbic()) {
					
					LOG.info("VALOR DEL ID :: " +bean.getId());
					if(actualizarDirecUbic(bean, bDir.getId()) == 1 )
						LOG.info("Se actualizaron correctamente los datos  en PeDirecUbic");
				}
				if (bDir.isPrincipal())
					changeStatusPrincipal("" + bDir.getId());
				error.setClave("0");
				error.setMessage(Constantes.EXITO);
			} else {
				error.setClave("1");
				error.setMessage(HardCode.ERROR_DIRECCION);
			}
		} catch (DataAccessException e) {
			LOG.error("Error actualizar Direccion.");
			error.setClave("1");
			error.setMessage(HardCode.ERROR_DIRECCION);
			throw new BusinessException( e);
		}
		respuesta.setError(error);
		return respuesta;
	}
	public int actualizarDirecUbic (BeanPeDireUbic  beanDUbic, Long idDireccion) throws BusinessException {
		String schema =Urls.getSchemaPersonas();
		StringBuilder qry = new StringBuilder();
		final  BeanPropertySqlParameterSource namedParameters  = new BeanPropertySqlParameterSource(beanDUbic);
		LOG.info("Actualizando Direc Ubic::::" + idDireccion);
		int bandera;
		try {
			qry.append(update+schema+"\"PeDireUbic\"" + 
					"   SET  \"idTipoUbicacion\"="+beanDUbic.getIdTipoUbicacion()+", \"estatus\"="+beanDUbic.getEstatus()+", \"idTipoNumeracion\"="+beanDUbic.getIdTipoNumeracion()+"," + 
					"        \"fechaModificacion\"='"+today+"', \"detalle\"='"+beanDUbic.getDetalle()+"' ," + 
					"       \"usuarioModificacion\"="+beanDUbic.getUsuarioModificacion()+ 
					" WHERE  \"idDireccion\" = "+idDireccion+" AND \"ORDEN\" = "+beanDUbic.getOrden());
			LOG.info("QUERY UBIC ::: " + qry.toString());
			bandera = this.namedJdbcTemplate.update(qry.toString(), namedParameters); 
			
			
		} catch (DataAccessException e) {
			LOG.error("Error actualizar Direccion.");
			throw new BusinessException(e);
		}
		return bandera;
	}


	/**
	 * Metodo para actualizar registro principal a registro normal
	 * 
	 */
	public void changeStatusPrincipal(String id) {
		String schema =Urls.getSchemaPersonas();
		StringBuilder qry = new StringBuilder();
		BeanPropertySqlParameterSource namedParameters = null;
		qry.append(update+schema+peDireccion);
		qry.append("SET  principal = false ");
		qry.append(" WHERE id != " + id + "");
		LOG.info("query para cambiar el estatus de principal" + qry.toString());
		this.namedJdbcTemplate.update(qry.toString(), namedParameters);
	}

	//
	// /*
	// *
	// * Metodo que valida si existe otro registro como principal
	// *
	// */
	public int validaPrincipalExista(String id) {
		String schema =Urls.getSchemaPersonas();

		StringBuilder qry = new StringBuilder();
		BeanPropertySqlParameterSource namedParameters = null;
		qry.append("SELECT count(*) FROM "+schema+peDireccion);
		qry.append(" where principal = true ");
		qry.append(" and id != " + id + "");
		LOG.info("Query   changeStatusPrincipal " + qry.toString());
		return this.namedJdbcTemplate.queryForObject(qry.toString(), namedParameters, Integer.class);
	}

	/**
	 * Funcion para Eliminar un registro de Domicilios
	 */
	@Override
	public Respuesta eliminaDomicilio(BeanDireccion bDir) throws BusinessException {
		LOG.info("Ejecutando metodo de eliminacion");
		 String schema =Urls.getSchemaPersonas();

		respuesta = new Respuesta();
		error = new ErrorDTO();
		int cantidadPrincipal = 1;
		int result = 0;
		try {
			cantidadPrincipal = validaPrincipalExista(bDir.getId());
			StringBuilder qry = new StringBuilder();
			qry.append(update+schema+peDireccion);
			qry.append(" SET estatus= 2 ");
			qry.append("WHERE id='" + bDir.getId() + "'");
			LOG.info("::QUERY BAJA::"+qry);
			BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(bDir);
			if (cantidadPrincipal > 0 && cantidadPrincipal == 1)
				result = this.namedJdbcTemplate.update(qry.toString(), namedParameters);
			if (result > 0) {
				error.setClave("0");
				error.setMessage(Constantes.EXITO);
			} else {
					error.setClave("1");
					if(cantidadPrincipal != 1)
						error.setMessage(HardCode.ERROR_KEY);
					else
						error.setMessage(HardCode.ERROR_DIRECCION);
			}
		} catch (DataAccessException e) {
			LOG.error("Error al borrar Direccion", e);
			error.setClave("1");
			error.setMessage(HardCode.ERROR_DIRECCION);
		}
		respuesta.setError(error);
		return respuesta;
	}

	

	public String getToday() {
		return today;
	}

	public void setToday(String today) {
		this.today = today;
	}

	 
}