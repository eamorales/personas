/**
 * 
 */
package mx.com.promesi.persistencia;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mx.Res.Respuesta;
import com.mx.Utilerias.Utileria;
import com.mx.beans.BeanIdentificacion;
import com.mx.beans.ErrorDTO;
import com.mx.constantes.Constantes;
import com.mx.constantes.HardCode;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.service.Urls;

/**
 * @author Mariel
 *
 */
@Repository
public class IdentificacionesDaoImp implements IdentificacionesDao {

	private NamedParameterJdbcTemplate namedJdbcTemplate;
	private static final Logger LOG = Logger.getLogger(IdentificacionesDaoImp.class); // Implementación	// del	// LOG

	String today = Utileria.getFechaActual();
    String tablaIdentificaciones = "\"PeIdentificacion\" ";
    String tablaCatalogoIdentificaciones ="\"PeTipoIdentificacion\" ";
    Respuesta respuesta = new Respuesta();
    ErrorDTO error = new ErrorDTO();
    
    
    @Resource(name="DataSource")
	public void setDataSource(DataSource dataSource){
		this.namedJdbcTemplate=new NamedParameterJdbcTemplate(dataSource);
	}
    
	@Override
	public Respuesta consultaIdentificacionesDetalle(BeanIdentificacion t) throws BusinessException {
		String schema =Urls.getSchemaPersonas();
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		List<BeanIdentificacion> identificacion =new ArrayList<>();
		error = new ErrorDTO();
		respuesta = new Respuesta();
		try {
			Long id= Long.parseLong(t.getIdIdeSeleccionado());
			namedParameters.addValue("id", id);
			StringBuilder query = new StringBuilder();
			
			query.append("SELECT  \"idTipoIdentificacion\" as tipoIdentificacionSelected, estatus as estatus, id as idIdeSeleccionado, \"fechaCreacion\" as fechaCreacion, \"fechaModificacion\" as fechaModificacion, valor as valor, principal as principal  FROM "+schema+tablaIdentificaciones);
			
			 // Valida si es la identificacion principal o es para una modificación
			if(t.getIdIdeSeleccionado().equals(Constantes.REGISTRODEFAULT))
			{
				query.append(" where principal = true and estatus = 1 ");
			}else{
				query.append(" where id = :id");
			}
			LOG.info("Query consultaIdentificacionesDetalle:::"+query.toString());
			identificacion = this.namedJdbcTemplate.query(query.toString(), 
					namedParameters, new BeanPropertyRowMapper<BeanIdentificacion>(BeanIdentificacion.class));
			if(identificacion.isEmpty()){
				error.setClave("1");
				error.setMessage(HardCode.ERROR);
			}
			else{
				error.setClave("0");
				error.setMessage(Constantes.EXITO);
				respuesta.setIdentificaciones(identificacion.get(0));
			}
				
		} catch (DataAccessException e) {
			LOG.error("Error al consultar la identificación seleccionada", e);
			error.setClave("1");
			error.setMessage(HardCode.ERROR);
		}
		respuesta.setError(error);
		return respuesta;
		
	}

	@Override
	public Respuesta actualizaridentificaciones(BeanIdentificacion t) throws BusinessException {
		String schema =Urls.getSchemaPersonas();
		respuesta = new Respuesta();
		error = new ErrorDTO();
		 BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(t);
		 int cantidadPrincipal=1;
		 int result =0;
		 if(!t.isPrincipal()){
			 cantidadPrincipal= validaPrincipalExista(t.getIdIdeSeleccionado());
		 }
		 LOG.info("::FECHA DE HOY::"+today);
		try {
			 
			StringBuilder qry = new StringBuilder();
			qry.append("UPDATE "+schema+tablaIdentificaciones);
			qry.append(" SET \"idPersona\"=1, \"idTipoIdentificacion\"= "+t.getTipoIdentificacionSelected()+", estatus="+t.getEstatus()+", \"fechaModificacion\"='"+today+"',");
			qry.append(" \"usuarioModificacion\"="+1+",");
			qry.append(" valor= '"+t.getValor()+"', principal = "+t.isPrincipal()+"");
			qry.append(" WHERE id = "+t.getIdIdeSeleccionado()+"");
			
			LOG.info("query:: "+qry.toString());
			if(cantidadPrincipal != 0){
				result = this.namedJdbcTemplate.update(qry.toString(), namedParameters);
			}
			 if(result==1){
					if(t.isPrincipal())
						changeprincipal(t.getIdIdeSeleccionado());
					error.setClave("0");
					error.setMessage(Constantes.EXITO);
				}else{
					error.setClave("1");
					if(cantidadPrincipal==0){
						error.setMessage(HardCode.ERROR_IDENTIFICACION_KEY);
					}else{
						error.setMessage(HardCode.ERRROR_IDENTIFICACION);
					}
				}
		} catch (DataAccessException e) {
			LOG.error("Error actualizar identificacion.");
			error.setClave("1");
			error.setMessage(HardCode.ERRROR_IDENTIFICACION);			
		}
		respuesta.setError(error);
		return respuesta;
	}

	@Override
	public Respuesta eliminarIdentificaciones(BeanIdentificacion t) throws BusinessException {
		String schema =Urls.getSchemaPersonas();
		LOG.info("::DELETE-IDENTIFICACION::");
		 respuesta = new Respuesta();
		 error = new ErrorDTO();
		 int cantidadPrincipal=1;
		 int result =0;		 
		try {
			cantidadPrincipal = validaPrincipalExista(t.getIdIdeSeleccionado());
			StringBuilder qry = new StringBuilder();
			qry.append("UPDATE "+schema+tablaIdentificaciones);
			qry.append("SET estatus= '"+t.getEstatus()+"'");
			qry.append("WHERE id='"+t.getIdIdeSeleccionado()+"'");
			LOG.info("Elimina Identificacion Qry: "+qry);
			BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(t);
			if(cantidadPrincipal ==1)
				result = namedJdbcTemplate.update(qry.toString(), namedParameters);
			if(result==1){
				if(t.isPrincipal() && cantidadPrincipal == 1)
					changeprincipal(t.getIdIdeSeleccionado());
				error.setClave("0");
				error.setMessage(Constantes.EXITO);
			}else{
				error.setClave("1");
				if(cantidadPrincipal!=1)
					error.setMessage(HardCode.ERROR_KEY);
				else
					error.setMessage(HardCode.ERRROR_IDENTIFICACION);
			}
			
		} catch (DataAccessException e) {
			LOG.error("Error al eliminar identificacion.");
			error.setClave("1");
			error.setMessage(HardCode.ERRROR_IDENTIFICACION);
		}
		respuesta.setError(error);
		return respuesta;
	}

	@Override
	public Respuesta insertarIdentificaciones(BeanIdentificacion t) throws BusinessException {
		String schema =Urls.getSchemaPersonas();
		respuesta = new Respuesta();
		error = new ErrorDTO();
		 BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(t);
		 LOG.info("::FECHA DE HOY::"+today);
		 try {
				int crearPrincipal = 0;
				if (t.isPrincipal()){
					changeprincipal(t.getIdIdeSeleccionado());
				}else{
				    crearPrincipal  = validaPrincipalExista("0");
				   if(crearPrincipal == 0)
				    t.setPrincipal(true);
				}
			 
				StringBuilder qry = new StringBuilder();
				qry.append("INSERT INTO "+schema+tablaIdentificaciones+"(\"idPersona\", \"idTipoIdentificacion\", estatus,\"fechaCreacion\", \"fechaModificacion\", \"usuarioCreacion\", \"usuarioModificacion\",");
				qry.append(" valor, principal) ");
				qry.append(" VALUES ("+1+","+t.getTipoIdentificacionSelected()+","+t.getEstatus()+",'"+today+"', '"+today+"',"+1+","+1+", '"+t.getValor()+"',"+t.isPrincipal()+");");
				LOG.info("Qry::"+qry.toString());
				int result = this.namedJdbcTemplate.update(qry.toString(),namedParameters);
				if(result ==1){
					error.setClave("0");
					error.setMessage(Constantes.EXITO);
				}else{
					error.setClave("1");
					error.setMessage(HardCode.ERRROR_TELEFONO);
				}
		 }catch (DataAccessException e) {
				LOG.error("Error al consultar identificacion seleccionado", e);
				error.setClave("1");
				error.setMessage(HardCode.ERRROR_TELEFONO);
		}
		respuesta.setError(error); 	
		return respuesta;
	}
	
	public void changeprincipal(String id){
		String schema =Urls.getSchemaPersonas();
		StringBuilder qry = new StringBuilder();
		 BeanPropertySqlParameterSource namedParameters = null;
		qry.append("UPDATE  "+schema+tablaIdentificaciones+" ");
		qry.append("SET  principal = false ");
		qry.append(" WHERE id != "+id+"");
		LOG.info("::QUERY ::"+qry.toString());
		 this.namedJdbcTemplate.update(qry.toString(),namedParameters);
	}
	@Override
	public Respuesta consultarCatalogoTipo() {
		String schema =Urls.getSchemaPersonas();
		 respuesta = new Respuesta();
		 error = new ErrorDTO();
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		StringBuilder qry = new StringBuilder();
		List<Map<String,Object>> object = new ArrayList<>();
		LinkedHashMap<String,String> comboBox = new LinkedHashMap<>();
		 try {
				qry.append("SELECT id, identificacion  FROM "+schema+tablaCatalogoIdentificaciones+" where estatus = 1  ORDER BY identificacion ASC");
				LOG.info("Query::"+qry.toString());
				object =namedJdbcTemplate.queryForList(qry.toString(),namedParameters);
				if(!object.isEmpty()){
					for (int a = 0 ; a<object.size();a++) {
						String id="";
						String valor="";
						int b = 0;
						for (Map.Entry<String , Object> entry : object.get(a).entrySet()) {
							if(b==0){
								id = entry.getValue()+"";
								b++;
							}else{
								valor =entry.getValue()+"";
								b=0;
							}
						}
						comboBox.put(id,valor);
					}
					respuesta.setCatalogoTipoIdentificacion(comboBox);
					error.setClave("0");
					error.setMessage(Constantes.EXITO);
				}else{
					error.setClave("1");
					error.setMessage(HardCode.ERRROR_IDENTIFICACION);
				}
		 }catch (DataAccessException e) {
			LOG.error("Error al consultar catalogo de identificaciones", e);
			error.setClave("1");
			error.setMessage(HardCode.ERRROR_IDENTIFICACION);
		}
		respuesta.setError(error); 	
		return respuesta;
	}
	@Override
	public Respuesta consultarCatalogoTipoExpresiones() {
		String schema =Urls.getSchemaPersonas();
		 respuesta = new Respuesta();
		 error = new ErrorDTO();
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		StringBuilder qry = new StringBuilder();
		List<Map<String,Object>> object = new ArrayList<>();
		LinkedHashMap<String,String> comboBox = new LinkedHashMap<>();
		 try {
				qry.append("SELECT id, \"expRegular\"  FROM "+schema+tablaCatalogoIdentificaciones+" where estatus = 1  ORDER BY identificacion ASC");
				LOG.info("Query::"+qry.toString());
				object =namedJdbcTemplate.queryForList(qry.toString(),namedParameters);
				if(!object.isEmpty()){
					for (int a = 0 ; a<object.size();a++) {
						String id="";
						String valor="";
						int b = 0;
						for (Map.Entry<String , Object> entry : object.get(a).entrySet()) {
							if(b==0){
								id = entry.getValue()+"";
								b++;
							}else{
								valor =entry.getValue()+"";
								b=0;
							}
						}
						comboBox.put(id,valor);
					}
					respuesta.setCatalogoTipoIdentificacionExpresiones(comboBox);
					error.setClave("0");
					error.setMessage(Constantes.EXITO);
				}else{
					error.setClave("1");
					error.setMessage(HardCode.ERRROR_IDENTIFICACION);
				}
		 }catch (DataAccessException e) {
			LOG.error("Error al consultar catalogo de identificaciones", e);
			error.setClave("1");
			error.setMessage(HardCode.ERRROR_IDENTIFICACION);
		}
		respuesta.setError(error); 	
		return respuesta;
	}
	
	@Override
	public Respuesta consultarTablaIdentificaciones() {
		String schema =Urls.getSchemaPersonas();
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		respuesta = new Respuesta();
		error = new ErrorDTO();
		StringBuilder qry = new StringBuilder();
		 try {
				qry.append("SELECT \"idTipoIdentificacion\"  as tipoIdentificacionSelected, estatus as estatus, id as idide, \"fechaCreacion\" as fechaCreacion, \"fechaModificacion\" as fechaModificacion, valor as valor, principal as principal  FROM "+schema+tablaIdentificaciones+" where estatus != 2");
				LOG.info("Query::"+qry.toString());
				 respuesta.setListaIdentificacion(this.namedJdbcTemplate.query(qry.toString(), namedParameters, new BeanPropertyRowMapper<BeanIdentificacion>(BeanIdentificacion.class)));
				 if(respuesta.getListaIdentificacion().isEmpty()){
					 error.setClave("1");
					 error.setMessage(HardCode.ERROR_IDENTIFICACIONES_TABLA);
				 }else{
					 error.setClave("0");
					 error.setMessage(Constantes.EXITO);
				 }
		 }catch (DataAccessException e) {
			LOG.error(":::Error al realizar la consulta:::", e);
			error.setClave("1");
			error.setMessage(HardCode.ERROR);
		}
		 respuesta.setError(error);
		 return respuesta;
	}
	
	public int validaPrincipalExista(String id){
		String schema =Urls.getSchemaPersonas();
		StringBuilder qry = new StringBuilder();
		 BeanPropertySqlParameterSource namedParameters = null;
		qry.append("SELECT count(*) FROM  "+schema+tablaIdentificaciones);
		qry.append(" where principal = true ");
		qry.append(" and id != "+id+"");
		LOG.info("Query changePrincipal"+qry.toString());
		return  this.namedJdbcTemplate.queryForObject(qry.toString(),namedParameters,Integer.class);
	}
	 

	
}