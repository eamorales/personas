package mx.com.promesi.persistencia;



import com.mx.Res.Respuesta;
import com.mx.beans.BeanExpedientes;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.service.BusquiedaServiceREST;

public interface ExpedientesDao {
	
	public Respuesta consultarCatalogoTipoDocumento () throws BusinessException; // Funcion para consultar los tipos de docuementos existentes
	public Respuesta insertarExpedientes (BeanExpedientes expediente) throws BusinessException; //Funcion para guardar expedientes
	public Respuesta consultarExpedientes (Integer id_persona)throws BusinessException;
	public Respuesta eliminarExpediente (Integer id_expediente);
	public Respuesta actualizarExpediente (BeanExpedientes expediente ) throws BusinessException;
}
