package mx.com.promesi.persistencia;

import org.springframework.beans.BeansException;

import com.mx.Res.Respuesta;
import com.mx.beans.BeanAuditoria;

public interface AuditoriaDao {
	
	public Respuesta insertarAuditoria(BeanAuditoria audit) throws BeansException; 

}
