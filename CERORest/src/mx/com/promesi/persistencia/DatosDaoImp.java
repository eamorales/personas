package mx.com.promesi.persistencia;

import java.util.LinkedHashMap;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mx.Res.Respuesta;
import com.mx.beans.BeanCatalogosDatos;
import com.mx.beans.ErrorDTO;
import com.mx.constantes.Constantes;
import com.mx.constantes.HardCode;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.service.Urls;
/**
 *@author Gustavo
 * 
 */
@Repository
public class DatosDaoImp implements DatosDao{
	
	private NamedParameterJdbcTemplate namedJdbcTemplate;
	private static final Logger log = Logger.getLogger(DatosDaoImp.class);
	ErrorDTO error = new ErrorDTO();
	Respuesta respuesta = new Respuesta();
	
	/*
	 * Se declara la conexion a DB
	 * 
	 * 
	 */
	@Resource(name = "DataSource")
	public void setDataSource(DataSource dataSource) {
		this.namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	/**
	 * 
	 *Funciones  para consultar los tipos de catalogos existentes para datos 
	 *@return Respuesta
	 * 
	 */
	
	@Override
	public Respuesta obtenerCatalogoSector() throws BusinessException {
		try {
			final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			List<BeanCatalogosDatos> listObject;
			Map<String, String> listCatalogo = new LinkedHashMap<>();
			StringBuilder qry = new StringBuilder();
			qry.append("SELECT clave, id, \"usuarioCreacion\", \"usuarioModificacion\", descripcion, \"fechaCreacion\", \"fechaModificacion\"  FROM " + Urls.getSchemaPersonas()
					+ "\"PeSector\";");
			listObject = this.namedJdbcTemplate.query(qry.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanCatalogosDatos>(BeanCatalogosDatos.class));
			if (listObject != null && !listObject.isEmpty()) {
				for (BeanCatalogosDatos object : listObject) {
					listCatalogo.put(object.getId(), object.getDescripcion());
				}
				respuesta.setListObjects(listCatalogo);
				error.setClave(Constantes.EXITO_EXECUTE);
				error.setMessage(HardCode.SUCCESS);
			} else {
				respuesta.setListObjects(new LinkedHashMap<>());
				error.setClave(Constantes.ERROR_EXECUTE);
				error.setMessage(HardCode.ERROR);

			}

		} catch (DataAccessException e) {
			log.info(error.getMessage());
		}
		respuesta.setError(error);
		return respuesta;
	}

	@Override
	public Respuesta obtenerCatalogoSituacion() throws BusinessException {
		try {
			final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			List<BeanCatalogosDatos> listObject;
			Map<String, String> listCatalogo = new LinkedHashMap<>();
			StringBuilder qry = new StringBuilder();
			qry.append("SELECT clave, id, \"usuarioCreacion\", \"usuarioModificacion\", descripcion, \"fechaCreacion\", \"fechaModificacion\"  FROM "+Urls.getSchemaPersonas()+"\"PeSituacion\";"+ "");
			log.info(qry);
			listObject = this.namedJdbcTemplate.query(qry.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanCatalogosDatos>(BeanCatalogosDatos.class));
			if (listObject != null && !listObject.isEmpty()) {
				for (BeanCatalogosDatos object : listObject) {
					listCatalogo.put(object.getId(), object.getDescripcion());
				}
				respuesta.setListObjects(listCatalogo);
				error.setClave(Constantes.EXITO_EXECUTE);
				error.setMessage(HardCode.SUCCESS);
			} else {
				respuesta.setListObjects(new LinkedHashMap<>());
				error.setClave(Constantes.ERROR_EXECUTE);
				error.setMessage(HardCode.ERROR);

			}

		} catch (DataAccessException e) {
			log.info(error.getMessage());
		}
		respuesta.setError(error);
		return respuesta;
	}

	@Override
	public Respuesta obtenerCatalogoCorreo() throws BusinessException {
		try {
			final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			List<BeanCatalogosDatos> listObject;
			Map<String, String> listCatalogo = new LinkedHashMap<>();
			StringBuilder qry = new StringBuilder();
			qry.append("  SELECT clave, estatus, id, \"usuarioCreacion\", usuariomodificacion, \"tipoCorreo\","
					+ "       \"fechaCreacion\", \"fechaModificacion\""
					+ "  FROM "+Urls.getSchemaPersonas()+"\"PeTipoCorreo\" WHERE estatus = 1;");
			log.info(qry);
			listObject = this.namedJdbcTemplate.query(qry.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanCatalogosDatos>(BeanCatalogosDatos.class));
			if (listObject != null && !listObject.isEmpty()) {
				for (BeanCatalogosDatos object : listObject) {
					listCatalogo.put(object.getId(), object.getTipoCorreo());
				}
				respuesta.setListObjects(listCatalogo);
				error.setClave(Constantes.EXITO_EXECUTE);
				error.setMessage(HardCode.SUCCESS);
			} else {
				respuesta.setListObjects(new LinkedHashMap<>());
				error.setClave(Constantes.ERROR_EXECUTE);
				error.setMessage(HardCode.ERROR);

			}

		} catch (DataAccessException e) {
			log.info(error.getMessage());
		}
		respuesta.setError(error);
		return respuesta;
	}


	@Override
	public Respuesta obtenerCatalogoEstablecimiento() throws BusinessException {
		try {
			final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			List<BeanCatalogosDatos> listObject;
			Map<String, String> listCatalogo = new LinkedHashMap<>();
			StringBuilder qry = new StringBuilder();
			qry.append("SELECT clave, id, \"usuarioCreacion\", \"usuarioModificacion\", descripcion, " + 
					"       \"fechaCreacion\", \"fechaModificacion\"" + 
					"  FROM "+Urls.getSchemaPersonas()+"\"PeEstablecimiento\"");
			log.info(qry);
			listObject = this.namedJdbcTemplate.query(qry.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanCatalogosDatos>(BeanCatalogosDatos.class));
			if (listObject != null && !listObject.isEmpty()) {
				for (BeanCatalogosDatos object : listObject) {
					listCatalogo.put(object.getId(), object.getDescripcion());
				}
				respuesta.setListObjects(listCatalogo);
				error.setClave(Constantes.EXITO_EXECUTE);
				error.setMessage(HardCode.SUCCESS);
			} else {
				respuesta.setListObjects(new LinkedHashMap<>());
				error.setClave(Constantes.ERROR_EXECUTE);
				error.setMessage(HardCode.ERROR);
			}

		} catch (DataAccessException e) {
			log.info(error.getMessage());
		}
		respuesta.setError(error);
		return respuesta;
	
	}


	@Override
	public Respuesta obtenerCatalogoFacultad() throws BusinessException {
		try {
			final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			List<BeanCatalogosDatos> listObject;
			Map<String, String> listCatalogo = new LinkedHashMap<>();
			StringBuilder qry = new StringBuilder();
			qry.append("	SELECT id, \"usuarioCreacion\", \"usuarioModificacion\", clave, descripcion, " + 
					"    estatus, \"fechaCreacion\", \"fechaModificacion\"" + 
					"FROM "+Urls.getSchemaPersonas()+"\"PeFacultad\" WHERE estatus  = 1;");
			log.info(qry);
			listObject = this.namedJdbcTemplate.query(qry.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanCatalogosDatos>(BeanCatalogosDatos.class));
			if (listObject != null && !listObject.isEmpty()) {
				for (BeanCatalogosDatos object : listObject) {
					listCatalogo.put(object.getId(), object.getDescripcion());
				}
				respuesta.setListObjects(listCatalogo);
				error.setClave(Constantes.EXITO_EXECUTE);
				error.setMessage(HardCode.SUCCESS);
			} else {
				respuesta.setListObjects(new LinkedHashMap<>());
				error.setClave(Constantes.ERROR_EXECUTE);
				error.setMessage(HardCode.ERROR);
			}

		} catch (DataAccessException e) {
			log.info(error.getMessage());
		}
		respuesta.setError(error);
		return respuesta;
	}
	


	@Override
	public Respuesta obtenerCatalogoTipoSociedad() throws BusinessException {
		try {
			final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			List<BeanCatalogosDatos> listObject;
			Map<String, String> listCatalogo = new LinkedHashMap<>();
			StringBuilder qry = new StringBuilder();
			qry.append("	SELECT clave, id, \"usuarioCreacion\", \"usuarioModificacion\", descripcion, " + 
					"    \"fechaCreacion\", \"fechaModificacion\"" + 
					"FROM "+Urls.getSchemaPersonas()+"\"PeTipoSociedad\";");
			log.info(qry);
			listObject = this.namedJdbcTemplate.query(qry.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanCatalogosDatos>(BeanCatalogosDatos.class));
			if (listObject != null && !listObject.isEmpty()) {
				for (BeanCatalogosDatos object : listObject) {
					listCatalogo.put(object.getId(), object.getDescripcion());
				}
				respuesta.setListObjects(listCatalogo);
				error.setClave(Constantes.EXITO_EXECUTE);
				error.setMessage(HardCode.SUCCESS);
			} else {
				respuesta.setListObjects(new LinkedHashMap<>());
				error.setClave(Constantes.ERROR_EXECUTE);
				error.setMessage(HardCode.ERROR);
			}

		} catch (DataAccessException e) {
			log.info(error.getMessage());
		}
		respuesta.setError(error);
		return respuesta;
	}

	@Override
	public Respuesta obtenerCatalogoPuesto() throws BusinessException {
		try {
			final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			List<BeanCatalogosDatos> listObject;
			Map<String, String> listCatalogo = new LinkedHashMap<>();
			StringBuilder qry = new StringBuilder();
			qry.append("  SELECT id, \"usuarioCreacion\", \"usuarioModificacion\", clave, descripcion, " + 
					"       estatus, \"fechaCreacion\", \"fechaModificacion\"" + 
					"  FROM "+Urls.getSchemaPersonas()+"\"PePuestos\" WHERE estatus = 1 ");
			log.info(qry);
			listObject = this.namedJdbcTemplate.query(qry.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanCatalogosDatos>(BeanCatalogosDatos.class));
			if (listObject != null && !listObject.isEmpty()) {
				for (BeanCatalogosDatos object : listObject) {
					listCatalogo.put(object.getId(), object.getDescripcion());
				}
				respuesta.setListObjects(listCatalogo);
				error.setClave(Constantes.EXITO_EXECUTE);
				error.setMessage(HardCode.SUCCESS);
			} else {
				respuesta.setListObjects(new LinkedHashMap<>());
				error.setClave(Constantes.ERROR_EXECUTE);
				error.setMessage(HardCode.ERROR);
			}

		} catch (DataAccessException e) {
			log.info(error.getMessage());
		}
		respuesta.setError(error);
		return respuesta;
	}

	@Override
	public Respuesta obtenerCatalogoGiro() throws BusinessException {
		try {
			final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			List<BeanCatalogosDatos> listObject;
			Map<String, String> listCatalogo = new LinkedHashMap<>();
			StringBuilder qry = new StringBuilder();
			qry.append(" SELECT clave, id, \"usuarioCreacion\", \"usuarioModificacion\", actividad, " + 
					"       descripcion, \"fechaCreacion\", \"fechaModificacion\"" + 
					"  FROM "+Urls.getSchemaPersonas()+"\"PeGiro\";" + 
					"");
			listObject = this.namedJdbcTemplate.query(qry.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanCatalogosDatos>(BeanCatalogosDatos.class));
			if (listObject != null && !listObject.isEmpty()) {
				for (BeanCatalogosDatos object : listObject) {
					listCatalogo.put(object.getId(), object.getDescripcion());
				}
				respuesta.setListObjects(listCatalogo);
				error.setClave(Constantes.EXITO_EXECUTE);
				error.setMessage(HardCode.SUCCESS);
			} else {
				respuesta.setListObjects(new LinkedHashMap<>());
				error.setClave(Constantes.ERROR_EXECUTE);
				error.setMessage(HardCode.ERROR);
			}

		} catch (DataAccessException e) {
			log.info(error.getMessage());
		}
		respuesta.setError(error);
		return respuesta;
	}

	@Override
	public Respuesta obtenerCatalogosActividad() throws BusinessException {
		try {
			final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			List<BeanCatalogosDatos> listObject;
			Map<String, String> listCatalogo = new LinkedHashMap<>();
			StringBuilder qry = new StringBuilder();
			qry.append("SELECT id,  clave, actividad, descripcion, riesgo   FROM "+Urls.getSchemaPersonas()+"\"peActividades\"");
			listObject = this.namedJdbcTemplate.query(qry.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanCatalogosDatos>(BeanCatalogosDatos.class));
			if (listObject != null && !listObject.isEmpty()) {
				for (BeanCatalogosDatos object : listObject) {
					listCatalogo.put(object.getId(), object.getDescripcion());
				}
				respuesta.setListObjects(listCatalogo);
				error.setClave(Constantes.EXITO_EXECUTE);
				error.setMessage(HardCode.SUCCESS);
			} else {
				respuesta.setListObjects(new LinkedHashMap<>());
				error.setClave(Constantes.ERROR_EXECUTE);
				error.setMessage(HardCode.ERROR);
			}

		} catch (DataAccessException e) {
			log.info(error.getMessage());
		}
		respuesta.setError(error);
		return respuesta;
	}

	

	@Override
	public Respuesta obtenerCatalogoTipoDocumentoPers() throws BusinessException {
		try {
			final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			List<BeanCatalogosDatos> listObject;
			Map<String, String> listCatalogo = new LinkedHashMap<>();
			StringBuilder qry = new StringBuilder();
			qry.append("SELECT id, clave, descripcion, estatus FROM "+Urls.getSchemaPersonas()+"\"PeTipoDocumentoPers\" WHERE estatus = "+Constantes.STATUS_ALTA+";");
			listObject = this.namedJdbcTemplate.query(qry.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanCatalogosDatos>(BeanCatalogosDatos.class));
			if (listObject != null && !listObject.isEmpty()) {
				for (BeanCatalogosDatos object : listObject) {
					listCatalogo.put(object.getId(), object.getDescripcion());
				}
				respuesta.setListObjects(listCatalogo);
				error.setClave(Constantes.EXITO_EXECUTE);
				error.setMessage(HardCode.SUCCESS);
			} else {
				respuesta.setListObjects(new LinkedHashMap<>());
				error.setClave(Constantes.ERROR_EXECUTE);
				error.setMessage(HardCode.ERROR);
			}

		} catch (DataAccessException e) {
			log.info(error.getMessage());
		}
		respuesta.setError(error);
		return respuesta;
	}

	@Override
	public Respuesta obtenerCatalogoDocumentoPers() throws BusinessException {
		try {
			final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			List<BeanCatalogosDatos> listObject;
			Map<String, String> listCatalogo = new LinkedHashMap<>();
			StringBuilder qry = new StringBuilder();
			qry.append("SELECT id, clave, descripcion, estatus FROM "+Urls.getSchemaPersonas()+"\"PeDocumentosPers\" WHERE estatus = "+Constantes.STATUS_ALTA+";");
			listObject = this.namedJdbcTemplate.query(qry.toString(), namedParameters,
					new BeanPropertyRowMapper<BeanCatalogosDatos>(BeanCatalogosDatos.class));
			if (listObject != null && !listObject.isEmpty()) {
				for (BeanCatalogosDatos object : listObject) {
					listCatalogo.put(object.getId(), object.getDescripcion());
				}
				respuesta.setListObjects(listCatalogo);
				error.setClave(Constantes.EXITO_EXECUTE);
				error.setMessage(HardCode.SUCCESS);
			} else {
				respuesta.setListObjects(new LinkedHashMap<>());
				error.setClave(Constantes.ERROR_EXECUTE);
				error.setMessage(HardCode.ERROR);
			}

		} catch (DataAccessException e) {
			log.info(error.getMessage());
		}
		respuesta.setError(error);
		return respuesta;
	}
	
	/**
	 * 
	 * Termina funciones de consulta de catalogos
	 * 
	 */
	
	
	
	
	
	
	
	

}
