package mx.com.promesi.persistencia;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mx.Res.Respuesta;
import com.mx.beans.BeanInvolucradosLista;
import com.mx.beans.BeanInvolucradosPrincipal;
import com.mx.beans.ErrorDTO;
import com.mx.constantes.Constantes;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.service.Urls;
@Repository
public class InvolucradosDaoImp implements InvolucradosDao{
	private static final Logger LOG = Logger.getLogger(InvolucradosDaoImp.class);
	private NamedParameterJdbcTemplate namedJdbcTemplate;
	Respuesta respuesta = new Respuesta();
	ErrorDTO error = new ErrorDTO();
	@Resource(name="DataSource")
	public void setDataSource(DataSource dataSource){
		this.namedJdbcTemplate=new NamedParameterJdbcTemplate(dataSource);
	}
	
	
	@Override
	public Respuesta consultaListaInv() throws BusinessException {
		String schema= Urls.getSchemaPersonas();
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		StringBuilder query = new StringBuilder();
		respuesta = new Respuesta();
		error = new ErrorDTO();
		
		BeanInvolucradosPrincipal invPrincipal = new BeanInvolucradosPrincipal();
		List<BeanInvolucradosLista> beanInvolucradosLista = new ArrayList<>();
		
		try {
			
			/* SELECT "PePersona"."id","PePersona"."nombre","PeInvolucrados"."idTipoInvolucrado"  

			FROM personas."PePersona", personas."PeInvolucrados"

			WHERE "idPersona" = 1 AND "idTipoInvolucrado" = 1 AND "idPersona"=1 */
			
			query.append("SELECT \"PePersona\".\"id\" as id,\"PePersona\".\"nombre\" as nombre,\"PeInvolucrados\".\"idTipoInvolucrado\" as idTipoInvolucrado"
					+ " FROM "+schema+"\"PePersona\", "+schema+"\"PeInvolucrados\""
					+ " WHERE \"idPersona\" = 1 AND \"idTipoInvolucrado\" = 1 AND \"idPersona\"=1");
			LOG.info("::QUERY::"+query);
			beanInvolucradosLista = (this.namedJdbcTemplate.query(query.toString(), namedParameters, new BeanPropertyRowMapper<BeanInvolucradosLista>(BeanInvolucradosLista.class)));
			LOG.info("::query::"+query);
			
			
			if(beanInvolucradosLista.isEmpty()){
				error.setClave("1");
				error.setMessage("NO HAY REGISTROS DISPONIBLES");
			}else{
				invPrincipal.setListaInv(beanInvolucradosLista);
				respuesta.setBeanInvolucradosPrincipal(invPrincipal);
				error.setClave("0");
				error.setMessage(Constantes.EXITO);	
			}
		} catch (Exception e) {
			error.setClave("1");
			error.setMessage(Constantes.ERROR);
		}
		
		
		LOG.info(".::ERROR::."+error.getMessage());
		respuesta.setError(error);
		return respuesta;
	}

}
