package mx.com.promesi.persistencia;

import com.mx.Res.Respuesta;
import com.mx.beans.BeanTelefonos;
import mx.com.promesi.exceptions.BusinessException;

/**
 * @author GBG
 *
 */
public interface TelefonosDao {

	public Respuesta insertarTelefono(BeanTelefonos t) throws BusinessException; // Funcion para guardar telefono 

	public Respuesta consultaTelefonoDetalle(BeanTelefonos t) throws BusinessException;//Funcion para consultar detalle del directorio telefonico 

	public Respuesta consultaDirectorioTelefono() throws BusinessException;//Funcion para consultar telefono

	public Respuesta actualizarTelefono(BeanTelefonos t) throws BusinessException;//Funcion para modificar telefono 

	public Respuesta eliminarTelefono(BeanTelefonos t) throws BusinessException;//Funcion para eliminar telefono 

	

}
