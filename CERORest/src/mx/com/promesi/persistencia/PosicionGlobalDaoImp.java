package mx.com.promesi.persistencia;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mx.Res.Respuesta;
import com.mx.Utilerias.Utileria;
import com.mx.beans.BeanPosicionGlobalAhorro;
import com.mx.beans.BeanPosicionGlobalCreditos;
import com.mx.beans.BeanPosicionGlobalInversion;
import com.mx.beans.BeanPosicionGlobalPrincipalBean;
import com.mx.beans.BeanPosicionGlobalSeguros;
import com.mx.beans.ErrorDTO;
import com.mx.constantes.Constantes;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.service.Urls;

@Repository
public class PosicionGlobalDaoImp implements PosicionGlobalDao {
	private static final Logger LOG = Logger.getLogger(PosicionGlobalDaoImp.class); // Implementación del LOG
	private NamedParameterJdbcTemplate namedJdbcTemplate;
	Respuesta respuesta = new Respuesta();
	ErrorDTO error = new ErrorDTO();
	@Resource(name="DataSourceNucleo")
	public void setDataSource(DataSource dataSource){
		this.namedJdbcTemplate=new NamedParameterJdbcTemplate(dataSource);
	}

	 /** Función para consultar Ahorro * */
	@Override
	public Respuesta consultaAhorro() throws BusinessException {
		String schema = Urls.getSchemaNucleoCentral(); 
		
		LOG.info("::DENTRO DE DAOIMPL PG::");
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		StringBuilder query = new StringBuilder();
		respuesta = new Respuesta();
		error = new ErrorDTO();
		BeanPosicionGlobalPrincipalBean principal =new BeanPosicionGlobalPrincipalBean();
		List <BeanPosicionGlobalAhorro> ahorroList= new  ArrayList<>();
		try {
			query.append("SELECT id as idAho, estatusah as estatus, tipocuentaah as tipoCuenta, cuentaah as cuenta, referenciaah as referencia, saldodisponibleah as saldoDisponible, saldototalah as saldoTotal, idpersona FROM "+schema+"ncposicionglobalah");
			ahorroList = (this.namedJdbcTemplate.query(query.toString(), namedParameters, new BeanPropertyRowMapper<BeanPosicionGlobalAhorro>(BeanPosicionGlobalAhorro.class)));
			LOG.info("::query::"+query);
			if(ahorroList.isEmpty()){
				error.setClave("1");
				error.setMessage(Constantes.ERROR);
			}else{
				principal.setListaAhorro(ahorroList);
				respuesta.setPosicionGlobalRespuesta(principal);
				error.setClave("0");
				error.setMessage(Constantes.EXITO);	
			}
		} catch (Exception e) {
			error.setClave("1");
			error.setMessage(Constantes.ERROR);
		}
		respuesta.setError(error);
		LOG.info("::ERROR POS GLO::"+error);
		return respuesta;
	}
	/*** Función para consultar Inversión * */
	@Override
	public Respuesta consultaInversion() throws BusinessException {
		String schema = Urls.getSchemaNucleoCentral(); 
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		StringBuilder query = new StringBuilder();
		respuesta = new Respuesta();
		error = new ErrorDTO();
		BeanPosicionGlobalPrincipalBean principal =new BeanPosicionGlobalPrincipalBean();
		List <BeanPosicionGlobalInversion> listaInversion= new  ArrayList<>();
		try {
			query.append("SELECT id as idInv, cuentain as cuenta, montoin as montoInvertido,to_char( fechain , 'dd/Mon/yyyy') as fechaInversion, modalidadin as modalida, plazoin as plazo,to_char( vencimientoin, 'dd/Mon/yyyy' ) as fechaVencimiento FROM "+schema+"ncposicionglobalin");
			listaInversion = (this.namedJdbcTemplate.query(query.toString(), namedParameters, new BeanPropertyRowMapper<BeanPosicionGlobalInversion>(BeanPosicionGlobalInversion.class)));
			
			LOG.info("::QUERY"+query);
			if(listaInversion.isEmpty()){
				error.setClave("1");
				error.setMessage(Constantes.ERROR);
			}else{
				for (BeanPosicionGlobalInversion item : listaInversion) {
					item.setFechaInversion(Utileria.cambioMeses(item.getFechaInversion()));
					item.setFechaVencimiento(Utileria.cambioMeses(item.getFechaVencimiento()));
				}
				
				
				principal.setListaInversion(listaInversion);
				respuesta.setPosicionGlobalRespuesta(principal);
				error.setClave("0");
				error.setMessage(Constantes.EXITO);	
			}
		} catch (Exception e) {
			error.setClave("1");
			error.setMessage(Constantes.ERROR);
		}
		respuesta.setError(error);
		return respuesta;
	}
	/*** Función para consultar creditos * */
	@Override
	public Respuesta consultaCredito() throws BusinessException {
		String schema = Urls.getSchemaNucleoCentral(); 

		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		StringBuilder query = new StringBuilder();
		respuesta = new Respuesta();
		error = new ErrorDTO();
		BeanPosicionGlobalPrincipalBean principal =new BeanPosicionGlobalPrincipalBean();
		List <BeanPosicionGlobalCreditos> listaCreditos= new  ArrayList<>();
		try {
			query.append("SELECT id as idCred, estadocr as estatus, productocr as producto, contratocr as contrato, saldoinsolutocr as saldoInsoluto, montosolicitadocr as montoSol,calificacioncr as calif, diasvencidocr as diasVencidos, avancecr as avance, disposicioncr as disposicion FROM "+schema+"ncposicionglobalcr");
			listaCreditos = (this.namedJdbcTemplate.query(query.toString(), namedParameters, new BeanPropertyRowMapper<BeanPosicionGlobalCreditos>(BeanPosicionGlobalCreditos.class)));
			LOG.info("QUERY::"+query);
			if(listaCreditos.isEmpty()){
				error.setClave("1");
				error.setMessage(Constantes.ERROR);
			}else{
				principal.setListaCreditos(listaCreditos);
				respuesta.setPosicionGlobalRespuesta(principal);
				error.setClave("0");
				error.setMessage(Constantes.EXITO);	
			}
		} catch (Exception e) {
			error.setClave("1");
			error.setMessage(Constantes.ERROR);
		}
		respuesta.setError(error);
		return respuesta;
	}
	/*** Función para consultar Seguros * */
	@Override
	public Respuesta consultSeguros() throws BusinessException {
		String schema = Urls.getSchemaNucleoCentral(); 

		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		StringBuilder query = new StringBuilder();
		respuesta = new Respuesta();
		error = new ErrorDTO();
		BeanPosicionGlobalPrincipalBean principal =new BeanPosicionGlobalPrincipalBean();
		List <BeanPosicionGlobalSeguros> listaSeguros= new  ArrayList<>();
		try {
			query.append("SELECT id as idSeg, estatusse as estatus, productose as producto, polizase as poiza, sumase as suma, to_char( fechavigencias ,'dd/Mon/yyyy') as fechaVig, idpersona FROM "+schema+"ncposicionglobalse");
			listaSeguros = (this.namedJdbcTemplate.query(query.toString(), namedParameters, new BeanPropertyRowMapper<BeanPosicionGlobalSeguros>(BeanPosicionGlobalSeguros.class)));
			LOG.info("::QRY::"+query);
			if(listaSeguros.isEmpty()){
				error.setClave("1");
				error.setMessage(Constantes.ERROR);
			}else{
				for(BeanPosicionGlobalSeguros  item : listaSeguros) {
					item.setFechaVig(Utileria.cambioMeses(item.getFechaVig()));
				}
				principal.setListaSeguros(listaSeguros);
				respuesta.setPosicionGlobalRespuesta(principal);
				error.setClave("0");
				error.setMessage(Constantes.EXITO);	
			}
		} catch (Exception e) {
			error.setClave("1");
			error.setMessage(Constantes.ERROR);
		}
		respuesta.setError(error);
		return respuesta;
	}

}
