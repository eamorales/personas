/**
 * 
 */
package mx.com.promesi.persistencia;


import mx.com.promesi.exceptions.BusinessException;

import com.mx.Res.Respuesta;
import com.mx.beans.BeanDireccion;

/**
 * @author G
 *
 */
public interface DomiciliosDao {

	public Respuesta consultaTipoUbicacionAll()throws BusinessException; /**Regres todas los tipos de ubicaciones para la sección de Domicilio*/
	
	public Respuesta consultaTipoNumeracionAll()throws BusinessException; /**Regres todas los tipos de Numeración para la sección de Domicilio*/
	
	public Respuesta consultaTabla() throws BusinessException; /**Regresa todos los registros de direciones*/
	
	public Respuesta insertaDomicilio(BeanDireccion bDir)throws BusinessException; /**Inserta la dirección marcada*/

	public Respuesta eliminaDomicilio(BeanDireccion bDir)throws BusinessException;/**Función para eliminar domicilio*/
	
	public Respuesta consultaDomicilioId(BeanDireccion bDir)throws BusinessException;/**Funcion para consultar domicilio especifico*/
	
	public Respuesta actualizaDomicilio(BeanDireccion bDir)throws BusinessException; /**Funcion para actualizar domicilio especifico*/

}
