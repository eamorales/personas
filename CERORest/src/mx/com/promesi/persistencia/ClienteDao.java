package mx.com.promesi.persistencia;

import com.mx.beans.BeanCatalogosDatos;
import com.mx.beans.Busqueda.BeanCliente;
import com.mx.beans.Busqueda.BeanClienteEntrada;

import mx.com.promesi.exceptions.BusinessException;

public interface ClienteDao {
	
	public long insertaCliente(BeanCatalogosDatos beanCatalogosDatos)throws BusinessException; 

	public BeanCliente eliminaCliente(BeanCliente BeanCliente)throws BusinessException;
	
	public BeanCatalogosDatos consultaClientePorId(long numeroCliente)throws BusinessException;
	
	public BeanCliente actualizaCliente(BeanCliente BeanCliente)throws BusinessException;
	
	public int actualizaCorreoCliente(BeanClienteEntrada beEntrada)throws BusinessException;
}
