package mx.com.promesi.persistencia;

import com.mx.Res.Respuesta;

import mx.com.promesi.exceptions.BusinessException;

public interface PosicionGlobalDao {
	
	public Respuesta consultaAhorro() throws BusinessException; /** Función para consultar Ahorro * */
	
	public Respuesta consultaInversion() throws BusinessException;/*** Función para consultar Inversión * */
	
	public Respuesta consultaCredito() throws BusinessException; /*** Función para consultar Seguros * */
	
	public Respuesta consultSeguros() throws BusinessException; /*** Función para consultar Seguros * */

}
