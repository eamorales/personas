/**
 * 
 */
package mx.com.promesi.persistencia;

import java.util.List;

import mx.com.promesi.exceptions.BusinessException;

import com.mx.Res.Respuesta;
import com.mx.beans.BeanAutorizacionesPendientes;

/**
 * @author G
 *
 */
public interface AutorizacionesPendientesDao {


	/**
	 * @param bAutPen
	 * @return
	 * @throws BusinessException
	 */
	public Respuesta consultaAutorizacionesPendientes(BeanAutorizacionesPendientes bAutPen) throws BusinessException;

	/**
	 * @param bAutPen
	 * @return
	 * @throws BusinessException
	 */
	public Respuesta actualizaAutorizacionesPendientes(List<BeanAutorizacionesPendientes> bAutPen) throws BusinessException;

}
