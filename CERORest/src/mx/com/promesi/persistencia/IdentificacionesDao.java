/**
 * 
 */
package mx.com.promesi.persistencia;
import com.mx.Res.Respuesta;
import com.mx.beans.BeanIdentificacion;
import mx.com.promesi.exceptions.BusinessException;

/**
 * @author Mariel
 *
 */
public interface IdentificacionesDao {
	
	public Respuesta insertarIdentificaciones(BeanIdentificacion t) throws BusinessException; /** * Función para insertar las identificaciones	 */
	
	public Respuesta actualizaridentificaciones(BeanIdentificacion t) throws BusinessException; /**	 * Función para actualizar las identificaciones	 */ 
	
	public Respuesta consultarCatalogoTipo() throws BusinessException;  /**	 * Función para obtener el catalogo de tipo identificaciones	 */    
	
	public Respuesta consultarCatalogoTipoExpresiones() throws BusinessException;  /**	 * Función para obtener el catalogo de tipo identificaciones Expresiones	 */
	
	public Respuesta consultaIdentificacionesDetalle(BeanIdentificacion t) throws BusinessException; /**	 * Función para consultar el detalle de la identificacion	 */

	public Respuesta consultarTablaIdentificaciones() throws BusinessException; /**	 * Función para consultar la tabla identificaciones	 */
	
	public Respuesta eliminarIdentificaciones(BeanIdentificacion t) throws BusinessException;/**	 * Función para eliminar una identificacion	 */ 
	
}
