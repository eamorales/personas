package mx.com.promesi.persistencia;

import com.mx.Res.Respuesta;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.service.BusquiedaServiceREST;

/**
 * 
 * 
 * @author Gustavo
 *
 */
public interface DatosDao {
	
	//Funciones para obtener  los distintos catalogos utilizados en datos
	public Respuesta obtenerCatalogosActividad () throws BusinessException;
	
	public Respuesta obtenerCatalogoSector() throws BusinessException;

	public Respuesta obtenerCatalogoSituacion() throws BusinessException;

	public Respuesta obtenerCatalogoCorreo() throws BusinessException;

	public Respuesta obtenerCatalogoEstablecimiento() throws BusinessException;

	public Respuesta obtenerCatalogoFacultad() throws BusinessException;

	public Respuesta obtenerCatalogoTipoSociedad() throws BusinessException;

	public Respuesta obtenerCatalogoPuesto() throws BusinessException;
	
	public Respuesta obtenerCatalogoGiro() throws BusinessException;
	
	public Respuesta obtenerCatalogoDocumentoPers() throws BusinessException;
	
	public Respuesta obtenerCatalogoTipoDocumentoPers() throws BusinessException;

	
	


	


}
