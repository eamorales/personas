package mx.com.promesi.persistencia;

import java.text.DateFormat;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.Resource;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.namedparam.BeanPropertySqlParameterSource;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import com.mx.Res.Respuesta;
import com.mx.Utilerias.Utileria;
import com.mx.beans.BeanCatalogosDatos;
import com.mx.beans.BeanExpedientes;
import com.mx.beans.ErrorDTO;
import com.mx.constantes.Constantes;
import com.mx.constantes.HardCode;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.service.Urls;

@Repository
public class ExpedientesDaoImp implements ExpedientesDao {

	private static final Logger log = Logger.getLogger(ExpedientesDaoImp.class);
	private NamedParameterJdbcTemplate namedJdbcTemplate;

	Date date = Calendar.getInstance().getTime();
	DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	String today = formatter.format(date);
	private String schema = "personas.";
	private ErrorDTO errorException = new ErrorDTO();
	private Respuesta respuesta;
	
	
	private StringBuilder  qry ;
	public String getToday() {
		return today;
	}

	/**
	 * Funcion para indicar conexion a BD
	 */
	@Resource(name = "DataSource")
	public void setDataSource(DataSource dataSource) {
		this.namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	@Override
	public Respuesta insertarExpedientes(BeanExpedientes expedienteBean) throws BusinessException {
		try {
			log.info(":::::::::  Comenzando insercion :::::::::::::::::");
			expedienteBean.setIdPersona(1);
			qry = new StringBuilder();
			final BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(expedienteBean);
			respuesta = new Respuesta();
			errorException = new ErrorDTO();
			qry.append("INSERT INTO "+schema+"\"PeExpendiente\"( \"idPersona\", \"idTipoDocumento\", estatus, \"fechaCreacion\"," + 
					" \"rutaAlfresco\", \"usuarioCreacion\", observacion, imagen, \"fechaExpedicion\")" + 
					"    VALUES ("+expedienteBean.getIdPersona()+", "+expedienteBean.getTipoDocumento()+","+expedienteBean.getEstatus()+""
							+ ", '"+Utileria.getFechaHoy()+"', '"+expedienteBean.getRuta()+"', "+1+",'"+
					expedienteBean.getObservaciones()+"', '"+expedienteBean.getNombreArchivo()+"', '"+expedienteBean.getFecha()+"')");
			log.info(qry);
			int resultado = this.namedJdbcTemplate.update(qry.toString(), namedParameters);
			if(resultado == 1 ) {
				errorException.setClave(Constantes.EXITO_EXECUTE);
				errorException.setMessage(HardCode.SUCCESS_SAVE_EXPEDIENTE);
			}else {
				errorException.setClave(Constantes.ERROR_EXECUTE);
				errorException.setMessage("NO SE GUARD\u00d3 EL NUEVO EXPEDIENTE ");
			}
			
		} catch (DataAccessException e) {
			log.info(e);
		}
		respuesta.setError(errorException);
		return respuesta;
	}

	@Override
	public Respuesta consultarExpedientes(Integer idPersona) throws BusinessException {
		try {
			log.info("Parametro de Entrada :: " + idPersona);
			errorException = new ErrorDTO();
			respuesta = new Respuesta();
			final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			qry = new StringBuilder();
			qry.append("SELECT  exp.\"idTipoDocumento\" AS \"tipoDocumento\", exp.id AS \"idExpediente\", exp.\"fechaExpedicion\" AS \"fecha\", \"idPersona\", exp.estatus ,  to_char (exp.\"fechaCreacion\", 'dd/MON/yyyy') as \"fechaCreacion\", to_char( exp.\"fechaModificacion\", 'dd/MON/yyyy')as \"fechaModificacion\", exp.\"rutaAlfresco\" AS ruta, exp.\"usuarioCreacion\", exp.\"usuarioModificacion\",  exp.observacion AS observaciones, exp.imagen , doc.\"vigencia\"  FROM "+schema+"\"PeTipoDocumento\" AS doc  INNER JOIN "+schema+"\"PeExpendiente\" AS exp  ON   doc.id = exp.\"idTipoDocumento\"  WHERE  exp.\"idPersona\" = 1  AND  exp.estatus = "+Constantes.getSTATUS_ALTA()+"");
			log.info(qry);
			respuesta.setListaExpedientes(this.namedJdbcTemplate.query(qry.toString(), namedParameters , new BeanPropertyRowMapper<BeanExpedientes>(BeanExpedientes.class)));

			if (respuesta.getListaExpedientes().isEmpty()) {
				respuesta.setListaExpedientes(new ArrayList<BeanExpedientes>());
				errorException.setClave( Constantes.ERROR_EXECUTE);
				errorException.setMessage("ERROR AL TRAER EL EXPEDIENTE");
			} else {
				for (BeanExpedientes element : respuesta.getListaExpedientes()) {
					if(element.getVigencia() != null && !element.getVigencia().isEmpty() ) {
						int  estado = Utileria.differenceofDays(Utileria.convertirFechatoString(element.getFecha()));
						int vigencia = Integer.parseInt(element.getVigencia());
						log.info("Dias calculados -> " +estado +"  :: Dias de Vigencia ->  "+element.getVigencia() +" >>>>> Excedio Dias Validos  -> "+(estado < vigencia ? "NO" : "SI")+" <<<<<<<<<");
						if (estado < vigencia) {
							element.setVigencia("Vigente");
						} else {
							element.setVigencia("Vencido");
						}
					}else {
						element.setVigencia("Vigente");
					}
					
					
				}
				errorException.setClave(Constantes.EXITO_EXECUTE);
				errorException.setMessage("SE OBTUVIERON LOS DATOS CORRECTAMENTE");
				log.info("RESULTADO  ::: " + errorException.getMessage() );

			}
			
		} catch (DataAccessException e) {
			log.info("ERROR IMPLEMENTS CONSULTA EXP ::: " + e.getMessage());
			
		}
		respuesta.setError(errorException);
		return respuesta;
	}
	
	

	@Override
	public Respuesta consultarCatalogoTipoDocumento() throws BusinessException {
		log.info("SERVICE EXPEDIENTES ::::: CONSULTAR TIPO DOCUMENTOS IMP ");
		respuesta = new Respuesta();
		qry = new StringBuilder();
		List<BeanCatalogosDatos> listaCatalogo;
		Map<String, String> listDocumentos = new LinkedHashMap<>();
		Map<String, String> listClaves = new LinkedHashMap<>();

		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		try {
			qry.append("SELECT id, clave, descripcion FROM "+Urls.getSchemaPersonas()+"\"PeTipoDocumento\";");
			log.info(qry);
			listaCatalogo = this.namedJdbcTemplate.query(qry.toString(), namedParameters, new BeanPropertyRowMapper<BeanCatalogosDatos>(BeanCatalogosDatos.class) );
			if (listaCatalogo.isEmpty()) {
				errorException.setClave(Constantes.ERROR_EXECUTE);
				errorException.setMessage(HardCode.ERROR_EXP_CATALOGO_DOCS);
			} else {
				log.info("mx.com.promesi.persistencia.ExpedientesDaoImp  cargaCatalogoPeTipoDocumento");
				for (BeanCatalogosDatos bean : listaCatalogo) {
					listClaves.put(bean.getId(), bean.getClave());
					listDocumentos.put(bean.getId(), bean.getDescripcion());
				}
				respuesta.setListaClaveDocumento(listClaves);
				respuesta.setCatalogoTipoDocumento(listDocumentos);
				errorException.setClave(Constantes.EXITO_EXECUTE);
				errorException.setMessage(HardCode.EXITO_EXP_CATALOGO_DOCS);
			}
		} catch (DataAccessException e) {
			log.info(HardCode.ERROR_EXP_CATALOGO_DOCS + "\t" + e.getMessage());

		}
		respuesta.setError(errorException);
		return respuesta;
	}

	@Override
	public Respuesta eliminarExpediente(Integer idExpediente) {
		try {
			log.info("DELETE IMPL");
			
			respuesta = new Respuesta();
			qry = new StringBuilder();
			errorException = new ErrorDTO();
			final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
			qry.append("UPDATE "+schema+"\"PeExpendiente\"" + 
					"   SET  estatus= " +Constantes.getESTATUS_BAJA()+ "   WHERE id = "+idExpediente+"");
			log.info(qry);
			int resultado = this.namedJdbcTemplate.update(qry.toString(), namedParameters);
			log.info("REsult"+ resultado);
			if( resultado > 0) {
				errorException.setClave(Constantes.EXITO_EXECUTE);
				errorException.setMessage(HardCode.SUCCESS);
				
			}else {
				errorException.setClave(Constantes.ERROR_EXECUTE);
				errorException.setMessage(HardCode.ERROR);
				
			}
		} catch (DataAccessException e) {
			log.info(HardCode.ERROR);
		}
		respuesta.setError(errorException);
		return respuesta;
	} 

	@Override
	public Respuesta actualizarExpediente (BeanExpedientes expediente) {
		try {
			log.info("ACTUALIZACION IMPL");
			respuesta = new Respuesta();
			qry = new StringBuilder();
			errorException = new ErrorDTO();
			final BeanPropertySqlParameterSource namedParameters = new BeanPropertySqlParameterSource(expediente);
			qry.append("UPDATE "+Urls.getSchemaPersonas()+"\"PeExpendiente\" SET \"idTipoDocumento\"="+expediente.getTipoDocumento()+", \"rutaAlfresco\"='"+expediente.getRuta()+"', \"usuarioModificacion\"=1, observacion='"+expediente.getObservaciones()+"', imagen='"+expediente.getNombreArchivo()+"', \"fechaExpedicion\"='"+expediente.getFecha()+"', \"fechaModificacion\"='"+Utileria.getFechaHoy()+"' WHERE id = "+expediente.getIdExpediente()+";");
			log.info(qry);
			int resultado = this.namedJdbcTemplate.update(qry.toString(), namedParameters);
			log.info("REsult"+ resultado);
			if( resultado > 0) {
				errorException.setClave(Constantes.EXITO_EXECUTE);
				errorException.setMessage(HardCode.SUCCESS);
				
			}else {
				errorException.setClave(Constantes.ERROR_EXECUTE);
				errorException.setMessage(HardCode.ERROR);
			}
		} catch (DataAccessException e) {
			log.info(HardCode.ERROR);
		}
		respuesta.setError(errorException);
		return respuesta;
	} 
	

}
