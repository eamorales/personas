package mx.com.promesi.persistencia;

import com.mx.Res.Respuesta;

import mx.com.promesi.exceptions.BusinessException;

public interface InvolucradosDao {
	public Respuesta consultaListaInv() throws BusinessException;/**Función que consulta la lista de involucrados**/

}
