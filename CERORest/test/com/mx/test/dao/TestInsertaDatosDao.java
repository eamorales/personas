package com.mx.test.dao;

import static org.junit.Assert.*;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import com.mx.beans.BeanCatalogosDatos;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.persistencia.ClienteDao;

@RunWith (SpringJUnit4ClassRunner.class)
@ContextConfiguration (locations = {"classpath:applicationTestContext.xml"})
public class TestInsertaDatosDao {
	final static Logger logger = Logger.getLogger(TestInsertaDatosDao.class);
	@Autowired
	ClienteDao clientDao ;	
	
	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testInsertaCliente() {
		BeanCatalogosDatos beanCatalogosDatos = new BeanCatalogosDatos();

		beanCatalogosDatos.getBeanCliente().getDatosGenerales().setPrimerNombre("primerNombre");
		beanCatalogosDatos.getBeanCliente().getDatosGenerales().setSegundoNombre("SegundoNombre");
		beanCatalogosDatos.getBeanCliente().getDatosGenerales().setPrimerApellido("primerApellido");
		beanCatalogosDatos.getBeanCliente().getDatosGenerales().setSegundoApellido("segundoApellido");
		beanCatalogosDatos.getBeanCliente().getBeanInformacion().setPersonalidad("2");
		beanCatalogosDatos.getBeanCliente().getDatosGenerales().setRfc("SRSP120211");
		beanCatalogosDatos.getBeanCliente().getDatosGenerales().setFechaNacimiento("2012-12-11");
		try {
			clientDao.insertaCliente(beanCatalogosDatos);
		} catch (BusinessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			fail("Not yet implemented");
		}
		
	}

}
