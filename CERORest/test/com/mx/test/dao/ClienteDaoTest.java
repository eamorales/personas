package com.mx.test.dao;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mx.beans.BeanCatalogosDatos;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.persistencia.ClienteDao;

@RunWith (SpringJUnit4ClassRunner.class)
//Indicamos el fichero de configuración de spring
@ContextConfiguration (locations = {"classpath:applicationTestContext.xml"})
public class ClienteDaoTest {
	final static Logger logger = Logger.getLogger(ClienteDaoTest.class);
	@Autowired
	ClienteDao clientDao ;	
	
	@BeforeClass
	public static void startUp ()  {
		
	}
					
	@Test
	public void testClienteId() throws BusinessException {		
		BeanCatalogosDatos cliente = clientDao.consultaClientePorId(43);
		Assert.assertNotNull(cliente);
		logger.info(cliente.getBeanCliente().getBeanInformacion().getNoCliente());
	}
	

}