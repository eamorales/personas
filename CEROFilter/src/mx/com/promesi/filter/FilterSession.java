package mx.com.promesi.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;

import com.mx.constantes.Constantes;

import BeanRespuesta.RespuestaBase;
import mx.com.promesi.beans.AppBean;
import mx.com.promesi.beans.AppSessions;
import mx.com.promesi.beans.UserSession;
import mx.com.promesi.rest.RestSession;
import mx.com.promesi.util.UrlConstantes;
import mx.com.promesi.util.encrypt.AppCrypt;


public class FilterSession implements Filter {

	
	private FilterConfig gobjConfigFiltro = null;
	 private static final Logger LOG = Logger.getLogger(FilterSession.class);
	  
	
	@Value("${IdApp}")
	private String idApp;
	
	@Value("${AppSesion}")
	private String appSesion;
	
	@Autowired
	private AppSessions sesiones;
	
	private String  urlServiceGlobal="";
	
	FilterSession() {
		super();
	}

	@Override
	public void destroy() {
	}

	@Override
	public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain chain)
			throws IOException, ServletException {
		
		HttpServletRequest httpServletRequest = (HttpServletRequest) request;
		HttpServletResponse httpServletResponse = (HttpServletResponse) response;
		LOG.info("URLS:::::::"+httpServletRequest.getRequestURI());
		if(
				
				httpServletRequest.getRequestURI().equals("/CERORest/Domicilio/ConsultaTipoUbicacion") 
				||httpServletRequest.getRequestURI().equals("/CERORest/Domicilio/ConsultaTipoNumeracion")
				||httpServletRequest.getRequestURI().equals("/CERORest/Identificaciones/ConsultarCatalogoTipoIdentificacion")
				||httpServletRequest.getRequestURI().equals("/CERORest/Identificaciones/ConsultarCatalogoTipoIdentificacionExpresiones")
				||httpServletRequest.getRequestURI().equals("/CERORest/Datos/CONSULTAR_CATALOGO_FACULTAD")
				||httpServletRequest.getRequestURI().equals("/CERORest/Datos/CONSULTAR_CATALOGO_SITUACION")
				||httpServletRequest.getRequestURI().equals("/CERORest/Datos/CONSULTAR_CATALOGO_CORREO")
				||httpServletRequest.getRequestURI().equals("/CERORest/Datos/CONSULTAR_CATALOGO_ESTABLECIMIENTO")
				||httpServletRequest.getRequestURI().equals("/CERORest/Datos/CONSULTAR_CATALOGO_GIRO")
				||httpServletRequest.getRequestURI().equals("/CERORest/Datos/CONSULTAR_CATALOGO_SECTOR")
				||httpServletRequest.getRequestURI().equals("/CERORest/Datos/CONSULTAR_CATALOGO_TIPO_DE_SOCIEDAD")
				||httpServletRequest.getRequestURI().equals("/CERORest/Datos/CONSULTAR_CATALOGO_PUESTO")
		) {
			LOG.info("::Catalogo Publico");
			chain.doFilter(request, response);
		}
		else if(this.validaSesion((HttpServletRequest) request)){
		chain.doFilter(request, response);
			LOG.info("::FilterSession::");
		}else {
		
		LOG.info("::La sesión ha finalizando::::FilterSession::");
		}
	
	}
		
	
	private boolean validaSesion(HttpServletRequest httpServletRequest){
		String auth=httpServletRequest.getHeader(Constantes.AUTH_HEADER);
		String urlServer=httpServletRequest.getRequestURL().toString().replace(httpServletRequest.getRequestURI(), Constantes.BLANK);
		urlServiceGlobal = urlServer;
		AppBean appBean=new AppBean(AppCrypt.encryptApp(idApp,auth));
//		AppBean appBean=new AppBean(AppCrypt.encryptApp(idApp,"123"));
		LOG.info("Url Accediendo"+urlServer);
		
//		if(sesiones.getSesiones().get(auth)==null){
		if(sesiones.getSesiones().get("123")==null){
			LOG.info("::FilterSession::SIN Sesion:: OBTENSESION");
			RestSession<AppBean, UserSession> call=new RestSession<>("", appBean, UserSession.class);;
			call.setUrl(urlServer + appSesion + UrlConstantes.OBTENSESION);
			UserSession sesion=call.call();
				if(sesion.getCodigoError().equals(Constantes.COD_ERROR_00000)){
					sesiones.getSesiones().put(sesion.getClaveUsuario(), sesion);
					return true;
				}
		}else{
			LOG.info("::FilterSession::CON Sesion:: VERIFICASESION");
			RestSession<AppBean, RespuestaBase> call=new RestSession<>("", appBean, RespuestaBase.class);
			call.setUrl(urlServer + appSesion + UrlConstantes.VERIFICASESION);
			RespuestaBase resp=call.call();
				if(resp.getCodigoError().equals("0")){
					return true;
				}
			  
		}
		return false;
	}
	/** 
	* 
	*/
	public void init(FilterConfig filterConfig) throws ServletException {
		gobjConfigFiltro = filterConfig;
	}

	public FilterConfig getFilterConfig() {
		return this.gobjConfigFiltro;
	}

	public void setFilterConfig(FilterConfig filterConfig) {
		this.gobjConfigFiltro = filterConfig;
	}



}
