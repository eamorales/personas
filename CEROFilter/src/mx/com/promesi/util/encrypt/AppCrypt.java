package mx.com.promesi.util.encrypt;

import java.util.Date;

import com.mx.constantes.Constantes;

import mx.com.promesi.beans.DataBean;

/**
 * Clase para Encryptado entre Aplicaciones con tiempo de 30 segundos de caducidad
 * @author Iguzman
 * @version 1.0 28/06/17
 */
public class AppCrypt {

	
	/**
	 *  Caducidad de codigos en milisegundos
	 */
	private static long tiempoValido=30000;
	
	/**
	 * Encripta Id App y Clave Usuario
	 * @param app
	 * @param idUser
	 * @return
	 */
	public static String encryptApp(final String app,final String idUser){
		
		final StringBuilder code = new StringBuilder();
		final Date fecha = new Date();
		code.append(Constantes.PIPE);
		code.append(app);
		code.append(Constantes.PIPE);
		code.append(idUser);
		code.append(Constantes.PIPE);
		code.append(fecha.getTime());
		code.append(Constantes.PIPE);
		
		DESEncryption encryp = null;
		String encryptedcode = Constantes.BLANK;
		try {
			encryp = new DESEncryption();
			encryptedcode = encryp.encrypt(code.toString());
			encryptedcode = Base64Coder.encodeString(encryptedcode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return encryptedcode;
	}
	
	/**
	 * Desencripta y valida tiempo de caducidad en caso de ser valido regresa datos de Id App y Clave Usuario
	 * @param codigo
	 * @return
	 */
	public static DataBean decryptApp(final String codigo){
		
		DESEncryption crypt = null;
		String decryptedcode = Constantes.BLANK;
		try {
			decryptedcode = Base64Coder.decodeString(codigo);
			crypt = new DESEncryption();
			decryptedcode = crypt.decrypt(decryptedcode);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String[] code=decryptedcode.split("\\|");
		
		String time=code[Constantes.TRES];
		
		final Date fechaActual = new Date();
		
		Long tiempo=fechaActual.getTime()-Long.parseLong(time);
		
		if(tiempo>tiempoValido)
			return null;
		
		return new DataBean(code[Constantes.UNO], code[Constantes.DOS]);
		
	}

} 