package mx.com.promesi.beans;

import java.io.Serializable;
import java.util.List;

public class Test implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3935331586221008579L;
	private Error error;
	private List<Object[]> dto ;

	public Test() {
	}



	public Error getError() {
		return error;
	}



	public void setError(Error error) {
		this.error = error;
	}

	public List<Object[]> getDto() {
		return dto;
	}

	public void setDto(List<Object[]> dto) {
		this.dto = dto;
	}






	
	
}
