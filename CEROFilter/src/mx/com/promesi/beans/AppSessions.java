package mx.com.promesi.beans;

import java.io.Serializable;
import java.util.LinkedHashMap;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import mx.com.promesi.beans.UserSession;


@Component
@Scope(scopeName="singleton",proxyMode= ScopedProxyMode.TARGET_CLASS)
public class AppSessions implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3935331586221008579L;
	
	public AppSessions() {
	}
	
	private LinkedHashMap<String, UserSession> sesiones;
	

	public LinkedHashMap<String, UserSession> getSesiones() {
		if(sesiones==null){
			sesiones=new LinkedHashMap<>();
		}
		return sesiones;
	}

	public void setSesiones(LinkedHashMap<String, UserSession> sesiones) {
		this.sesiones = sesiones;
	}

	
}
