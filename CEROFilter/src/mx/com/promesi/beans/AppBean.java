package mx.com.promesi.beans;

public class AppBean {
	
	private String id;
	private String nombre;
	private String codigo;

	public AppBean() {
	}
	
	public AppBean(String codigo) {
		super();
		this.codigo = codigo;
	}


	public AppBean(String id, String nombre) {
		super();
		this.id = id;
		this.nombre = nombre;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}
	
	
	

}
