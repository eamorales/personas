package mx.com.promesi.beans;

import java.io.Serializable;

public class Error implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3935331586221008579L;
	private String message;
	private String clave ;
	
	

	public Error() {
	}



	public String getMessage() {
		return message;
	}



	public void setMessage(String message) {
		this.message = message;
	}



	public String getClave() {
		return clave;
	}



	public void setClave(String clave) {
		this.clave = clave;
	}



	
}
