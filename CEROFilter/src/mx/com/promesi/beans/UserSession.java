package mx.com.promesi.beans;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import BeanRespuesta.RespuestaBase;

public class UserSession extends RespuestaBase implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3935331586221008579L;
	private String claveUsuario;
	private String usuarioid;
	private String no_empleado;
	private String contrasenia;
	private String token;
	private Set<String> urlAllow ;
	private String userEncriptado;
	private String urlService;
	
	
	

	public String getUrlService() {
		return urlService;
	}

	public void setUrlService(String urlService) {
		this.urlService = urlService;
	}

	public UserSession() {
	}
	
	public String getClaveUsuario() {
		return claveUsuario;
	}

	public void setClaveUsuario(String claveUsuario) {
		this.claveUsuario = claveUsuario;
	}

	public String getUsuarioid() {
		return usuarioid;
	}

	public void setUsuarioid(String usuarioid) {
		this.usuarioid = usuarioid;
	}

	public String getNo_empleado() {
		return no_empleado;
	}

	public void setNo_empleado(String no_empleado) {
		this.no_empleado = no_empleado;
	}

	public String getContrasenia() {
		return contrasenia;
	}

	public void setContrasenia(String pass) {
		contrasenia = pass;
	}

	public UserSession(String codigoError, String mensaje) {
		super.setRespuestaBase(codigoError, mensaje);
	}

	
	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}
	public Set<String> getUrlAllow() {
		if(urlAllow==null){
			urlAllow= new HashSet<String>();
		}
		return urlAllow;
	}
	
	public void setUrlAllow(Set<String> urlAllow) {
	
		this.urlAllow = urlAllow;
	}

	public String getUserEncriptado() {
		return userEncriptado;
	}

	public void setUserEncriptado(String userEncriptado) {
		this.userEncriptado = userEncriptado;
	}
	
	
	
}
