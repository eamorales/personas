package mx.com.promesi.beans;

public class DataBean {
	
	private String idApp;
	private String idUser;
	
	
	
	public DataBean(String idApp, String idUser) {
		super();
		this.idApp = idApp;
		this.idUser = idUser;
	}
	
	public String getIdApp() {
		return idApp;
	}
	public void setIdApp(String idApp) {
		this.idApp = idApp;
	}
	public String getIdUser() {
		return idUser;
	}
	public void setIdUser(String idUser) {
		this.idUser = idUser;
	}

	

}
