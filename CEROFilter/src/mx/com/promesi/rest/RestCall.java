package mx.com.promesi.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.mx.constantes.Constantes;

import BeanRespuesta.RespuestaBase;
import mx.com.promesi.beans.Error;
import mx.com.promesi.beans.Test;


public class RestCall<E, S extends RespuestaBase> {
	
	  private static final int EXITO = 200;
	  private static final Logger LOG = Logger.getLogger(RestSession.class);
	  
	  private ResteasyClientBuilder clientBuilder = new ResteasyClientBuilder().connectionPoolSize(20);
	  private ResteasyClient client = clientBuilder.build();
	  private E entrada;
	  private S salida;
	  private String url;
	  private JavaType dtoType;
	  private Class<S> clase;
	  private AuthHeadersRequest auth;
	  
	  public RestCall() {
	  }
	  
	  public RestCall(final String url,final E entrada , final Class<S> s,AuthHeadersRequest authRequest) {
		    this.url = url;
		    this.entrada = entrada;
		    this.clase=s;
		    this.auth=authRequest;
	  }
	  
	  private void generate(){
		  this.dtoType = TypeFactory.defaultInstance().constructFromCanonical(clase.getCanonicalName());
		    try {
		    	this.salida = clase.newInstance();
		    	this.salida.setCodigoError(Constantes.COD_ERROR_11111);
		    	this.salida.setMensaje("Error Generico");
		    } catch (InstantiationException e1) {
		    	e1.printStackTrace();
	        } catch (IllegalAccessException e1) {
	        	e1.printStackTrace();
	        }
	  }
	  
	  
	  public S call(){
		  this.generate();
		  
		try{
			final ObjectMapper mapper = new ObjectMapper(); 
			String tramaFinal = "";
				 
		    tramaFinal =  mapper.writeValueAsString(entrada);
		     
		    ResteasyWebTarget target = client.target(this.url);
		    client.register(this.auth);
		    Response response = target.request().post(Entity.entity(tramaFinal, MediaType.APPLICATION_JSON));
		    
		    if( this.validaRespuesta(response)){
			   String data= response.readEntity(String.class);
			   response.close();  
			   if(data==null || data.isEmpty() ) return salida;
			   return mapper.readValue(data, this.dtoType);
		     }
		    	response.close();  
		} catch (Exception e) {
			    e.printStackTrace();
		}
		    return salida;  
	  }

	  
	  
	  private boolean validaRespuesta(final Response response) throws IOException {
		  if (response.getStatus() != EXITO) {
		     return false;
	      } else {
	    	 // LOG.info("Peticion::" + this.url + "::Exito : HTTP CODE  " + response.getStatus()+" ::");
	    	  if(Constantes.COD_STATUS_ERROR.equalsIgnoreCase(response.getHeaderString(Constantes.HEADER_STATUS))){
	    		  LOG.info(response.getHeaderString(Constantes.HEADER_STATUS)+"::"+response.getHeaderString(Constantes.HEADER_MESSAGE));
	    		  
	              return false;
	            }
	      }
	      return true;
		    
		}
	  
	  public E getEntrada() {
		return entrada;
	}

	public void setEntrada(E entrada) {
		this.entrada = entrada;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Class<S> getClase() {
		return clase;
	}

	public void setClase(Class<S> s) {
		this.clase = s;
	}

	public AuthHeadersRequest getAuth() {
		return auth;
	}

	public void setAuth(AuthHeadersRequest auth) {
		this.auth = auth;
	}

	public static void main(String[] args) {
		  
			 
	    try {
//	    	UserSession sesion=new UserSession();
//	    	sesion.setClaveUsuario("EMORALES");
//	    	sesion.setContrasenia("Pr0mes1");
//	    	sesion.setNo_empleado("24872");
//	    	sesion.setUsuarioid("4158");
	    	Test t=new Test();
	    	t.setError(new Error());
	    	
	    	List<Object[]> list=new ArrayList<>();
	    	Object[] t1=new Object[3];
	    	t1[0]=2;
	    	t1[1]="Telefono";
	    	t1[2]="Alta";
	    	list.add(t1);
	    	Object[] t2=new Object[3];
	    	t2[0]=1;
	    	t2[1]="Cel";
	    	t2[2]="Alta B";
	    	list.add(t2);
	    	t.setDto(list);
	    	
	    	final ObjectMapper mapper = new ObjectMapper(); 
//	    	String tramaFinal = "";
//			tramaFinal =  mapper.writeValueAsString(t);
//			System.out.println(tramaFinal);
//			
			 String d="{\"error\":{\"message\":\"\",\"clave\":\"0\"},\"dto\":[[1,\"TELCEL\",\"ALTA\"],[2,\"TELMEX\",\"ALTA\"],[3,\"TELNOR\",\"ALTA\"],[4,\"MOVISTAR\",\"ALTA\"],[5,\"NEXTEL\",\"ALTA\"],[6,\"MEGACABLE\",\"ALTA\"],[7,\"ATT\",\"ALTA\"]]}";

			 Test data= mapper.readValue(d, Test.class);
			 
			 System.out.println(data.getError().getClave());
			 
			 for(Object[] tmp:data.getDto()){
				 System.out.println(tmp[0]);
				 System.out.println(tmp[1]);
				 System.out.println(tmp[2]);
			 }
			 
				try {
					Class<?> c = Class.forName("mx.com.promesi.beans.BeanTipoTelefono");
					
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
		} catch ( IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	   //	    ConsultaWebService{"error":{"message":"","clave":"0"},
//	    	"dto":[[2,"TELÃ‰FONO CONVENCIONAL PROPIO","ALTA"],[3,"TELÃ‰FONO CELULAR","ALTA"],[4,"TELÃ‰FONO DE RECADOS","ALTA"],[5,"TELÃ‰FONO DEL NEGOCIO","ALTA"],[6,"TELÃ‰FONO DEL TRABAJO","ALTA"],[7,"TELÃ‰FONO DEL TRABAJO","ALTA"],[8,"TELÃ‰FONO REFERENCIA","ALTA"]]}
//		
//		AuthHeadersRequest auth=new AuthHeadersRequest("EMORALES");
//		
//		  telefonos t=new telefonos();
//		  t.setCodigoError(1234);
//		  t.setMaxchar(10);
//		  
//		  RestCall<telefonos, telefonos> r=new RestCall<telefonos, telefonos>("http://localhost:8080/CERORest/Telefono/Consulta", t, telefonos.class,auth);
//		  r.call();
//		  
		  
//		  RestSession<telefonos, telefonos> r2=new RestSession<telefonos, telefonos>();
//		  r2.setUrl("http://localhost:8080/CERORest/Telefono/Consulta");
//		  r2.setEntrada(t);
//		  r2.setClase(telefonos.class);
//		  r2.call();
		  
		  
	}
}
