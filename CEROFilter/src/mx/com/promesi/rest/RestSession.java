package mx.com.promesi.rest;

import java.io.IOException;

import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.jboss.resteasy.client.jaxrs.ResteasyClient;
import org.jboss.resteasy.client.jaxrs.ResteasyClientBuilder;
import org.jboss.resteasy.client.jaxrs.ResteasyWebTarget;

import com.fasterxml.jackson.databind.JavaType;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.type.TypeFactory;
import com.mx.constantes.Constantes;

import BeanRespuesta.RespuestaBase;


public class RestSession<E, S extends RespuestaBase> {
	
	  private static final int EXITO = 200;
	  private static final Logger LOG = Logger.getLogger(RestSession.class);
	  
	  private ResteasyClientBuilder clientBuilder = new ResteasyClientBuilder().connectionPoolSize(20);
	  private ResteasyClient client = clientBuilder.build();
	  private E entrada;
	  private S salida;
	  private String url;
	  private JavaType dtoType;
	  private Class<S> clase;
	  
	  public RestSession() {
	  }
	  
	  public RestSession(final String url,final E entrada , final Class<S> s) {
		    this.url = url;
		    this.entrada = entrada;
		    this.clase=s;
	  }
	  
	  private void generate(){
		  this.dtoType = TypeFactory.defaultInstance().constructFromCanonical(clase.getCanonicalName());
		    try {
		    	this.salida = clase.newInstance();
		    	this.salida.setCodigoError(Constantes.COD_ERROR_11111);
		    	this.salida.setMensaje("Error Generico");
		    } catch (InstantiationException e1) {
		    	e1.printStackTrace();
	        } catch (IllegalAccessException e1) {
	        	e1.printStackTrace();
	        }
	  }
	  
	  
	  public S call(){
		  this.generate();
		  
		try{
			final ObjectMapper mapper = new ObjectMapper(); 
			String tramaFinal = "";
				 
		    tramaFinal =  mapper.writeValueAsString(entrada);
		     
		    ResteasyWebTarget target = client.target(this.url);
		    
		    Response response = target.request().post(Entity.entity(tramaFinal, MediaType.APPLICATION_JSON));
		    
//		    final HTTPBasicAuthFilter authFilter3 = new HTTPBasicAuthFilter("xxxx", "xxx!x");
//		    client.register(authFilter3);
		    
		    if( this.validaRespuesta(response)){
			   String data= response.readEntity(String.class);
			   response.close();  
			   if(data==null || data.isEmpty() ) return salida;
			   return mapper.readValue(data, this.dtoType);
		     }
		    	response.close();  
		} catch (Exception e) {
			    e.printStackTrace();
		}
		    return salida;  
	  }
	  
	  private boolean validaRespuesta(final Response response) throws IOException {
		  if (response.getStatus() != EXITO) {
		     return false;
	      } else {
	    	  LOG.info("Peticion::" + this.url + "::Exito : HTTP CODE  " + response.getStatus()+" ::");
	    	  if(Constantes.COD_STATUS_ERROR.equalsIgnoreCase(response.getHeaderString(Constantes.HEADER_STATUS))){
	    		  LOG.info(response.getHeaderString(Constantes.HEADER_STATUS)+"::"+response.getHeaderString(Constantes.HEADER_MESSAGE));
	    		  
	              return false;
	            }
	      }
	      return true;
		    
		}
	  
	  public E getEntrada() {
		return entrada;
	}

	public void setEntrada(E entrada) {
		this.entrada = entrada;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public Class<S> getClase() {
		return clase;
	}

	public void setClase(Class<S> s) {
		this.clase = s;
	}

	public static void main(String[] args) {
		  
//		  telefonos t=new telefonos();
//		  t.setCodigoError(1234);
//		  t.setMaxchar(10);
//		  
//		  RestSession<telefonos, telefonos> r=new RestSession<telefonos, telefonos>("http://localhost:8080/CERORest/Telefono/Consulta", t, telefonos.class);
//		  r.call();
//		  
//		  
//		  RestSession<telefonos, telefonos> r2=new RestSession<telefonos, telefonos>();
//		  r2.setUrl("http://localhost:8080/CERORest/Telefono/Consulta");
//		  r2.setEntrada(t);
//		  r2.setClase(telefonos.class);
//		  r2.call();
//		  
		  
	}
}
