/*
 * @(#)ContrasenaUsuarioResponse.java 1.0 10/03/17 
 * 
 */
package net.cero.seguridad.datos;

import java.io.Serializable;

/**
 * Respuesta del servicio de mantenimiento de la tabla ContrasenaUsuario.
 * @author Inver-Tu
 * @version 1.0 10/03/17
 */
public class ContrasenaUsuarioResponse extends SeResponse implements Serializable{
	/** Numero de serializacion */
	private static final long serialVersionUID = 1L;
	/** Arreglo de Acciones */
    private ContrasenaUsuario contrasenaUsuario;
	/**
	 * @return the contrasenaUsuario
	 */
	public ContrasenaUsuario getContrasenaUsuario() {
		return contrasenaUsuario;
	}
	/**
	 * @param contrasenaUsuario the contrasenaUsuario to set
	 */
	public void setContrasenaUsuario(ContrasenaUsuario contrasenaUsuario) {
		this.contrasenaUsuario = contrasenaUsuario;
	}

}
