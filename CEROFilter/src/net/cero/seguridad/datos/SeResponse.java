/*
 * @(#)SeResponse.java 1.0 10/03/17 
 * 
 */
package net.cero.seguridad.datos;

import java.io.Serializable;

/**
 * Respuesta del servicio de mantenimiento de tablas.
 * @author Inver-Tu
 * @version 1.0 10/03/17
 */
public class SeResponse implements Serializable{
	/** Numero de serializacion */
	private static final long serialVersionUID = 1L;
	/** Codigo de respuesta */
	private String codeEstatus;
	/** Mensaje de respuesta */
	private String messageEstatus;
	/**
	 * @return the codeEstatus
	 */
	public String getCodeEstatus() {
		return codeEstatus;
	}
	/**
	 * @param codeEstatus the codeEstatus to set
	 */
	public void setCodeEstatus(String codeEstatus) {
		this.codeEstatus = codeEstatus;
	}
	/**
	 * @return the messageEstatus
	 */
	public String getMessageEstatus() {
		return messageEstatus;
	}
	/**
	 * @param messageEstatus the messageEstatus to set
	 */
	public void setMessageEstatus(String messageEstatus) {
		this.messageEstatus = messageEstatus;
	}
}
