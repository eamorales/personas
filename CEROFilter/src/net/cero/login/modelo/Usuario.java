package net.cero.login.modelo;

import java.util.Date;

public class Usuario {
	private int bloqueado;
	private Date ultimoAcceso;
	private int noEmpleado;
	private int usuarioId;
	private boolean vacaciones;
	/**
	 * @return the bloqueado
	 */
	public int getBloqueado() {
		return bloqueado;
	}
	/**
	 * @param bloqueado the bloqueado to set
	 */
	public void setBloqueado(int bloqueado) {
		this.bloqueado = bloqueado;
	}
	/**
	 * @return the ultimoAcceso
	 */
	public Date getUltimoAcceso() {
		return ultimoAcceso;
	}
	/**
	 * @param ultimoAcceso the ultimoAcceso to set
	 */
	public void setUltimoAcceso(Date ultimoAcceso) {
		this.ultimoAcceso = ultimoAcceso;
	}
	/**
	 * @return the noEmpleado
	 */
	public int getNoEmpleado() {
		return noEmpleado;
	}
	/**
	 * @param noEmpleado the noEmpleado to set
	 */
	public void setNoEmpleado(int noEmpleado) {
		this.noEmpleado = noEmpleado;
	}
	/**
	 * @return the usuarioId
	 */
	public int getUsuarioId() {
		return usuarioId;
	}
	/**
	 * @param usuarioId the usuarioId to set
	 */
	public void setUsuarioId(int usuarioId) {
		this.usuarioId = usuarioId;
	}
	/**
	 * @return the vacaciones
	 */
	public boolean isVacaciones() {
		return vacaciones;
	}
	/**
	 * @param vacaciones the vacaciones to set
	 */
	public void setVacaciones(boolean vacaciones) {
		this.vacaciones = vacaciones;
	}
	
}
