package com.mx.test.rest;

import java.io.IOException;

import javax.ws.rs.core.UriBuilder;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.Res.Respuesta;
import com.mx.beans.Busqueda.BeanClienteEntrada;
import com.mx.url.UrlService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

/**
 * @author Eduardo
 *
 */
public class testServiceREST {

	@Test
	public void test() {
		//BusquiedaServiceREST busquiedaServiceREST = new BusquiedaServiceREST();
		//final HTTPBasicAuthFilter authFilter3 = new HTTPBasicAuthFilter("x", "xxx!x");

		BeanClienteEntrada clienteEntrada = new BeanClienteEntrada();
		clienteEntrada.setNumeroCelular("8855669988");
		
		final ObjectMapper mapper = new ObjectMapper();
		String tramaFinal = "";
		String url3 = "http://localhost:8080/CEROSesion"+UrlService.BUSQUEDA_CLIENTE+UrlService.BUSQUEDA_CLIENTE_AVANZADA;
		try {
			tramaFinal = mapper.writeValueAsString(clienteEntrada);
			Client client3 = Client.create();
			//client3.addFilter(authFilter3);
			WebResource webResource3 = client3.resource(UriBuilder.fromUri(url3).build());
			ClientResponse responsex3 = webResource3.type("application/json").post(ClientResponse.class, tramaFinal);
			String output = responsex3.getEntity(String.class);			
			Respuesta respuesta = mapper.readValue(output, Respuesta.class);
			System.out.println("Output from Server .... \n");
			System.out.println(respuesta.getCliente()+" clave de error: "+respuesta.getError().getClave());			

		} catch (JsonProcessingException e) {
			System.out.println(e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			System.out.println(e);
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
