package com.mx.test.dao;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.mx.beans.Busqueda.BeanCliente;
import com.mx.beans.Busqueda.BeanClienteEntrada;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.persistencia.ClienteDao;

@RunWith (SpringJUnit4ClassRunner.class)
//Indicamos el fichero de configuración de spring
@ContextConfiguration (locations = {"classpath:applicationTestContext.xml"})
public class ClienteDaoTest {
	final static Logger logger = Logger.getLogger(ClienteDaoTest.class);
	@Autowired
	ClienteDao clientDao ;	
	
	@BeforeClass
	public static void startUp ()  {
		
	}
					
	@Test
	public void testClienteId() throws BusinessException {		
		BeanCliente cliente = clientDao.consultaClientePorId(43);
		Assert.assertNotNull(cliente);
		logger.info(cliente.getBeanInformacion().getNoCliente());
	}
	
	@Test(expected = BusinessException.class)
	public void testErrorClienteNombresApellidos() throws BusinessException {		
		BeanClienteEntrada beEntrada = new BeanClienteEntrada();
		beEntrada.setPrimerNombre("");
		beEntrada.setSegundoNombre("");
		beEntrada.setApellidoPaterno("");
		beEntrada.setApellidoMaterno("");
		BeanCliente cliente = clientDao.consultaClientePorParametros(beEntrada);
			
		Assert.assertNotNull(cliente);
		logger.info(cliente.getBeanInformacion().getNoCliente());
	}
	
	@Test
	public void testClienteNombresApellidos() throws BusinessException {		
		BeanClienteEntrada beEntrada = new BeanClienteEntrada();
		beEntrada.setPrimerNombre("yo");
		beEntrada.setSegundoNombre("t");
		beEntrada.setApellidoPaterno("tu");
		beEntrada.setApellidoMaterno("ella");
		BeanCliente cliente = clientDao.consultaClientePorParametros(beEntrada);
		Assert.assertNotNull(cliente);
		logger.info(cliente.getBeanInformacion().getNoCliente());
	}
	
	@Test
	public void testClienteNombreApellidoNumeroCel() throws BusinessException {		
		BeanClienteEntrada beEntrada = new BeanClienteEntrada();
		beEntrada.setPrimerNombre("yo");
		beEntrada.setSegundoNombre("");
		beEntrada.setApellidoPaterno("");
		beEntrada.setApellidoMaterno("ella");
		beEntrada.setNumeroCelular("8855669988");
		BeanCliente cliente = clientDao.consultaClientePorParametros(beEntrada);
		Assert.assertNotNull(cliente);
		logger.info(cliente.getBeanInformacion().getNoCliente());
	}

	
	@Test
	public void testClienteNombreApellidoNumeroCelError() throws BusinessException {		
		BeanClienteEntrada beEntrada = new BeanClienteEntrada();
		beEntrada.setPrimerNombre("yo");
		beEntrada.setSegundoNombre("");
		beEntrada.setApellidoPaterno("");
		beEntrada.setApellidoMaterno("ella");
		beEntrada.setNumeroCelular("885566998");
		BeanCliente cliente = clientDao.consultaClientePorParametros(beEntrada);
		Assert.assertNull(cliente);
		logger.info("no se encontraron datos con este telefono");
	}
	
	@Test
	public void testClienteNombreApellidoFecNacimiento() throws BusinessException {		
		BeanClienteEntrada beEntrada = new BeanClienteEntrada();
		beEntrada.setPrimerNombre("yo");
		beEntrada.setSegundoNombre("");
		beEntrada.setApellidoPaterno("");
		beEntrada.setApellidoMaterno("ella");
		beEntrada.setNumeroCelular("");
		beEntrada.setFechaNacimiento("2012-12-11");
		BeanCliente cliente = clientDao.consultaClientePorParametros(beEntrada);
		Assert.assertNotNull(cliente);
		logger.info(cliente.getBeanInformacion().getNoCliente());
	}
	
	@Test
	public void testClienteSoloCelular() throws BusinessException {		
		BeanClienteEntrada beEntrada = new BeanClienteEntrada();
		beEntrada.setPrimerNombre("");
		beEntrada.setSegundoNombre("");
		beEntrada.setApellidoPaterno("");
		beEntrada.setApellidoMaterno("");
		beEntrada.setNumeroCelular("");
		beEntrada.setNumeroCelular("8855669988");
		BeanCliente cliente = clientDao.consultaClientePorParametros(beEntrada);
		Assert.assertNotNull(cliente);
		logger.info(cliente.getBeanInformacion().getNoCliente());
	}
	
	
	@Test
	public void testActualizaCorreo() throws BusinessException {		
		BeanClienteEntrada beEntrada = new BeanClienteEntrada();
		beEntrada.setPrimerNombre("43");
		beEntrada.setSegundoNombre("");
		beEntrada.setApellidoPaterno("");
		beEntrada.setApellidoMaterno("");
		beEntrada.setCorreo("correoNuevo@yoyo.com");
		int status = clientDao.actualizaCorreoCliente(beEntrada);
		Assert.assertEquals(0, status);
		logger.info("estatus "+status);
	}
	
}