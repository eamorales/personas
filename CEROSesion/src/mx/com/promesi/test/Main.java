package mx.com.promesi.test;

import java.net.URL;
import java.rmi.RemoteException;

import javax.xml.rpc.ServiceException;

import com.google.gson.Gson;

import mx.com.promesi.login.token.Envia2FA;
import mx.com.promesi.login.token.Respuesta;
import mx.com.promesi.login.token.WS2FA;
import mx.com.promesi.login.token.WS2FAProxy;
import mx.com.promesi.login.token.WS2FAService;
import mx.com.promesi.login.token.WS2FAServiceLocator;

public class Main {

	public static void main(String[] args) {
//		envia2FA (String idUs,String "",String usuario) -- Método que genera el token y envía el correo
//	    valida2FA (String token, String idUs) -- Método que valida que el token corresponda al enviado
	    String idUs="1001";
		String vacio="";
		String usuario="EMORALES";
//		URL wsdl;
//		Envia2FA envia = new Envia2FA();
//		envia.setArg0(idUs);
//		envia.setArg1(vacio);
//		envia.setArg2(usuario);
		enviarCodigo();
		System.out.println("***********************");
	}
	
		private static String enviarCodigo(){
			String secfactor = "111111";
			int noEmpleado =101;
			int usuarioCero =1010;
			String respuesta = "";
			WS2FAService ws = new WS2FAServiceLocator();
			WS2FA envia2fa = new WS2FAProxy();
			try {
				envia2fa = ws.getWS2FAPort();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				String envia= envia2fa.valida2FA (secfactor, String.valueOf(noEmpleado));
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			try {
				String valida = envia2fa.envia2FA (String.valueOf(noEmpleado),	String.valueOf(usuarioCero), String.valueOf(usuarioCero));
			} catch (RemoteException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			String res = "{\"resultado\":0,\"respuesta\":\"\",\"msgHost\":\"Codigo 2FA enviado \",\"referencia\":\"\",\"numAutorizacion\":\"\",\"resultadoCatel\":-1}";
			Gson gson = new Gson();
			Respuesta respuesta2fa = gson.fromJson(res, Respuesta.class);
			
			if(respuesta2fa.getResultado() == 0){
				respuesta = "";
			} else {
				respuesta = respuesta2fa.getMsgHost();
			}
			
			return respuesta;
		}
		
		
		
		// wsdl = new URL
		// (ConsultaBuroPMIfzService.WSDL_LOCATION.toString().replace("localhost",
		// "sr-mule.integraopciones.mx"));
//		wsdl = new URL(WS2FAService.WSDL_LOCATION.toString().replace("localhost", "172.148.49.3"));
//		ConsultaBuroPMIfzService service1 = new ConsultaBuroPMIfzService(wsdl);
//		ConsultaBuroPMIfz port1 = service1.getConsultaBuroPMIfzPort();
//
//		ConsultaBuroPMRequest req = new ConsultaBuroPMRequest();
//		HeaderWS header = new HeaderWS();
//		header.setIdEmpresa(1l);
//
//		req.setHeader(header);
//
//		req.setProspectoId(1000000000284L);
//		req.setProductoId(999950L);
//		req.setNombre("TESTFOURTEENSADECV");
//		req.setRfc("TMO860310EF3");
//		req.setSistema("ambos");
//		req.setMunicipio("CELAYA");
//		req.setEntidadFederativa("GTO");
//		req.setLocalidad("CELAYA");
//		req.setColonia("CENTRO");
//		req.setDomicilio("BLVD ADOLFO LOPEZ MATEOS 608 OTE");
//
//		req.setCp("38070");
//		req.setMonto(new BigDecimal("100"));
//
//		ConsultaBuroPMResponse response = port1.procesar(req);
//		RespuestaXML respuesta = response.getReturn();
//
//		if (respuesta.getErrores().getCodigoError() != 0) {
//			for (RespuestaErrorXML res : respuesta.getErrores().getErrores()) {
//				System.out.println("Codigo Error: " + res.getCodigoError());
//				System.out.println(" Descripcion: " + Errores.descError.get(res.getCodigoError()));
//				for (ParametroBody res1 : respuesta.getBody().getParams()) {
//					System.out.println(res1.getNombre() + ": " + res1.getValor());
//				}
//			}
//		} else {
//
//			for (ParametroBody res : respuesta.getBody().getParams()) {
//				System.out.println(res.getNombre() + ": " + res.getValor());
//			}
//		}
//	}
}