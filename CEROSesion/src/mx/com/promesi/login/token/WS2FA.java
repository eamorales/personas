/**
 * WS2FA.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.promesi.login.token;

public interface WS2FA extends java.rmi.Remote {
    public java.lang.String valida2FA(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public java.lang.String envia2FA(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public mx.com.promesi.login.token.Respuesta validar2FaAhorro(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
}
