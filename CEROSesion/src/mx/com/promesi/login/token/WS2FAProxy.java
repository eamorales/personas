package mx.com.promesi.login.token;

public class WS2FAProxy implements mx.com.promesi.login.token.WS2FA {
  private String _endpoint = null;
  private mx.com.promesi.login.token.WS2FA wS2FA = null;
  
  public WS2FAProxy() {
    _initWS2FAProxy();
  }
  
  public WS2FAProxy(String endpoint) {
    _endpoint = endpoint;
    _initWS2FAProxy();
  }
  
  private void _initWS2FAProxy() {
    try {
      wS2FA = (new mx.com.promesi.login.token.WS2FAServiceLocator()).getWS2FAPort();
      if (wS2FA != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wS2FA)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wS2FA)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wS2FA != null)
      ((javax.xml.rpc.Stub)wS2FA)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public mx.com.promesi.login.token.WS2FA getWS2FA() {
    if (wS2FA == null)
      _initWS2FAProxy();
    return wS2FA;
  }
  
  public java.lang.String valida2FA(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wS2FA == null)
      _initWS2FAProxy();
    return wS2FA.valida2FA(arg0, arg1);
  }
  
  
  public java.lang.String envia2FA(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (wS2FA == null)
      _initWS2FAProxy();
    return wS2FA.envia2FA(arg0, arg1, arg2);
  }
  
   public mx.com.promesi.login.token.Respuesta validar2FaAhorro(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wS2FA == null)
      _initWS2FAProxy();
    return wS2FA.validar2FaAhorro(arg0, arg1);
  }
  
  
}