/**
 * WS2FAServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.promesi.login.token;

public class WS2FAServiceLocator extends org.apache.axis.client.Service implements mx.com.promesi.login.token.WS2FAService {

    public WS2FAServiceLocator() {
    }


    public WS2FAServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WS2FAServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WS2FAPort
    private java.lang.String WS2FAPort_address = "http://172.148.49.3:8080/WS2FA/WS2FA";
//    private java.lang.String WS2FAPort_address = "http://sr-servicios.integraopciones.mx:8080/WS2FA/WS2FA";

    public java.lang.String getWS2FAPortAddress() {
        return WS2FAPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WS2FAPortWSDDServiceName = "WS2FAPort";

    public java.lang.String getWS2FAPortWSDDServiceName() {
        return WS2FAPortWSDDServiceName;
    }

    public void setWS2FAPortWSDDServiceName(java.lang.String name) {
        WS2FAPortWSDDServiceName = name;
    }

    public mx.com.promesi.login.token.WS2FA getWS2FAPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WS2FAPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWS2FAPort(endpoint);
    }

    public mx.com.promesi.login.token.WS2FA getWS2FAPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.promesi.login.token.WS2FAServiceSoapBindingStub _stub = new mx.com.promesi.login.token.WS2FAServiceSoapBindingStub(portAddress, this);
            _stub.setPortName(getWS2FAPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWS2FAPortEndpointAddress(java.lang.String address) {
        WS2FAPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (mx.com.promesi.login.token.WS2FA.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.promesi.login.token.WS2FAServiceSoapBindingStub _stub = new mx.com.promesi.login.token.WS2FAServiceSoapBindingStub(new java.net.URL(WS2FAPort_address), this);
                _stub.setPortName(getWS2FAPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WS2FAPort".equals(inputPortName)) {
            return getWS2FAPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://ws/", "WS2FAService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://ws/", "WS2FAPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WS2FAPort".equals(portName)) {
            setWS2FAPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
