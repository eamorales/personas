/**
 * WS2FAService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.promesi.login.token;

public interface WS2FAService extends javax.xml.rpc.Service {
    public java.lang.String getWS2FAPortAddress();

    public mx.com.promesi.login.token.WS2FA getWS2FAPort() throws javax.xml.rpc.ServiceException;

    public mx.com.promesi.login.token.WS2FA getWS2FAPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
