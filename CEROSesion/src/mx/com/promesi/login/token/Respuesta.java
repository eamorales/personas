/**
 * Respuesta.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.promesi.login.token;

public class Respuesta  implements java.io.Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = -2823473884361316733L;

	private java.lang.String msgHost;

    private java.lang.String numAutorizacion;

    private java.lang.Object objeto;

    private java.lang.String referencia;

    private java.lang.String respuesta;

    private java.lang.Integer resultado;

    private java.lang.Integer resultadoCatel;

    public Respuesta() {
    }

    public Respuesta(
           java.lang.String msgHost,
           java.lang.String numAutorizacion,
           java.lang.Object objeto,
           java.lang.String referencia,
           java.lang.String respuesta,
           java.lang.Integer resultado,
           java.lang.Integer resultadoCatel) {
           this.msgHost = msgHost;
           this.numAutorizacion = numAutorizacion;
           this.objeto = objeto;
           this.referencia = referencia;
           this.respuesta = respuesta;
           this.resultado = resultado;
           this.resultadoCatel = resultadoCatel;
    }


    /**
     * Gets the msgHost value for this Respuesta.
     * 
     * @return msgHost
     */
    public java.lang.String getMsgHost() {
        return msgHost;
    }

    /**
     * Sets the msgHost value for this Respuesta.
     * 
     * @param msgHost
     */
    public void setMsgHost(java.lang.String msgHost) {
        this.msgHost = msgHost;
    }


    /**
     * Gets the numAutorizacion value for this Respuesta.
     * 
     * @return numAutorizacion
     */
    public java.lang.String getNumAutorizacion() {
        return numAutorizacion;
    }


    /**
     * Sets the numAutorizacion value for this Respuesta.
     * 
     * @param numAutorizacion
     */
    public void setNumAutorizacion(java.lang.String numAutorizacion) {
        this.numAutorizacion = numAutorizacion;
    }


    /**
     * Gets the objeto value for this Respuesta.
     * 
     * @return objeto
     */
    public java.lang.Object getObjeto() {
        return objeto;
    }


    /**
     * Sets the objeto value for this Respuesta.
     * 
     * @param objeto
     */
    public void setObjeto(java.lang.Object objeto) {
        this.objeto = objeto;
    }


    /**
     * Gets the referencia value for this Respuesta.
     * 
     * @return referencia
     */
    public java.lang.String getReferencia() {
        return referencia;
    }


    /**
     * Sets the referencia value for this Respuesta.
     * 
     * @param referencia
     */
    public void setReferencia(java.lang.String referencia) {
        this.referencia = referencia;
    }


    /**
     * Gets the respuesta value for this Respuesta.
     * 
     * @return respuesta
     */
    public java.lang.String getRespuesta() {
        return respuesta;
    }


    /**
     * Sets the respuesta value for this Respuesta.
     * 
     * @param respuesta
     */
    public void setRespuesta(java.lang.String respuesta) {
        this.respuesta = respuesta;
    }


    /**
     * Gets the resultado value for this Respuesta.
     * 
     * @return resultado
     */
    public java.lang.Integer getResultado() {
        return resultado;
    }


    /**
     * Sets the resultado value for this Respuesta.
     * 
     * @param resultado
     */
    public void setResultado(java.lang.Integer resultado) {
        this.resultado = resultado;
    }


    /**
     * Gets the resultadoCatel value for this Respuesta.
     * 
     * @return resultadoCatel
     */
    public java.lang.Integer getResultadoCatel() {
        return resultadoCatel;
    }


    /**
     * Sets the resultadoCatel value for this Respuesta.
     * 
     * @param resultadoCatel
     */
    public void setResultadoCatel(java.lang.Integer resultadoCatel) {
        this.resultadoCatel = resultadoCatel;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Respuesta)) return false;
        Respuesta other = (Respuesta) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.msgHost==null && other.getMsgHost()==null) || 
             (this.msgHost!=null &&
              this.msgHost.equals(other.getMsgHost()))) &&
            ((this.numAutorizacion==null && other.getNumAutorizacion()==null) || 
             (this.numAutorizacion!=null &&
              this.numAutorizacion.equals(other.getNumAutorizacion()))) &&
            ((this.objeto==null && other.getObjeto()==null) || 
             (this.objeto!=null &&
              this.objeto.equals(other.getObjeto()))) &&
            ((this.referencia==null && other.getReferencia()==null) || 
             (this.referencia!=null &&
              this.referencia.equals(other.getReferencia()))) &&
            ((this.respuesta==null && other.getRespuesta()==null) || 
             (this.respuesta!=null &&
              this.respuesta.equals(other.getRespuesta()))) &&
            ((this.resultado==null && other.getResultado()==null) || 
             (this.resultado!=null &&
              this.resultado.equals(other.getResultado()))) &&
            ((this.resultadoCatel==null && other.getResultadoCatel()==null) || 
             (this.resultadoCatel!=null &&
              this.resultadoCatel.equals(other.getResultadoCatel())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getMsgHost() != null) {
            _hashCode += getMsgHost().hashCode();
        }
        if (getNumAutorizacion() != null) {
            _hashCode += getNumAutorizacion().hashCode();
        }
        if (getObjeto() != null) {
            _hashCode += getObjeto().hashCode();
        }
        if (getReferencia() != null) {
            _hashCode += getReferencia().hashCode();
        }
        if (getRespuesta() != null) {
            _hashCode += getRespuesta().hashCode();
        }
        if (getResultado() != null) {
            _hashCode += getResultado().hashCode();
        }
        if (getResultadoCatel() != null) {
            _hashCode += getResultadoCatel().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Respuesta.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://ws/", "respuesta"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("msgHost");
        elemField.setXmlName(new javax.xml.namespace.QName("", "msgHost"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("numAutorizacion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "numAutorizacion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("objeto");
        elemField.setXmlName(new javax.xml.namespace.QName("", "objeto"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "anyType"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("referencia");
        elemField.setXmlName(new javax.xml.namespace.QName("", "referencia"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respuesta");
        elemField.setXmlName(new javax.xml.namespace.QName("", "respuesta"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("resultadoCatel");
        elemField.setXmlName(new javax.xml.namespace.QName("", "resultadoCatel"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
