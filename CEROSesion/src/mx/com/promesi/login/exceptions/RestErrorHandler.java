package mx.com.promesi.login.exceptions;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.mx.constantes.Constantes;


@ControllerAdvice	
public class  RestErrorHandler {
	
	  private static final Logger LOG = Logger.getLogger(RestErrorHandler.class);
	
	@ExceptionHandler(Exception.class)
    @ResponseStatus(HttpStatus.OK)
    public void handleTodoNotFoundException(HttpServletRequest req,HttpServletResponse response,Exception exception) {
		
		
			LOG.error("Se maneja Error, y se manda respuesta:"+exception.getMessage());
			response.setHeader(Constantes.HEADER_STATUS, Constantes.COD_STATUS_ERROR);
			response.setHeader(Constantes.HEADER_MESSAGE, exception.getMessage());		
			response.setHeader(Constantes.HEADER_CONTET, Constantes.CONTENT_TYPE);		
			exception.printStackTrace();
    }
	
		
}
