package mx.com.promesi.login.util;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import mx.com.promesi.beans.AppBean;

@Service
public class Apps {
	
	private static final Object LOCK = new Object();
	private AppProperties appProperties = null;
	
	
	@Scheduled(fixedRateString = "${period}")
	public void init(){
		Properties datos = null;
		try {
			datos = LoadProperties.getInstance().getProperties("apps");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}		
		
		synchronized (LOCK) {
			if(datos!=null){
				this.appProperties = new AppProperties(datos);
			}
		}
	}
	
	public AppProperties getAppProperties(){
		AppProperties _appProperties = null;
		synchronized (LOCK) {
			if(this.appProperties == null){
				init();
			}
			_appProperties = this.appProperties;
			
		}
		return _appProperties;
	}
	
	public class AppProperties{
		
		LinkedHashMap<String,AppBean> appMap = new LinkedHashMap<>(); 
		List<AppBean> lstApps = Collections.emptyList();
	
		private AppProperties(Properties datos){
			Set<Object> keySet = null;
			
			lstApps = new ArrayList<AppBean>();
			AppBean app = null;
			String nombreApp = "";
			keySet = datos.keySet();
			
			for(Object appId:keySet){
				if(!"LAST_MODIFIED".equals(appId)){
					if(datos.containsKey(appId)){
						try {
							nombreApp = datos.getProperty(appId.toString());
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						app = new AppBean(appId.toString(), nombreApp);
						appMap.put(nombreApp, app);
						lstApps.add(app);
					}
				}
			}
		}

		public AppBean getApp(String appId){
			return appMap.get(appId);
		}
		
		
		public List<AppBean> getColeccionApps() {
			return lstApps;
		}
		
	}
	
	

}
