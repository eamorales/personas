package mx.com.promesi.login.util;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;
import java.util.Set;

import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

@Service
public class WebConfig {
	
	private static final Object LOCK = new Object();
	private WebConfigProperties webConfigProperties = null;
	
	
	@Scheduled(fixedRateString = "${period}")
	public void init(){
		Properties datos = null;
		try {
			datos = LoadProperties.getInstance().getProperties("Webconfig");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}		
		
		synchronized (LOCK) {
			if(datos!=null){
				this.webConfigProperties = new WebConfigProperties(datos);
			}
		}
	}
	
	public WebConfigProperties getWebConfigProperties(){
		WebConfigProperties _webConfigProperties = null;
		synchronized (LOCK) {
			if(this.webConfigProperties == null){
				init();
			}
			_webConfigProperties = this.webConfigProperties;
			
		}
		return _webConfigProperties;
	}
	
	public class WebConfigProperties{
		
		LinkedHashMap<String,String> appMap = new LinkedHashMap<>(); 
		List<String> lstApps = Collections.emptyList();
	
		private WebConfigProperties(Properties datos){
			Set<Object> keySet = null;
			
			lstApps = new ArrayList<String>();
			String data = "";
			keySet = datos.keySet();
			
			for(Object idWebConfig:keySet){
				if(!"LAST_MODIFIED".equals(idWebConfig)){
					if(datos.containsKey(idWebConfig)){
						try {
							data = datos.getProperty(idWebConfig.toString());
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						appMap.put(idWebConfig.toString(), data);
						lstApps.add(data);
					}
				}
			}
		}

		public String getWebConfig(String webConfig){
			return appMap.get(webConfig);
		}
		
		
		public List<String> getColeccionWebConfig() {
			return lstApps;
		}
		
	}
	
	

}
