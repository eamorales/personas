package mx.com.promesi.login.util;

import java.io.File;
import java.io.FileFilter;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

import org.springframework.stereotype.Service;


@Service
public class LoadProperties {
	private static String RUTAS = "/mx/com/promesi/login/config/rutas.properties";
	
	
	/**Constante del tipo Object que sirve como candado y 
	 * sirve para sincronizar los thread con el cache*/
	private static Object LOCK = new Object();
	/**Propiedad privada del tipo Map<String,Properties> que sirve como cache para 
	 * almacenar los properties
	 */
	private Map<String,Properties> cache = new HashMap<String,Properties>();
	/**Constante que contiene la extension de los archivos permitidos (*.properties).
	 */
	private static String FILE_EXTENSION = ".properties";
	/**Constante del tipo String que tiene el nombre del property que contendra
	 * el tiempo para comparar la fecha de modificacion
	 */
	private static String LAST_MODIFIED = "LAST_MODIFIED";
	/**Constante del tipo long que contiene el tiempo de retardo en milisegundos
	 */
	private static long DELAY = 47300;
	/**Propiedad privada del tipo Timer que permite sincronizar cada determinado tiempo
	 * los archivos de propiedad
	 */
	private Timer timer;
	
	/**Constante del tipo String que contiene la expresion regular para validar el nombre de los archivos
	 */
	private static String REG_EXP_EXT_FILE = "([a-zA-Z0-9|\\.| |\\-||\\(\\)])+(properties)$";

	/**Propiedad estatica privada del tipo LoadProperties
	 */
	private static LoadProperties loadProperties = new LoadProperties();
	
	/**
	 * Metodo que sirve para obtene una instancia de la clase LoadProperties aplicando el patron singleton
	 * @return LoadProperties
	 */
	public static LoadProperties getInstance(){
		return loadProperties;
	}
	/**
	 * Constructor privado que inicializa el objeto y el Timer
	 */
	private LoadProperties(){
	    super();
	    initLoad();
	    final LoadProperties obj = this;
	    TimerTask timerTask = new TimerTask(){
	    	public void run(){
	    		obj.initLoad();
	        }
	    };
	    timer = new Timer();
	    timer.scheduleAtFixedRate(timerTask, 0, DELAY);
	    
	}

	
	private boolean matchFile(File file){
		boolean flag = false;
		if(file != null && file.isFile()){
			if(file.getName().matches(REG_EXP_EXT_FILE)){
				flag = true;
			}
		}
		return flag;
	}
	
	private List<File> obtieneArchivos(File file){
		List<File> listFile = Collections.emptyList();
		File[] arrayFiles = null;
		arrayFiles = file.listFiles(new FileFilter(){
			@Override
			public boolean accept(File pathname) {
				boolean flag = false;
				if(pathname.isFile()){
					flag = matchFile(pathname);
				}
				return flag;
			}
			
		});
		if(arrayFiles!= null && arrayFiles.length>0){
			listFile = Arrays.asList(arrayFiles);
		}
		return listFile;
	}
	
	
	private void initLoad(){
		List<File> listFiles = new ArrayList<File>();
		List<File> listFilesTemp = Collections.emptyList();
		String fileName = "";
		Properties properties = new Properties();
		Properties propertiesTemp = null;
		String valueProperty = null;
		File file = null;
		Set<String> setKey = Collections.emptySet();
		InputStream inputStream = null;
		try {                                                  
			inputStream = this.getClass().getResourceAsStream(RUTAS);
			if(inputStream!= null){
				properties.load(inputStream);
			
				inputStream.close();
				inputStream = null;
			}
			setKey = properties.stringPropertyNames();
			for(String obj: setKey){
				
				valueProperty = properties.getProperty(obj);
				if(valueProperty!= null && !"".equals(valueProperty)){
					file = new File(valueProperty.trim());
					if(file.exists()){
						if(file.isDirectory()){
							listFilesTemp = obtieneArchivos(file);
							listFiles.addAll(listFilesTemp);
						}else if(file.isFile()){
							if(matchFile(file)){
								listFiles.add(file);
							}
						}
					}
				}
			}
			for(File fileTemp:listFiles){
				fileName = fileTemp.getName().substring(0,fileTemp.getName().indexOf(FILE_EXTENSION));
				synchronized (LOCK) {
					if(cache.containsKey(fileName)){						
						properties = cache.get(fileName);
						Long  last_modified = (Long)properties.get(LAST_MODIFIED);
						if(fileTemp.lastModified() > last_modified.longValue() ){
							propertiesTemp = new Properties();
							inputStream = new FileInputStream(fileTemp);
							propertiesTemp.load(inputStream);
							inputStream.close();
							inputStream = null;
							propertiesTemp.put(LAST_MODIFIED, fileTemp.lastModified());
							cache.put(fileName, propertiesTemp);
						}
					}else{
						propertiesTemp = new Properties();
						inputStream = new FileInputStream(fileTemp);
						propertiesTemp.load(inputStream);
						inputStream.close();
						inputStream = null;
						propertiesTemp.put(LAST_MODIFIED, fileTemp.lastModified());
						cache.put(fileName, propertiesTemp);
					}
				}
				
			
				
			}
		} catch (IOException e) {
			e.printStackTrace();
			if(inputStream!= null){
				try {
					inputStream.close();
				} catch (IOException e1) {
					e1.printStackTrace();
				}
				inputStream = null;
			}
		}
	}
	
	private void cancel(){
		timer.cancel();
	}
	
	public Properties getProperties(String fileName) throws FileNotFoundException {
		Properties config = null;
		synchronized (LOCK) {
			if(cache.containsKey(fileName)){
				config = cache.get(fileName);
			}else{
				throw new FileNotFoundException("No se encuentra el archivo de configuracion: "+fileName);
			}
		}
		return config;	
	}
	
	public String getProperty(final String fileName, final String propertyName)throws FileNotFoundException  {
		String valorProperty = null;
		Properties config = null;
		synchronized (LOCK) {
			if(cache.containsKey(fileName)){
				config = cache.get(fileName);
				valorProperty = config.getProperty(propertyName);
			}else{
				throw new FileNotFoundException("No se encuentra el archivo de configuracion: "+fileName);
			}
		}
		return valorProperty;
	}
	
	
	public void fin(){
		cancel();
	}
	
	
}
