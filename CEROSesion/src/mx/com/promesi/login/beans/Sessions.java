package mx.com.promesi.login.beans;

import java.io.Serializable;

import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

import mx.com.promesi.beans.UserSession;

@Component
@Scope(scopeName="session",proxyMode= ScopedProxyMode.TARGET_CLASS)
public class Sessions implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 3935331586221008579L;
	
	private UserSession sesion;
	
	public Sessions() {
		// TODO Auto-generated constructor stub
	}

	public UserSession getSesion() {
		return sesion;
	}

	public void setSesion(UserSession sesion) {
		this.sesion = sesion;
	}
	
	
}