/*
 * @(#)SeTransaccionResponse.java 1.0 10/03/17 
 * 
 */
package mx.com.promesi.login.beans;


import java.io.Serializable;

/**
 * Respuesta del servicio de mantenimiento de la tabla seTransaccion.
 * @author Inver-Tu
 * @version 1.0 10/03/17
 */
public class SeTransaccionResponse extends SeResponse implements Serializable{
	/** Numero de serializacion */
	private static final long serialVersionUID = 1L;
	/** Arreglo de Transacciones */
    private SeTransaccionResult[] resultados;
	/**
	 * @return the resultados
	 */
	public SeTransaccionResult[] getResultados() {
		return resultados;
	}
	/**
	 * @param resultados the resultados to set
	 */
	public void setResultados(SeTransaccionResult[] resultados) {
		this.resultados = resultados;
	}
}
