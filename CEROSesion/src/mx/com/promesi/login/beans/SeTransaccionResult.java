/*
 * @(#)SeTerminalResult.java 1.0 10/03/17 
 * 
 */
package mx.com.promesi.login.beans;


import java.util.Date;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * DTO para la consulta de la tabla seTerminal
 * @author Inver-Tu
 * @version 1.0 10/03/17
 */
@XmlRootElement
public class SeTransaccionResult implements java.io.Serializable {
	/** Numero de serializacion */
	private static final long serialVersionUID = 1L;
	/** Identificador del registro */
	private long id;
	/** Estatus del Terminal */
	private String seestatus;
	/** Usuario quien creo el registro */
	private long usuarioCreacion;
	/** Fecha de creacion */
	private Date fechaCreacion;
	/** Usuario quien modifico el registro */
	private Long usuarioModificacion;
	/** Fecha de modificacion */
	private Date fechaModificacion;
	/** Nombre del Terminal */
	private String nombre;
	/** Descripcion del Terminal */
	private String descripcion;

	/**
	 * Constructor
	 */
	public SeTransaccionResult() {
	}

	/**
	 * Constructor con parametros
	 * @param id Identificador de Terminal
	 * @param usuarioCreacion Usuario de creacion
	 * @param fechaCreacion Fecha de creacion
	 */
	public SeTransaccionResult(long id, long usuarioCreacion, Date fechaCreacion) {
		this.id = id;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaCreacion = fechaCreacion;
	}
	/**
	 * Constructor con parametros
	 * @param id Identificador de Terminal
	 * @param seestatus Esatus del Terminal
	 * @param usuarioCreacion Usuario de creacion
	 * @param fechaCreacion Fecha de creacion
	 * @param usuarioModificacion Usuario de modificacion
	 * @param fechaModificacion Fecha de modificacion
	 * @param nombre Nombre del Terminal
	 * @param descripcion Descripcion del Terminal
	 */
	public SeTransaccionResult(long id, String seestatus, long usuarioCreacion,
			Date fechaCreacion, Long usuarioModificacion,
			Date fechaModificacion, String nombre, String descripcion) {
		this.id = id;
		this.seestatus = seestatus;
		this.usuarioCreacion = usuarioCreacion;
		this.fechaCreacion = fechaCreacion;
		this.usuarioModificacion = usuarioModificacion;
		this.fechaModificacion = fechaModificacion;
		this.nombre = nombre;
		this.descripcion = descripcion;
	}

	/**
	 * Obtiene el identificador del Terminal
	 * @return id
	 */
	public long getId() {
		return this.id;
	}
	/**
	 * Define el identificador del Terminal
	 * @param id identificador del Terminal
	 */
	public void setId(long id) {
		this.id = id;
	}
	/**
	 * Obtiene el estatus
	 * @return estatus
	 */
	public String getSeestatus() {
		return this.seestatus;
	}
	/**
	 * Define el estatus
	 * @param seestatus estatus
	 */
	public void setSeestatus(String seestatus) {
		this.seestatus = seestatus;
	}
	/**
	 * Obtiene el usuario de creacion
	 * @return usuario de creacion
	 */
	public long getUsuarioCreacion() {
		return this.usuarioCreacion;
	}
	/**
	 * Define el  usuario de creacion
	 * @param usuarioCreacion usuario de creacion
	 */
	public void setUsuarioCreacion(long usuarioCreacion) {
		this.usuarioCreacion = usuarioCreacion;
	}
	/**
	 * Obtiene la fecha de creacion
	 * @return la fecha de creacion
	 */
	public Date getFechaCreacion() {
		return this.fechaCreacion;
	}
	/**
	 * Define la fecha de creacion
	 * @param fechaCreacion la fecha de creacion
	 */
	public void setFechaCreacion(Date fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	/**
	 * Obtiene el usuario de modificacion
	 * @return usuario de modificacion
	 */
	public Long getUsuarioModificacion() {
		return this.usuarioModificacion;
	}
	/**
	 * Define el  usuario de modificacion
	 * @param usuarioModificacion usuario de modificacion
	 */
	public void setUsuarioModificacion(Long usuarioModificacion) {
		this.usuarioModificacion = usuarioModificacion;
	}
	/**
	 * Obtiene la fecha de modificacion
	 * @return la fecha de modificacion
	 */
	public Date getFechaModificacion() {
		return this.fechaModificacion;
	}
	/**
	 * Define la fecha de modificacion
	 * @param fechaModificacion la fecha de modificacion
	 */
	public void setFechaModificacion(Date fechaModificacion) {
		this.fechaModificacion = fechaModificacion;
	}
	/**
	 * Obtiene el nombre del Terminal
	 * @return nombre del Terminal
	 */
	public String getNombre() {
		return this.nombre;
	}
	/**
	 * Define el  nombre del Terminal
	 * @param nombre nombre del Terminal
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	/**
	 * Obtiene la descripcion del Terminal
	 * @return la descripcion del Terminal
	 */
	public String getDescripcion() {
		return this.descripcion;
	}
	/**
	 * Define la descripcion del Terminal
	 * @param descripcion la descripcion del Terminal
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


}
