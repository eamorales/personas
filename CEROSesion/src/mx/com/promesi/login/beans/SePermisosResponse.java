package mx.com.promesi.login.beans;

import java.io.Serializable;
 

public class SePermisosResponse implements Serializable{
	/** Numero de serializacion */
	private static final long serialVersionUID = 1L;
//	private SeTransaccionResponse seTransaccionResponse;
	private Usuario usuario;
	private ContrasenaUsuario contrasenaUsuario;
	/**
	 * @return the seTransaccionResponse
	 */
//	public SeTransaccionResponse getSeTransaccionResponse() {
//		return seTransaccionResponse;
//	}
//	/**
//	 * @param seTransaccionResponse the seTransaccionResponse to set
//	 */
//	public void setSeTransaccionResponse(SeTransaccionResponse seTransaccionResponse) {
//		this.seTransaccionResponse = seTransaccionResponse;
//	}
	/**
	 * @return the usuario
	 */
	public Usuario getUsuario() {
		return usuario;
	}
	/**
	 * @param usuario the usuario to set
	 */
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	/**
	 * @return the contrasenaUsuario
	 */
	public ContrasenaUsuario getContrasenaUsuario() {
		return contrasenaUsuario;
	}
	/**
	 * @param contrasenaUsuario the contrasenaUsuario to set
	 */
	public void setContrasenaUsuario(ContrasenaUsuario contrasenaUsuario) {
		this.contrasenaUsuario = contrasenaUsuario;
	}
}
