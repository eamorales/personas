/**
 * 
 */
package mx.com.promesi.login.controller;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.rmi.RemoteException;
import java.sql.Timestamp;

import javax.xml.rpc.ServiceException;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mx.constantes.Constantes;
import BeanRespuesta.RespuestaBase;
import BeanRespuesta.ValidaServicioToken;
import mx.com.promesi.beans.AppBean;
import mx.com.promesi.beans.AppSessions;
import mx.com.promesi.beans.DataBean;
import mx.com.promesi.beans.UserSession;
import mx.com.promesi.login.beans.SePermisosResponse;
import mx.com.promesi.login.token.Respuesta;
import mx.com.promesi.login.token.WS2FA;
import mx.com.promesi.login.token.WS2FAProxy;
import mx.com.promesi.login.token.WS2FAService;
import mx.com.promesi.login.token.WS2FAServiceLocator;
import mx.com.promesi.login.util.Apps;
import mx.com.promesi.login.util.Apps.AppProperties;
import mx.com.promesi.login.util.WebConfig;
import mx.com.promesi.login.util.WebConfig.WebConfigProperties;
import mx.com.promesi.rest.RestJson;
import mx.com.promesi.util.UrlConstantes;
import mx.com.promesi.util.encrypt.AppCrypt;
import net.cero.capacontrol.datos.HeaderWS;
import net.cero.seguridad.utilidades.CapaControlReqt;
import net.cero.seguridad.utilidades.CapaControlResp;


@RestController
public class LoginController {
	
	
	@Autowired
	private AppSessions appsSesion;

	@Autowired
	private Apps apps;
	
	@Autowired
	private WebConfig webConfig;
	
	private static final String urlObtenIDRefencia="urlObtenIDRefencia";
	private static final Logger LOG = Logger.getLogger(LoginController.class); // Implementación del LOG

	public SePermisosResponse consultarPermisosDelUsuario(String clave, String urlservice) {
		SePermisosResponse servicio = new SePermisosResponse();
		try {
			URL url = new URL(urlservice+clave);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			while ((output = br.readLine()) != null) {
				LOG.info("ValidaServicioToken" + output);
				Timestamp t = new Timestamp(System.currentTimeMillis());
				GsonBuilder gsonBuilder = new GsonBuilder();
	            gsonBuilder.setDateFormat("yyyy-MM-dd hh:mm:ss.S").create().toJson(t);
	            Gson gson = gsonBuilder.create();
//				Gson gson = new Gson();
				servicio = gson.fromJson(output, SePermisosResponse.class);
			}
		} catch (Exception e) {
			LOG.info(e);
		}
		return servicio;

	}
	public static ValidaServicioToken validaServiceToken(String urlws) {
		ValidaServicioToken servicio = new ValidaServicioToken();
		
		return servicio;
	}
	@RequestMapping(value ="login",method = RequestMethod.POST)
	@ResponseBody
	public RespuestaBase login(@RequestBody UserSession data) {
		WebConfigProperties properties =webConfig.getWebConfigProperties();
		SePermisosResponse respuesta =consultarPermisosDelUsuario(data.getClaveUsuario(),data.getUrlService());
		data.setNo_empleado(respuesta.getUsuario().getNoEmpleado()+"");
		data.setUsuarioid(respuesta.getUsuario().getUsuarioId()+"");
		appsSesion.getSesiones().put(data.getClaveUsuario(), data);
		return new RespuestaBase("00000","Ejecucion Perfecta de Servicio de Login::"+appsSesion.getSesiones().size());
//		HeaderWS hdr = new HeaderWS();
//		hdr.setIdCanalAtencion(Long.valueOf(properties.getWebConfig("idCanalAtencion")));
//		hdr.setIdSucursal(Long.valueOf(properties.getWebConfig("idSucursal")));
//		hdr.setIpHost(properties.getWebConfig("ipHost"));
//		hdr.setNameHost(properties.getWebConfig("nameHost"));
//		hdr.setUsuarioClave(data.getClaveUsuario());
//		hdr.setTerminal(properties.getWebConfig("terminal"));
//		hdr.setIdTransaccion(Long.valueOf(properties.getWebConfig("idTransaccion")));
//		
//		
//		hdr.setIdReferencia("");
//		
		
		
//		StringBuilder url=new StringBuilder("/ldap/autentica/");
//		url.append(data.getClaveUsuario());
//		url.append(Constantes.SLASH);
//		url.append(data.getContrasenia());
//		
//		RestJson<CapaControlReqt, CapaControlResp> rest=new RestJson<>();
//		rest.setUrl(url.toString());
//		
//		
//		StringBuilder url2=new StringBuilder("/permisos/consulta/");
//		url2.append(data.getClaveUsuario());
//		
//		
			
	}
	@RequestMapping(value ="cerrarsession",method = RequestMethod.POST)
	@ResponseBody
	public void cerrarSession(UserSession data) {
		appsSesion.getSesiones().remove(data.getClaveUsuario());
	}
	
	@RequestMapping(value="enviaToken", method = RequestMethod.POST)
	@ResponseBody
	public RespuestaBase enviaToken(@RequestBody UserSession dataTemporal){
		UserSession  data = appsSesion.getSesiones().get(dataTemporal.getClaveUsuario());
		WS2FAService ws = new WS2FAServiceLocator();
		WS2FA envia2fa = new WS2FAProxy();
		String noEmpleado =data.getNo_empleado();
		String usuarioCero =data.getUsuarioid();
		String envia="";
		String respuesta = "";
		try {
			envia2fa = ws.getWS2FAPort();
		} catch (ServiceException e) {
			LOG.info(e);
		}
		try {
			envia = envia2fa.envia2FA (noEmpleado,	usuarioCero, usuarioCero);
			LOG.info("respuesta::enviarToken"+envia);
		} catch (RemoteException e) {
			LOG.info(e);
		}
		Gson gson = new Gson();
		Respuesta respuesta2fa = gson.fromJson(envia, Respuesta.class);
		
		if(respuesta2fa.getResultado() == 0){
			return new RespuestaBase("00000",respuesta);
		} else {
			respuesta = respuesta2fa.getMsgHost();
				return new RespuestaBase("00001",respuesta);
		}
	}
	@RequestMapping(value ="validaToken",method = RequestMethod.POST)
	@ResponseBody
	public RespuestaBase validaToken(@RequestBody UserSession dataTemporal) {
			UserSession data = appsSesion.getSesiones().get(dataTemporal.getClaveUsuario());
			data.setToken(dataTemporal.getToken());
			String respuesta = "";
			String secfactor = data.getToken();
			String noEmpleado =data.getNo_empleado();
			String valida="";
			WS2FAService ws = new WS2FAServiceLocator();
			WS2FA envia2fa = new WS2FAProxy();
			try {
				envia2fa = ws.getWS2FAPort();
			} catch (ServiceException e) {
				LOG.info(e);
			}
			try {
				valida = envia2fa.valida2FA (secfactor, noEmpleado);
			} catch (RemoteException e) {
				LOG.info(e);
					
			}
			Gson gson = new Gson();
			Respuesta respuesta2fa = gson.fromJson(valida, Respuesta.class);
			
			if(respuesta2fa.getResultado() == 0){
				respuesta =respuesta2fa.getMsgHost();
				return new RespuestaBase("00000",respuesta);
			} else {
				respuesta = respuesta2fa.getMsgHost();
				if(data.getToken().equals("111111")) { /**Se crea dummy para poder acceder*/
					return new RespuestaBase("00000","Se ha ingresado utilizando el Dummy");
				}
					return new RespuestaBase("00001",respuesta);
			}
			
		
		
	}
	
	
	private String getIdReferencia(String claveUsuario){
		WebConfigProperties properties =webConfig.getWebConfigProperties();
		
		HeaderWS hdr = new HeaderWS();
		hdr.setIdCanalAtencion(Long.valueOf(properties.getWebConfig("idCanalAtencion")));
		hdr.setIdSucursal(Long.valueOf(properties.getWebConfig("idSucursal")));
		hdr.setIpHost(properties.getWebConfig("ipHost"));
		hdr.setNameHost(properties.getWebConfig("nameHost"));
		hdr.setUsuarioClave(claveUsuario);
		hdr.setTerminal(properties.getWebConfig("terminal"));
		hdr.setIdTransaccion(Long.valueOf(properties.getWebConfig("idTransaccion")));
		CapaControlReqt cReq=new CapaControlReqt();
		cReq.setHeader(hdr);
		
		StringBuilder urlRefencia=new StringBuilder(urlObtenIDRefencia);
		
		RestJson<CapaControlReqt, CapaControlResp> rest=new RestJson<>();
		rest.setUrl(urlRefencia.toString());
		rest.setEntrada(cReq);
		
		return claveUsuario;
	}
	
	@RequestMapping(value = UrlConstantes.OBTENSESION)
	@ResponseBody
	public UserSession getSesion(@RequestBody AppBean app) {

		AppProperties properties =apps.getAppProperties();
		
		DataBean data=AppCrypt.decryptApp(app.getCodigo());
		AppBean n=properties.getApp(data.getIdApp());
		
		if(n!=null){
//			UserSession appSe=appsSesion.getSesiones().get(data.getIdUser());
			UserSession appSe=appsSesion.getSesiones().get("gustavo");
			if(appSe==null){
				appSe=new UserSession(Constantes.COD_ERROR_99998,"Sesion Invalida");
			}else{
				appSe.setCodigoError(Constantes.COD_ERROR_00000);
			}
			return appSe;
		}else{
			return new UserSession(Constantes.COD_ERROR_99999,"Codigo Aplicacion Invalido");
		}
	}
	
	@RequestMapping(value ="error")
	@ResponseBody
	public RespuestaBase errorSesion() {
		System.out.println("ErrorSesion");
		return new RespuestaBase("00000", "");
	}

	@RequestMapping(value =UrlConstantes.VERIFICASESION)
	@ResponseBody
	public RespuestaBase verifySesion(@RequestBody AppBean app) {
		AppProperties properties =apps.getAppProperties();
		
		DataBean data=AppCrypt.decryptApp(app.getCodigo());
		AppBean n=properties.getApp(data.getIdApp());
		
		if(n!=null){
			UserSession appSe=appsSesion.getSesiones().get(data.getIdUser());
			if(appSe==null){
				return new RespuestaBase(Constantes.COD_ERROR_99998,"No Existe Sesion");
			}else{
				return new RespuestaBase(Constantes.COD_ERROR_00000,"Sesion Existe");
			}
		}else{
			return new RespuestaBase(Constantes.COD_ERROR_99999,"No se encontro Aplicacion");
		}
	}
	

	
}
