package mx.com.promesi.login.controller;

import mx.com.promesi.exceptions.BusinessException;
import mx.com.promesi.persistencia.ClienteDao;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.mx.Res.Respuesta;
import com.mx.beans.ErrorDTO;
import com.mx.beans.Busqueda.BeanClienteEntrada;
import com.mx.constantes.Constantes;
import com.mx.url.UrlService;

/**
 * @author EAMA
 *
 */
@RestController
@RequestMapping(UrlService.BUSQUEDA_CLIENTE)
public class BusquiedaServiceREST {
	private static final Logger logger = Logger.getLogger(BusquiedaServiceREST.class);

	@Autowired
	private ClienteDao clienteDao;

	@RequestMapping(value = UrlService.BUSQUEDA_CLIENTE_ID)
	@ResponseBody
	public Respuesta buscaClientePorId(@RequestBody BeanClienteEntrada noCliente) {
		Respuesta resCliente = new Respuesta();
		ErrorDTO error = new ErrorDTO();
		logger.info("Consulta Busqueda cliente rest");
		try {
			resCliente.setCliente(clienteDao.consultaClientePorId(noCliente.getNoCliente()));
			error.setClave("0");
		} catch (BusinessException e) {
			logger.error("ha ocurrido un error al consultar la busqueda del cliente");
			error.setClave("60");
			error.setMessage("ERROR EN C AL BUSCAR DATOS CLIENTE");
		}
		resCliente.setError(error);
		return resCliente;
	}

	@RequestMapping(value = UrlService.BUSQUEDA_CLIENTE_AVANZADA)
	@ResponseBody
	public Respuesta buscaClientePorAvanzada(@RequestBody BeanClienteEntrada bClienteEntrada) {
		Respuesta resCliente = new Respuesta();
		ErrorDTO error = new ErrorDTO();
		logger.info("Consulta Busqueda avanzada cliente rest");
		try {
			resCliente.setCliente(clienteDao.consultaClientePorParametros(bClienteEntrada));
			error.setClave("0");
		} catch (BusinessException e) {
			logger.error("ha ocurrido un error al consultar la busqueda del cliente");
			error.setClave("60");
			error.setMessage("ERROR EN C AL BUSCAR DATOS CLIENTE");
		}
		resCliente.setError(error);
		return resCliente;
	}
	
	@RequestMapping(value = UrlService.BUSQUEDA_CLIENTE_ACT_CORREO)
	@ResponseBody
	public Respuesta actualizarCorreo(@RequestBody BeanClienteEntrada bClienteEntrada) {
		Respuesta resCliente = new Respuesta();
		ErrorDTO error = new ErrorDTO();
		logger.info("Actualiza correo cliente rest");
		try {
			int respuesta = clienteDao.actualizaCorreoCliente(bClienteEntrada);
			if(respuesta == 0){
				error.setClave("0");
			}else{
				error.setClave("0");
				error.setMessage("ERROR EN C AL ACTUALIZAR CORREO DEL  CLIENTE");
			}
			
		} catch (BusinessException e) {
			logger.error("ha ocurrido un error al consultar la busqueda del cliente");
			error.setClave("60");
			error.setMessage("ERROR EN C AL ACTUALIZAR CORREO DEL  CLIENTE");
		}
		resCliente.setError(error);
		return resCliente;
	}
}