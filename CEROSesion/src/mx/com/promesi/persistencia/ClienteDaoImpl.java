/**
 * 
 */
package mx.com.promesi.persistencia;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.List;

import javax.annotation.Resource;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import com.mx.beans.BeanDatosGenerales;
import com.mx.beans.BeanInformacion;
import com.mx.beans.Busqueda.BeanCliente;
import com.mx.beans.Busqueda.BeanClienteEntrada;

import mx.com.promesi.exceptions.BusinessException;


/**
 * @author EAMA
 *
 */
@Repository
public class ClienteDaoImpl implements ClienteDao {

	private NamedParameterJdbcTemplate namedJdbcTemplate;

	@Resource(name = "DataSource")
	public void setDataSource(DataSource dataSource) {
		this.namedJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
	}

	static String schema = "personas.";

	final static Logger logger = Logger.getLogger(ClienteDaoImpl.class);

	private class ClienteMapper implements RowMapper<BeanCliente> {

		@Override
		public BeanCliente mapRow(ResultSet rs, int rowNum) throws SQLException {
			BeanCliente beanCliente = new BeanCliente();
			BeanInformacion beanInformacion = new BeanInformacion();
			BeanDatosGenerales beandatosGenerales = new BeanDatosGenerales();
		
			beanInformacion.setNoCliente((long) rs.getObject("id"));

			beanInformacion.setNombre(rs.getString("nombre"));

			beandatosGenerales.setPrimerNombre(rs.getString("nombre"));

			beandatosGenerales.setSegundoNombre(rs.getString("segundoNombre"));
			logger.debug("Informacion del rs.getString(segundoNombre): " + rs.getString("segundoNombre"));
			beandatosGenerales.setPrimerApellido(rs.getString("apaterno"));
			beandatosGenerales.setSegundoApellido(rs.getString("amaterno"));
			beanInformacion.setTipocliente(rs.getString("tipo_cliente"));

			beanInformacion.setDireccion(rs.getString("direccion"));
			
			beanInformacion.setSucursalOrigen(rs.getString("idSucursal"));

			beanInformacion.setPersonalidad(rs.getString("idTipoPersona"));
			beandatosGenerales.setPersonalidad(rs.getString("tipoPersona"));
			//TODO tipo persona esta definido en la tabla con un int y en este caso esta como un boolean
			//beandatosGenerales.setTipoPersona(tipoPersona);

			beanInformacion.setCelular(rs.getString("telefono"));

			beandatosGenerales.setCorreoElectronico(rs.getString("correo"));
			beanInformacion.setCorreo(rs.getString("correo"));

			// TODO Se modifico bean datos generales por que marcaba error con los tipos definidos para sexo
			beanInformacion.setSexo(rs.getString("nombre"));
			beandatosGenerales.setFechaNacimiento(rs.getString("fechaNac"));
			int edad = calcularEdad(beandatosGenerales.getFechaNacimiento(), "yyyy-MM-dd");
			beanInformacion.setEdad(edad);

			beanCliente.setBeanInformacion(beanInformacion);
			beanCliente.setDatosGenerales(beandatosGenerales);
			logger.debug("Informacion del mapper: " + beanCliente.toString());
			return beanCliente;
		}

		/**
		 * Calcula la edad en base a la fecha de nacimiento
		 * 
		 * @param fechaNacimiento
		 * @param formato
		 *            formato de fecha ej: yyyy-MM-dd
		 * @return int edad en años
		 */
		private int calcularEdad(String fechaNacimiento, String formato) {
			logger.debug("datos entrada fecha nacimiento" + fechaNacimiento + " formato: " + formato);
			DateTimeFormatter fmt = DateTimeFormatter.ofPattern(formato);
			LocalDate fechaNac = LocalDate.parse(fechaNacimiento, fmt);
			LocalDate ahora = LocalDate.now();
			Period periodo = Period.between(fechaNac, ahora);
			int edad = periodo.getYears();
			logger.debug("edad: " + edad);
			return edad;
		}

	}

	@Override
	public BeanCliente insertaCliente(BeanClienteEntrada BeanCliente) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BeanCliente eliminaCliente(BeanCliente BeanCliente) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public BeanCliente consultaClientePorId(long numeroCliente) throws BusinessException {
		logger.info("consultaClientePorId");
		BeanCliente cliente = null;
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("id", numeroCliente);
		StringBuffer sql = new StringBuffer("");
		sql.append("SELECT a.id, \"idTipoPersona\" ,\"tipoPersona\",nombre, apaterno, amaterno, \"idSucursal\", \"idTipoCliente\",");
		sql.append("  descripcion tipo_cliente, \"segundoNombre\", direccion, telefono, sexo, \"fechaNac\" , correo ");
		
		sql.append(" FROM personas.\"PePersona\" a LEFT JOIN personas.\"PeTipoCliente\" b on a.\"idTipoCliente\" = b.clave ");
		sql.append(" LEFT JOIN personas.\"PeDatosPersonales\" c on a.id = c.\"idPersona\"");
		sql.append(" LEFT JOIN  personas.\"PeDireccion\" d on a.id = d.\"idPersona\"");
		sql.append(" LEFT JOIN  personas.\"PeTelefono\" e on a.id = e.\"idPersona\" ");
		sql.append(" LEFT JOIN  personas.\"PeDatosAdicionales\" f on a.id = f.\"idPersona\"");
		sql.append(" LEFT JOIN  personas.\"PeTipoPersona\" g on a.\"idTipoPersona\" = g.id");
		sql.append(" where  a.id = :id");
		sql.append(" and (e.\"idTipoTel\" = (select id from personas.\"NcTipoTel\" e where \"tipoTelefono\" like ('celular'))");
		sql.append("or not EXISTS ( select  e.\"idTipoTel\" from personas.\"PeTelefono\" where a.id = e.\"idPersona\"  ) )");
		
		logger.info(sql);
		List<BeanCliente> clientes = namedJdbcTemplate.query(sql.toString(), namedParameters, new ClienteMapper());
		if (clientes.size() > 0) {
			cliente = clientes.get(0);
			cliente.getBeanInformacion().setCodigoError("0");
		}
		return cliente;
	}

	@Override
	public BeanCliente consultaClientePorParametros(BeanClienteEntrada datoEntrada) throws BusinessException {
		logger.info("consultaClientePorParametros");
		BeanCliente cliente = null;
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		StringBuffer sql = new StringBuffer("");
		if (!"".equals(datoEntrada.getPrimerNombre()) && datoEntrada.getPrimerNombre()!=null && !"".equals(datoEntrada.getApellidoPaterno())
				&& datoEntrada.getApellidoPaterno()!= null && !"".equals(datoEntrada.getApellidoMaterno())
				&& datoEntrada.getApellidoMaterno() != null) {
			namedParameters.addValue("nombre", datoEntrada.getPrimerNombre());
			namedParameters.addValue("nombre2", datoEntrada.getSegundoNombre());
			namedParameters.addValue("apaterno", datoEntrada.getApellidoPaterno());
			namedParameters.addValue("amaterno", datoEntrada.getApellidoMaterno());
			sql.append("SELECT a.id, \"idTipoPersona\" ,\"tipoPersona\",nombre, apaterno, amaterno, \"idSucursal\", \"idTipoCliente\",");
			sql.append("  descripcion tipo_cliente, \"segundoNombre\", direccion, telefono, sexo, \"fechaNac\" , correo ");
			sql.append("FROM personas.\"PePersona\" a, personas.\"PeTipoCliente\" b, personas.\"PeDatosPersonales\" c,");
			sql.append(" personas.\"PeDireccion\" d, personas.\"PeTelefono\" e , personas.\"PeDatosAdicionales\" f,");
			sql.append("  personas.\"PeTipoPersona\" g" );
			sql.append(" where a.\"idTipoCliente\" = b.clave ");
			sql.append("and a.id = c.\"idPersona\" ");
			sql.append("and a.id = d.\"idPersona\" ");
			sql.append("and a.id = e.\"idPersona\" and ");
			sql.append("a.id = f.\"idPersona\" ");
			sql.append("and a.\"idTipoPersona\" = g.id ");
			sql.append("and e.\"idTipoTel\" = (select id from personas.\"NcTipoTel\" e where \"tipoTelefono\" like ('celular'))");
			sql.append(" and nombre = :nombre and (apaterno = :apaterno ) and (amaterno = :amaterno ) and (\"segundoNombre\" = :nombre2 or \"segundoNombre\" is null)  ;");
			logger.info(sql);
		} else if ((null!= datoEntrada.getPrimerNombre() && ( !"".equals(datoEntrada.getPrimerNombre()))  || 
				(null !=datoEntrada.getSegundoNombre() && !"".equals(datoEntrada.getSegundoNombre())))
				&& ((null !=datoEntrada.getApellidoPaterno() && !"".equals(datoEntrada.getApellidoPaterno()))
						|| (null !=datoEntrada.getApellidoMaterno() && !"".equals(datoEntrada.getApellidoMaterno()))
				&& (!"".equals(datoEntrada.getNumeroCelular()) && null != datoEntrada.getNumeroCelular()))) {
			namedParameters.addValue("telefono", datoEntrada.getNumeroCelular());
			namedParameters.addValue("nombre", datoEntrada.getPrimerNombre());
			namedParameters.addValue("nombre2", datoEntrada.getSegundoNombre());
			namedParameters.addValue("apaterno", datoEntrada.getApellidoPaterno());
			namedParameters.addValue("amaterno", datoEntrada.getApellidoMaterno());
			sql.append("SELECT a.id, \"idTipoPersona\" ,\"tipoPersona\",nombre, apaterno, amaterno, \"idSucursal\", \"idTipoCliente\",");
			sql.append("  descripcion tipo_cliente, \"segundoNombre\", direccion, telefono, sexo, \"fechaNac\" , correo ");
			sql.append("FROM personas.\"PePersona\" a, personas.\"PeTipoCliente\" b, personas.\"PeDatosPersonales\" c,");
			sql.append(" personas.\"PeDireccion\" d, personas.\"PeTelefono\" e , personas.\"PeDatosAdicionales\" f,");
			sql.append("  personas.\"PeTipoPersona\" g" );
			sql.append(" where a.\"idTipoCliente\" = b.clave ");
			sql.append("and a.id = c.\"idPersona\" ");
			sql.append("and a.id = d.\"idPersona\" ");
			sql.append("and a.id = e.\"idPersona\" and ");
			sql.append("a.id = f.\"idPersona\" ");
			sql.append("and a.\"idTipoPersona\" = g.id ");
			sql.append("and e.\"idTipoTel\" = (select id from personas.\"NcTipoTel\" e where \"tipoTelefono\" like ('celular'))");
			sql.append(" and (nombre = :nombre or (nombre is null and (\"segundoNombre\" = :nombre2 ))) and ((apaterno = :apaterno or apaterno is null ) ");
			sql.append( "or (amaterno = :amaterno or amaterno is null )) ");
			sql.append(" and (\"segundoNombre\" = :nombre2 or (\"segundoNombre\" is null and nombre = :nombre)) ");
			sql.append(" and telefono = :telefono  ;");
			logger.info(sql);
		} else if ((null!= datoEntrada.getPrimerNombre() && ( !"".equals(datoEntrada.getPrimerNombre()))  || 
				(null !=datoEntrada.getSegundoNombre() && !"".equals(datoEntrada.getSegundoNombre())))
				&& ((null !=datoEntrada.getApellidoPaterno() && !"".equals(datoEntrada.getApellidoPaterno()))
						|| (null !=datoEntrada.getApellidoMaterno() && !"".equals(datoEntrada.getApellidoMaterno()))
				&& (!"".equals(datoEntrada.getFechaNacimiento()) && null != datoEntrada.getFechaNacimiento() ))) {
			SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			java.util.Date fechaNac;
			try {
				fechaNac = sdf1.parse(datoEntrada.getFechaNacimiento());
			} catch (ParseException e) {
				logger.error("error al convertir la fecha");
				throw new BusinessException("error al convertir la fecha");
			}
			
			namedParameters.addValue("fecNacimiento", fechaNac);
			namedParameters.addValue("nombre", datoEntrada.getPrimerNombre());
			namedParameters.addValue("nombre2", datoEntrada.getSegundoNombre());
			namedParameters.addValue("apaterno", datoEntrada.getApellidoPaterno());
			namedParameters.addValue("amaterno", datoEntrada.getApellidoMaterno());
			sql.append("SELECT a.id, \"idTipoPersona\" ,\"tipoPersona\",nombre, apaterno, amaterno, \"idSucursal\", \"idTipoCliente\",");
			sql.append("  descripcion tipo_cliente, \"segundoNombre\", direccion, telefono, sexo, \"fechaNac\" , correo ");
			sql.append("FROM personas.\"PePersona\" a, personas.\"PeTipoCliente\" b, personas.\"PeDatosPersonales\" c,");
			sql.append(" personas.\"PeDireccion\" d, personas.\"PeTelefono\" e , personas.\"PeDatosAdicionales\" f,");
			sql.append("  personas.\"PeTipoPersona\" g" );
			sql.append(" where a.\"idTipoCliente\" = b.clave ");
			sql.append("and a.id = c.\"idPersona\" ");
			sql.append("and a.id = d.\"idPersona\" ");
			sql.append("and a.id = e.\"idPersona\" and ");
			sql.append("a.id = f.\"idPersona\" ");
			sql.append("and a.\"idTipoPersona\" = g.id ");
			sql.append("and e.\"idTipoTel\" = (select id from personas.\"NcTipoTel\" e where \"tipoTelefono\" like ('celular'))");
			sql.append(" and (nombre = :nombre or (nombre is null and (\"segundoNombre\" = :nombre2 ))) and ((apaterno = :apaterno or apaterno is null ) ");
			sql.append( "or (amaterno = :amaterno or amaterno is null )) ");
			sql.append(" and (\"segundoNombre\" = :nombre2 or (\"segundoNombre\" is null and nombre = :nombre)) ");
			sql.append(" and \"fechaNac\" = :fecNacimiento  ;");
			logger.info(sql);
		} else if (!"".equals(datoEntrada.getNumeroCelular()) && null != datoEntrada.getNumeroCelular()) {
			namedParameters.addValue("telefono", datoEntrada.getNumeroCelular());
			sql.append("SELECT a.id, \"idTipoPersona\" ,\"tipoPersona\",nombre, apaterno, amaterno, \"idSucursal\", \"idTipoCliente\",");
			sql.append("  descripcion tipo_cliente, \"segundoNombre\", direccion, telefono, sexo, \"fechaNac\" , correo ");
			sql.append("FROM personas.\"PePersona\" a, personas.\"PeTipoCliente\" b, personas.\"PeDatosPersonales\" c,");
			sql.append(" personas.\"PeDireccion\" d, personas.\"PeTelefono\" e , personas.\"PeDatosAdicionales\" f,");
			sql.append("  personas.\"PeTipoPersona\" g" );
			sql.append(" where a.\"idTipoCliente\" = b.clave ");
			sql.append("and a.id = c.\"idPersona\" ");
			sql.append("and a.id = d.\"idPersona\" ");
			sql.append("and a.id = e.\"idPersona\" and ");
			sql.append("a.id = f.\"idPersona\" ");
			sql.append("and a.\"idTipoPersona\" = g.id ");
			sql.append("and e.\"idTipoTel\" = (select id from personas.\"NcTipoTel\" e where \"tipoTelefono\" like ('celular'))");
			sql.append(" and telefono = :telefono  ;");
			logger.info(sql);
		} else {
			logger.error("numero de parametros incorrectos " + datoEntrada.toString());
			throw new BusinessException("numero de Parametros incorrectos");
		}

		List<BeanCliente> listaClientes = namedJdbcTemplate.query(sql.toString(), namedParameters, new ClienteMapper());

		if (listaClientes.size() > 0) {
			cliente = listaClientes.get(0);
		}
		return cliente;
	}

	@Override
	public BeanCliente actualizaCliente(BeanCliente BeanCliente) throws BusinessException {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public int actualizaCorreoCliente(BeanClienteEntrada beEntrada) throws BusinessException {
		logger.info("actualizaCorreoCliente " + beEntrada.toString());
		final MapSqlParameterSource namedParameters = new MapSqlParameterSource();
		namedParameters.addValue("id", beEntrada.getNoCliente());
		namedParameters.addValue("correo", beEntrada.getCorreo());
		StringBuffer sql = new StringBuffer("");
		sql.append("UPDATE personas.\"PeDatosAdicionales\" a");
		sql.append("  SET correo = :correo");
		sql.append(" WHERE a.\"idPersona\" = :id");
		logger.info(sql);
		int status = namedJdbcTemplate.update(sql.toString(), namedParameters);
		logger.info("status "+status);
		return status;
	}	
	
}
