package mx.com.promesi.persistencia;

import com.mx.beans.Busqueda.BeanCliente;
import com.mx.beans.Busqueda.BeanClienteEntrada;

import mx.com.promesi.exceptions.BusinessException;

public interface ClienteDao {
	
	public BeanCliente insertaCliente(BeanClienteEntrada BeanCliente)throws BusinessException; 

	public BeanCliente eliminaCliente(BeanCliente BeanCliente)throws BusinessException;
	
	public BeanCliente consultaClientePorId(long numeroCliente)throws BusinessException;
	
	public BeanCliente consultaClientePorParametros(BeanClienteEntrada datosCliente)throws BusinessException;
	
	public BeanCliente actualizaCliente(BeanCliente BeanCliente)throws BusinessException;
	
	public int actualizaCorreoCliente(BeanClienteEntrada beEntrada)throws BusinessException;
}
