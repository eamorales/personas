/*
 * JSONResponseDTO.java 1.0 12/03/2017
*/
/**
 * 
 * @author Inver-tu
 * @version 1.0, 12/03/2017
 * @since 1.0
 */
package net.cero.nucleocentral.datos;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

import com.google.gson.Gson;

/**
 * @author Administrador
 *
 */
@XmlRootElement
public class JSONResponseDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4621588886366724340L;
	private ErrorDTO error;
	private Object dto;
	
	
	/**
	 * @return the error
	 */
	public ErrorDTO getError() {
		return error;
	}
	/**
	 * @param error the error to set
	 */
	public void setError(ErrorDTO error) {
		this.error = error;
	}
	/**
	 * @return the dto
	 */
	public Object getDto() {
		return dto;
	}
	/**
	 * @param dto the dto to set
	 */
	public void setDto(Object dto) {
		this.dto = dto;
	}
	
	/**
     * Convierte esta instancia a un String en formato JSON 
     * @return un string en formato JSON
     */
    public String toJSONString() {
        return new Gson().toJson(this);
    }
}
