package com.mx.promesi.util;
/**
 * Clase que carga todos los Catalogo al incio del servidor
 * */
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.apache.log4j.Logger;
import com.google.gson.Gson;
import com.mx.Res.Respuesta;
import com.mx.promesi.conf.UrlService;

import BeanRespuesta.ResUserYPass;
import BeanRespuesta.ValidaServicioToken;



public class Call {
	  private Call() {
		    throw new IllegalStateException("Utility class");
		  }

	private static final Logger LOG = Logger.getLogger(Call.class); // Implementación del LOG
	static Respuesta respuesta = new Respuesta();

	
	//second, minute, hour, day of month, month, day(s) of week

	/**
	 * ---------------------------------------------------DOMICILIOS VIEW------------------------------
	 */
	
	public static ResUserYPass getUserYPass(String user, String pass) {
		LOG.info(UrlService.getWsValidarUserYPass()+user+"/"+pass);
		return consultaWebService(UrlService.getWsValidarUserYPass()+user+"/"+pass);
	}
		
	public static ValidaServicioToken validaServiceToken(String urlws) {
		ValidaServicioToken servicio = new ValidaServicioToken();
		try {
			URL url = new URL(urlws);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				LOG.info("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			
			while ((output = br.readLine()) != null) {
				LOG.info("ValidaServicioToken" + output);
				Gson gson = new Gson();
				servicio = gson.fromJson(output, ValidaServicioToken.class);
			}
		} catch (Exception e) {
			LOG.info(e);
		}
	
		return servicio;
	}
	/**
	 * -----------------------SERVICIO GENERICO PARA LA CONEXION CON EL WEB SERVICE----------------------
	 */
	public static ResUserYPass consultaWebService(String urlws) {
		ResUserYPass resultado = new ResUserYPass();
		try {
			URL url = new URL(urlws);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				LOG.info("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			
			while ((output = br.readLine()) != null) {
				LOG.info("ValidaUseryPass" + output);
				Gson gson = new Gson();
				resultado = gson.fromJson(output, ResUserYPass.class);
			}
		} catch (Exception e) {
			LOG.info(e);
		}
		return resultado;
	}
	

	public static Respuesta getRespuesta() {
		return respuesta;
	}

	public static void setRespuesta(Respuesta respuesta) {
		Call.respuesta = respuesta;
	}

	

}
