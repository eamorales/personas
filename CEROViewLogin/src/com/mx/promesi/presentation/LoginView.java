package com.mx.promesi.presentation;

import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.NoSuchProviderException;
import java.security.SecureRandom;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mx.Res.Respuesta;
import com.mx.constantes.Constantes;
import com.mx.constantes.HardCode;
import com.mx.promesi.Req.Request;
import com.mx.promesi.Req.UserSession;
import com.mx.promesi.conf.UrlService;
import com.mx.promesi.util.Call;
import com.mx.promesi.util.RestCall2;

import BeanRespuesta.ResUserYPass;
import BeanRespuesta.RespuestaBase;
import BeanRespuesta.ValidaServicioToken;

@Component
@ManagedBean(name = "loginView")
@SessionScoped
public class LoginView implements Serializable {
	/**
	 * 
	 */ 
	private static final Logger LOG = Logger.getLogger(LoginView.class); // Implementación del LOG
	private static final long serialVersionUID = -5004824793601559231L;
	private String user;
	private String pass;
	private String encriptado;

	private String falso;

	public void init() {
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession sesion;
		setUser("");
		setPass("");
		setFalso("false");
		LOG.info("init LoginView");
		sesion = (HttpSession) context.getExternalContext().getSession(true);
		sesion.setAttribute("sesionActiva", falso);
		sesion.setAttribute("token2", falso);
		sesion.setAttribute("claveUsuario", "");
		sesion.setAttribute("idCliente",null); 

	}

	public void validar() {
		LOG.info("Validando user y pass");
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession sesion;
		Request request = new Request();
		UserSession session = new UserSession();

//		if (getUser() != null && getPass() != null && getUser().length() > 0 && getPass().length() > 5	&& getPass().length() < 10) {  /**Valida que el usuario o la contraseña no sean vacios o tengan el minimo de caracteres*/
//			if (validaDosCaracteresAlfabeticos() && validaDosCaracteresNumericos()) { /**Valida que la contraseña tenga las validaciones solicitadas*/
//				LOG.info("USER:"+getUser());
//				LOG.info("PASSWORD:"+getPass());
//				String regeneratedPassowrdToVerify = getSecurePassword(getPass(), getUser()); /**encripta el password*/
//
//				ResUserYPass resp = Call.getUserYPass(getUser().toUpperCase(), regeneratedPassowrdToVerify);
//				if (resp != null && resp.getCodeEstatus().equals("SUCCESS")) { /**Valida que la autenticación del servicio sea correcta*/
//					RestCall2<UserSession, RespuestaBase> r2 = new RestCall2<>();
					session.setClaveUsuario(getUser());
					session.setContrasenia(getPass());
					session.setUserEncriptado(getEncriptado());
					session.setUrlService(UrlService.getWsconsultaPermisos());
					request.setSesion(session);
					LOG.info("URL::: " + UrlService.getUrlBaseBack()+ UrlService.getLogin());
//					r2.setUrl(UrlService.getUrlBaseBack() + UrlService.getLogin());
//					r2.setEntrada(request.getSesion());
//					r2.setClase(RespuestaBase.class);
//					RespuestaBase respuesta;
//					respuesta = r2.call();
//					if(respuesta.getCodigoError()==null) {
//						context.addMessage(null,
//								new FacesMessage(FacesMessage.SEVERITY_ERROR, "No se ha podido conectar al Service", null));
//					}
//					else if (respuesta.getCodigoError().equals(Constantes.CLAVE_CORRECTA)) { /**Valida que se halla generado la sesión del usuario*/
//						
//					if(validaServicioToken()) { /**El servicio para generar el token esta activo*/
//						
//							if(enviaToken()) {
								sesion = (HttpSession) context.getExternalContext().getSession(true);
								sesion.setAttribute("sesionActiva", "true");
								sesion.setAttribute("token2", "false");
								sesion.setAttribute("claveUsuario", getUser());
								String ruta = UrlService.getUrlBase() + "/token.xhtml";
								LOG.info(ruta);
								try {
									context.getExternalContext().redirect(ruta);
								} catch (IOException e) {
									LOG.info(e);
								}	
//							}else {
//								LOG.info("El token no se ha enviado");
//							}
//					}else { /**El servicio para generar el token no esta activo*/
//						sesion = (HttpSession) context.getExternalContext().getSession(true);
//						sesion.setAttribute("sesionActiva", "true");
//						sesion.setAttribute("claveUsuario", getUser());
//						sesion.setAttribute("token2", "true");
//						try {
//							 String ruta =UrlService.getUrlBase()+"/busqueda.xhtml";
//							context.getExternalContext().redirect(ruta);
//						} catch (IOException e) {
//							LOG.info(e.toString());
//						}
//					}
//					
//					 }else {  /**Fallo la Validación de que se halla generado la sesión del usuario*/
//							context.addMessage(null,
//									new FacesMessage(FacesMessage.SEVERITY_ERROR, HardCode.ERROR_SERVICIO, null));
//					 }
//				} else {/**Falla la Validación de  que la autenticación del servicio sea correcta*/
//					if (resp != null && resp.getCodeEstatus().equals("FAIL")) { /**Falla la Validación de  que la autenticación del servicio sea correcta*/
//						resp.setMessageEstatus(resp.getMessageEstatus().replace("autenticaciÃ³n", "autenticaci\u00F3n"));
//						context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,
//								resp.getMessageEstatus().toUpperCase(), null));
//					} else { /**Ocurrio un error inesperado*/
//						context.addMessage(null,
//								new FacesMessage(FacesMessage.SEVERITY_ERROR, HardCode.ERROR_SERVICIO, null));
//					}
//				}
//			}else { /**En caso de que la contraseña no cumpla con las validaciones*/
//				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, HardCode.USERYPASS, null));
//			}
//		} else { /**En caso de que el usuario y contraseña esten vacios*/
//			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, HardCode.USERPASSEMPTY, null));
//		}

	}
	private boolean enviaToken() {
		UserSession session = new UserSession();
		session.setClaveUsuario(getUser());
		RestCall2<UserSession, RespuestaBase> r2 = new RestCall2<>();
		LOG.info("URL::: " + UrlService.getUrlBaseBack()+ UrlService.getEnviaToken());
		r2.setUrl(UrlService.getUrlBaseBack() + UrlService.getEnviaToken());
		r2.setEntrada(session);
		r2.setClase(RespuestaBase.class);
		RespuestaBase respuesta;
		respuesta = r2.call();
		if(respuesta.getCodigoError().equals("00000")) {
			LOG.info("EXITO");
			return true;
		}else {
			LOG.info("Ocurrio un error al enviar el token");
			return true;
		}
	}
	private boolean validaServicioToken() {
		ValidaServicioToken resp = Call.validaServiceToken(UrlService.getWsconsultaEstatusServicioToken());
		String estatus ="0";
		if(resp.getResultados()!=null) {
		 estatus = resp.getResultados().get(0).getIdEstatus();
		}
		boolean est = false;
		if(estatus.equals("1"))
			est = true;
		else 
			est = false;
		
		return est;
		
	}

	/**
	 * Validar al menos 2 numeros
	 */
	public boolean validaDosCaracteresAlfabeticos() {
		boolean bandera = false;
		String numeros ="[1234567890]";
		String pass = getPass();
		int contador = 0;
		for(int i = 0 ; i < pass.length();i++) {
			boolean var = Pattern.matches(numeros, pass.charAt(i)+"");
//			LOG.info(var);
			if(var)
				contador++;
		}
		if(contador>1)
			bandera = true;
		return bandera;
	}

	/**
	 * Validar al menos 2 numeros
	 */
	public boolean validaDosCaracteresNumericos() {
		boolean bandera = false;
		String letras ="[abcdefghijklmnñopqrstuvwxyz]";
		String pass = getPass().toLowerCase();
		int contador = 0;
		for(int i = 0 ; i < pass.length();i++) {
			boolean var = Pattern.matches(letras, pass.charAt(i)+"");
//			LOG.info(var);
			if(var)
				contador++;
		}
		if(contador>1)
			bandera = true;	
		return bandera;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getFalso() {
		return falso;
	}

	public void setFalso(String falso) {
		this.falso = falso;
	}

	public String getEncriptado() {
		return encriptado;
	}

	public void setEncriptado(String encriptado) {
		this.encriptado = encriptado;
	}

	/** ------- METODOS PARA EL ENCRIPTADO DE LA CONTRASENIA **/
	// función para encriptar
	public static String getSecurePassword(String preHash, String usuario) {
		String generatedPassword = null;
		try {
			// Crear instancia SHA-256
			MessageDigest md = MessageDigest.getInstance("SHA-256");
			// agregarle la "Salt"
			md.update(getSalt(preHash, usuario));
			// Obtener el hash del password
			byte[] bytes = md.digest(preHash.getBytes());
			// Pasar arreglo de Bytes a hexadecimal
			StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			// Obtener el hash completo y formateado
			generatedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (NoSuchProviderException e) {
			e.printStackTrace();
		}
		return generatedPassword;
	}

	// Función de ADD SALT
	private static byte[] getSalt(String PreHash, String usuario)
			throws NoSuchAlgorithmException, NoSuchProviderException {
		// Secure Random, garantiza que será un número totalmente al azar
		SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		String secureHash = PreHash + usuario; // codigo PRE HASH + USUARIO
		try {
			sr.setSeed(secureHash.getBytes("us-ascii"));
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// crea un byte[] para la salt
		byte[] salt = new byte[16];
		// de las multiples posibilidades que genero agarramos el byte proximo.
		sr.nextBytes(salt);
		// return salt
		return salt;
	}

}
