package com.mx.promesi.presentation;

import java.io.IOException;
import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;
import com.mx.Res.Respuesta;
import com.mx.beans.Busqueda.BeanClienteEntrada;
import com.mx.beans.Busqueda.BeanClienteVista;
import com.mx.constantes.Constantes;
import com.mx.promesi.conf.UrlService;
import com.mx.promesi.util.RestCall2;
import com.mx.promesi.util.RestCall3;

import BeanRespuesta.RespuestaBase;

@Component
@SessionScope
@ManagedBean(name = "busquedaView")
public class BusquedaView extends BeanClienteVista implements Serializable {

	private static final long serialVersionUID = 1600437140318465585L;
	private static final Logger LOG = Logger.getLogger(BusquedaView.class);
	private boolean avanzada = true;
	private boolean mostrarResultado = false;
	private String accesoRapido;
	private String sesionActiva;
	private long noCliente; 


	public long getNoCliente() {
		return noCliente;
	}
	public void setNoCliente(long noCliente) {
		this.noCliente = noCliente;
	}
	@PostConstruct
	public void initialize() {
		LOG.info("::Iniciando Busqueda::");
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session;
		session = (HttpSession) context.getExternalContext().getSession(true);
		if(mostrarResultado == false){
			session.setAttribute("idCliente", null);
		}
//		  if(session.getAttribute(sesionActiva) == null || !session.getAttribute(sesionActiva).equals("true")) {
//				LOG.info("No existe Sesión o ya se ha llenado el token");
//				enviarLogin(context);
//				LOG.info("Ocurrio un error al Redireccionar para el login");
//		  }else {
//			  LOG.info("Existe Sesión");
//		  }
	
		this.setBeanClienteEntrada(new BeanClienteEntrada());
	}
	public void enviarLogin(FacesContext context) {
		try {
			 String ruta =UrlService.getUrlBase()+"/index.xhtml";
			context.getExternalContext().redirect(ruta);
		} catch (IOException e) {
			LOG.info("ha ocurrido un error al redireccionar al login");
		}
	}

	public void enviarPersonas() {
		LOG.info("::DENTRO DE ENVIAR PERSONAS::");
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		try {
			 String ruta =UrlService.getUrlBasePersonas()+"/index_personas.xhtml?sesionActiva="+session.getAttribute("sesionActiva").toString()+"&token="+session.getAttribute("token2").toString()+"&id="+session.getAttribute("idCliente")+"&accesoRapido="+accesoRapido;
			 LOG.info("::RUTA REDIRECCIONA INDEX PERSONAS ::"+ruta);
			context.getExternalContext().redirect(ruta);
		} catch (IOException e) {
			LOG.info(e.toString());
		}
	}

	public void busquedaClienteId() {
		LOG.info("::DENTRO DE METODO busquedaClienteId::");
		noCliente = 0;
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session;
		session = (HttpSession) context.getExternalContext().getSession(true);
		
		RestCall3<BeanClienteEntrada, Respuesta> r2 = new RestCall3<>();
		r2.setUrl(UrlService.getUrlBaseBack() + UrlService.BUSQUEDA_CLIENTE + UrlService.BUSQUEDA_CLIENTE_ID);
		LOG.info(UrlService.getUrlBaseBack() + UrlService.BUSQUEDA_CLIENTE + UrlService.BUSQUEDA_CLIENTE_ID);
		r2.setEntrada(this.getBeanClienteEntrada());
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		if (null != respuesta && null != respuesta.getError() && respuesta.getError().getClave().equals("0")) {
			LOG.info("::llamado exitoso::");
			LOG.info("salida respuesta "+respuesta.getCliente());
			setBeanCliente(respuesta.getCliente());
			mostrarResultado = true;
			session.setAttribute("idCliente", respuesta.getCliente().getBeanInformacion().getNoCliente());
			noCliente = respuesta.getCliente().getBeanInformacion().getNoCliente();
		} else {
			LOG.info("::problemas en el llamado::");
			LOG.info("Mensaje: "+respuesta.getError().getMessage()+ "error"+respuesta.getError().getClave());
			setMensaje(respuesta.getError().getMessage());
		}		
	}

	public void busquedaClienteAvanzada() {
		LOG.info("::DENTRO DE METODO busquedaClienteAvanzada::");
		noCliente = 0;
		RestCall3<BeanClienteEntrada, Respuesta> r2 = new RestCall3<>();
		r2.setUrl(UrlService.getUrlBaseBack() + UrlService.BUSQUEDA_CLIENTE + UrlService.BUSQUEDA_CLIENTE_AVANZADA);
		LOG.info(UrlService.getUrlBaseBack() + UrlService.BUSQUEDA_CLIENTE + UrlService.BUSQUEDA_CLIENTE_AVANZADA);
		r2.setEntrada(this.getBeanClienteEntrada());
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		if (respuesta.getError().getClave().equals("0")) {
			LOG.info("::llamado exitoso::");
			this.setBeanCliente(respuesta.getCliente());
			mostrarResultado = true;
			avanzada = false;
		} else {
			LOG.info("::problemas en el llamado::");
			setMensaje(respuesta.getError().getMessage());
		}
	}

	public void actualizaCorreo() {
		LOG.info("::DENTRO DE METODO actualizaCorreo::");
		RestCall3<BeanClienteEntrada, Respuesta> r2 = new RestCall3<>();
		r2.setUrl(UrlService.getUrlBaseBack() + UrlService.BUSQUEDA_CLIENTE + UrlService.BUSQUEDA_CLIENTE_ACT_CORREO);
		LOG.info(UrlService.getUrlBaseBack() + UrlService.BUSQUEDA_CLIENTE + UrlService.BUSQUEDA_CLIENTE_ACT_CORREO);
		r2.setEntrada(this.getBeanClienteEntrada());
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		if (respuesta.getError().getClave().equals(0)) {
			setBeanCliente(respuesta.getCliente());
		} else {
			setMensaje(respuesta.getError().getMessage());
		}
	}

	public boolean isAvanzada() {
		return avanzada;
	}

	public void setAvanzada(boolean avanzada) {
		this.avanzada = avanzada;
	}

	public boolean isMostrarResultado() {
		return mostrarResultado;
	}

	public void setMostrarResultado(boolean mostrarResultado) {
		this.mostrarResultado = mostrarResultado;
	}

	public String getAccesoRapido() {
		return accesoRapido;
	}

	public void setAccesoRapido(String accesoRapido) {
		this.accesoRapido = accesoRapido;
	}

	public String getSesionActiva() {
		return sesionActiva;
	}

	public void setSesionActiva(String sesionActiva) {
		this.sesionActiva = sesionActiva;
	}
	
	
	
}
