package com.mx.promesi.presentation;

import java.io.IOException;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import com.mx.Res.Respuesta;
import com.mx.constantes.Constantes;
import com.mx.constantes.HardCode;
import com.mx.promesi.Req.UserSession;
import com.mx.promesi.conf.UrlService;
import com.mx.promesi.util.RestCall2;

import BeanRespuesta.RespuestaBase;

import javax.servlet.http.HttpSession;


@Component
@ManagedBean(name="token")
@SessionScoped
public class TokenView  implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -2733031180197777249L;
	private static final Logger LOG = Logger.getLogger(TokenView.class); // Implementación del LOG
	
	private int token;
	private String valor;
	private String sesionActiva;
	
	
	public void init() {
		LOG.info("Token View");
		setSesionActiva("sesionActiva");
		setValor("");
		token = 60;
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session;
		session = (HttpSession) context.getExternalContext().getSession(true);
		  if(session.getAttribute(sesionActiva) == null || !session.getAttribute(sesionActiva).equals("true") || session.getAttribute("token2").equals("true")) {
				LOG.info("No existe Sesión o ya se ha llenado el token");
				enviarLogin(context);
				LOG.info("Ocurrio un error al Redireccionar para el login");
		  }else {
			  LOG.info("Existe Sesión");
		  }
	}
	
	public void enviarLogin(FacesContext context) {
		try {
			 String ruta =UrlService.getUrlBase()+"/index.xhtml";
			context.getExternalContext().redirect(ruta);
		} catch (IOException e) {
			LOG.info("ha ocurrido un error al redireccionar al login");
		}
	}
	public void validarToken()  {
		LOG.info("Validando token");
		FacesContext context = FacesContext.getCurrentInstance();
		
		if(getValor()!=null && getValor().length() >0  ) {			
			HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
			session.setAttribute("token2", "true");
			UserSession sesion = new UserSession();
			sesion.setToken(getValor());
			sesion.setClaveUsuario(session.getAttribute("claveUsuario")+"");
			RestCall2<UserSession, RespuestaBase> r2=new RestCall2<>();
			
			LOG.info("URL::: "+UrlService.getUrlBaseBack()+UrlService.getValidaToken());
//			  r2.setUrl(UrlService.getUrlBaseBack()+UrlService.getValidaToken());
//			  r2.setEntrada(sesion);
//			  r2.setClase(RespuestaBase.class);
//			  RespuestaBase respuesta;
//			  respuesta = r2.call();
			
//			  if (!respuesta.getCodigoError().equals(Constantes.CLAVE_CORRECTA)) {
//					context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.TOKENINVALIDO,""));
//			  }else {
					context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,HardCode.TOKENCORRECTO,""));
					try {
						 String ruta =UrlService.getUrlBase()+"/busqueda.xhtml";
						context.getExternalContext().redirect(ruta);
					} catch (IOException e) {
						LOG.info(e.toString());
					}
			  }
//		}else {
//			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.TOKENINVALIDO,""));
//		}
		
	 }
    
    public void decrementa() {
    	token--;
        if(token==0) {
        	FacesContext context = FacesContext.getCurrentInstance();
        	enviarLogin(context);
        }
        	
    }
    
    public int getToken() {
        return token;
    }
	public void setToken(int token) {
		this.token = token;
	}
	public String getSesionActiva() {
		return sesionActiva;
	}
	public void setSesionActiva(String sesionActiva) {
		this.sesionActiva = sesionActiva;
	}

	public String getValor() {
		return valor;
	}

	public void setValor(String valor) {
		this.valor = valor;
	}
	
	
	
	
}
