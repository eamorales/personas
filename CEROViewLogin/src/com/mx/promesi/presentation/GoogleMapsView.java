package com.mx.promesi.presentation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;


@Component
@SessionScope
@ManagedBean(name = "GoogleMapsView")
public class GoogleMapsView implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = Logger.getLogger(GoogleMapsView.class);
	private MapModel geoModel;
    private MapModel revGeoModel;
    
    private MapModel emptyModel;
    
    private String title;
      
    private double lat;
      
    private double lng;
    
    


	private String centerGeoMap = "23.7214652, -99.1595868";
//    private String centerRevGeoMap = "41.850033, -87.6500523";

    public void init() {
    	LOG.info("::Dentro del initi google maps view ::");
        geoModel = new DefaultMapModel();
        revGeoModel = new DefaultMapModel();
        RestCall();
//        addMarker();
    }
    //funcin para setear un markador
    //referencia https://www.primefaces.org/showcase/ui/data/gmap/addMarkers.xhtml
    public void addMarker() {
    	setLat(23.7214652);
    	setLng(-99.1595868);
        Marker marker = new Marker(new LatLng(lat, lng), title);
        emptyModel.addOverlay(marker);
          
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Marker Added", "Lat:" + lat + ", Lng:" + lng));
    }
    
     public void RestCall(){
    	 try {
    		 	//consume el ws de google y se envian los parametris en addres 87000, victoria, tamaulipas
    			URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?address=87000+victoria+tamaulipas&key=AIzaSyAyBviIXn-gDl_4Sy7AVCFO1Gb9Jl8xAWs");
    			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
    			conn.setRequestMethod("GET");
    			conn.setRequestProperty("Accept", "application/json");
    			
    			
    			LOG.info("::RESPUESTA DE CONEXION ::"+conn.getResponseCode());
    			if (conn.getResponseCode() != 200) {
    				throw new RuntimeException("Failed : HTTP error code : "
    						+ conn.getResponseCode());
    			}else{
    				//si la respuesta es 200 (EXITO) hace la extraccion de la respuesta
    				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
        			String output;
        			String trama="";
        			System.out.println("Output from Server .... \n");
        			while ((output = br.readLine()) != null) {
        				System.out.println(output);
        				trama = trama+output;
        			}
        			LOG.info("::TRAMA GOOGLE::"+trama);
//        			jsonTransform(trama);
    			}
    			conn.disconnect();
    		  } catch (MalformedURLException e) {
    			e.printStackTrace();
    		  } catch (IOException e) {
    			e.printStackTrace();
    		  }
    }
     
// 	public static void jsonTransform(String data) {
// 		LOG.info("::DENTRO DE TRANSFORM::");
//		JSONObject object = new JSONObject(data);
//		
//		 JSONArray jsonArray = object.getJSONArray("geometry");
//		    for (int i = 0; i < jsonArray.length(); i++) {
//		        JSONObject explrObject = jsonArray.getJSONObject(i);
//		}
//	}
     

	public String getCenterGeoMap() {
		return centerGeoMap;
	}

	public void setCenterGeoMap(String centerGeoMap) {
		this.centerGeoMap = centerGeoMap;
	}

//	public String getCenterRevGeoMap() {
//		return centerRevGeoMap;
//	}
//
//	public void setCenterRevGeoMap(String centerRevGeoMap) {
//		this.centerRevGeoMap = centerRevGeoMap;
//	}

	public MapModel getGeoModel() {
		return geoModel;
	}

	public void setGeoModel(MapModel geoModel) {
		this.geoModel = geoModel;
	}

	public MapModel getRevGeoModel() {
		return revGeoModel;
	}

	public void setRevGeoModel(MapModel revGeoModel) {
		this.revGeoModel = revGeoModel;
	}
	
    public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

}
