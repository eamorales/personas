package com.mx.promesi.conf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.InetAddress;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

@Service
public class UrlService {
	
	  private static final Logger LOG = Logger.getLogger(UrlService.class);

	  private static String ipServidor;
	  private static String ipServidorServicios;
	  private static String ipFront;
	  private static String puerto;
	  private static String puertoServidor;
	  private static String webService;
	  private static String webServiceBack;
	  private static String webServicePersonas;
	  private static String urlBase;
	  private static String urlBaseBack;
	  private static String urlBasePersonas;
	  private static String wsconsultaEstatusServicioToken;
	  private static String wsconsultaPermisos;
	  private static String wsValidarUserYPass;
	  private static String login = "/login";
	  private static String validaToken ="/validaToken";
	  private static String enviaToken = "/enviaToken";
	  /*Busqueda */ 
		public static final String BUSQUEDA_CLIENTE="/buscaCliente/";
		public static final String BUSQUEDA_CLIENTE_ID = "id";
		public static final String BUSQUEDA_CLIENTE_AVANZADA = "avanzada";
		public static final String BUSQUEDA_CLIENTE_ACT_CORREO = "actCorreo";
	public static String getUrlBase() {
		return urlBase;
	}
	
	public static String getIpServidorServicios() {
		return ipServidorServicios;
	}

	public static void setIpServidorServicios(String ipServidorServicios) {
		UrlService.ipServidorServicios = ipServidorServicios;
	}

	public static String getEnviaToken() {
		return enviaToken;
	}

	public static void setEnviaToken(String enviaToken) {
		UrlService.enviaToken = enviaToken;
	}

	public static String getUrlBaseBack() {
		return urlBaseBack;
	}

	public static void setUrlBaseBack(String urlBaseBack) {
		UrlService.urlBaseBack = urlBaseBack;
	}

	public static String getWebServiceBack() {
		return webServiceBack;
	}

	public static void setWebServiceBack(String webServiceBack) {
		UrlService.webServiceBack = webServiceBack;
	}

	public static String getUrlBasePersonas() {
		return urlBasePersonas;
	}

	public static void setUrlBasePersonas(String urlBasePersonas) {
		UrlService.urlBasePersonas = urlBasePersonas;
	}

	public static String getWebServicePersonas() {
		return webServicePersonas;
	}

	public static void setWebServicePersonas(String webServicePersonas) {
		UrlService.webServicePersonas = webServicePersonas;
	}

	public static void setUrlBase(String urlBase) {
		UrlService.urlBase = urlBase;
	}
	
	
	public static String getPuerto() {
		return puerto;
	}

	public static void setPuerto(String puerto) {
		UrlService.puerto = puerto;
	}
	

	public static String getPuertoServidor() {
		return puertoServidor;
	}

	public static void setPuertoServidor(String puertoServidor) {
		UrlService.puertoServidor = puertoServidor;
	}

	public static String getWebService() {
		return webService;
	}

	public static void setWebService(String webService) {
		UrlService.webService = webService;
	}

	public static String getLogin() {
		return login;
	}

	public static void setLogin(String login) {
		UrlService.login = login;
	}

	public static String getValidaToken() {
		return validaToken;
	}

	public static void setValidaToken(String validaToken) {
		UrlService.validaToken = validaToken;
	}
	
	public static String getIpServidor() {
		return ipServidor;
	}

	public static void setIpServidor(String ipServidor) {
		UrlService.ipServidor = ipServidor;
	}

	public static String getIpFront() {
		return ipFront;
	}

	public static void setIpFront(String ipFront) {
		UrlService.ipFront = ipFront;
	}

	public static String getWsconsultaEstatusServicioToken() {
		return wsconsultaEstatusServicioToken;
	}

	public static void setWsconsultaEstatusServicioToken(String wsconsultaEstatusServicioToken) {
		UrlService.wsconsultaEstatusServicioToken = wsconsultaEstatusServicioToken;
	}

	public static String getWsconsultaPermisos() {
		return wsconsultaPermisos;
	}

	public static void setWsconsultaPermisos(String wsconsultaPermisos) {
		UrlService.wsconsultaPermisos = wsconsultaPermisos;
	}
	

	public static String getWsValidarUserYPass() {
		return wsValidarUserYPass;
	}

	public static void setWsValidarUserYPass(String wsValidarUserYPass) {
		UrlService.wsValidarUserYPass = wsValidarUserYPass;
	}

	UrlService(){
		Properties propiedades = new Properties();
		try {
			/**Se crear el properties*/
			/**Cargamos el archivo desde la ruta especificada*/
			propiedades.load(getClass().getResourceAsStream("personas.properties"));
			setIpServidor(propiedades.getProperty("ipServidor"));
			setIpServidorServicios(propiedades.getProperty("ipServidorServicios"));
			setIpFront(propiedades.getProperty("ipFront"));
			setPuerto(propiedades.getProperty("puerto"));
			setPuertoServidor(propiedades.getProperty("puertoServidor"));
			setWebService(propiedades.getProperty("webService"));
			setWebServiceBack(propiedades.getProperty("webServiceBack"));
			setWebServicePersonas(propiedades.getProperty("webServicePersonas"));
			setWsconsultaEstatusServicioToken(getIpServidorServicios()+getPuertoServidor()+propiedades.getProperty("wsconsultaEstatusServicioToken"));
			setWsconsultaPermisos(getIpServidorServicios()+getPuertoServidor()+propiedades.getProperty("wsconsultaPermisos"));
			setWsValidarUserYPass(getIpServidorServicios()+getPuertoServidor()+propiedades.getProperty("wsValidaUserYPass"));
			setUrlBase(getIpFront()+getPuerto()+getWebService());
			setUrlBaseBack(getIpServidor()+getPuerto()+getWebServiceBack());
			setUrlBasePersonas(getIpFront()+getPuerto()+getWebServicePersonas());
		
			/**Obtenemos los parametros definidos en el archivo*/
		} catch (FileNotFoundException e) {
			LOG.info("Error, El archivo no exite");
		} catch (IOException e) {
			LOG.info("ha ocurrido un error al leer el archivo");
		}
	}
}
