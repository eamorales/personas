/*Validación de solo numeros*/

function justNumbers(e) {
	var keynum = window.event ? window.event.keyCode : e.which;
	if ((keynum == 8) || (keynum == 45))
		return true;

	return /\d/.test(String.fromCharCode(keynum));
}

function cargalogtoken(){
	var passwd = $("#login\\:txtContrasena").val();
	var txtUser = $("#login\\:txtUser").val();
	if (passwd.length<=5){
		$("#login\\:txtContrasena").css( 'border-color','red');
	}
	if(txtUser==""){
		$("#login\\:txtUser").css( 'border-color','red');		
	} else if(txtUser!="" && passwd.length>5) {
		$("#txtContrasena").css( 'border-color','gray');
	}
	
}
function colorGris(){
	$("#login\\:txtContrasena").css( 'border-color','gray');
}
function colorGristxtUser(){
	$("#login\\:txtUser").css( 'border-color','gray');
}

  function hidenFunction() {
  	//aqui cambiamos los valores de pass y usuario por los del front.
  	var pass = 	$("#login\\:txtContrasena").val().toUpperCase();	
	var usuario = 	$("#login\\:txtUser").val().toUpperCase();	
     	hexString= pass+ usuario;
     	  var hex, i;

	    var result = "";
	    for (i=0; i<hexString.length; i++) {
        hex = hexString.charCodeAt(i).toString(16);
        result += (""+hex).slice(-4);
    }

//result sería el valor final de nuestro PRE HASH
  result=xor_str(result);
  $("#login\\:arrowLog2").val(result);
 	 
}
function xor_str(x)
{
	var to_enc = x;

	var xor_key="1";
	var the_res="";//the result will be here
	for(i=0;i<to_enc.length;++i)
	{
		the_res+=String.fromCharCode(xor_key^to_enc.charCodeAt(i));
	}
	return the_res;
}
