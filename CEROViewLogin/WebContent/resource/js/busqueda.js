var busqueda = {
	init : function() {
		/**
		 * @author JMATA
		 * @Date 25-JUL-2017
		 */
		$('.buscarLink').on('click', function() {
			$('.criteriosBusquedaInputs').css('visibility', 'visible');
			$('.criteriosBusquedaInputs').css('height', 'auto');
			$('.resultadosBusquedaLabel').css('visibility', 'hidden');
			$('.resultadosBusquedaLabel').css('height', '1px');
		})

		$(".cancelarBusqueda").on('click', function() {
			$('.criteriosBusquedaInputs').css('visibility', 'hidden');
			$('.criteriosBusquedaInputs').css('height', '1px');
			$('.form-control').val('');
		});

		$(".buscarIcon").on('click', function() {
			
		});

		$(".search").focusout(function() {
			if ($(".search").val().trim() != "") {
				$('.resultadosBusquedaLabel').css('visibility', 'visible');
				$('.resultadosBusquedaLabel').css('height', 'auto');
			} else {
				$('.resultadosBusquedaLabel').css('visibility', 'hidden');
				$('.resultadosBusquedaLabel').css('height', '1px');
			}
		});
		
		mostrarResultados();
	}
}
$(document).ready(busqueda.init);

function mostrarResultados(){
	if ($("#isMostrar").val() != "true") {
		$('.resultadosBusquedaLabel').css('visibility', 'hidden');
		$('.resultadosBusquedaLabel').css('height', '1px');
	}else{
		$('.resultadosBusquedaLabel').css('visibility', 'visible');
		$('.resultadosBusquedaLabel').css('height', 'auto');
	}
}