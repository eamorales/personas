$(document).ready(function() {
	
	// Disparar el modal de manera automatica
	if($('#modalAuto').val() == 'OK'){
		$('.md-content-modal').addClass('md-mostrar-modal'); 
	}
	
	// Disparar el modal de manera manual
	$('.trigger-modal').on('load', function() {
		$('.md-content-modal').addClass('md-mostrar-modal');
	})
	
	// Cerrar el modal de manera manual
	$('.md-close').on('click', function() {
//		$('#modalRenovToken').empty();
		$('.md-content-modal').removeClass('md-mostrar-modal');
	})
});