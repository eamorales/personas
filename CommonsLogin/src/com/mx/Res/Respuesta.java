package com.mx.Res;



import com.mx.beans.ErrorDTO;
import com.mx.beans.Busqueda.BeanCliente;

import BeanRespuesta.RespuestaBase;

public class Respuesta extends RespuestaBase{
	private ErrorDTO error;
	/**Clientes**/
	
	private BeanCliente cliente;
	
	public BeanCliente getCliente() {
		return cliente;
	}

	public void setCliente(BeanCliente cliente) {
		this.cliente = cliente;
	}

	public ErrorDTO getError() {
		return error;
	}

	public void setError(ErrorDTO error) {
		this.error = error;
	}
	
}
