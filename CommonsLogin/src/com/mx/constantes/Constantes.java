package com.mx.constantes;
/**
 * @author Gustavo Bujano
 * @version 1.0
 * @fecha 06/06/2017
 * @descripci�n: aqui estar�n todos los datos de variables que se puedan utilizar en la pantalla
 * */
public interface Constantes {
	/*Genericos*/
	public static String BLANK="";
	public static String PIPE="|";
	public static String SLASH="/";
	public static String AUTH_HEADER="Authorization";
	public static String CLAVE_CORRECTA = "00000";
	
	final static int UNO = 1;
	final static int DOS = 2;
	final static int TRES = 3;
	final static int OCHO = 8;
	final static int SIETE= 7;
	final static int CERO = 0;
	final static String REGISTRODEFAULT= "0";
	final static String ERROR ="ERROR";
	final static String EXITO ="EXITO";
	

	
	// Tipos de Header
	final public static String HEADER_STATUS="COD_STATUS";
	final public static String HEADER_IDMESSAGE="IDMESSAGE";
	final public static String HEADER_MESSAGE="MESSAGE";
	final public static String HEADER_SESSION="SESSION_PARAM";
	final public static String HEADER_NAVEGADOR="user-agent";
	final public static String HEADER_CONTET="Content-Type";
	final public static String HEADER_ACCEPT="Accept";
	final public static String COD_STATUS_OK="OK";
	final public static String COD_STATUS_ERROR="ERROR";
	final public static String CONTENT_TYPE="application/json;charset=UTF-8";
	final public static String COD_ERROR_99999="99999";
	final public static String COD_ERROR_99998="99998";
	final public static String COD_ERROR_11111="11111";
	final public static String COD_ERROR_00000="00000";
	
}
