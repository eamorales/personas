package com.mx.constantes;


/**
 * @author Gustavo Bujano
 * @fecha: 06/06/2017
 * @version 1.0
 * @descripcion: En esta interface se guardan todos los mensajes de la pantalla
 * */
public final class HardCode {
	  private HardCode() {
		    throw new IllegalStateException("Utility class");
		  }
	public static final  String MSG_SAVE_SUCCESS ="Datos guardados correctamente.";
	public static final  String ERRROR_TELEFONO = "ERROR INESPERADO EN C AL  CONSULTAR DATOS TELEFONO";
	public static final  String ERROR ="Error";
	public static final  String SUCCESS ="Exito";
	public static final  String ERROR_LLENE_CAMPOS_REQUERIDOS="LLENE LOS CAMPOS REQUERIDOS";
	public static final  String SELECCIONE = "XXXX";
	public static final  String USERYPASS = "EL USUARIO O LA CONTRASE\u00D1A ESTAN INCORRECTOS";
	public static final  String USERPASSEMPTY = "EL USUARIO O LA CONTRASE\u00D1A NO PUEDEN ESTAR VACIOS";
	public static final  String TOKENINVALIDO= "Clave Dinamica Incorrecta. Favor de validar";
	public static final  String TOKENCORRECTO = "Clave Dinamica Correcta";
	public static final  String CANTIDADDIGITOSERROR = "INTRODUZCA LOS 6 CARACTERES";
	
	public static final  String ERROR_SERVICIO ="HA OCURRIDO UN ERROR, FAVOR DE CONTACTAR AL ADMINISTRADOR"; 
	
}
