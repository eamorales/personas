package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class BeanIdentificacion extends RespuestaBase {
	private String idide;
	private List<String> tipoIdentificacion;
	private String tipoIdentificacionSelected;
	private String valor;
	private String estatus;
	private boolean principal;
	/*Listado para llenar la tabla*/
	private List<BeanIdentificacion> listaDirectorio; // Tabla
	/*Listado para llenar la tabla seleccionad*/
	private BeanIdentificacion listaDirectorioSelected; // Tabla
	
	/*Id seleccioado de la tabla*/
	private String idIdeSeleccionado;
	
	public String getIdide() {
		return idide;
	}
	public void setIdide(String idide) {
		this.idide = idide;
	}
	public List<String> getTipoIdentificacion() {
		return tipoIdentificacion;
	}
	public void setTipoIdentificacion(List<String> tipoIdentificacion) {
		this.tipoIdentificacion = tipoIdentificacion;
	}
	public String getValor() {
		return valor;
	}
	public void setValor(String valor) {
		this.valor = valor;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public boolean isPrincipal() {
		return principal;
	}
	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}
	
	public List<BeanIdentificacion> getListaDirectorio() {
		return listaDirectorio;
	}
	public void setListaDirectorio(List<BeanIdentificacion> listaDirectorio) {
		this.listaDirectorio = listaDirectorio;
	}
	public BeanIdentificacion getListaDirectorioSelected() {
		return listaDirectorioSelected;
	}
	public void setListaDirectorioSelected(BeanIdentificacion listaDirectorioSelected) {
		this.listaDirectorioSelected = listaDirectorioSelected;
	}
	public String getIdIdeSeleccionado() {
		return idIdeSeleccionado;
	}
	
	public void setIdIdeSeleccionado(String idTelSeleccionado) {
		this.idIdeSeleccionado = idTelSeleccionado;
	}
	public String getTipoIdentificacionSelected() {
		return tipoIdentificacionSelected;
	}
	public void setTipoIdentificacionSelected(String tipoIdentificacionSelected) {
		this.tipoIdentificacionSelected = tipoIdentificacionSelected;
	}
	
	
	
	
}
