package com.mx.beans;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class posicionGlobalInversion extends RespuestaBase {
	String cuenta;
	String montoInvertido;
	String fechaInversion;
	String modalida;
	String plazo;
	String fechaVencimiento;
	
	
	public String getCuenta() {
		return cuenta;
	}
	public void setCuenta(String cuenta) {
		this.cuenta = cuenta;
	}
	public String getMontoInvertido() {
		return montoInvertido;
	}
	public void setMontoInvertido(String montoInvertido) {
		this.montoInvertido = montoInvertido;
	}
	public String getFechaInversion() {
		return fechaInversion;
	}
	public void setFechaInversion(String fechaInversion) {
		this.fechaInversion = fechaInversion;
	}
	public String getModalida() {
		return modalida;
	}
	public void setModalida(String modalida) {
		this.modalida = modalida;
	}
	public String getPlazo() {
		return plazo;
	}
	public void setPlazo(String plazo) {
		this.plazo = plazo;
	}
	public String getFechaVencimiento() {
		return fechaVencimiento;
	}
	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}
	
	

}
