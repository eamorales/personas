package com.mx.beans;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class BeanDatosAdicionales extends RespuestaBase {
	String primerNombre;
	String segundoNombre;
	String primerApellido;
	String seundoApellido;
	String conyugue;
	int numHijos;
	boolean LenguaIndigena;
	boolean redesSociales;
	boolean discapacidad;
	boolean usoInternet;
	/*persona moral*/
	boolean situacion;
	boolean establecimiento;
	String fechaOperaciones;
	
	
	public String getPrimerNombre() {
		return primerNombre;
	}
	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}
	public String getSegundoNombre() {
		return segundoNombre;
	}
	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}
	public String getPrimerApellido() {
		return primerApellido;
	}
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	public String getSeundoApellido() {
		return seundoApellido;
	}
	public void setSeundoApellido(String seundoApellido) {
		this.seundoApellido = seundoApellido;
	}
	public String getConyugue() {
		return conyugue;
	}
	public void setConyugue(String conyugue) {
		this.conyugue = conyugue;
	}
	public int getNumHijos() {
		return numHijos;
	}
	public void setNumHijos(int numHijos) {
		this.numHijos = numHijos;
	}
	public boolean isLenguaIndigena() {
		return LenguaIndigena;
	}
	public void setLenguaIndigena(boolean lenguaIndigena) {
		LenguaIndigena = lenguaIndigena;
	}
	public boolean isRedesSociales() {
		return redesSociales;
	}
	public void setRedesSociales(boolean redesSociales) {
		this.redesSociales = redesSociales;
	}
	public boolean isDiscapacidad() {
		return discapacidad;
	}
	public void setDiscapacidad(boolean discapacidad) {
		this.discapacidad = discapacidad;
	}
	public boolean isUsoInternet() {
		return usoInternet;
	}
	public void setUsoInternet(boolean usoInternet) {
		this.usoInternet = usoInternet;
	}
	public boolean isSituacion() {
		return situacion;
	}
	public void setSituacion(boolean situacion) {
		this.situacion = situacion;
	}
	public boolean isEstablecimiento() {
		return establecimiento;
	}
	public void setEstablecimiento(boolean establecimiento) {
		this.establecimiento = establecimiento;
	}
	public String getFechaOperaciones() {
		return fechaOperaciones;
	}
	public void setFechaOperaciones(String fechaOperaciones) {
		this.fechaOperaciones = fechaOperaciones;
	}
	
}
