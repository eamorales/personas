package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;
/**
 * @author JMata
 * @date 11/06/2017
 * 
 * */

public class posicionGlobalPrincipalBean extends RespuestaBase {

	
	List<posicionGlobalAhorro> listaAhorro;
	List<posicionGlobalCreditos> listaCreditos;
	List<posicionGlobalInversion> listaInversion;
	List<posicionGlobalSeguros> listaSeguros;
	
	
	
	public List<posicionGlobalAhorro> getListaAhorro() {
		return listaAhorro;
	}
	public void setListaAhorro(List<posicionGlobalAhorro> listaAhorro) {
		this.listaAhorro = listaAhorro;
	}
	public List<posicionGlobalCreditos> getListaCreditos() {
		return listaCreditos;
	}
	public void setListaCreditos(List<posicionGlobalCreditos> listaCreditos) {
		this.listaCreditos = listaCreditos;
	}
	public List<posicionGlobalInversion> getListaInversion() {
		return listaInversion;
	}
	public void setListaInversion(List<posicionGlobalInversion> listaInversion) {
		this.listaInversion = listaInversion;
	}
	public List<posicionGlobalSeguros> getListaSeguros() {
		return listaSeguros;
	}
	public void setListaSeguros(List<posicionGlobalSeguros> listaSeguros) {
		this.listaSeguros = listaSeguros;
	}
	
	
}
