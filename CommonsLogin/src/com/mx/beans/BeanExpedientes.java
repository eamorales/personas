package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class BeanExpedientes extends RespuestaBase {
	
	String tipoDocumento;
	String fechaExpedicion;
	String estatus;
	String observaciones;
	String fechaCreacion;
	String ruta;
	String nombreArchivo;
	boolean seleccionado;
	int idExpediente;
	int indice_seleccionado;
	int idExpedienteSeleccionado;
	
	
	public int getIndice_seleccionado() {
		return indice_seleccionado;
	}
	public void setIndice_seleccionado(int indice_seleccionado) {
		this.indice_seleccionado = indice_seleccionado;
	}
	public int getIdExpedienteSeleccionado() {
		return idExpedienteSeleccionado;
	}
	public void setIdExpedienteSeleccionado(int idExpedienteSeleccionado) {
		this.idExpedienteSeleccionado = idExpedienteSeleccionado;
	}
	private List<BeanExpedientes> listaExpedientes;//tabla
	
	public String getTipoDocumento() {
		return tipoDocumento;
	}
	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}
	public String getFechaExpedicion() {
		return fechaExpedicion;
	}
	public void setFechaExpedicion(String fechaExpedicion) {
		this.fechaExpedicion = fechaExpedicion;
	}
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getObservaciones() {
		return observaciones;
	}
	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}
	public String getFechaCreacion() {
		return fechaCreacion;
	}
	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getRuta() {
		return ruta;
	}
	public void setRuta(String ruta) {
		this.ruta = ruta;
	}
	public String getNombreArchivo() {
		return nombreArchivo;
	}
	public void setNombreArchivo(String nombreArchivo) {
		this.nombreArchivo = nombreArchivo;
	}
	public boolean isSeleccionado() {
		return seleccionado;
	}
	public void setSeleccionado(boolean seleccionado) {
		this.seleccionado = seleccionado;
	}
	public int getIdExpediente() {
		return idExpediente;
	}
	public void setIdExpediente(int idExpediente) {
		this.idExpediente = idExpediente;
	}
	public List<BeanExpedientes> getListaExpedientes() {
		return listaExpedientes;
	}
	public void setListaExpedientes(List<BeanExpedientes> listaExpedientes) {
		this.listaExpedientes = listaExpedientes;
	}
	
	
	
}
