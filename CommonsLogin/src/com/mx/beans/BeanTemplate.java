package com.mx.beans;
 
import BeanRespuesta.RespuestaBase;

/**
 * @autor: GBG
 * @Fecha: 27/05/2017
 * @versi�n 1.1
 * @comentario: datos globales.
 * */
public class BeanTemplate extends RespuestaBase{
	private String hora;
	private String minutos;
	public String fecha;
	private String usuario;
	private String grupo;
	private String cabecera;
	private String subCabecera;
	private String accesosRapidos;
	private String mensaje;
	private String tipoPersonas="2";
	/*
	 * 1.- Persona Fisica
	 * 2.-Persona Moral
	 * */
	
	public void BeanTeamplate(String hora,	String minutos,	String fecha,	String usuario,	String grupo,	String cabecera,	String subCabecera,	String accesosRapidos,	String mensaje){
		this.hora = hora;
		this.minutos = minutos ;
		this.fecha= fecha ;
		this.usuario= usuario ;
		this.grupo=grupo  ;
		this.cabecera=cabecera  ;
		this.subCabecera=subCabecera  ;
		this.accesosRapidos= accesosRapidos ;
		this.mensaje=mensaje  ;
	}
	public String getTipoPersonas() {
		return tipoPersonas;
	}
	public void setTipoPersonas(String tipoPersonas) {
		this.tipoPersonas = tipoPersonas;
	}
	public String getHora() {
		return hora;
	}
	public void setHora(String hora) {
		this.hora = hora;
	}
	public String getMinutos() {
		return minutos;
	}
	public void setMinutos(String minutos) {
		this.minutos = minutos;
	}
	
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getUsuario() {
		return usuario;
	}
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
	public String getGrupo() {
		return grupo;
	}
	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}
	public String getCabecera() {
		return cabecera;
	}
	public void setCabecera(String cabecera) {
		this.cabecera = cabecera;
	}
	public String getSubCabecera() {
		return subCabecera;
	}
	public void setSubCabecera(String subCabecera) {
		this.subCabecera = subCabecera;
	}
	public String getAccesosRapidos() {
		return accesosRapidos;
	}
	public void setAccesosRapidos(String accesosRapidos) {
		this.accesosRapidos = accesosRapidos;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	
	
}
