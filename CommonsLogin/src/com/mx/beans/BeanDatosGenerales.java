package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class BeanDatosGenerales extends RespuestaBase {
	boolean tipoPersona;
	String rfc;
	String primerNombre;
	String segundoNombre;
	String primerApellido;
	String segundoApellido;
	boolean sexo;
	String curp;
	List<String>estadoCivil;
	List<String>ocupacion;
	List<String>gradoEstudios;
	List<String>nacionalidad;
	List<String>estadoNacimiento;
	List<String>lugarNacimiento;
	List<String>dominioCorreo;
	String fechaNacimiento;
	String correoElectronico;
	/*persona moral*/
	String empresa;
	List<String> giro;
	String actividad;
	String sector;
	String personalidad;
	String tipoSociedad;
	
	
	public boolean isTipoPersona() {
		return tipoPersona;
	}
	public void setTipoPersona(boolean tipoPersona) {
		this.tipoPersona = tipoPersona;
	}
	public String getRfc() {
		return rfc;
	}
	public void setRfc(String rfc) {
		this.rfc = rfc;
	}
	public String getPrimerNombre() {
		return primerNombre;
	}
	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}
	public String getSegundoNombre() {
		return segundoNombre;
	}
	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}
	public String getPrimerApellido() {
		return primerApellido;
	}
	public void setPrimerApellido(String primerApellido) {
		this.primerApellido = primerApellido;
	}
	public String getSegundoApellido() {
		return segundoApellido;
	}
	public void setSegundoApellido(String segundoApellido) {
		this.segundoApellido = segundoApellido;
	}
	public boolean isSexo() {
		return sexo;
	}
	public void setSexo(boolean sexo) {
		this.sexo = sexo;
	}
	public String getCurp() {
		return curp;
	}
	public void setCurp(String curp) {
		this.curp = curp;
	}
	public List<String> getEstadoCivil() {
		return estadoCivil;
	}
	public void setEstadoCivil(List<String> estadoCivil) {
		this.estadoCivil = estadoCivil;
	}
	public List<String> getOcupacion() {
		return ocupacion;
	}
	public void setOcupacion(List<String> ocupacion) {
		this.ocupacion = ocupacion;
	}
	public List<String> getGradoEstudios() {
		return gradoEstudios;
	}
	public void setGradoEstudios(List<String> gradoEstudios) {
		this.gradoEstudios = gradoEstudios;
	}
	public List<String> getNacionalidad() {
		return nacionalidad;
	}
	public void setNacionalidad(List<String> nacionalidad) {
		this.nacionalidad = nacionalidad;
	}
	public List<String> getEstadoNacimiento() {
		return estadoNacimiento;
	}
	public void setEstadoNacimiento(List<String> estadoNacimiento) {
		this.estadoNacimiento = estadoNacimiento;
	}
	public List<String> getLugarNacimiento() {
		return lugarNacimiento;
	}
	public void setLugarNacimiento(List<String> lugarNacimiento) {
		this.lugarNacimiento = lugarNacimiento;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getCorreoElectronico() {
		return correoElectronico;
	}
	public void setCorreoElectronico(String correoElectronico) {
		this.correoElectronico = correoElectronico;
	}
	public String getEmpresa() {
		return empresa;
	}
	public void setEmpresa(String empresa) {
		this.empresa = empresa;
	}
	public List<String> getGiro() {
		return giro;
	}
	public void setGiro(List<String> giro) {
		this.giro = giro;
	}
	public String getActividad() {
		return actividad;
	}
	public void setActividad(String actividad) {
		this.actividad = actividad;
	}
	public String getSector() {
		return sector;
	}
	public void setSector(String sector) {
		this.sector = sector;
	}
	public String getPersonalidad() {
		return personalidad;
	}
	public void setPersonalidad(String personalidad) {
		this.personalidad = personalidad;
	}
	public String getTipoSociedad() {
		return tipoSociedad;
	}
	public void setTipoSociedad(String tipoSociedad) {
		this.tipoSociedad = tipoSociedad;
	}
	public List<String> getDominioCorreo() {
		return dominioCorreo;
	}
	public void setDominioCorreo(List<String> dominioCorreo) {
		this.dominioCorreo = dominioCorreo;
	}
	@Override
	public String toString() {
		return "BeandatosGenerales [tipoPersona=" + tipoPersona + ", rfc=" + rfc + ", primerNombre=" + primerNombre
				+ ", segundoNombre=" + segundoNombre + ", primerApellido=" + primerApellido + ", segundoApellido="
				+ segundoApellido + ", sexo=" + sexo + ", curp=" + curp + ", estadoCivil=" + estadoCivil
				+ ", ocupacion=" + ocupacion + ", gradoEstudios=" + gradoEstudios + ", nacionalidad=" + nacionalidad
				+ ", estadoNacimiento=" + estadoNacimiento + ", lugarNacimiento=" + lugarNacimiento + ", dominioCorreo="
				+ dominioCorreo + ", fechaNacimiento=" + fechaNacimiento + ", correoElectronico=" + correoElectronico
				+ ", empresa=" + empresa + ", giro=" + giro + ", actividad=" + actividad + ", sector=" + sector
				+ ", personalidad=" + personalidad + ", tipoSociedad=" + tipoSociedad + "]";
	}
	
	
}
