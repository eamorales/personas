package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class BeanDomicilios extends RespuestaBase {
//	String idDom;
	String codigoPostal;
	List<String> colonias;
	boolean principal;
	List<String> ubicacion1;
	String descripcionU1;
	List<String> ubicacion2;
	String descripcionU2;
	List<String> ubicacion3;
	String descripcionU3;
	List<String> numeracion1;
	String descripcionN1;
	List<String> numeracion2;
	String descripcionN2;
	String referencia;
	String direccion;
	String latitud;
	String longitud;
	String foto;
	
	List<BeanDomiciliosHistorico> listaDomicilios ;
	
	List<BeanDomiciliosHistorico> listaDomiciliosSelected ; 
	
	
	public List<BeanDomiciliosHistorico> getListaDomiciliosSelected() {
		return listaDomiciliosSelected;
	}
	public void setListaDomiciliosSelected(List<BeanDomiciliosHistorico> listaDomiciliosSelected) {
		this.listaDomiciliosSelected = listaDomiciliosSelected;
	}
	public List<BeanDomiciliosHistorico> getListaDomicilios() {
		return listaDomicilios;
	}
	public void setListaDomicilios(List<BeanDomiciliosHistorico> listaDomicilios) {
		this.listaDomicilios = listaDomicilios;
	}
	
	
	
//	public String getIdDom() {
//		return idDom;
//	}
//	public void setIdDom(String idDom) {
//		this.idDom = idDom;
//	}
	public String getCodigoPostal() {
		return codigoPostal;
	}
	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}
	public boolean isPrincipal() {
		return principal;
	}
	public void setPrincipal(boolean principal) {
		this.principal = principal;
	}
	public List<String> getUbicacion1() {
		return ubicacion1;
	}
	public void setUbicacion1(List<String> ubicacion1) {
		this.ubicacion1 = ubicacion1;
	}
	public String getDescripcionU1() {
		return descripcionU1;
	}
	public void setDescripcionU1(String descripcionU1) {
		this.descripcionU1 = descripcionU1;
	}
	public List<String> getUbicacion2() {
		return ubicacion2;
	}
	public void setUbicacion2(List<String> ubicacion2) {
		this.ubicacion2 = ubicacion2;
	}
	public String getDescripcionU2() {
		return descripcionU2;
	}
	public void setDescripcionU2(String descripcionU2) {
		this.descripcionU2 = descripcionU2;
	}
	public List<String> getUbicacion3() {
		return ubicacion3;
	}
	public void setUbicacion3(List<String> ubicacion3) {
		this.ubicacion3 = ubicacion3;
	}
	public String getDescripcionU3() {
		return descripcionU3;
	}
	public void setDescripcionU3(String descripcionU3) {
		this.descripcionU3 = descripcionU3;
	}
	public List<String> getNumeracion1() {
		return numeracion1;
	}
	public void setNumeracion1(List<String> numeracion1) {
		this.numeracion1 = numeracion1;
	}
	public String getDescripcionN1() {
		return descripcionN1;
	}
	public void setDescripcionN1(String descripcionN1) {
		this.descripcionN1 = descripcionN1;
	}
	public List<String> getNumeracion2() {
		return numeracion2;
	}
	public void setNumeracion2(List<String> numeracion2) {
		this.numeracion2 = numeracion2;
	}
	public String getDescripcionN2() {
		return descripcionN2;
	}
	public void setDescripcionN2(String descripcionN2) {
		this.descripcionN2 = descripcionN2;
	}
	public String getReferencia() {
		return referencia;
	}
	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public String getLatitud() {
		return latitud;
	}
	public void setLatitud(String latitud) {
		this.latitud = latitud;
	}
	public String getLongitud() {
		return longitud;
	}
	public void setLongitud(String longitud) {
		this.longitud = longitud;
	}
	public String getFoto() {
		return foto;
	}
	public void setFoto(String foto) {
		this.foto = foto;
	}
	public List<String> getColonias() {
		return colonias;
	}
	public void setColonias(List<String> colonias) {
		this.colonias = colonias;
	}

}
