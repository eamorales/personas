package com.mx.beans;

import BeanRespuesta.RespuestaBase;

/*
 * @autor: JMata
 * @Fecha: 23/05/2017
 * 
 * */
public class posicionGlobalCreditos extends RespuestaBase {
	/*posicion global/ creditos*/
	String estatus;
	String producto;
	String credito;
	String saldoInsoluto;
	String calif;
	String diasVencidos;
	String avance;
	String disposicion;
	
	public String getSaldoInsoluto() {
		return saldoInsoluto;
	}
	public void setSaldoInsoluto(String saldoInsoluto) {
		this.saldoInsoluto = saldoInsoluto;
	}
	
	public String getEstatus() {
		return estatus;
	}
	public void setEstatus(String estatus) {
		this.estatus = estatus;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public String getCredito() {
		return credito;
	}
	public void setCredito(String credito) {
		this.credito = credito;
	}

	public String getCalif() {
		return calif;
	}
	public void setCalif(String calif) {
		this.calif = calif;
	}
	public String getDiasVencidos() {
		return diasVencidos;
	}
	public void setDiasVencidos(String diasVencidos) {
		this.diasVencidos = diasVencidos;
	}
	public String getAvance() {
		return avance;
	}
	public void setAvance(String avance) {
		this.avance = avance;
	}
	public String getDisposicion() {
		return disposicion;
	}
	public void setDisposicion(String disposicion) {
		this.disposicion = disposicion;
	}
	
	
}
