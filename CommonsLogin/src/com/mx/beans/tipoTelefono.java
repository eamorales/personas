package com.mx.beans;


import java.util.List;

import BeanRespuesta.RespuestaBase;
/**
 * @author Gustavo Bujano
 * @fecha: 06/06/2017
 * @version 1.0
 * @descripción: sirve para cargar el catalogo de telefonos
 * */
public class tipoTelefono extends RespuestaBase {
	String tipoTelefono;
	List<String> listaTipoCompiania;
	public String getTipoTelefono() {
		return tipoTelefono;
	}
	public void setTipoTelefono(String tipoTelefono) {
		this.tipoTelefono = tipoTelefono;
	}
	public List<String> getListaTipoCompiania() {
		return listaTipoCompiania;
	}
	public void setListaTipoCompiania(List<String> listaTipoCompiania) {
		this.listaTipoCompiania = listaTipoCompiania;
	}
	

	
	
}
