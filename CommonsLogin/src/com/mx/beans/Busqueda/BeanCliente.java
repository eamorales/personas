package com.mx.beans.Busqueda;

import com.mx.beans.BeanDatosGenerales;
import com.mx.beans.BeanInformacion;

public class BeanCliente {
	public BeanDatosGenerales datosGenerales;
	public BeanInformacion BeanInformacion;

	public BeanDatosGenerales getDatosGenerales() {
		return datosGenerales;
	}
	public void setDatosGenerales(BeanDatosGenerales datosGenerales) {
		this.datosGenerales = datosGenerales;
	}
	public BeanInformacion getBeanInformacion() {
		return BeanInformacion;
	}
	public void setBeanInformacion(BeanInformacion beanInformacion) {
		BeanInformacion = beanInformacion;
	}
	@Override
	public String toString() {
		return "BeanCliente [ datosGenerales=" + datosGenerales + ", BeanInforacion="
				+ BeanInformacion + "]";
	}	
	
}
