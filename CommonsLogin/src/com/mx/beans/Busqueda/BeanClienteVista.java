package com.mx.beans.Busqueda;

import com.mx.beans.Busqueda.BeanClienteEntrada;
import com.mx.beans.Busqueda.BeanClienteVista;

import BeanRespuesta.RespuestaBase;

public class BeanClienteVista extends RespuestaBase {
	
	BeanClienteEntrada beanClienteEntrada;
	BeanCliente beanCliente;
	public BeanClienteEntrada getBeanClienteEntrada() {
		return beanClienteEntrada;
	}
	public void setBeanClienteEntrada(BeanClienteEntrada beanClienteEntrada) {
		this.beanClienteEntrada = beanClienteEntrada;
	}
	public BeanCliente getBeanCliente() {
		return beanCliente;
	}
	public void setBeanCliente(BeanCliente beanCliente) {
		this.beanCliente = beanCliente;
	}
	
}
