package com.mx.beans.Busqueda;

public class BeanClienteEntrada {
	Long noCliente;
	String primerNombre;
	String segundoNombre;
	String apellidoPaterno;
	String apellidoMaterno;
	String fechaNacimiento;
	String numeroCelular;
	String correo;
	
	public Long getNoCliente() {
		return noCliente;
	}
	public void setNoCliente(Long noCliente) {
		this.noCliente = noCliente;
	}
	public String getPrimerNombre() {
		return primerNombre;
	}
	public void setPrimerNombre(String primerNombre) {
		this.primerNombre = primerNombre;
	}
	public String getSegundoNombre() {
		return segundoNombre;
	}
	public void setSegundoNombre(String segundoNombre) {
		this.segundoNombre = segundoNombre;
	}
	public String getApellidoPaterno() {
		return apellidoPaterno;
	}
	public void setApellidoPaterno(String apellidoPaterno) {
		this.apellidoPaterno = apellidoPaterno;
	}
	public String getApellidoMaterno() {
		return apellidoMaterno;
	}
	public void setApellidoMaterno(String apellidoMaterno) {
		this.apellidoMaterno = apellidoMaterno;
	}
	public String getFechaNacimiento() {
		return fechaNacimiento;
	}
	public void setFechaNacimiento(String fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}
	public String getNumeroCelular() {
		return numeroCelular;
	}
	public void setNumeroCelular(String numeroCelular) {
		this.numeroCelular = numeroCelular;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	@Override
	public String toString() {
		return "BeanClienteEntrada [noCliente=" + noCliente + ", primerNombre=" + primerNombre + ", segudoNombre="
				+ segundoNombre + ", apellidoPaterno=" + apellidoPaterno + ", apellidoMaterno=" + apellidoMaterno
				+ ", fechaNacimiento=" + fechaNacimiento + ", numeroCelular=" + numeroCelular + ", correo=" + correo
				+ "]";
	}
	
}
