package com.mx.beans;

import java.util.List;

import BeanRespuesta.RespuestaBase;

public class actualizarCorreo extends RespuestaBase {
	
	String correo;
	List<String>dominio;
	
	
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public List<String> getDominio() {
		return dominio;
	}
	public void setDominio(List<String> dominio) {
		this.dominio = dominio;
	}
	
	
}
