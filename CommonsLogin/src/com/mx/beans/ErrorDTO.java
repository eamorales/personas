/*
 * ErrorDTO.java 1.0 12/03/2017
*/
/**
 * 
 * @author Inver-tu
 * @version 1.0, 12/03/2017
 * @since 1.0
 */
package com.mx.beans;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlRootElement;

/**
 * @author Administrador
 *
 */
@XmlRootElement
public class ErrorDTO implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8364809659019952404L;

	public static final ErrorDTO SUCCESS = new ErrorDTO("0", "");
	
	private String message;
	private String clave;
	
	
	public ErrorDTO(String clave, String message){
        this.clave = clave;
        this.message = message;
    }
	
	public ErrorDTO(){
    }


	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}


	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}


	/**
	 * @return the clave
	 */
	public String getClave() {
		return clave;
	}


	/**
	 * @param clave the clave to set
	 */
	public void setClave(String clave) {
		this.clave = clave;
	}
	
}
