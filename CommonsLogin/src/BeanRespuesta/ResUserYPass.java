package BeanRespuesta;

public class ResUserYPass {
	private String codeEstatus;
	private String messageEstatus;
	
	public String getCodeEstatus() {
		return codeEstatus;
	}
	public void setCodeEstatus(String codeEstatus) {
		this.codeEstatus = codeEstatus;
	}
	public String getMessageEstatus() {
		return messageEstatus;
	}
	public void setMessageEstatus(String messageEstatus) {
		this.messageEstatus = messageEstatus;
	}
	
}
