package BeanRespuesta;

import com.mx.beans.BeanGenerico;
/**
 * @Autor: Gustavo Bujano Guzm�n
 * @Fecha: 06/06/2017
 * @version: 1.0
 * */
public class RespuestaBase extends BeanGenerico{
	/*Si el codigoError != 0, ocurrio un problema*/
	private String codigoError;
	/*Se muestra el texto del error en pantalla*/
	private String mensaje;
	
	
	public RespuestaBase() {
		
	}
	
	public RespuestaBase(String codigoError, String mensaje) {
		super();
		this.codigoError = codigoError;
		this.mensaje = mensaje;
	}
	
	public String getCodigoError() {
		return codigoError;
	}
	public void setCodigoError(String codigoError) {
		this.codigoError =codigoError;
	}

	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje= mensaje;
	}
	public void setRespuestaBase(String codigoError, String mensaje){
		this.codigoError = codigoError;
		this.mensaje = mensaje;
	}
	
	
}
