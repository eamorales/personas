package BeanRespuesta;

import java.util.ArrayList;

public class ValidaServicioToken {
	String codeEstatus;
	String messageEstatus;
	ArrayList<resultados> resultados = new ArrayList<>();
	public String getCodeEstatus() {
		return codeEstatus;
	}
	public void setCodeEstatus(String codeEstatus) {
		this.codeEstatus = codeEstatus;
	}
	public String getMessageEstatus() {
		return messageEstatus;
	}
	public void setMessageEstatus(String messageEstatus) {
		this.messageEstatus = messageEstatus;
	}
	public ArrayList<resultados> getResultados() {
		return resultados;
	}
	public void setResultados(ArrayList<resultados> resultados) {
		this.resultados = resultados;
	}

}
