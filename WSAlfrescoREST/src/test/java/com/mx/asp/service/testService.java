
package com.mx.asp.service;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import org.apache.log4j.Logger;
import org.junit.Before;
import org.junit.Test;

import com.asp.service.AlfrescoServiceImpl;
import com.asp.service.IAlfrescoService;
import com.mx.beans.BeanImagenAlfresco;

/**
 * @author eduardo
 *
 */
public class testService {		
	final static Logger logger = Logger.getLogger(testService.class);
	/**
	 * @throws java.lang.exception
	 */
	@Before
	public void setup() throws Exception {
	}

	@Test
	public void testInsertaImagen() {
		IAlfrescoService alfresoservice = new AlfrescoServiceImpl();
		
		File file = new File("c:\\pruebaalfresco\\contenido\\promesi.jpg");
		BeanImagenAlfresco imagenalfresco= null;
		InputStream imageninputstream = null;
		try {
			imageninputstream = new FileInputStream(file);	
			//byte[] bytearr = new byte[] { 0xc, 0xa, 0xf, 0xe };
			
			byte[] bytearr =new byte[(int) file.length()];
			imageninputstream.read(bytearr);
			imageninputstream.close();					
					
			imagenalfresco= alfresoservice.salvaImagen(bytearr, "imagen1.jpg", "" );
			
			assertNotNull(imagenalfresco);
			assertNotEquals("", imagenalfresco.getIdImagen());
			logger.info("el id generado: "+imagenalfresco.getIdImagen());
		} catch (FileNotFoundException e) {
			fail("no se pudo cargar la imagen");
			logger.error("error", e);			
		} catch (IOException e) {
			logger.error("error", e);
		}
	}
	
	@Test
	public void testBuscaImagen() {
		IAlfrescoService alfresoservice = new AlfrescoServiceImpl();
		BeanImagenAlfresco imagenalfresco= null;
		String idImagen = "2eea77b9-b1bb-47fe-9ccc-986d8e7fd006";
		
		try {
			imagenalfresco= alfresoservice.buscaImagen(idImagen);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}			
		
		assertNotNull(imagenalfresco);
		assertNotNull(imagenalfresco.getIdImagen());
		assertNotEquals("", imagenalfresco.getIdImagen());
		crearArchivo(imagenalfresco.getImagen(),"c:\\pruebaalfresco\\contenido\\salida.jpg");
	}
	
	
	public void crearArchivo(byte[] fileBytes, String ruta) {
		OutputStream out;
		try {
			out = new FileOutputStream(ruta);
			out.write(fileBytes); 
		     out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void testCrearCarpeta() {
		IAlfrescoService alfresoservice = new AlfrescoServiceImpl();
		String nombreCarpeta = "otra";
		alfresoservice.crearCarpeta(nombreCarpeta);		
	}
	
	
	@Test
	public void testInsertaDocumento() {
		IAlfrescoService alfresoservice = new AlfrescoServiceImpl();
		
		File file = new File("c:\\pruebaalfresco\\contenido\\promesi.jpg");
		BeanImagenAlfresco imagenalfresco= null;
		InputStream imageninputstream = null;
		try {
			imageninputstream = new FileInputStream(file);	
			//byte[] bytearr = new byte[] { 0xc, 0xa, 0xf, 0xe };
			
			byte[] bytearr =new byte[(int) file.length()];
			imageninputstream.read(bytearr);
			imageninputstream.close();					
					
			imagenalfresco= alfresoservice.salvaDocumento(bytearr, "imagen1", "image/jpeg", "otra");
			
			assertNotNull(imagenalfresco);
			assertNotEquals("", imagenalfresco.getIdImagen());
			logger.info("el id generado: "+imagenalfresco.getIdImagen());
		} catch (FileNotFoundException e) {
			fail("no se pudo cargar la imagen");
			logger.error("error", e);			
		} catch (IOException e) {
			logger.error("error", e);
		}
	}
	
}
