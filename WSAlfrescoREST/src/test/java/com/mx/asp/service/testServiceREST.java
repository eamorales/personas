/**
 * 
 */
package com.mx.asp.service;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.fail;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import javax.ws.rs.core.UriBuilder;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.mx.Req.ImagenAlfrescoReq;
import com.mx.Res.Respuesta;
import com.mx.url.UrlService;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

/**
 * @author Eduardo
 *
 */
public class testServiceREST {

	@Test
	public void testInserta() {
		ImagenAlfrescoReq rq = new ImagenAlfrescoReq();
		byte[] byteArr = new byte[] { 0xC, 0xA, 0xF, 0xE };
		rq.setFile(byteArr);
		rq.setNombreImagen("nuevo.jpg");
		rq.setCarpeta("otra");
		final ObjectMapper mapper = new ObjectMapper();
		String tramaFinal = "";
		String url3 = "";

		try {
			tramaFinal = mapper.writeValueAsString(rq);

			url3 = "http://localhost:8080//WSAlfrescoREST/data/"+UrlService.SUBIR_IMAGENES;
			Respuesta salida = null;
			Client client3 = Client.create();
			final HTTPBasicAuthFilter authFilter3 = new HTTPBasicAuthFilter("x", "xxx!x");
			client3.addFilter(authFilter3);
			WebResource webResource3 = client3.resource(UriBuilder.fromUri(url3).build());
			ClientResponse responsex3 = webResource3.type("application/json").post(ClientResponse.class, tramaFinal);
			String cadenaSalida = responsex3.getEntity(String.class);
			 salida = mapper.readValue(cadenaSalida, Respuesta.class);

			assertNotNull(salida);
			assertNotNull(salida.getImagenAlfresco());
			assertNotEquals("", salida.getImagenAlfresco());
			System.out.println("Output from Server .... \n");
			System.out.println(salida.getImagenAlfresco().getIdImagen());
			
			if (responsex3.getStatus() == 404) {
				fail("el servicio no encontro los datos");
			}
		} catch (JsonProcessingException e) {
			fail("falla de mapeo");
			e.printStackTrace();
		} catch (IOException e) {
			fail("falla de mapeo");
			e.printStackTrace();
		}
	}
	
	@Test
	public void testBusqueda() {
		ImagenAlfrescoReq rq = new ImagenAlfrescoReq();
		rq.setId("2eea77b9-b1bb-47fe-9ccc-986d8e7fd006");
		final ObjectMapper mapper = new ObjectMapper();
		String tramaFinal = "";
		String url3 = "";

		try {
			tramaFinal = mapper.writeValueAsString(rq);

			url3 = "http://localhost:8080//WSAlfrescoREST/data/"+UrlService.CONSULTA_IMAGENES;
			Respuesta salida = null;
			Client client3 = Client.create();
			final HTTPBasicAuthFilter authFilter3 = new HTTPBasicAuthFilter("x", "xxx!x");
			client3.addFilter(authFilter3);
			WebResource webResource3 = client3.resource(UriBuilder.fromUri(url3).build());
			ClientResponse responsex3 = webResource3.type("application/json").post(ClientResponse.class, tramaFinal);
			String cadenaSalida = responsex3.getEntity(String.class);
			 salida = mapper.readValue(cadenaSalida, Respuesta.class);

			assertNotNull(salida);
			assertNotNull(salida.getImagenAlfresco());
			assertNotEquals("", salida.getImagenAlfresco());
			System.out.println("Output from Server .... \n");
			System.out.println(salida.getImagenAlfresco().getIdImagen());
			System.out.println(salida.getImagenAlfresco().getTipo());
			if (responsex3.getStatus() == 404) {
				fail("el servicio no encontro los datos");
			}
			crearArchivo(salida.getImagenAlfresco().getImagen(),"c:\\pruebaalfresco\\contenido\\salida.jpg");
			
		} catch (JsonProcessingException e) {
			fail("falla de mapeo");
			e.printStackTrace();
		} catch (IOException e) {
			fail("falla de mapeo");
			e.printStackTrace();
		}
	}
	
	public void crearArchivo(byte[] fileBytes, String ruta) {
		OutputStream out;
		try {
			out = new FileOutputStream(ruta);
			out.write(fileBytes); 
		     out.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}

}
