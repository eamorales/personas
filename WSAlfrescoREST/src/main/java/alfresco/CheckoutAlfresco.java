package alfresco;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.HashMap;
import java.util.Map;

import org.apache.chemistry.opencmis.client.api.CmisObject;
import org.apache.chemistry.opencmis.client.api.Document;
import org.apache.chemistry.opencmis.client.api.Folder;
import org.apache.chemistry.opencmis.client.api.Session;
import org.apache.chemistry.opencmis.client.runtime.DocumentImpl;
import org.apache.chemistry.opencmis.commons.PropertyIds;
import org.apache.chemistry.opencmis.commons.data.ContentStream;
import org.apache.chemistry.opencmis.commons.enums.VersioningState;
import org.apache.chemistry.opencmis.commons.exceptions.CmisContentAlreadyExistsException;
import org.apache.chemistry.opencmis.commons.exceptions.CmisObjectNotFoundException;
import org.apache.log4j.Logger;
import org.apache.poi.util.IOUtils;

import com.mx.beans.BeanImagenAlfresco;

public class CheckoutAlfresco extends CMISAlfrescoBase {

	final static Logger logger = Logger.getLogger(CheckoutAlfresco.class);

	public BeanImagenAlfresco createDocumentFromFileWithCustomType(Session session, Folder parentFolder,
			String documentName, InputStream imagenInputStream) {
		BeanImagenAlfresco imagenAlfresco = new BeanImagenAlfresco();
		// Check if document already exist, if not create it
		Document newDocument = (Document) getObject(session, parentFolder, documentName);
		if (newDocument == null) {
			// Setup document metadata
			Map<String, Object> newDocumentProps = new HashMap<String, Object>();
			// newDocumentProps.put(PropertyIds.OBJECT_TYPE_ID, "D:myc:itDoc");
			newDocumentProps.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
			newDocumentProps.put(PropertyIds.NAME, documentName);

			String mimetype = "image/jpeg";
			ContentStream contentStream = session.getObjectFactory().createContentStream(documentName,
					imagenInputStream.toString().length(), mimetype, imagenInputStream);

			// Create versioned document object
			newDocument = parentFolder.createDocument(newDocumentProps, contentStream, VersioningState.MAJOR);
			logger.info("Created new document: [version=" + newDocument.getVersionLabel() + "][creator="
					+ newDocument.getCreatedBy() + "]");
			imagenAlfresco.setCodigoError("0");
			imagenAlfresco.setMensaje("OK");
			imagenAlfresco.setIdImagen(newDocument.getId());
			logger.info("Document already exist: ");// +
		} else {
			logger.info("Document id: "+newDocument.getId());
													// getDocumentPath(newDocument));
			imagenAlfresco.setCodigoError("1");
			imagenAlfresco.setIdImagen("0");
			imagenAlfresco.setMensaje("Document already exist");
		}

		return imagenAlfresco;
	}

	/**
	 * Get a CMIS Object by name from a specified folder.
	 *
	 * @param parentFolder
	 *            the parent folder where the object might exist
	 * @param objectName
	 *            the name of the object that we are looking for
	 * @return the Cmis Object if it existed, otherwise null
	 */
	private CmisObject getObject(Session session, Folder parentFolder, String objectName) {
		CmisObject object = null;

		try {
			String path2Object = parentFolder.getPath();
			if (!path2Object.endsWith("/")) {
				path2Object += "/";
			}
			path2Object += objectName;
			object = session.getObjectByPath(path2Object);
		} catch (CmisObjectNotFoundException nfe0) {
			logger.info("Document no exist: ");
		}

		return object;
	}

	/**
	 * Get a CMIS Object by ID.
	 * 
	 * @param session
	 * @param id
	 * @return
	 */
	public CmisObject getObjectById(Session session, String id) {
		CmisObject object = null;

		try {
			object = session.getObject(id);
		} catch (CmisObjectNotFoundException nfe0) {
			logger.info("Document no exist: ");
		}

		return object;
	}

	public BeanImagenAlfresco buscaImagen(Session session, String id) throws Exception {
		BeanImagenAlfresco imagenAlfresco = new BeanImagenAlfresco();

		CmisObject object = session.getObject(id);
		if (object != null) {
			try {
				if (object instanceof DocumentImpl) {
					Document doc = (DocumentImpl) object;
					ContentStream contentStream = doc.getContentStream();
					imagenAlfresco.setImagen(IOUtils.toByteArray(contentStream.getStream()));
					imagenAlfresco.setNombre(contentStream.getFileName());
					imagenAlfresco.setTipo(contentStream.getMimeType());
					imagenAlfresco.setCodigoError("0");
					imagenAlfresco.setMensaje("OK");
					imagenAlfresco.setIdImagen(id);
					logger.info("imagen leida con exito");
					return imagenAlfresco;
				} else {
					logger.info("No content.");
					logger.info("Documento no existe: " + id);
					imagenAlfresco.setCodigoError("1");
					imagenAlfresco.setIdImagen("0");
					imagenAlfresco.setMensaje("Document no exist");
				}
			} catch (IOException e) {
				logger.error("error " + e);
				e.printStackTrace();
				throw new Exception(e);
			}

		} else {
			logger.info("No content.");
			logger.info("Documento no existe: " + id);
			imagenAlfresco.setCodigoError("1");
			imagenAlfresco.setIdImagen("0");
			imagenAlfresco.setMensaje("Document no exist");
		}

		return imagenAlfresco;
	}

	/**
	 * Helper method to get the contents of a stream
	 * 
	 * @param stream
	 * @return
	 * @throws IOException
	 */
	private static byte[] getContentAsByte(ContentStream stream) throws IOException {
		StringBuilder sb = new StringBuilder();
		Reader reader = new InputStreamReader(stream.getStream(), "UTF-8");

		try {
			final char[] buffer = new char[4 * 1024];
			int b;
			while (true) {
				b = reader.read(buffer, 0, buffer.length);
				if (b > 0) {
					sb.append(buffer, 0, b);
				} else if (b == -1) {
					break;
				}
			}
		} finally {
			reader.close();
		}

		return sb.toString().getBytes();
	}
	
	public void crearFolder( Folder parentFolder,String nombreFolder){
		// properties
		// (minimal set: name and object type id)
		Map<String, Object> properties = new HashMap<String, Object>();
		properties.put(PropertyIds.OBJECT_TYPE_ID, "cmis:folder");
		properties.put(PropertyIds.NAME, nombreFolder);
		try{
			parentFolder.createFolder(properties);
		}catch(CmisContentAlreadyExistsException e){
			logger.info("Folder already exist: ");
		}
	}

	public BeanImagenAlfresco createDocument(Session session, Folder folder, String nombre, String mimeType,
			String carpeta, InputStream imagenInputStream) {
			BeanImagenAlfresco imagenAlfresco = new BeanImagenAlfresco();
			// Check if document already exist, if not create it
			Document newDocument = (Document) getObject(session, folder, nombre);
			if (newDocument == null) {
				// Setup document metadata
				Map<String, Object> newDocumentProps = new HashMap<String, Object>();
				// newDocumentProps.put(PropertyIds.OBJECT_TYPE_ID, "D:myc:itDoc");
				newDocumentProps.put(PropertyIds.OBJECT_TYPE_ID, "cmis:document");
				newDocumentProps.put(PropertyIds.NAME, nombre);

				String mimetype = mimeType;
				ContentStream contentStream = session.getObjectFactory().createContentStream(nombre,
						imagenInputStream.toString().length(), mimetype, imagenInputStream);

				// Create versioned document object
				newDocument = folder.createDocument(newDocumentProps, contentStream, VersioningState.MAJOR);
				logger.info("Created new document: [version=" + newDocument.getVersionLabel() + "][creator="
						+ newDocument.getCreatedBy() + "]");
				imagenAlfresco.setCodigoError("0");
				imagenAlfresco.setMensaje("OK");
				imagenAlfresco.setIdImagen(newDocument.getId());
				logger.info("Document already exist: ");// +
			} else {
				logger.info("Document id: "+newDocument.getId());
														// getDocumentPath(newDocument));
				imagenAlfresco.setCodigoError("1");
				imagenAlfresco.setIdImagen("0");
				imagenAlfresco.setMensaje("Document already exist");
			}

			return imagenAlfresco;
	}
}
