package alfresco;

import org.alfresco.cmis.client.impl.AlfrescoItemTypeImpl;

/**
 * This class holds common properties and methods for the example classes.
 */
public class AlfrescoBase {
    private String user;
    private String password;
    private String folderName;
    
	public static void doUsage(String message) {
		System.out.println(message);
		System.exit(0);
	}
	
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getFolderName() {
		return folderName;
	}

	public void setFolderName(String folderName) {
		this.folderName = folderName;
	}
	
	private static String[][] mimeTypes = new String[][] {
        { "txt", "text/plain" },
        { "html", "text/html" },
        { "xhtml", "application/xhtml+xml" },
        { "ps", "application/postscript" },
        { "aiff", "audio/x-aiff" },
        { "acp", "application/acp" },
        { "au", "audio/basic" },
        { "avi", "video/x-msvideo" },
        { "asf", "video/x-ms-asf" },
        { "wmv", "video/x-ms-wmv" },
        { "wma", "video/x-ms-wma" },
        { "avx", "video/x-rad-screenplay" },
        { "bcpio", "application/x-bcpio" },
        { "bin", "application/octet-stream" },
        { "cdf", "application/x-netcdf" },
        { "cer", "application/x-x509-ca-cert" },
        { "cgm", "image/cgm" },
        { "class", "application/java" },
        { "cpio", "application/x-cpio" },
        { "csh", "application/x-csh" },
        { "css", "text/css" },
        { "doc", "application/msword" },
        { "xml", "text/xml" },
        { "dvi", "application/x-dvi" },
        { "etx", "text/x-setext" },
        { "gif", "image/gif" },
        { "gml", "application/sgml" },
        { "gtar", "application/x-gtar" },
        { "gzip", "application/x-gzip" },
        { "hdf", "application/x-hdf" },
        { "hqx", "application/mac-binhex40" },
        { "ief", "image/ief" },
        { "bmp", "image/bmp" },
        { "jpg", "image/jpeg" },
        { "js", "application/x-javascript" },
        { "latex", "application/x-latex" },
        { "man", "application/x-troff-man" },
        { "me", "application/x-troff-me" },
        { "ms", "application/x-troff-mes" },
        { "mif", "application/x-mif" },
        { "mpg", "video/mpeg" },
        { "mp3", "audio/x-mpeg" },
        { "mp4", "video/mp4" },
        { "mpeg2", "video/mpeg2" },
        { "mov", "video/quicktime" },
        { "movie", "video/x-sgi-movie" },
        { "oda", "application/oda" },
        { "pbm", "image/x-portable-bitmap" },
        { "pdf", "application/pdf" },
        { "pgm", "image/x-portable-graymap" },
        { "png", "image/png" },
        { "pnm", "image/x-portable-anymap" },
        { "ppm", "image/x-portable-pixmap" },
        { "ppt", "application/vnd.powerpoint" },
        { "ras", "image/x-cmu-raster" },
        { "rgb", "image/x-rgb" },
        { "tr", "application/x-troff" },
        { "rtf", "application/rtf" },
        { "rtx", "text/richtext" },
        { "sgml", "text/sgml" },
        { "sh", "application/x-sh" },
        { "shar", "application/x-shar" },
        { "src", "application/x-wais-source" },
        { "sv4cpio", "application/x-sv4cpio" },
        { "sv4crc", "application/x-sv4crc" },
        { "swf", "application/x-shockwave-flash" },
        { "tar", "application/x-tar" },
        { "tcl", "application/x-tcl" },
        { "tex", "application/x-tex" },
        { "texinfo", "application/x-texinfo" },
        { "tiff", "image/tiff" },
        { "tsv", "text/tab-separated-values" },
        { "ustar", "application/x-ustar" },
        { "wav", "audio/x-wav" },
        { "wrl", "x-world/x-vrml" },
        { "xbm", "image/x-xbitmap" },
        { "xls", "application/vnd.excel" },
        { "xpm", "image/x-xpixmap" },
        { "xwd", "image/x-xwindowdump" },
        { "z", "application/x-compress" },
        { "zip", "application/zip" },
        { "dwg", "image/x-dwg" },
        { "dwt", "image/x-dwt" },
        { "msg", "message/rfc822" },
        { "odt", "application/vnd.oasis.opendocument.text" },
        { "ott", "application/vnd.oasis.opendocument.text-template" },
        { "oth", "application/vnd.oasis.opendocument.text-web" },
        { "odm", "application/vnd.oasis.opendocument.text-master" },
        { "odg", "application/vnd.oasis.opendocument.graphics" },
        { "otg", "application/vnd.oasis.opendocument.graphics-template" },
        { "odp", "application/vnd.oasis.opendocument.presentation" },
        { "otp", "application/vnd.oasis.opendocument.presentation-template" },
        { "ods", "application/vnd.oasis.opendocument.spreadsheet" },
        { "ots", "application/vnd.oasis.opendocument.spreadsheet-template" },
        { "odc", "application/vnd.oasis.opendocument.chart" },
        { "odf", "application/vnd.oasis.opendocument.formula" },
        { "odb", "application/vnd.oasis.opendocument.database" },
        { "odi", "application/vnd.oasis.opendocument.image" },
        { "sxc", "application/vnd.sun.xml.calc" },
        { "sxd", "application/vnd.sun.xml.draw" },
        { "sxi", "application/vnd.sun.xml.impress" },
        { "sxw", "application/vnd.sun.xml.writer" },
        { "sda", "application/vnd.stardivision.draw" },
        { "sdc", "application/vnd.stardivision.calc" },
        { "sdd", "application/vnd.stardivision.impress" },
        { "sdp", "application/vnd.stardivision.impress-packed" },
        { "sds", "application/vnd.stardivision.chart" },
        { "sdw", "application/vnd.stardivision.writer" },
        { "sgl", "application/vnd.stardivision.writer-global" },
        { "smf", "application/vnd.stardivision.math" } };
        
       
        

}
