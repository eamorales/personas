package com.asp.controler;

import java.io.IOException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.asp.service.IAlfrescoService;
import com.mx.Req.ImagenAlfrescoReq;
import com.mx.Res.Respuesta;
import com.mx.beans.BeanImagenAlfresco;
import com.mx.beans.ErrorDTO;
import com.mx.constantes.Constantes;
import com.mx.url.UrlService;

@RestController
@RequestMapping(UrlService.WEBSERVICEAlfresco)
public class AlfrescoController {
	@Autowired
	private IAlfrescoService alfrescoService;
	final static Logger logger = Logger.getLogger(AlfrescoController.class);

	@RequestMapping(method = RequestMethod.POST, value = UrlService.SUBIR_IMAGENES)
	@ResponseBody
	public Respuesta guardarImagenAllfresco(@RequestBody ImagenAlfrescoReq datosImagenRequest) {
		logger.info("acceso exitoso al servidor");
		Respuesta respuesta = new Respuesta();
		ErrorDTO error = new ErrorDTO();
		BeanImagenAlfresco imagenAlfresco = null;
		try {
			imagenAlfresco = alfrescoService.salvaImagen(datosImagenRequest.getFile(),
					datosImagenRequest.getNombreImagen(), datosImagenRequest.getCarpeta());
			if (imagenAlfresco == null) {
				imagenAlfresco = new BeanImagenAlfresco();
				error.setClave("301");
				error.setMessage("error al guardar la imagen en alfresco");
			} else {
				error.setClave(Constantes.CLAVE_CORRECTA);
				error.setMessage(Constantes.EXITO);
			}
		} catch (IOException e) {
			error.setClave("301");
			error.setMessage("error al guardar la imagen en alfresco");
			logger.error("error accediendo al servicio de Alfresco", e);
		}
		respuesta.setImagenAlfresco(imagenAlfresco);
		respuesta.setError(error);
		return respuesta;
	}

	@RequestMapping(method = RequestMethod.POST, value = UrlService.CONSULTA_IMAGENES)
	@ResponseBody
	public Respuesta getImagenAlfresco(@RequestBody ImagenAlfrescoReq datosImagenRequest) {
		logger.info("acceso exitoso al servidor");
		Respuesta respuesta = new Respuesta();
		ErrorDTO error = new ErrorDTO();
		BeanImagenAlfresco imagenAlfresco = null;
		try {
			imagenAlfresco = alfrescoService.buscaImagen(datosImagenRequest.getId());

			if (imagenAlfresco == null) {
				error.setClave("301");
				error.setMessage("error al consultar la imagen en alfresco");
				logger.info("error al consultar la imagen en alfrescor");
			}else if(!imagenAlfresco.getCodigoError().equals("0")){
				error.setClave("301");
				error.setMessage("error al consultar la imagen en alfresco");
				logger.info("error al consultar la imagen en alfrescor");
			}else {
				error.setClave(Constantes.CLAVE_CORRECTA);
				error.setMessage(Constantes.EXITO);
				respuesta.setImagenAlfresco(imagenAlfresco);
			}
		} catch (Exception e) {
			error.setClave("301");
			error.setMessage("error al consultar la imagen en alfresco");
			logger.info("error al consultar la imagen en alfrescor");
			logger.error("error",e);
		}
		respuesta.setError(error);
		return respuesta;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = UrlService.GUARDA_DOCUMENTO)
	@ResponseBody
	public Respuesta guardarDocumentoAlfresco(@RequestBody ImagenAlfrescoReq datosImagenRequest) {
		logger.info("acceso exitoso al servidor");
		Respuesta respuesta = new Respuesta();
		ErrorDTO error = new ErrorDTO();
		BeanImagenAlfresco imagenAlfresco = null;
		try {
			imagenAlfresco = alfrescoService.salvaDocumento(datosImagenRequest.getFile(), datosImagenRequest.getNombreImagen(),
					datosImagenRequest.getMimetype(), datosImagenRequest.getCarpeta());
			if (imagenAlfresco == null) {
				imagenAlfresco = new BeanImagenAlfresco();
				error.setClave("301");
				error.setMessage("error al guardar el documento en alfresco");
			} else {
				error.setClave(Constantes.CLAVE_CORRECTA);
				error.setMessage(Constantes.EXITO);
			}
		} catch (IOException e) {
			error.setClave("301");
			error.setMessage("error al guardar el documento en alfresco");
			logger.error("error accediendo al servicio de Alfresco", e);
		}
		respuesta.setImagenAlfresco(imagenAlfresco);
		respuesta.setError(error);
		return respuesta;
	}
	
	@RequestMapping(method = RequestMethod.POST, value = UrlService.CREAR_CARPETA)
	@ResponseBody
	public Respuesta crearCarpetaAfresco(@RequestBody ImagenAlfrescoReq datosImagenRequest) {
		logger.info("acceso exitoso al servidor");
		Respuesta respuesta = new Respuesta();
		ErrorDTO error = new ErrorDTO();
		BeanImagenAlfresco imagenAlfresco = null;
		try {
			imagenAlfresco = alfrescoService.salvaImagen(datosImagenRequest.getFile(),
					datosImagenRequest.getNombreImagen(), datosImagenRequest.getCarpeta());
			if (imagenAlfresco == null) {
				imagenAlfresco = new BeanImagenAlfresco();
				error.setClave("301");
				error.setMessage("error al guardar la imagen en alfresco");
			} else {
				error.setClave(Constantes.CLAVE_CORRECTA);
				error.setMessage(Constantes.EXITO);
			}
		} catch (IOException e) {
			error.setClave("301");
			error.setMessage("error al guardar la imagen en alfresco");
			logger.error("error accediendo al servicio de Alfresco", e);
		}
		respuesta.setImagenAlfresco(imagenAlfresco);
		respuesta.setError(error);
		return respuesta;
	}
}