package com.asp.service;

import java.io.IOException;

import com.mx.beans.BeanImagenAlfresco;

public interface IAlfrescoService {
  public BeanImagenAlfresco salvaImagen( byte [] file,String nombreFinal, String carpeta) throws IOException;

public BeanImagenAlfresco buscaImagen(String idImagen) throws Exception;

public void crearCarpeta(String nombreCarpeta);

public BeanImagenAlfresco salvaDocumento(byte []file, String nombre,String mimeType, String carpeta) throws IOException;

}
