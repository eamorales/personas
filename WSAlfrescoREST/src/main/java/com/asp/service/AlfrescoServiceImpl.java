package com.asp.service;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.apache.chemistry.opencmis.client.api.Folder;
import org.springframework.stereotype.Component;

import com.mx.beans.BeanImagenAlfresco;

import alfresco.CheckoutAlfresco;

@Component
public class AlfrescoServiceImpl implements IAlfrescoService {

	@Override
	public BeanImagenAlfresco salvaImagen(byte[] imagenInputStream, String nombreFinal, String carpeta)  throws IOException{
		 CheckoutAlfresco cce = new CheckoutAlfresco();
		 BeanImagenAlfresco imagenAlfresco= null;
		 InputStream is = new ByteArrayInputStream(imagenInputStream);
		 cce.setUser("admin");
		 cce.setPassword("asp2002$");
	     cce.setServiceUrl("http://172.148.49.3:8081/alfresco/api/-default-/public/cmis/versions/1.1/atom");	     
	     Folder folder = (Folder) cce.getSession().getObjectByPath("/Alfresco/Personas/"+carpeta);
	     imagenAlfresco = cce.createDocumentFromFileWithCustomType(cce.getSession(), folder, nombreFinal,is );
	    
		return imagenAlfresco; 
	}
	
	

	@Override
	public BeanImagenAlfresco buscaImagen(String idImagen) throws Exception {
		 CheckoutAlfresco cce = new CheckoutAlfresco();
		 BeanImagenAlfresco imagenAlfresco= null;
		 cce.setUser("admin");
		 cce.setPassword("asp2002$");
	     cce.setServiceUrl("http://172.148.49.3:8081/alfresco/api/-default-/public/cmis/versions/1.1/atom");
		 imagenAlfresco = cce.buscaImagen(cce.getSession(),idImagen);
		return imagenAlfresco;
	}



	@Override
	public void crearCarpeta(String nombreCarpeta) {
		 CheckoutAlfresco cce = new CheckoutAlfresco();
		 cce.setUser("admin");
		 cce.setPassword("asp2002$");
	     cce.setServiceUrl("http://172.148.49.3:8081/alfresco/api/-default-/public/cmis/versions/1.1/atom");	     
	     Folder folder = (Folder) cce.getSession().getObjectByPath("/Alfresco/Personas");
	     
	   	 cce.crearFolder( folder,nombreCarpeta );
		
	}



	@Override
	public BeanImagenAlfresco salvaDocumento(byte[] imagenInputStream, String nombre, String mimeType, String carpeta) {
		 CheckoutAlfresco cce = new CheckoutAlfresco();
		 BeanImagenAlfresco imagenAlfresco= null;
		 InputStream is = new ByteArrayInputStream(imagenInputStream);
		 cce.setUser("admin");
		 cce.setPassword("asp2002$");
	     cce.setServiceUrl("http://172.148.49.3:8081/alfresco/api/-default-/public/cmis/versions/1.1/atom");	     
	     Folder folder = (Folder) cce.getSession().getObjectByPath("/Alfresco/Personas/"+carpeta);
	     imagenAlfresco = cce.createDocument(cce.getSession(), folder, nombre,mimeType,carpeta ,is);
	     return imagenAlfresco; 
	}

}
