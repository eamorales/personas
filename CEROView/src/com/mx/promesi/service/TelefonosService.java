package com.mx.promesi.service;

import java.util.List;

import com.mx.beans.BeanPersona;
import com.mx.beans.BeanTelefonos;
import com.mx.beans.BeanTipoTelefono;
/**
 * @author JonaMata
 * @date 28/05/2017
 * @category Telefonos Service
 */
public interface TelefonosService {
	
	/**
	 * -----METODOS PARA DATOS TELEFONOS
	 **/
	
	/*Consulta catalogo de datos telefonos*/
	public List<BeanTipoTelefono> consultaCatalogo();
	
	/*Consulta Datos telefono*/
	public BeanTelefonos ConsultarDatosTelefono(BeanPersona persona, String idTel);
	
	
	/**
	 * -----METODOS PARA DIRECTORIO TELEFONOS
	 **/
	/*Consulta catalogo de datos telefonos*/
	public BeanTelefonos consultaTabla_telefonos();
	

	
}
