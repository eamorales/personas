package com.mx.promesi.service;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mx.beans.BeanTemplate;

@Component
public class TemplateServiceImp implements TemplateService{
	  private static final Logger LOG = Logger.getLogger(TemplateServiceImp.class);

	@Override
	public BeanTemplate ConsultarInformacion() {
		LOG.info("ConsultarInformación");
//		 LOG.info("URL Web Service::"+Constantes.IP+Constantes.PUERTO+Constantes.WEBSERVICE+UrlService.consultarTemplate);				
//		 BeanTemplate template = new BeanTemplate();
//		 template.setCodigoError(123);
//		 
//		 RestCall<BeanTemplate, BeanTemplate> r2=new RestCall<BeanTemplate, BeanTemplate>();
//		  r2.setUrl(Constantes.IP+Constantes.PUERTO+Constantes.WEBSERVICE+UrlService.consultarTemplate);
//		  r2.setEntrada(template);
//		  r2.setClase(BeanTemplate.class);
//		  BeanTemplate respuesta;
//		  respuesta = r2.call();
//		  System.out.println("datos"+respuesta.getCodigoError());
	
//		return respuesta;
		BeanTemplate template = new BeanTemplate();
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss a");
		String strDate = sdf.format(cal.getTime());
	    template.setFecha(strDate);
	    template.setUsuario("yaromero1");
	    template.setGrupo("Administrador de Sistemas1");
	    template.setCabecera ("Personas");
	    template.setSubCabecera ("Alta persona");
	    template.setAccesosRapidos("DATOS= CTRL + D;  DOMICILIO= CTRL + D+O;  CORREOS= CTRL + C;  EXPEDIENTE= CTRL + E;  PROVEEDOR = CTRL+ P;  INTERVINIENTES=CTRL+I; PLD= CTRL+P+L");
	    template.setMensaje  ("Mensaje al usuario");
		return template;
	}

}
