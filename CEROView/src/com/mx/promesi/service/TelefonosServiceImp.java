package com.mx.promesi.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.mx.beans.BeanPersona;
import com.mx.beans.BeanTelefonos;
import com.mx.beans.BeanTipoTelefono;
/**
 * @author JonaMata
 * @date 28/05/2017
 * @category Telefonos Service Impl
 */
@Service
public class TelefonosServiceImp implements TelefonosService {
	  private static final Logger LOG = Logger.getLogger(TemplateServiceImp.class);
		 
	      /**
		  * -----METODOS PARA DATOS TELEFONOS
		  **/
		 
		 /*Consulta catalogo de datos telefonos*/
	  @Override
		public List<BeanTipoTelefono> consultaCatalogo() {
			
			List<BeanTipoTelefono> tel = new ArrayList<>();
			
			return tel;
		}
		 
		/*Consulta Datos telefono*/
		
		@Override
		public BeanTelefonos ConsultarDatosTelefono(BeanPersona persona, String idTel) {
			LOG.info("Consultando Datos Telefono -- Service");
			BeanTelefonos tel = new BeanTelefonos();

			return tel;
			
		}
		
		/**
		 * -----METODOS PARA DIRECTORIO TELEFONOS
		 **/
			
		 /*Consulta catalogo de datos telefonos*/
	@Override
	public BeanTelefonos consultaTabla_telefonos() {
	
		
		
		return new BeanTelefonos();
	}
	


	
}
