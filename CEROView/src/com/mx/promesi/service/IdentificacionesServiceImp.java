package com.mx.promesi.service;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.mx.beans.BeanIdentificacion;

public class IdentificacionesServiceImp implements IdentificacionesService{
	 private static final Logger LOG = Logger.getLogger(IdentificacionesServiceImp.class);
	/**
	 * -----METODOS PARA DIRECTORIO IDENTIFICACIONES
	 **/
	/*Consulta catalogo de datos identificaciones*/
	@Override
	public BeanIdentificacion consultaTabla_identificaciones() {
		LOG.info("Conexion con el WS.");
		/*CArga tabla*/
		BeanIdentificacion ide = new BeanIdentificacion();
		List<BeanIdentificacion> listaDirectorio = new ArrayList<>(); // Tabla
		 List<String> idide = new ArrayList<>();
		 List<String> tipoIdentificacion= new ArrayList<>();
		 List<String> valor= new ArrayList<>();
		 List<String> estatus= new ArrayList<>();
		 idide.add("101");
		 idide.add("102");
		tipoIdentificacion.add("Licencia");
		tipoIdentificacion.add("IFE");
		valor.add("Valor1");
		valor.add("Valor2");
		estatus.add("Activo");
		estatus.add("Activo");
		for(int i = 0 ; i < idide.size();i++){
			BeanIdentificacion x = new BeanIdentificacion();
			x.setIdide(idide.get(i));
			x.setTipoIdentificacionSelected(tipoIdentificacion.get(i));
			x.setValor(valor.get(i));
			x.setEstatus(estatus.get(i));
			if(i%2==0)
	       		x.setPrincipal(false);
	       	else
	       		x.setPrincipal(true);
			listaDirectorio.add(x);
		}
		
		ide.setListaDirectorio(listaDirectorio);
		return ide;
	}
}
