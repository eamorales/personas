package com.mx.promesi.service;

import com.mx.beans.BeanIdentificacion;

public interface IdentificacionesService {
	/**
	 * -----METODOS PARA DIRECTORIO IDENTIFICACIONES
	 **/
	/*Consulta catalogo de datos identificaciones*/
	BeanIdentificacion consultaTabla_identificaciones();

}
