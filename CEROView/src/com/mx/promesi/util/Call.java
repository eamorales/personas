package com.mx.promesi.util;
/**
 * Clase que carga todos los Catalogo al incio del servidor
 * */
import java.io.BufferedReader;

import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.primefaces.json.JSONArray;
import org.primefaces.json.JSONObject;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import com.mx.Req.DatosReq;
import com.mx.Req.IdentificacionesReq;
import com.mx.Req.TipoUbicacionReq;
import com.mx.Res.Respuesta;
import com.mx.Utilerias.Utileria;
import com.mx.beans.BeanAlertas;
//import com.mx.beans.BeanBitacora;
import com.mx.beans.BeanCatalogosDatos;
import com.mx.beans.BeanDomicilios;
import com.mx.beans.BeanInvolucradosLista;
import com.mx.beans.BeanTipoNumeracion;
import com.mx.beans.BeanTipoTelefono;
import com.mx.beans.BeanTipoUbicacion;
import com.mx.constantes.Constantes;
import com.mx.promesi.conf.Urls;
import com.mx.url.UrlService;


@Service
public class Call {
	private static final Logger LOG = Logger.getLogger(Call.class); // Implementación del LOG
	static Respuesta respuesta = new Respuesta();

	
	//second, minute, hour, day of month, month, day(s) of week
	@PostConstruct
	@Scheduled(cron = "0 1 1 * * ?") //Se recargará todos los dias a la 1:am con 1 minuto
	public void init() {
		LOG.info("Cargando Catalogo Call");
		
		/** Carga los catalogo de Telefonos */
		respuesta.setListatipoTelefonos(callURLTipoTelefonos(Urls.getWebSerciceConsultaTipoTelefonos()));
		respuesta.setListaCompTelefonos(callURLCompaniaTelefonos(Urls.getWebServiceConsultaCompaniaTelefonos()));
		
		/**Involucrados*/
		respuesta.setListaEdoCivil(callURLEstadoCivil(Urls.getWebServiceConsultaEstadoCivil()));
		respuesta.setListaOcupacion(callURLOcupacion(Urls.getWebServiceConsultaOcupaciones()));
		respuesta.setListaNacionalidad(callURLNacionalidad(Urls.getWebServiceConsultaPaises()));
		respuesta.setListaNacimiento(callURLEstados(Urls.getWebServiceConsultaEstadoNac()));
		/**Datos */
		respuesta.setListaGrado(callServiceUrlGradoEstudios(Urls.getWebServiceConsultaGrado()));
		respuesta.setListaMunicipios(callURLServiceMunicipios(Urls.getWebServiceConsultaMunicipios()));
		respuesta.setListaLocalidades(callURLServiceLocalidades(Urls.getWebServiceConsultaLocalidades()));
		/** Carga los catalogo de Domicilios */
		respuesta.setListaColonias(callURLColonias(Urls.getWebServiceConsultaColoniaDomicilios()));
		
		respuesta.setListatipoUbicaciones(consultaTipoUbicaciones(Urls.getWebServiceConsultaTipoUbicaciones()));
		
		respuesta.setListaTipoNumeraciones(consultaTipoNumeraciones(Urls.getUrlBase() + UrlService.DOMICILIO + UrlService.CONSULTA_TIPONUMERACIONES));
		
		/**Carga los catalogo de identificacion*/
		respuesta.setCatalogoTipoIdentificacion(consultarCatalogoIdentificacion(Urls.getUrlBase() + UrlService.IDENTIFICACIONES + UrlService.CONSULTAR_CATALOGO_TIPO_IDENTIFICACION));
		
		respuesta.setCatalogoTipoIdentificacionExpresiones(consultarCatalogoIdentificacionExpresiones(Urls.getUrlBase() + UrlService.IDENTIFICACIONES + UrlService.CONSULTAR_CATALOGO_TIPO_IDENTIFICACION_EXPRESIONES));
		
		respuesta.setListaTipoDocumentosPers(callServiceUrlTipoDocumentosPers(Urls.getUrlBase() + UrlService.DATOS + UrlService.CONSULTAR_CATALOGO_TIPODOCUMENTOPERS));

		respuesta.setListaDocumentosPers(callServiceUrlDocumentosPers(Urls.getUrlBase() + UrlService.DATOS + UrlService.CONSULTAR_CATALOGO_DOCUMENTOPERS));
		
		respuesta.setListaActividad(callServiceUrlActividad(Urls.getUrlBase() + UrlService.DATOS + UrlService.CONSULTAR_CATALOGO_ACTIVIDAD));

		respuesta.setListaSector(callServiceUrlSector(Urls.getUrlBase() + UrlService.DATOS + UrlService.CONSULTAR_CATALOGO_SECTOR));
		
		respuesta.setListaCorreo(callServiceUrlTipoCorreo(Urls.getUrlBase() + UrlService.DATOS + UrlService.CONSULTAR_CATALOGO_CORREO));
		
		respuesta.setListaEstablecimiento(callServiceUrlEstablecimiento(Urls.getUrlBase() + UrlService.DATOS + UrlService.CONSULTAR_CATALOGO_ESTABLECIMIENTO));
	
		respuesta.setListaFacultad(callServiceUrlFacultad(Urls.getUrlBase() + UrlService.DATOS + UrlService.CONSULTAR_CATALOGO_FACULTAD));
	
		respuesta.setListaPuesto(callServiceUrlTipoPuesto(Urls.getUrlBase() + UrlService.DATOS + UrlService.CONSULTAR_CATALOGO_PUESTO));
		
		respuesta.setListaSituacion(callServiceUrlSituacion(Urls.getUrlBase() + UrlService.DATOS + UrlService.CONSULTAR_CATALOGO_SITUACION));
		
		respuesta.setListaSociedad(callServiceUrlTipoSociedad(Urls.getUrlBase() + UrlService.DATOS + UrlService.CONSULTAR_CATALOGO_TIPO_DE_SOCIEDAD));
		
		respuesta.setListaGiro(callServiceUrlGiro(Urls.getUrlBase() + UrlService.DATOS + UrlService.CONSULTAR_CATALOGO_GIRO));

		
		
		obtenerGoogleMap("https://maps.googleapis.com/maps/api/geocode/json");
	}

	/**
	 * ---------------------------------------------------DOMICILIOS VIEW------------------------------
	 */
	public static void obtenerGoogleMap(String URL) {
//		JSONArray object = consultaWebService2(URL);
//		if (object != null) {
//			for (int i = 0; i < object.length(); i++) {
//				
//				JSONArray a = object.getJSONArray(i);
//				System.out.println(a.get(2).toString());
//				System.out.println(a.get(4).toString());
//				System.out.println(String.valueOf(a.get(1)));
//				System.out.println(String.valueOf(a.get(0)));
//			}
//		}
	}
	
	public static Respuesta callURLLocalidades() {
		return getRespuesta();
	}

	public static Map<String, String> callURLServiceLocalidades(String url) {
		JSONArray object = consultaWebService(url);
		 Map<String, String> listResponse = new LinkedHashMap<>();
		if (object != null) {
			for (int i = 0; i < object.length(); i++) {
				JSONArray a = object.getJSONArray(i);
				if(a.get(2).toString().equals("ALTA")) {
					listResponse.put(a.get(0).toString(), a.get(1).toString());
				}
			}
		}else {
			return new LinkedHashMap<>();
		}
		return listResponse;
	}
	
	public static Respuesta callUrlAlertas(String suc) {
		
		respuesta.setAlertas(callServiceAlertas(suc));

		
		return getRespuesta();
	}
	
	
	
	 
	public static List<BeanAlertas>  callServiceAlertas(  String sucursal){
		JSONArray arrayObjects = consultaWebService(Urls.getWebServiceAlertas());
		List<BeanAlertas> listObjects = new ArrayList<>();
		if(arrayObjects != null) {
			for (int i = 0; i < arrayObjects.length(); i++) {
				 BeanAlertas bean = new BeanAlertas();
				JSONArray	array =  arrayObjects.getJSONArray(i);
		    boolean  valido = Utileria.validarRangoFechas(Utileria.convertirMilisegundoToDate(array.getLong(3)),Utileria.convertirMilisegundoToDate(array.getLong(4)));
			//FIXME  por el momento la bandera valido esta en tru para entrar a la condicion 
		        valido = true;
		    if(valido && array.getString(5).equals(sucursal)) {
					bean.setId(array.getInt(0));
					bean.setAlerta(array.get(1).toString());
					bean.setEstatus(array.get(2).toString());
					bean.setFechades(array.getLong(3));
					bean.setFechaHasta(array.getLong(4));
					bean.setSucursal(array.get(5).toString());
					listObjects.add(bean);
			}
		    
		  
			}
			
		} else {
			return new ArrayList<>();
		}
		return listObjects;
		
	}
	
	
	public static Respuesta callURLMunicipios() {
		return getRespuesta();
	}

	public static Map<String, String> callURLServiceMunicipios(String url) {
		JSONArray object = consultaWebService(url);
		 Map<String, String> listResponse = new LinkedHashMap<>();
		if (object != null) {
			for (int i = 0; i < object.length(); i++) {
				JSONArray a = object.getJSONArray(i);
				if(a.get(5).toString().equals("ALTA")) {
					listResponse.put(a.get(0).toString(), a.get(1).toString());
				}
			}
		}
		return listResponse;
	}
	
	
	public static Respuesta callURLColonias() {
		return getRespuesta();
	}

	public static List<BeanDomicilios> callURLColonias(String url) {
		JSONArray object = consultaWebService(url);
		List<BeanDomicilios> list = new ArrayList<>();
		List<String> colonias = new ArrayList<>();
		if (object != null) {
			for (int i = 0; i < object.length(); i++) {
				BeanDomicilios domicilios = new BeanDomicilios();
				JSONArray a = object.getJSONArray(i);
				domicilios.setCodigoPostal(a.get(2).toString());
				domicilios.setStatus(a.get(4).toString());
				colonias.add(String.valueOf(a.get(1)));
				domicilios.setColonias(colonias);
				domicilios.setIdColonia(String.valueOf(a.get(0)));
				list.add(domicilios);
			}
		}
		return list;
	}

	public static Respuesta consultaTipoUbicaciones() {
		return getRespuesta();
	}

	public List<BeanTipoUbicacion> consultaTipoUbicaciones(String url) {
		TipoUbicacionReq tipoUbicReq = new TipoUbicacionReq();
		RestCall2<TipoUbicacionReq, Respuesta> r2 = new RestCall2<>();
		LOG.info("URL del catalogo de Ubicación::: " + Urls.getWebServiceConsultaTipoUbicaciones());
		r2.setUrl(url);
		r2.setEntrada(tipoUbicReq);
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		if (!respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA))
			respuesta.setListatipoUbicaciones(new ArrayList<BeanTipoUbicacion>());
		return respuesta.getListatipoUbicaciones();
	}

	public static Respuesta consultaTipoNumeraciones() {
		return getRespuesta();
	}

	public List<BeanTipoNumeracion> consultaTipoNumeraciones(String url) {
		TipoUbicacionReq tipoUbicReq = new TipoUbicacionReq();
		RestCall2<TipoUbicacionReq, Respuesta> r2 = new RestCall2<>();
		LOG.info("URL del catalogo de Numeración::: " + url);
		r2.setUrl(url);
		r2.setEntrada(tipoUbicReq);
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		if (!respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) 
			respuesta.setListaTipoNumeraciones(new ArrayList<BeanTipoNumeracion>());
		return respuesta.getListaTipoNumeraciones(); 
	}

	/**
	 * --------------------------------------- TELEFONOS VIEW ---------------------------------------
	 */
	/** Llamado desde la vista */
	public static Respuesta callURLTipoTelefonos() {
		return getRespuesta();
	}

	/** Llamado al WS tipo telefono */
	public static List<BeanTipoTelefono> callURLTipoTelefonos(String url) {
		JSONArray object = consultaWebService(url);
		List<BeanTipoTelefono> list = new ArrayList<>();
		if (object != null)
			for (int i = 0; i < object.length(); i++) {
				BeanTipoTelefono tel = new BeanTipoTelefono();
				JSONArray a = object.getJSONArray(i);
				tel.setIdTipoTelefono(a.get(0).toString());
				tel.setTipoTelefono(a.get(1).toString());
				tel.setTipoTelefono(tel.getTipoTelefono().replaceAll("Ã‰", "É"));
				if(a.get(2).toString()==null)
					tel.setValidaExtension("0");
				else 	
					tel.setValidaExtension(a.get(2).toString());
				tel.setEstatus(a.get(3).toString());
				list.add(tel);
			}
		return list;

	
	}

	/** Llamado desde la vista */
	public static Respuesta callURLCompaniaTelefonos() {
		return getRespuesta();
	}

	/** Llamado al WS tipo telefono */
	public static List<BeanTipoTelefono> callURLCompaniaTelefonos(String url) {
		JSONArray object = consultaWebService(url);
		List<BeanTipoTelefono> list = new ArrayList<>();
		if (object != null)
			for (int i = 0; i < object.length(); i++) {
				BeanTipoTelefono tel = new BeanTipoTelefono();
				JSONArray a = object.getJSONArray(i);
				tel.setIdTipoCompania(a.get(0).toString());
				tel.setTipoCompania(a.get(1).toString());
				tel.setEstatus(a.get(2).toString());
				list.add(tel);
			}
		return list;
	}

	/**
	 * ------------------------------IDENTIFICACIONES----------------------------------------------------
	 */
	/**
	 * Llamado desde la vista
	 */
	public static Respuesta consultarCatalogoIdentificacion() {
		return getRespuesta();
	}

	/** Carga el catalogo de identificación del WS */
	public static LinkedHashMap<String,String> consultarCatalogoIdentificacion(String url) {
		RestCall2<IdentificacionesReq, Respuesta> r2 = new RestCall2<>();
		LOG.info("URL:::" + url);
		// AuthHeadersRequest header = new AuthHeadersRequest("EMORALES");
		// r2.setAuth(header);
		r2.setUrl(url);
		r2.setEntrada(new IdentificacionesReq());
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		if (respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA))
			respuesta.setCatalogoTipoIdentificacion(respuesta.getCatalogoTipoIdentificacion());
		else {
			respuesta.setCatalogoTipoIdentificacion(new LinkedHashMap<String, String>());
		}
		return respuesta.getCatalogoTipoIdentificacion();
	}
	public static Respuesta consultarCatalogoIdentificacionExpresiones() {
		return getRespuesta();
	}

	/** Carga el catalogo de identificación Expresiones Regulares del WS */
	public static LinkedHashMap<String,String> consultarCatalogoIdentificacionExpresiones(String url) {
		RestCall2<IdentificacionesReq, Respuesta> r2 = new RestCall2<>();
		LOG.info("URL:::" + url);
		// AuthHeadersRequest header = new AuthHeadersRequest("EMORALES");
		// r2.setAuth(header);
		r2.setUrl(url);
		r2.setEntrada(new IdentificacionesReq());
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		if (respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA))
			respuesta.setCatalogoTipoIdentificacionExpresiones(respuesta.getCatalogoTipoIdentificacionExpresiones());
		else {
			respuesta.setCatalogoTipoIdentificacionExpresiones(new LinkedHashMap<String, String>());
		}
		return respuesta.getCatalogoTipoIdentificacionExpresiones();
	}

	/**-------------------------INVOLUCRADOS VIEW--------------------*/
	public static Respuesta callURLEstadoCivil() {
		return getRespuesta();
	}
	public static List<BeanInvolucradosLista> callURLEstadoCivil(String URL){
		JSONArray object =consultaWebService(URL);
		
		LOG.info("ConsultaWebService"+object);
		List<BeanInvolucradosLista> List = new ArrayList<>();
		if(object != null){
			for(int i = 0 ; i < object.length() ; i++){
				BeanInvolucradosLista estadoCivil = new BeanInvolucradosLista();
				JSONArray row = object.getJSONArray(i);
				estadoCivil.setId(row.get(0).toString());
				estadoCivil.setEstadoCivil(row.get(1).toString());
				estadoCivil.setEstatus(row.get(2).toString());
				List.add(estadoCivil);
			}
		}
		return List;
	}
	
	
	public static Respuesta callDireccionCoordenadasMaps(String URL){
		LOG.info("::CONSULTA COORDENADAS DE GOOGLE MAPS::");
		LOG.info("::URL GOOGLE::"+URL);
		JSONArray object =consultaWebService(URL);
		
		LOG.info("ConsultaWebService"+object);
//		List<BeanInvolucradosLista> List = new ArrayList<>();
//		if(object != null){
//			for(int i = 0 ; i < object.length() ; i++){
//				BeanInvolucradosLista estadoCivil = new BeanInvolucradosLista();
//				JSONArray row = object.getJSONArray(i);
//				estadoCivil.setId(row.get(0).toString());
//				estadoCivil.setEstadoCivil(row.get(1).toString());
//				estadoCivil.setEstatus(row.get(2).toString());
//				List.add(estadoCivil);
//			}
//		}
//		return List;
		return respuesta;
	}
	
	public static Respuesta callURLOcupacion() {
		return getRespuesta();
	}
	public static List<BeanInvolucradosLista> callURLOcupacion(String url){
		JSONArray object =consultaWebService(url);
		
		LOG.info("ConsultaWebService"+object);
		List<BeanInvolucradosLista> List = new ArrayList<>();
		if(object != null){
			for(int i = 0 ; i < object.length() ; i++){
				BeanInvolucradosLista ocupacion = new BeanInvolucradosLista();
				JSONArray row = object.getJSONArray(i);
				
				ocupacion.setId(row.get(0).toString());
				ocupacion.setOcupacion(row.get(1).toString());
				ocupacion.setEstatus(row.get(2).toString());
				List.add(ocupacion);
			}
		}
		return List;
	}
	
	public static Respuesta callURLNacionalidad() {
		return getRespuesta();
	}
	public static List<BeanInvolucradosLista> callURLNacionalidad(String URL){
		JSONArray object =consultaWebService(URL);
		LOG.info("ConsultaWebService"+object);
		List<BeanInvolucradosLista> List = new ArrayList<>();
		if(object != null){
			for(int i = 0 ; i < object.length() ; i++){
				BeanInvolucradosLista nacionalidad = new BeanInvolucradosLista();
				JSONArray row = object.getJSONArray(i);
				nacionalidad.setId(row.get(0).toString());
				nacionalidad.setNacionalidad(row.get(1).toString());
				nacionalidad.setEstatus(row.get(2).toString());
				List.add(nacionalidad);
			}
		}
		return List;
	}
	public static Respuesta callURLEstados() {
		return getRespuesta();
	}

	public static List<BeanInvolucradosLista> callURLEstados(String URL){
		JSONArray object =consultaWebService(URL);
		LOG.info("ConsultaWebService"+object);
		List<BeanInvolucradosLista> List = new ArrayList<>();
		if(object != null){
			for(int i = 0 ; i < object.length() ; i++){
				BeanInvolucradosLista estados = new BeanInvolucradosLista();
				JSONArray row = object.getJSONArray(i);
				estados.setId(row.get(0).toString());
				estados.setEstadoNac(row.get(1).toString());
				estados.setEstatus(row.get(2).toString());
				estados.setZona(row.get(3).toString());
				estados.setPais(row.get(4).toString());
				List.add(estados);
			}
		}
		
		return List;
	}
	
	
	/***
	 * 
	 * -------------------------------- Datos Generales ------------- 
	 * 
	 */
	public static Respuesta callUrlGradoEstudios() {
		return getRespuesta();
	}
	
	public List<BeanCatalogosDatos> callServiceUrlGradoEstudios(String url) {
		JSONArray obJsonArray = consultaWebService(url);
		LOG.info("Consultando Wer Service :::::::... " + obJsonArray);
		List<BeanCatalogosDatos> listaGrados = new ArrayList<>();
		if (obJsonArray != null) {
			BeanCatalogosDatos beanCatalogo = new BeanCatalogosDatos();
			for (int j = 0; j < obJsonArray.length(); j++) {
				JSONArray element = obJsonArray.getJSONArray(j);
				beanCatalogo.setId(element.get(0).toString());
				beanCatalogo.setGrado(element.get(1).toString());
				beanCatalogo.setEstatus(element.get(2).toString());
				listaGrados.add(beanCatalogo);
			}
			return listaGrados;
		} else {
			return new ArrayList<>();
		}
	}
	
	public static Respuesta callUrlCatalogoTipoDocumentosPers() {
		return getRespuesta();
	}
	public static Map<String, String> callServiceUrlTipoDocumentosPers(String url){
		RestCall2<DatosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(url);
		callService.setEntrada(new DatosReq());
		callService.setClase(Respuesta.class);
		Respuesta response = callService.call();
		if(response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			return response.getListObjects();
		}else {
			return new LinkedHashMap<>();
		}
	}
	
	public static Respuesta callUrlCatalogoDocumentosPers() {
		return getRespuesta();
	}
	public static Map<String, String> callServiceUrlDocumentosPers(String url){
		RestCall2<DatosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(url);
		callService.setEntrada(new DatosReq());
		callService.setClase(Respuesta.class);
		Respuesta response = callService.call();
		if(response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			return response.getListObjects();
		}else {
			return new LinkedHashMap<>();
		}
	}
	
	
	
	
	public static Respuesta callUrlCatalogoActididad() {
		return getRespuesta();
	}
	public static Map<String, String> callServiceUrlActividad(String url){
		RestCall2<DatosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(url);
		callService.setEntrada(new DatosReq());
		callService.setClase(Respuesta.class);
		Respuesta response = callService.call();
		if(response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			return response.getListObjects();
		}else {
			return new LinkedHashMap<>();
		}
	}
	
	
	
	
	public static Respuesta callUrlCatalogoSector() {
		return getRespuesta();
	}
	public static Map<String, String> callServiceUrlSector(String url){
		RestCall2<DatosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(url);
		callService.setEntrada(new DatosReq());
		callService.setClase(Respuesta.class);
		Respuesta response = callService.call();
		if(response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			return response.getListObjects();
		}else {
			return new LinkedHashMap<>();
		}
	}
	
	public static Respuesta callUrlCatalogoSituacion() {
		return getRespuesta();
	}
	public static Map<String, String> callServiceUrlSituacion(String url){
		RestCall2<DatosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(url);
		callService.setEntrada(new DatosReq());
		callService.setClase(Respuesta.class);
		Respuesta response = callService.call();
		if(response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			return response.getListObjects();
		}else {
			return new LinkedHashMap<>();
		}
	}
	
	public static Respuesta callUrlCatalogoFacultad() {
		return getRespuesta();
	}
	public static Map<String, String> callServiceUrlFacultad(String url){
		RestCall2<DatosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(url);
		callService.setEntrada(new DatosReq());
		callService.setClase(Respuesta.class);
		Respuesta response = callService.call();
		if(response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			return response.getListObjects();
		}else {
			return new LinkedHashMap<>();
		}
	}
	public static Respuesta callUrlCatalogoEstablecimiento() {
		return getRespuesta();
	}
	public static Map<String, String> callServiceUrlEstablecimiento(String url){
		RestCall2<DatosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(url);
		callService.setEntrada(new DatosReq());
		callService.setClase(Respuesta.class);
		Respuesta response = callService.call();
		if(response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			return response.getListObjects();
		}else {
			return new LinkedHashMap<>();
		}
	}
	public static Respuesta callUrlCatalogoGiro() {
		return getRespuesta();
	}
	public static Map<String, String> callServiceUrlGiro(String url){
		RestCall2<DatosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(url);
		callService.setEntrada(new DatosReq());
		callService.setClase(Respuesta.class);
		Respuesta response = callService.call();
		if(response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			return response.getListObjects();
		}else {
			return new LinkedHashMap<>();
		}
	}
	public static Respuesta callUrlCatalogoTipoSociedad() {
		return getRespuesta();
	}
	public static Map<String, String> callServiceUrlTipoSociedad(String url){
		RestCall2<DatosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(url);
		callService.setEntrada(new DatosReq());
		callService.setClase(Respuesta.class);
		Respuesta response = callService.call();
		if(response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			return response.getListObjects();
		}else {
			return new LinkedHashMap<>();
		}
	}
	
	public static Respuesta callUrlCatalogoPuesto() {
		return getRespuesta();
	}
	public static Map<String, String> callServiceUrlTipoPuesto(String url){
		RestCall2<DatosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(url);
		callService.setEntrada(new DatosReq());
		callService.setClase(Respuesta.class);
		Respuesta response = callService.call();
		if(response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			return response.getListObjects();
		}else {
			return new LinkedHashMap<>();
		}
	}
	
	
	public static Respuesta callUrlCatalogoCorreo() {
		return getRespuesta();
	}
	public static Map<String, String> callServiceUrlTipoCorreo(String url){
		RestCall2<DatosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(url);
		callService.setEntrada(new DatosReq());
		callService.setClase(Respuesta.class);
		Respuesta response = callService.call();
		if(response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			return response.getListObjects();
		}else {
			return new LinkedHashMap<>();
		}
	}
	/***
	 * 
	 * -------------------------------- Termina Datos Generales ------------- 
	 * 
	 */
	
	

	/**
	 * -----------------------SERVICIO GENERICO PARA LA CONEXION CON EL WEB SERVICE----------------------
	 */
	public static JSONArray consultaWebService(String URL) {
		JSONArray object = null;
		try {
			URL url = new URL(URL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Accept", "application/json");

			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			JSONObject obj = null;
			while ((output = br.readLine()) != null) {
				LOG.info("ConsultaWebService" + output);
				obj = new JSONObject(output);
				object = obj.getJSONArray("dto");
			}
		} catch (Exception e) {
			LOG.info(e);
		}
		return object;
	}
	public static JSONArray consultaWebService2(String URL) {
		JSONArray object = null;
		try {
			URL = URL+"?address=240&key=AIzaSyAyBviIXn-gDl_4Sy7AVCFO1Gb9Jl8xAWs";
			URL url = new URL(URL);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("accept", "*/*");
            conn.setRequestProperty("connection", "Keep-Alive");
            conn.setRequestProperty("user-agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1;SV1)");

//			conn.setRequestProperty("address", "240&key=AIzaSyAyBviIXn-gDl_4Sy7AVCFO1Gb9Jl8xAWs");
			System.out.println(URL);
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}

			BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
			String output;
			JSONObject obj = null;
			while ((output = br.readLine()) != null) {
				LOG.info("ConsultaWebService" + output);
				obj = new JSONObject(output);
				object = obj.getJSONArray("geometry");
			}
			
		} catch (Exception e) {
			LOG.info(e);
		}
		return object;
	}
	
	
	
//	public static void bitacora(BeanBitacora bitacora){
//		
//		
//		
//	}

	public static Respuesta getRespuesta() {
		return respuesta;
	}

	public static void setRespuesta(Respuesta respuesta) {
		Call.respuesta = respuesta;
	}

}
