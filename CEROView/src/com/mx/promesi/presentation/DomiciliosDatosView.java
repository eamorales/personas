package com.mx.promesi.presentation;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.stereotype.Component;

import com.mx.Req.DomicilioReq;
import com.mx.Req.DomiciliosReq;
import com.mx.Req.ImagenAlfrescoReq;
import com.mx.Res.Respuesta;
import com.mx.beans.BeanDireccion;
import com.mx.beans.BeanDomicilios;
import com.mx.beans.BeanDomiciliosHistorico;
import com.mx.beans.BeanPeDireUbic;
import com.mx.beans.BeanTipoNumeracion;
import com.mx.beans.BeanTipoUbicacion;
import com.mx.constantes.Constantes;
import com.mx.constantes.HardCode;
import com.mx.promesi.conf.Urls;
import com.mx.promesi.util.Call;
import com.mx.promesi.util.RestCall2;
import com.mx.url.UrlService;
/**
 * @author M CD
 * @fecha 28/05/2017
 **/
@Component
@ManagedBean(name="domiciliosDatosView")
@SessionScoped
public class DomiciliosDatosView extends BeanDomicilios implements Serializable{

	private static final long serialVersionUID = -3028175917481276053L;
	private static final Logger LOG = Logger.getLogger(DomiciliosDatosView.class); /** Implementación delLOG*/
	private String colIdCmbo;
	private Map<String,String> ubic1= new LinkedHashMap<>(); /**Combo Box ubicación 1*/
	private String ubicIdCmbo1;
	private Map<String,String> ubic2= new LinkedHashMap<>(); /**Combo Box ubicación 2*/
	private String ubicIdCmbo2;
	private Map<String,String> ubic3= new LinkedHashMap<>(); /**Combo Box ubicación 3*/
	private String ubicIdCmbo3;
	private Map<String,String> num1= new LinkedHashMap<>(); /**Combo Box numero 1*/
	private String numIdCmbo1;
	private Map<String,String> num2= new LinkedHashMap<>();/**Combo Box numero 2*/ 
	private String numIdCmbo2;
	private Map<String,String> listColonias = new LinkedHashMap<>();/**ComboBox para colonias*/
	private Map<String,String> listColoniasFiltro = new LinkedHashMap<>();/**ComboBox para colonias*/
	private Map<String, String> listCP = new LinkedHashMap<>(); /**Arreglo de Codigo Postal*/
	private int activeIndex = 0;
	private UploadedFile file;  
    
    public UploadedFile getFile() {  
        return file;  
    }  
  
    public void setFile(UploadedFile file) {  
        this.file = file;  
    }  
	private String disableUbic01;
	



	/**
	 * Función que se inicializa al cargar la pantalla
	 * */
	public void init (){
		LOG.info("Se carga init del apartado Domicilios");
		this.setPantalla(Constantes.DOMICILIOSTAB01); /**Pantalla para acceso rapido*/
		this.setIdDom(Constantes.REGISTRODEFAULT);
		setPrincipal(false);
		this.consultaCatalogoColonias(); /**Función que carga las colonias del Web Service*/
		this.consultaTipoubicaciones(); /**Función para Consultar la lista de Tipo de Ubicaciones*/
		this.consultaTipoNumeraciones(); /**función para consulta la lista de Tipo de Numeraciones*/
		this.tablaDomicilios(); /**Función para consultar la tabla*/
		this.consultarDomicilio(); /**Funcion para consultar los domicilios*/
	}
	

	
	/**
	 * Metodo para obtener las colonias
	 * */
	public void consultaCatalogoColonias() {
		LOG.info("Consultando Catalogo de Colonias por WebService");
		Respuesta respuesta = Call.callURLColonias(); //Llama a la función del Web Service
		List<BeanDomicilios> listCol = respuesta.getListaColonias();
		if(listCol!=null){ /**Valida que no sea nulla la respuesta*/
			for (int i = 0; i < listCol.size(); i++) {
				if(listCol.get(i).getStatus().equals("ALTA")){ /**Valida el estatus del catalogo*/
					listColonias.put(listCol.get(i).getIdColonia(),listCol.get(i).getColonias().get(i)); /**Agregar la información al Combobox*/
					listCP.put(listCol.get(i).getIdColonia(),listCol.get(i).getCodigoPostal()); /**Agregamos la información del Codigo Postal*/
				}
			}
		}
	}
	/** Metodo para obtener todos los registros de tipo 
	 * ubicacion para llenar los combos de domicilios 
	 * 
	 * pos 0 val 3
	 * pos 1 val 1
	 * pos 2 val 2
	 * 
	 * */
	private void consultaTipoubicaciones() {
		FacesContext context = FacesContext.getCurrentInstance();
		Respuesta respuesta = Call.consultaTipoUbicaciones();
		if (respuesta.getListatipoUbicaciones().isEmpty()) {
		   	   context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.ERROR, HardCode.ERROR_CARGANDO_CATALOGOS));
		}else{
			List<BeanTipoUbicacion> listTipUb = respuesta.getListatipoUbicaciones();
			LOG.info("::TAMANO DE LISTA UBICACION::"+listTipUb.size());
			for(int i = 0; i < listTipUb.size(); i++)
			{
				ubic1.put(listTipUb.get(i).getId().toString(), listTipUb.get(i).getTipoUbicacion());
				ubic2.put(listTipUb.get(i).getId().toString(), listTipUb.get(i).getTipoUbicacion());
				ubic3.put(listTipUb.get(i).getId().toString(), listTipUb.get(i).getTipoUbicacion());
			}
		}
	}
	/** Metodo para obtener todos los registros de tipo 
	 * numeracion para llenar los combos de domicilios 
	 * */
	private void consultaTipoNumeraciones() {
		FacesContext context = FacesContext.getCurrentInstance();
		Respuesta respuesta  = Call.consultaTipoNumeraciones();
		if (respuesta.getListaTipoNumeraciones().isEmpty()) {
			   context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.ERROR, HardCode.ERROR_CARGANDO_CATALOGOS));
		}else {
				List<BeanTipoNumeracion> listTipNum = respuesta.getListaTipoNumeraciones();
				for(int i = 0; i < listTipNum.size(); i++)
				{
					num1.put(listTipNum.get(i).getId().toString(), listTipNum.get(i).getTipoNumeracion());
					num2.put(listTipNum.get(i).getId().toString(), listTipNum.get(i).getTipoNumeracion());
				}
		}
	}

	public void tablaDomicilios(){
		
		RestCall2<DomiciliosReq, Respuesta> r2=new RestCall2<>();
		DomiciliosReq request = new DomiciliosReq();
		r2.setUrl(Urls.getUrlBase()+UrlService.DOMICILIO+UrlService.CONSULTA_TABLADIRECCION);
		request.setIdSesion("123");
		r2.setEntrada(request);
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		this.setPantalla(Constantes.telefonos_Directoriotelefonico_pantalla);
		if(respuesta.getError()==null) {
			
		}else if(respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			List<BeanDomiciliosHistorico> listaDomicilio = respuesta.getDirecciones();
			for(BeanDomiciliosHistorico beanDomicilio : listaDomicilio){
				if(beanDomicilio.getEstatus().equals("1"))
					beanDomicilio.setEstatus("ACTIVO");
				else
					beanDomicilio.setEstatus("INACTIVO");
				beanDomicilio.setDireccion(beanDomicilio.getDireccion().toUpperCase());
				beanDomicilio.setEstatus(beanDomicilio.getEstatus());
			}
			this.setListaDomicilios(listaDomicilio);
		}else {
			setMensaje(respuesta.getError().getMessage());
		}
	}
	
	private boolean validaCamposRequeridos() {
		return (getCodigoPostal().length() != 5 
				|| getDescripcionU1().length() == 0 
				|| getDescripcionU2().length() == 0
				|| getDescripcionU3().length() == 0 
				|| getDescripcionN1().length() == 0 
				|| getColIdCmbo() == null
				|| getUbicIdCmbo1() == null 
				|| getUbicIdCmbo2() == null 
				|| getUbicIdCmbo3() == null
				|| getNumIdCmbo1() == null);
	}

	/**Metodo para guardar (valida )*/
	public void save(){
		LOG.info("Guardando..:");
		boolean update;
		FacesContext context = FacesContext.getCurrentInstance();
		if (validaCamposRequeridos()) 
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,"ERROR", "Llene los campos requeridos "));
		else {
			DomiciliosReq request = new DomiciliosReq();
			BeanDireccion  objectDireccion = new BeanDireccion();
			/**
			 * 
			 * Se asignan datos a insertar
			 * 
			 * */
			objectDireccion.setId(Long.valueOf(getIdDom()));
			objectDireccion.setIdColonia(Long.valueOf(getColIdCmbo()));
			objectDireccion.setEstatus(Long.valueOf(Constantes.ESTATUS_ACTIVO));
			objectDireccion.setPrincipal(isPrincipal());
			objectDireccion.setReferencia(getReferencia());
			objectDireccion.setLatitud("0");
			objectDireccion.setLongitud("0");
			
			/**Se genera la dirección*/
			
			String direccion = ubic1.get(getUbicIdCmbo1())+" "+getDescripcionU1()+" ENTRE "+ubic2.get(getUbicIdCmbo2())+" "+getDescripcionU2()+" Y "+ubic3.get(getUbicIdCmbo3())+" "+getDescripcionU3()+","+
					num1.get(getNumIdCmbo1()) +" "+getDescripcionN1() +" ";
			if(getDescripcionN2()!= null && getDescripcionN2().length()>0)
				direccion+=num2.get(getNumIdCmbo2()) +" "+getDescripcionN2();
			objectDireccion.setDireccion(direccion);
			
			/**Se generan los datos para la tabla de PeDireUbic*/
			List<BeanPeDireUbic> addresses = new ArrayList<>();
			BeanPeDireUbic direccionRow = new BeanPeDireUbic();
			direccionRow.setIdTipoNumeracion("0");
			direccionRow.setIdTipoUbicacion(getUbicIdCmbo1());
			direccionRow.setDetalle(getDescripcionU1());
			direccionRow.setEstatus(Constantes.ESTATUS_ACTIVO);
			direccionRow.setOrden("1");
			addresses.add(direccionRow);
			
			direccionRow = new BeanPeDireUbic();
			direccionRow.setIdTipoNumeracion("0");
			direccionRow.setIdTipoUbicacion(getUbicIdCmbo2());
			direccionRow.setDetalle(getDescripcionU2());
			direccionRow.setEstatus(Constantes.ESTATUS_ACTIVO);
			direccionRow.setOrden("2");
			addresses.add(direccionRow);
			
			direccionRow = new BeanPeDireUbic();
			direccionRow.setIdTipoNumeracion("0");
			direccionRow.setIdTipoUbicacion(getUbicIdCmbo3());
			direccionRow.setDetalle(getDescripcionU3());
			direccionRow.setEstatus(Constantes.ESTATUS_ACTIVO);
			direccionRow.setOrden("3");
			addresses.add(direccionRow);
			
			direccionRow = new BeanPeDireUbic();
			direccionRow.setIdTipoNumeracion(getNumIdCmbo1());
			direccionRow.setIdTipoUbicacion("0");
			direccionRow.setDetalle(getDescripcionN1());
			direccionRow.setEstatus(Constantes.ESTATUS_ACTIVO);
			direccionRow.setOrden("4");
			addresses.add(direccionRow);
			direccionRow = new BeanPeDireUbic();
			direccionRow.setIdTipoUbicacion("0");
			direccionRow.setEstatus(Constantes.ESTATUS_ACTIVO);
			direccionRow.setOrden("5");
			if(!getDescripcionN2().isEmpty()) {
				direccionRow.setIdTipoNumeracion(getNumIdCmbo2());
				direccionRow.setDetalle(getDescripcionN2());
			}else {
				direccionRow.setIdTipoNumeracion("0");
				direccionRow.setDetalle("");
			}
			addresses.add(direccionRow);
			/**Se guardan parametros de entrada de Web Service*/
			objectDireccion.setPeDireUbic(addresses);
			request.setDomicilio(objectDireccion);
			Respuesta respuesta=null;
			if ((getIdDom()+"").equals(Constantes.REGISTRODEFAULT)) {
				respuesta = this.insertarDomicilioInvokeService(request);
				update = false; 
			} else {
				respuesta = this.actualizarDomiclioInvokeService(request);
				update = true;
			}
				if(respuesta.getError().getMessage().equals(Constantes.EXITO)){
					if(update) /** se ejecuto una actualización*/
						context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,HardCode.SUCCESS, HardCode.SUCCESS_UPDATE_DIRECCION));
					else /** se ejecuto una inserción*/
						context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,HardCode.SUCCESS, HardCode.SUCCESS_SAVE_DIRECCION));
					this.tablaDomicilios();

				}else
					context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, HardCode.ERROR, respuesta.getError().getMessage()));
		}

	}
	
	/**
	 * 
	 * Metodo para invocar servicio de insercion 
	 * 
	 */
	private Respuesta insertarDomicilioInvokeService(DomiciliosReq request) {
		LOG.info("::::::::::: Comenzando insercion de datos");
		final RestCall2<DomiciliosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(Urls.getUrlBase()+UrlService.DOMICILIO+UrlService.INSERTAR_DIRECION);
		callService.setEntrada(request);
		callService.setClase(Respuesta.class);
		final Respuesta response = callService.call(); // Se captura respuesta de Web Service
		if (response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			LOG.info("Exito al actualizar datos en Direcciones y Ubicacion");
		} else {
			LOG.error("Error al acuatulizar el domicilio");
		}
		return response;
	
	}
	/**
	 * 
	 * Metodo para invocar servicio de actualizacion
	 * 
	 */
	private Respuesta actualizarDomiclioInvokeService(DomiciliosReq request) {
		LOG.info("::::::::::: Comenzando actulizacion de domicilios");
		final RestCall2<DomiciliosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(Urls.getUrlBase()+UrlService.DOMICILIO+UrlService.ACTUALIZAR_DIRECION);
		callService.setEntrada(request);
		callService.setClase(Respuesta.class);
		final Respuesta response  =  callService.call();
		if (response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			LOG.info("Exito al actualizar datos en Direcciones y Ubicacion");
		} else {
			LOG.error("Error al acuatulizar el domicilio");
		}
		return response;
	}
	

	/**
	 * Metodo para Agregar nueva dirección
	 * */
	public void add(){
		LOG.info("ADD");
		setDireccion("");
		setCodigoPostal("");
		setDescripcionN1("");
		setDescripcionN2("");
		setDescripcionU1("");
		setDescripcionU2("");
		setDescripcionU3("");
		setColIdCmbo(""); 
		setNumIdCmbo1("");
		setNumIdCmbo2("");
		setUbicIdCmbo3("");
		setUbicIdCmbo1(""); 
		setUbicIdCmbo2("");
		setReferencia("");
		validarColonia();
		setIdDom(Constantes.REGISTRODEFAULT);
		setPrincipal(false);
		
		
		if (!(getIdDom()+"").equals(Constantes.REGISTRODEFAULT)) {
				setDisableUbic01("true");
		} else {
			setDisableUbic01("false");
		}
		
	}
	/**
	 * Metodo para eliminar un registro
	 * */
	public void eliminar(){
		LOG.info("Eliminar Registro");
		FacesContext context = FacesContext.getCurrentInstance();
		RestCall2<DomiciliosReq, Respuesta> call =new   RestCall2<>();
		call.setUrl(Urls.getUrlBase()+UrlService.DOMICILIO+UrlService.ELIMINAR_DIRECION);
		final DomiciliosReq req = new DomiciliosReq();
		BeanDireccion domicilio = new BeanDireccion();
		domicilio.setId(Long.valueOf(getIdDom()));
		req.setDomicilio(domicilio);
		call.setEntrada(req);
		call.setClase(Respuesta.class);
		final Respuesta response = call.call();
		if(response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)){
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,HardCode.SUCCESS, HardCode.SUCESS_DELETE_DIRECCION));
			this.tablaDomicilios();
		}else{
			context.addMessage(null,new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.ERROR, response.getError().getMessage()));
		}

	}

	public void fileUpload(FileUploadEvent event) throws IOException {
		LOG.info("fileUpload");
		String nombre = event.getFile().getFileName();
		byte[] bytesArray;
		bytesArray= IOUtils.toByteArray(event.getFile().getInputstream());
		DomicilioReq request = new DomicilioReq();
		request.setFile(bytesArray);
		request.setNombreImagen(nombre);
		RestCall2<DomicilioReq, Respuesta> r2=new RestCall2<>();
		LOG.info("Datos para Subir a Alfresco::: "+Urls.getWebServiceAlfresco()+UrlService.WEBSERVICEAlfresco+UrlService.SUBIR_IMAGENES);
		r2.setUrl(Urls.getWebServiceAlfresco()+UrlService.WEBSERVICEAlfresco+""+UrlService.SUBIR_IMAGENES);
		r2.setEntrada(request);
		r2.setClase(Respuesta.class);
		r2.call();
	}
	/**
	 * 
	 * Metodo para consultar domicilio 
	 * 
	 */
	public void consultarDomicilio(){
		LOG.info("::::::::::  Consultando el domicilio principal :::::: ");
		DomiciliosReq request = new DomiciliosReq();
		BeanDireccion beanDireccion = new BeanDireccion();
		beanDireccion.setId(Long.valueOf(getIdDom()));
		request.setDomicilio(beanDireccion);
		RestCall2<DomiciliosReq, Respuesta> callService = new RestCall2<>();
		callService.setUrl(Urls.getWebServiceConsultaDomicilioById());
		callService.setEntrada(request);
		callService.setClase(Respuesta.class);
		try {
			
			Respuesta response = callService.call();
			if (response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
				LOG.info("::::::::::  Exito en la consulta de direccion :::::: ");
				BeanDireccion objectResponse;
				objectResponse= response.getDireccion();
				
				/**
				 * 
				 * Se asigan valores de la consultas a elementos de la vista, para el domicilio principal
				 * 
				 * **/				
				this.setIdDom(objectResponse.getId()+"");
				this.setColIdCmbo(""+response.getDireccion().getIdColonia());
				this.setBeanDireccion(objectResponse);
				this.setCodigoPostal(listCP.get(objectResponse.getIdColonia()+""));
				validarColonia();
				this.setPrincipal(response.getDireccion().isPrincipal());
				
				this.setUbicIdCmbo1(objectResponse.getPeDireUbic().get(0).getIdTipoUbicacion());
				this.setDescripcionU1(objectResponse.getPeDireUbic().get(0).getDetalle());
				
				this.setUbicIdCmbo2(objectResponse.getPeDireUbic().get(1).getIdTipoUbicacion());
				this.setDescripcionU2(objectResponse.getPeDireUbic().get(1).getDetalle());

				this.setUbicIdCmbo3(objectResponse.getPeDireUbic().get(2).getIdTipoUbicacion());
				this.setDescripcionU3(objectResponse.getPeDireUbic().get(2).getDetalle());
								
				this.setNumIdCmbo1(objectResponse.getPeDireUbic().get(3).getIdTipoNumeracion());
				this.setDescripcionN1(objectResponse.getPeDireUbic().get(3).getDetalle());

				this.setNumIdCmbo2(objectResponse.getPeDireUbic().get(4).getIdTipoNumeracion());
				this.setDescripcionN2(objectResponse.getPeDireUbic().get(4).getDetalle());
				
				this.setReferencia(objectResponse.getReferencia());
				
				this.setLatitud(objectResponse.getLatitud());
				this.setLongitud(objectResponse.getLongitud());
				this.setDireccion(objectResponse.getDireccion());
				
			if (!(getIdDom()+"").equals(Constantes.REGISTRODEFAULT)) {
					setDisableUbic01("true");
			} else {
				setDisableUbic01("false");
			}
				
				
				
			} else {
				LOG.error("A ocurridor un error al consultar direccion por id"+ response.getError().getClave());
			}
		} catch (Exception e) {
			LOG.error("ERROR: "+ e.getMessage());
		}
	}
	public void validarColonia(){
		listColoniasFiltro.clear();
		LOG.info("ValidarColonia");
		if(getCodigoPostal().length()==5){
			for (Map.Entry<String, String> entry : listCP.entrySet()) {
				if(entry.getValue().equals(getCodigoPostal())){
					LOG.info("Colonias:"+entry.getKey()+"," +entry.getValue());
					for (Map.Entry<String, String> entry2 : listColonias.entrySet()) {
						if(entry.getKey().equals(entry2.getKey())){
							LOG.info("Colonias:"+entry2.getKey()+"," +entry2.getValue());
							listColoniasFiltro.put(entry2.getKey(), entry2.getValue());
						}
					}
				}
			}
		}else{
			listColoniasFiltro.clear();
		}
	}
	/***
	 * 
	 * Metodo para capturar evente de seleccion en la tabla 
	 * 
	 * */	
	public void onRowSelect() {
		setActiveIndex(0);
		LOG.info("SE ha seleccionado un elemento :: " +	getBeanDomicilioSelect().getIdDom());
		this.setIdDom(getBeanDomicilioSelect().getIdDom());
		consultarDomicilio();
		
	}
	
	
	
	 public void handleFileUpload(FileUploadEvent event) {
	        FacesMessage message = new FacesMessage("Archivo guardado correctamente", event.getFile().getFileName() + " esta en alfresco.");
	        FacesContext.getCurrentInstance().addMessage(null, message);
	        setFoto(event.getFile().getFileName());
	        
	        if(getFoto() != null && event != null) {
		        LOG.info(getFoto());
//	        	subirArchivoAlFresco(event);
	        }
	}
	 
	 
	 private void subirArchivoAlFresco(FileUploadEvent event)  {
		 LOG.info("fileUpload");
			try {
				String nombre = event.getFile().getFileName();
				byte[] bytesArray = new byte[(int) event.getFile().getSize()];
				bytesArray= IOUtils.toByteArray(event.getFile().getInputstream());
				ImagenAlfrescoReq request = new ImagenAlfrescoReq();
				request.setFile(bytesArray);
				request.setNombreImagen(nombre);
				RestCall2<ImagenAlfrescoReq, Respuesta> r2=new RestCall2<>();
				LOG.info("Datos para Subir a Alfresco::: "+Urls.getWebServiceAlfresco()+UrlService.WEBSERVICEAlfresco+UrlService.SUBIR_IMAGENES);
				r2.setUrl(Urls.getWebServiceAlfresco()+UrlService.WEBSERVICEAlfresco+""+UrlService.SUBIR_IMAGENES);
				r2.setEntrada(request);
				r2.setClase(Respuesta.class);
				r2.call();
			} catch (IOException e) {
				 LOG.info("ERROR ENTRADA/SALIDA DEL ARCHIVO");
			}
	 }
	 
	
	
	
	/**
	 * Set & Get 
	 * @return
	 */
	public int getActiveIndex() {
		return activeIndex;
	}
	public void setActiveIndex(int activeIndex) {
		this.activeIndex = activeIndex;
	}
	public Map<String, String> getUbic1() {
		return ubic1;
	}

	public Map<String, String> getListColoniasFiltro() {
		return listColoniasFiltro;
	}

	public void setListColoniasFiltro(Map<String, String> listColoniasFiltro) {
		this.listColoniasFiltro = listColoniasFiltro;
	}

	public Map<String, String> getListCP() {
		return listCP;
	}

	public void setListCP(Map<String, String> listCP) {
		this.listCP = listCP;
	}

	public void setUbic1(Map<String, String> ubic1) {
		this.ubic1 = ubic1;
	}

	public Map<String, String> getUbic2() {
		return ubic2;
	}

	public void setUbic2(Map<String, String> ubic2) {
		this.ubic2 = ubic2;
	}

	public Map<String, String> getUbic3() {
		return ubic3;
	}

	public void setUbic3(Map<String, String> ubic3) {
		this.ubic3 = ubic3;
	}

	public Map<String, String> getNum1() {
		return num1;
	}

	public void setNum1(Map<String, String> num1) {
		this.num1 = num1;
	}

	public Map<String, String> getNum2() {
		return num2;
	}

	public void setNum2(Map<String, String> num2) {
		this.num2 = num2;
	}
	public String getColIdCmbo() {
		return colIdCmbo;
	}
	public void setColIdCmbo(String colIdCmbo) {
		this.colIdCmbo = colIdCmbo;
	}
	public String getUbicIdCmbo1() {
		return ubicIdCmbo1;
	}
	public void setUbicIdCmbo1(String ubicIdCmbo1) {
		this.ubicIdCmbo1 = ubicIdCmbo1;
	}
	public String getUbicIdCmbo2() {
		return ubicIdCmbo2;
	}
	public void setUbicIdCmbo2(String ubicIdCmbo2) {
		this.ubicIdCmbo2 = ubicIdCmbo2;
	}
	public String getUbicIdCmbo3() {
		return ubicIdCmbo3;
	}
	public void setUbicIdCmbo3(String ubicIdCmbo3) {
		this.ubicIdCmbo3 = ubicIdCmbo3;
	}
	public String getNumIdCmbo1() {
		return numIdCmbo1;
	}
	public void setNumIdCmbo1(String numIdCmbo1) {
		this.numIdCmbo1 = numIdCmbo1;
	}
	public String getNumIdCmbo2() {
		return numIdCmbo2;
	}
	public void setNumIdCmbo2(String numIdCmbo2) {
		this.numIdCmbo2 = numIdCmbo2;
	}
	public Map<String,String> getListColonias() {
		return listColonias;
	}
	public void setListColonias(Map<String,String> listColonias) {
		this.listColonias = listColonias;
	} 
	
	public String getDisableUbic01() {
		return disableUbic01;
	}
	
	public void setDisableUbic01(String disableUbic01) {
		this.disableUbic01 = disableUbic01;
	}
}
