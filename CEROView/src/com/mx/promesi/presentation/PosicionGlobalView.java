package com.mx.promesi.presentation;


import java.io.IOException;
import java.io.Serializable;

import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.primefaces.event.SelectEvent;
import org.springframework.stereotype.Component;
import org.springframework.web.context.annotation.SessionScope;

import com.mx.Req.PosicionGlobalReq;
import com.mx.Res.Respuesta;
import com.mx.beans.BeanPosicionGlobalPrincipalBean;
import com.mx.constantes.Constantes;
import com.mx.promesi.conf.Urls;
import com.mx.promesi.util.RestCall2;
import com.mx.url.UrlService;

@Component
@SessionScope
@ManagedBean(name="posicionGlobal")
public class PosicionGlobalView extends BeanPosicionGlobalPrincipalBean implements Serializable {

	/**
	 * @author JMata
	 * @date 11/06/2017
	 * 
	 */
	private static final long serialVersionUID = 4361540973271348169L;
	private static final Logger LOG = Logger.getLogger(BeanPosicionGlobalPrincipalBean.class); 
	
	
	public void init(){
		this.tablaAhorro();
		this.tablaCreditos();
		this.tablaInversion();
		this.tablaSeguros();
	}
	
	
	public void tablaAhorro(){
		LOG.info("::DENTRO DE METODO AHORRO::");
		RestCall2<PosicionGlobalReq, Respuesta> r2=new RestCall2<>();
		PosicionGlobalReq request = new PosicionGlobalReq(); 
		r2.setUrl(Urls.getUrlBase()+UrlService.POSICIONGLOBAL+UrlService.CONSULTA_AHORRO);
		LOG.info(Urls.getUrlBase()+UrlService.POSICIONGLOBAL+UrlService.CONSULTA_AHORRO);
		r2.setEntrada(request);
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		if(respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)){
			setListaAhorro(respuesta.getPosicionGlobalRespuesta().getListaAhorro());			
		}else{
			setMensaje(respuesta.getError().getMessage());
		}
	}
	
	public void tablaCreditos(){
		LOG.info("::DENTRO DE METODO CREDITOS::");
		
		RestCall2<PosicionGlobalReq, Respuesta> r2=new RestCall2<>();
		PosicionGlobalReq request = new PosicionGlobalReq(); 
		r2.setUrl(Urls.getUrlBase()+UrlService.POSICIONGLOBAL+UrlService.CONSULTA_CREDITO);
		LOG.info(Urls.getUrlBase()+UrlService.POSICIONGLOBAL+UrlService.CONSULTA_CREDITO);
		r2.setEntrada(request);
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		
		if(respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)){
			setListaCreditos(respuesta.getPosicionGlobalRespuesta().getListaCreditos());
			  }else{
			   setMensaje(respuesta.getError().getMessage());
			  }
	}
	public void tablaInversion(){
		LOG.info("::DENTRO DE METODO INVERSION::");
		
		RestCall2<PosicionGlobalReq, Respuesta> r2=new RestCall2<>();
		PosicionGlobalReq request = new PosicionGlobalReq(); 
		r2.setUrl(Urls.getUrlBase()+UrlService.POSICIONGLOBAL+UrlService.CONSULTA_INVERSION);
		LOG.info(Urls.getUrlBase()+UrlService.POSICIONGLOBAL+UrlService.CONSULTA_INVERSION);
		r2.setEntrada(request);
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		if(respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)){
			setListaInversion(respuesta.getPosicionGlobalRespuesta().getListaInversion());
			  }else{
			   setMensaje(respuesta.getError().getMessage());
			  }
	}
	
	public void tablaSeguros(){
		LOG.info("::DENTRO DE METODO SEGUROS::");
		RestCall2<PosicionGlobalReq, Respuesta> r2=new RestCall2<>();
		PosicionGlobalReq request = new PosicionGlobalReq(); 
		r2.setUrl(Urls.getUrlBase()+UrlService.POSICIONGLOBAL+UrlService.CONSULTA_SEGUROS);
		LOG.info(Urls.getUrlBase()+UrlService.POSICIONGLOBAL+UrlService.CONSULTA_SEGUROS);
		r2.setEntrada(request);
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		if(respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)){
			setListaSeguros(respuesta.getPosicionGlobalRespuesta().getListaSeguros());
			  }else{
			   setMensaje(respuesta.getError().getMessage());
			  }
	}
	
	public void onRowSelect() {
		LOG.info("::DENTRO DEL ON ROW SELECTED::");
//		LOG.info(getCreditos());
//		this.getCreditos();
//		this.getListaCreditos();
		ExternalContext externalContext = FacesContext.getCurrentInstance().getExternalContext();
	    try {
			externalContext.redirect("http://www.google.com");
		} catch (IOException e) {
			LOG.info(e);
		}
	}
	
}
