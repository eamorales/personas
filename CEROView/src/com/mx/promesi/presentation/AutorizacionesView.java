package com.mx.promesi.presentation;

import java.io.Serializable;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mx.Req.AutorizacionesReq;
import com.mx.Res.Respuesta;
import com.mx.beans.BeanAutorizacionesPendientes;
import com.mx.constantes.Constantes;
import com.mx.constantes.HardCode;
import com.mx.promesi.conf.Urls;
import com.mx.promesi.util.RestCall2;
import com.mx.url.UrlService;

@Component
@SessionScoped
@ManagedBean(name = "autorizaciones")
public class AutorizacionesView extends BeanAutorizacionesPendientes implements Serializable {
	/**
	 * @author G
	 * @date 08/07/2017
	 * @category Autorizaciones View
	 */
	/** ID Variable Serializa*/
	private static final long serialVersionUID = 7231146230185384856L;
	/** Variable LOG4j para control de logs*/
	private static final Logger LOG = Logger.getLogger(AutorizacionesView.class); // Implementación del LOG

	/**
	 * PostConstruct
	 * 
	 * */
	@PostConstruct
	public void iniciando() {
		//this.setMaxchar(Constantes.CERO);
	}
	/**
	 *Funcion que carga los datos de la pantalla principal de Autorizaciones 
	 * */
	public void init() {
		this.consultaListaAutorizaciones();  /*Consulta la lista de autorizaciones pendientes*/
	}

	/**
	 * Consulta Listado de Autorizaciones pendientes
	 * 
	 * */	
	public void consultaListaAutorizaciones() {
		LOG.info("::DENTRO DE METODO LISTADO AUTORIZACIONES::");
		RestCall2<AutorizacionesReq, Respuesta> llamadoListaAutori = new RestCall2<>();
		AutorizacionesReq obtenerListaAut = new AutorizacionesReq();
		BeanAutorizacionesPendientes beanAut = new BeanAutorizacionesPendientes();
		beanAut.setDato_gen("158452");

		llamadoListaAutori.setUrl(Urls.getUrlBase()+UrlService.AUTORIZACIONESPENDIENTES+UrlService.CONSULTA_AUTORIZACIONES);
		LOG.info(Urls.getUrlBase()+UrlService.AUTORIZACIONESPENDIENTES+UrlService.CONSULTA_AUTORIZACIONES);

		obtenerListaAut.setAutorizacionesPe(beanAut);
		llamadoListaAutori.setEntrada(obtenerListaAut);
		llamadoListaAutori.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = llamadoListaAutori.call();
		if(respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			setListaAutorizacionesVista(respuesta.getListaAutorizacionesDao());			
		}else{
			setMensaje(respuesta.getError().getMessage());
		}
	}


	/**
	 * Cancela la modificación
	 **/
	public void cancel(){
		LOG.info("Cancelar la modificacion a Autorizaciones");
		/*Consulta la lista de autorizaciones pendientes*/
		this.consultaListaAutorizaciones();  
	}

	/**
	 *  Guarda Campos seleccionados a ser autorizados para el cliente seleccionado  
	 **/
	public void save() {
		LOG.info("Guardar las modificaciones a Autorizaciones");
		
		FacesContext context = FacesContext.getCurrentInstance();

		if(getListaAutorizacionesVista()!= null && !getListaAutorizacionesVista().isEmpty())
		{
			for(int i = 0; i < getListaAutorizacionesVista().size();i++){
				BeanAutorizacionesPendientes bean = getListaAutorizacionesVista().get(i);
				LOG.info("GetEstatus "+i+"= "+bean.getEstatus());
				LOG.info("GetEstatusVista "+i+"= "+bean.getEstatusVista());
			}

			RestCall2<AutorizacionesReq, Respuesta> llamadoActualizaListaAutori = new RestCall2<>();
			AutorizacionesReq actualizarListaAut = new AutorizacionesReq();

			llamadoActualizaListaAutori.setUrl(Urls.getUrlBase() + UrlService.AUTORIZACIONESPENDIENTES + UrlService.ACTUALIZA_AUTORIZACIONES);
			LOG.info(Urls.getUrlBase() + UrlService.AUTORIZACIONESPENDIENTES + UrlService.ACTUALIZA_AUTORIZACIONES);

			actualizarListaAut.setListaAutorizaciones(getListaAutorizacionesVista());
			llamadoActualizaListaAutori.setEntrada(actualizarListaAut);
			llamadoActualizaListaAutori.setClase(Respuesta.class);
			Respuesta respuesta;
			respuesta = llamadoActualizaListaAutori.call();
			if(respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
				setListaAutorizacionesVista(respuesta.getListaAutorizacionesDao());
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, HardCode.SUCCESS, respuesta.getError().getMessage()));
				LOG.info("OK: Se actualizo correctamente");
			}else{
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.ERROR,respuesta.getError().getMessage()));
			}
		}
		else {
			
			LOG.info("Lista Vacia, no se actualizan registrios");
		}
		
		this.consultaListaAutorizaciones();
	}
}
