package com.mx.promesi.presentation;

import java.io.IOException;

import java.io.Serializable;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.event.TabChangeEvent;
import org.springframework.stereotype.Component;

import com.mx.Res.Respuesta;
import com.mx.beans.BeanTemplate;
import com.mx.promesi.business.TemplateBuss;
import com.mx.promesi.business.TemplateBussImp;
import com.mx.promesi.conf.Urls;
import com.mx.promesi.util.Call;
import com.mx.url.UrlService;

@Component
@ManagedBean(name = "template") // Nombre del objeto para la vista
@SessionScoped
public class TemplateView extends BeanTemplate implements Serializable {
	/**
	* 
	*/
	private static final long serialVersionUID = 1L;

	private static final Logger LOG = Logger.getLogger(TemplateView.class); // Implementación del LOG

	// @Autowired
	TemplateBuss templatebuss;

	public void init() {
		this.callServiceAlertas("La Paz");
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		session.setAttribute("id", null);
		ExternalContext externalContext = context.getExternalContext();
		Map<String, String> parameterMap = (Map<String, String>) externalContext.getRequestParameterMap();
		String id = parameterMap.get("id");
		String token = parameterMap.get("token");
		
		String sesionActiva = parameterMap.get("sesionActiva");
		if (id != null && token != null & sesionActiva != null && token.equals("true") && sesionActiva.equals("true")) {
			session.setAttribute("token2", "true");
			session.setAttribute("sesionActiva", "true");
			session.setAttribute("id", id);
			session.setAttribute("Registrada", "true");
			 try {
			 context.getExternalContext().redirect(Urls.getUrlBasePersonas()+"/index_personas.xhtml");
			 } catch (IOException e) {
			 LOG.info("ha ocurrido un error al repintar la pantalla principal");
			 }
		} else if (session.getAttribute("Registrada") == null) {
			enviarLogin();
		} else if (!session.getAttribute("Registrada").equals("true")) {
			enviarLogin();
		}
		LOG.info("token::" + session.getAttribute("token2"));
		LOG.info("sesionActiva::" + session.getAttribute("sesionActiva"));
		System.out.println("INIT template");
		if (templatebuss == null)
			templatebuss = new TemplateBussImp();
		BeanTemplate x = templatebuss.ConsultarInformacion();
		this.BeanTeamplate(x.getHora(), x.getMinutos(), x.getFecha(), x.getUsuario(), x.getGrupo(), x.getCabecera(),
				x.getSubCabecera(), x.getAccesosRapidos(), x.getMensaje());

	}

	public void enviarLogin() {
		FacesContext context = FacesContext.getCurrentInstance();
		try {
			
			String ruta = Urls.getUrlBaseLogin() + "/index.xhtml";
			LOG.info("Ruta que redirecionar al login"+ruta);
			context.getExternalContext().redirect(ruta);
		} catch (IOException e) {
			LOG.info("ha ocurrido un error al redireccionar al login");
		}
	}

	public void callServiceAlertas(String sucursal) {
		Respuesta response = Call.callUrlAlertas(sucursal);
		if(response.getAlertas() != null && !response.getAlertas().isEmpty()) {
			setMensaje(response.getAlertas().get(0).getAlerta());
			LOG.info("Se cargo alerta ::::  "+  getMensaje());
		} else {
			LOG.info(">> No hay alerta que mostrar <<");

		}
	
	}
	
	
	@PostConstruct // Para que el PostConstruct funcione debe de agregarse el bean en
					// SpringCoreConfig
	public void iniciando() {
		LOG.info("El template se ha inicializado");
	}

	public void changeListener(final TabChangeEvent event) {
		AccordionPanel tv = (AccordionPanel) event.getComponent();
		LOG.info("------Cambiando TAB------" + tv.getActiveIndex());
	}

	public void onTabChange(TabChangeEvent event) {
		FacesMessage msg = new FacesMessage("Tab Changed", "Active Tab: " + event.getTab().getTitle());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}

}
