package com.mx.promesi.presentation;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.event.SelectEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.stereotype.Component;

import com.mx.Req.ExpedienteReq;
import com.mx.Req.ImagenAlfrescoReq;
import com.mx.Res.Respuesta;
import com.mx.Utilerias.Utileria;
import com.mx.beans.BeanExpedientes;
import com.mx.constantes.Constantes;
import com.mx.constantes.HardCode;
import com.mx.promesi.conf.Urls;
import com.mx.promesi.util.RestCall2;
import com.mx.url.UrlService;

/**
 * @author M CD
 * @fecha 28/05/2017
 **/
@Component
@ManagedBean(name = "expedientesView")
@SessionScoped
public class ExpedientesView extends BeanExpedientes implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7686857727544590186L;
	private static final Logger log = Logger.getLogger(ExpedientesView.class); // Implementació del LOG
	private Map<String, String> listTipoDocumento = new LinkedHashMap<>();/* ComboBox Tipo Documentos */
	private Map<String, String> listaClaves = new LinkedHashMap<>();/* ComboBox Tipo Documentos */
	List<BeanExpedientes> lista = new ArrayList<>();
	private List<BeanExpedientes> expedientestoSave = new ArrayList<BeanExpedientes>(); 
	private String claveFile;
	public static int v1 = 0;
	public static int v2 = 0;
	public static int v3 = 0;


	private Respuesta response;
	private String today = Utileria.getFechaActual();
	private BeanExpedientes row = new BeanExpedientes();
	

	RestCall2<ExpedienteReq, Respuesta> callService;
	ExpedienteReq request;
	public void init() {
		log.info("init: ExpendienetsView");
		this.consultarTiposDeDocumentos();
		this.consultarExpedientes();
	}

	public void consultarTiposDeDocumentos() {
		response = new Respuesta();
		request = new ExpedienteReq();
		callService = new RestCall2<>();
		callService.setUrl(Urls.getUrlBase() + UrlService.EXPEDIENTES + UrlService.CONSULTAR_CATALOGO_TIPO_DOCUMENTOS);
		callService.setEntrada(request);
		callService.setClase(Respuesta.class);
		response = callService.call();
		if (response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			log.info(response.getError().getMessage());
			setListTipoDocumento(response.getCatalogoTipoDocumento());
			setListaClaves(response.getListaClaveDocumento());
		} else {
			log.info(HardCode.ERROR_EXP_CATALOGO_DOCS);
		}
	}

	private void consultarExpedientes() {
		log.info("INICIANDO CONSULTA DE EXPEDIENTES");
		callService = new RestCall2<>();
		request = new ExpedienteReq();
		callService.setUrl(Urls.getUrlBase() + UrlService.EXPEDIENTES + UrlService.CONSULTA_EXPEDIENTES);
		request.setIdPersona(1);
		callService.setEntrada(request);
		callService.setClase(Respuesta.class);
		response = callService.call();
		if( response.getListaExpedientes() != null &&  !response.getListaExpedientes().isEmpty() && response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			expedientestoSave = new ArrayList<>();
			int index = 0;
			lista.clear();
			for (BeanExpedientes beanExpedientes : response.getListaExpedientes()) {
				beanExpedientes.setIndiceseleccionado(index);
				lista.add(beanExpedientes);
				++index;
			}
			setListaExpedientes(lista);
		}else {
			setLista(new ArrayList<>());
			setListaExpedientes(new  ArrayList<>());
			log.info(response.getError().getMessage());
		}

	}



	public void save() {
		if( verificarSeleccion()  ) {
			callServiceActualizar(getListaExpedientes());
		} else {
			if(expedientestoSave.isEmpty()) {
				FacesContext message = FacesContext.getCurrentInstance();
				message.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN, HardCode.ERROR, " SE DEBE SELECCIONAR EL REGISTRO A ACTUALIZAR"));
			}
		}
		if(expedientestoSave !=null && !expedientestoSave.isEmpty()  && !verificarSeleccion() && getIdExpedienteSeleccionado() == 0 ) {
			callServiceGuardar(expedientestoSave);
		}


	}

	private boolean verificarSeleccion() {
		boolean ban = false;
		for (BeanExpedientes element : getListaExpedientes()) {
			if(element.isSeleccionado()) {
				ban = true ;
				break;
			}
		}
		return ban;
	}

	public void callServiceGuardar(List<BeanExpedientes> expedientes) {
		FacesContext contextMessage = FacesContext.getCurrentInstance();
		response = new Respuesta();
		request = new ExpedienteReq();
		callService = new RestCall2<>();
		request.setListaExpedientes(expedientes);
		callService.setUrl(Urls.getUrlBase() + UrlService.EXPEDIENTES + UrlService.INSERTAR_EXPEDIENTES);
		callService.setEntrada(request);
		callService.setClase(Respuesta.class);
		response = callService.call();
		if (response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			contextMessage.addMessage(null,
					new FacesMessage(FacesMessage.SEVERITY_INFO, HardCode.SUCCESS, response.getError().getMessage()));
		} else {
			log.info(response.getError());
			if(!response.isValido()) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.ERROR , response.getError().getMessage());
				FacesContext.getCurrentInstance().addMessage(null, message);
			}else {
				FacesMessage message = new FacesMessage(response.getError().getMessage());
				FacesContext.getCurrentInstance().addMessage(null, message);
			}

		}

	}

	public void add() {
		BeanExpedientes bean = new BeanExpedientes();
		bean.setTipoDocumento(getTipoDocumento());
		bean.setEstatus("1");
		bean.setVigencia("Vigente");
		bean.setFechaCreacion(Utileria.getFechaActual());
		bean.setFecha(getFecha());
		bean.setObservaciones(getObservaciones());
		bean.setNombreArchivo(getNombreArchivo());
		bean.setRuta(getRuta());
		expedientestoSave.add(bean);
		getListaExpedientes().add(bean);
	}

	public void remove() {
		if (verificarSeleccion()) {
			if (!getListaExpedientes().isEmpty()) {
				response = new Respuesta();
				request = new ExpedienteReq();
				callService = new RestCall2<>();
				request.setListaExpedientes(getListaExpedientes());
				callService.setUrl(Urls.getUrlBase() + UrlService.EXPEDIENTES + UrlService.BAJA_EXPEDIENTE);
				callService.setEntrada(request);
				callService.setClase(Respuesta.class);
				response = callService.call();
				if (response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, HardCode.SUCCESS,
							HardCode.SUCCESS_MESSAGE_DELETE_EXPEDIENTE));

				} else {
					FacesContext context = FacesContext.getCurrentInstance();
					context.addMessage(null,
							new FacesMessage(FacesMessage.SEVERITY_INFO, HardCode.SUCCESS, HardCode.ERROR));

				}
				this.consultarExpedientes();

			} else {

				FacesContext context = FacesContext.getCurrentInstance();
				context.addMessage(null,
						new FacesMessage(FacesMessage.SEVERITY_ERROR, HardCode.ERROR, HardCode.ERROR_NO_SELECCIONADO));

			}

		}

	}

	private void callServiceActualizar (List<BeanExpedientes> expedientes) {
		FacesContext contextMessage = FacesContext.getCurrentInstance();
		response = new Respuesta();
		request = new ExpedienteReq();
		callService = new RestCall2<>();
		request.setListaExpedientes(expedientes);
		callService.setUrl(Urls.getUrlBase() + UrlService.EXPEDIENTES + UrlService.ACTUALIZAR_EXPEDIENTE);
		callService.setEntrada(request);
		callService.setClase(Respuesta.class);
		response = callService.call();
		if (response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			contextMessage.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, HardCode.SUCCESS, HardCode.SUCCESS_UPDATE_EXPEDIENTE));
		} else {
			log.info(response.getError());
			FacesMessage message = new FacesMessage(response.getError().toString());
			FacesContext.getCurrentInstance().addMessage(null, message);
		}


	}


	UploadedFile file;


	public UploadedFile getFile() {
		return file;
	}


	public void setFile(UploadedFile file) {
		this.file = file;
	}



	public void handleFileUpload(FileUploadEvent event) {
		try {
			if(event != null && event.getFile() != null) {
				setNombreArchivo(generarNombreArchivo(event.getFile().getFileName()));
				log.info(getNombreArchivo() +" Nuevo Nombre::: "+getNombreArchivo());
				boolean valido = subirArchivoAlFresco(event);
				if( valido) {
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, HardCode.SUCCESS,"Archivo guardado correctamente"+  getNombreArchivo() + " esta en alfresco.");
					FacesContext.getCurrentInstance().addMessage(null, message);
				}else {
					FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, HardCode.ERROR,"ERROR AL GUARDAR IMAGEN EN ALFRESCO");
					FacesContext.getCurrentInstance().addMessage(null, message);
				}
			}
		}catch (NullPointerException e) {
			e.getMessage();
		}
	}


	private boolean subirArchivoAlFresco(FileUploadEvent event)  {
		boolean  valido = false;
		log.info("fileUpload");
		try {
			response = new Respuesta();
			String nombre = event.getFile().getFileName();
			byte[] bytesArray = new byte[(int) event.getFile().getSize()];
			bytesArray= IOUtils.toByteArray(event.getFile().getInputstream());
			ImagenAlfrescoReq request = new ImagenAlfrescoReq();
			request.setFile(bytesArray);
			request.setNombreImagen(generarNombreArchivo(nombre));
			RestCall2<ImagenAlfrescoReq, Respuesta> peticion=new RestCall2<>();
			log.info("Datos para Subir a Alfresco::: "+Urls.getWebServiceAlfresco()+UrlService.WEBSERVICEAlfresco+UrlService.SUBIR_IMAGENES);
			peticion.setUrl(Urls.getWebServiceAlfresco()+UrlService.WEBSERVICEAlfresco+""+UrlService.SUBIR_IMAGENES);
			peticion.setEntrada(request);
			peticion.setClase(Respuesta.class);
			response = peticion.call();
			
			if(response.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, HardCode.SUCCESS,"ARCHIVO GUARDADO CORRECTAMENTE"+  getNombreArchivo() + " ESTA EN ALFRESCO.");
				FacesContext.getCurrentInstance().addMessage(null, message);
			}else {
				FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_FATAL, HardCode.ERROR,"ERROR AL GUARDAR IMAGEN EN ALFRESCO");
				FacesContext.getCurrentInstance().addMessage(null, message);

			}

		} catch (IOException e) {
			log.info("ERROR ENTRADA/SALIDA DEL ARCHIVO");
		}
		return valido ;
	}


	private String  generarNombreArchivo(String nombreArchivo) {
		String ext = FilenameUtils.getExtension(nombreArchivo);
		String nombreFile = (this.claveFile != null ? this.claveFile: "")+""+Utileria.obtenerFechaArchivoAlFresco().concat(getVersion())+"."+ext;	
		return nombreFile;
	}
	public void onRowSelect(SelectEvent event) {

		int index = ((BeanExpedientes)event.getObject()).getIndiceseleccionado();
		this.row = getListaExpedientes().get(index);
		getListaExpedientes().get(index).setSeleccionado(true);
		setSeleccionado(true);
		log.info("Index :::"+ row.getIdExpediente() +" "+row.getTipoDocumento() + " " + listaClaves.get(row.getTipoDocumento()));
		this.claveFile =  listaClaves.get(row.getTipoDocumento());
	}


	public Map<String, String> getListTipoDocumento() {
		return listTipoDocumento;
	}



	public void setListTipoDocumento(Map<String, String> listTipoDocumento) {
		this.listTipoDocumento = listTipoDocumento;
	}

	public String getToday() {
		return today;
	}


	public Map<String, String> getListaClaves() {
		return listaClaves;
	}

	public void setListaClaves(Map<String, String> listaClaves) {
		this.listaClaves = listaClaves;
	}

	
	public BeanExpedientes getRow() {
		return row;
	}

	public void setRow(BeanExpedientes row) {
		this.row = row;
	}

	public List<BeanExpedientes> getLista() {
		return lista;
	}
	
	public void setLista(List<BeanExpedientes> lista) {
		this.lista = lista;
	}
	
	public static String getVersion() {
		v2 =  ++v1;	
		if(String.valueOf(v2).length() == 1)	
			return "00"+v2;
		if(String.valueOf(v2).length() == 2)	
			return "0"+v2;
		if(String.valueOf(v2).length() > 2)
			return String.valueOf(v2);
		return String.valueOf(v2);
	}
	public static void main(String[] args) {
		for (int i = 0; i < 1000; i++) {
			System.out.println(getVersion());
		}
	}



}
