package com.mx.promesi.presentation;

import java.io.Serializable;
import java.util.Collections;
import java.util.Date;
import java.util.Enumeration;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mx.Res.Respuesta;
import com.mx.Utilerias.Utileria;
import com.mx.beans.BeanCatalogosDatos;
import com.mx.beans.BeanDomicilios;
import com.mx.beans.BeanInvolucradosLista;
import com.mx.beans.BeanTelefonos;
import com.mx.beans.BeanTipoTelefono;
import com.mx.beans.Busqueda.BeanClienteEntrada;
import com.mx.constantes.Constantes;
import com.mx.promesi.conf.Urls;
import com.mx.promesi.util.Call;
import com.mx.promesi.util.RestCall2;
import com.mx.url.UrlService;

@Component
@SessionScoped
@ManagedBean(name="datosGral")
public class DatosGralView extends BeanCatalogosDatos implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 4325978968052043184L;

	private static final Logger log =  Logger.getLogger(DatosGralView.class);
	private Map<String,String> listaCompaniaTel = new LinkedHashMap<>();
	private Map<String,String> listaTipoTel = new LinkedHashMap<>();
	private Map<String, String> listEstadoCivil = new LinkedHashMap<>();
	private Map<String, String> listaocupaciones = new LinkedHashMap<>();
	private Map<String, String> listanacionalidad = new LinkedHashMap<>();
	private Map<String, String> listaestado = new LinkedHashMap<>();
	private Map<String, String> listaGrado = new LinkedHashMap<>();
	private Map<String, String> listaSector = new LinkedHashMap<>();
	private Map<String, String> listaEstablecimiento = new LinkedHashMap<>();
	private Map<String, String> listaGiro = new LinkedHashMap<>();
	private Map<String, String> listaPuesto = new LinkedHashMap<>();
	private Map<String, String> listaFacultad = new LinkedHashMap<>();
	private Map<String, String> listaCorreo = new LinkedHashMap<>();
	private Map<String, String> listaSituacion = new LinkedHashMap<>();
	private Map<String, String> listaSociedad = new LinkedHashMap<>();
	private Map<String, String> listaHijos = new LinkedHashMap<>();
	private Map<String, String> listaActividad  = new LinkedHashMap<>();
	private Map<String, String> listaColonias = new LinkedHashMap<>();
	private Map<String, String> listaTipoDocumentoPers = new LinkedHashMap<>();
	private Map<String, String> listaDocumentosPers = new LinkedHashMap<>();
	private Map<String, String> listaLocalidades = new LinkedHashMap<>();
	private Map<String, String> listaMunicipios = new LinkedHashMap<>();
	private boolean existeCliente = false;
	
	Date fechaNacimiento;


	public Date getFechaNacimiento() {
		return fechaNacimiento;
	}

	public void setFechaNacimiento(Date fechaNacimiento) {
		this.fechaNacimiento = fechaNacimiento;
	}

	public void init() {
		log.info("Inicializando los catalogos Generales View");
		this.consultaCatalogoColonias();
		this.consultaCatalogoTipoTelefonos();
		this.consultaCatalogoCompaniasTelefonos();
		this.catalogoEstadoCivil();
		this.consultaCatalogoNacionalidad();
		this.consultaCatalogoOcupaciones();
		this.consultaCatalogoEstadoNac();
		this.catalogoGradosEstudios();
		this.catalogoActividad();
		this.catalogoSector();
		this.catalogoEstablecimiento();
		this.catalogoGiro();
		this.catalogoPuesto();
		this.catalogoFacultad();
		this.catalogoCorreo();
		this.catalogoSituacion();
		this.catalogoTiposDeSociedad();
		this.catalogoHijos();
		this.catalogoTipoDocumentosPers();
		this.catalogoDocumentosPers();
        this.catalogoLocalidades();
        this.catalogoMunicipios();
        
        
        log.info("::Carga sesion datos generales::");
		FacesContext context = FacesContext.getCurrentInstance();
		HttpSession session = (HttpSession) context.getExternalContext().getSession(true);
		log.info("objeto id sesion "+session.getAttribute("id"));
		
		this.setBeanClienteEntrada(new BeanClienteEntrada());
		Object idCliente = session.getAttribute("id");
		log.info("objeto id sesion "+idCliente);
		 if(idCliente != null && !idCliente.equals("null") && !idCliente.equals("0")) {
			 busquedaClienteId(Long.valueOf(idCliente.toString()));
			 setExisteCliente(true);
		 }
        
        
	}
	
	public void consultaCatalogoColonias() {
		Respuesta respuesta = Call.callURLColonias(); //Llama a la función del Web Service
		List<BeanDomicilios> listCol = respuesta.getListaColonias();
		if(listCol!=null){ /**Valida que no sea nulla la respuesta*/
			for (int i = 0; i < listCol.size(); i++) {
				if(listCol.get(i).getStatus().equals("ALTA")){ /**Valida el estatus del catalogo*/
					listaColonias.put(listCol.get(i).getIdColonia(),listCol.get(i).getColonias().get(i)); /**Agregar la información al Combobox*/
//					listCP.put(listCol.get(i).getIdColonia(),listCol.get(i).getCodigoPostal()); /**Agregamos la información del Codigo Postal*/
				}
			}
		}
	}
	
	public void consultaCatalogoCompaniasTelefonos() {
		List<BeanTipoTelefono> listtel  = null;
		Respuesta respuesta = Call.callURLCompaniaTelefonos();
		listtel= respuesta.getListaCompTelefonos();
		listaCompaniaTel.clear();
	    if(listtel!=null){
			for (int i = 0; i < listtel.size(); i++) {
				if(listtel.get(i).getEstatus().equals("ALTA"))
					listaCompaniaTel.put( listtel.get(i).getIdTipoCompania(),listtel.get(i).getTipoCompania());
			}
		}  
	}
	public void consultaCatalogoTipoTelefonos() {
		BeanTelefonos bean = new BeanTelefonos();
		bean.setIdTelSeleccionado(Constantes.REGISTRODEFAULT);
	    List<BeanTipoTelefono> listtel  = null;
	    Respuesta respuesta = Call.callURLTipoTelefonos();
	    listtel = respuesta.getListatipoTelefonos();
	    listaTipoTel.clear();
	    if(listtel != null){
			for (int i = 0; i < listtel.size(); i++) {
				if(listtel.get(i).getEstatus().equals("ALTA"))
					listaTipoTel.put( listtel.get(i).getIdTipoTelefono() ,listtel.get(i).getTipoTelefono());
			}
		}
	}
	
	public void catalogoEstadoCivil () {
		Respuesta respuesta = Call.callURLEstadoCivil(); //Llama a la función del Web Service
		if(respuesta.getListaEdoCivil()!=null){ /**Valida que no sea nulla la respuesta*/
			for (int i = 0; i < respuesta.getListaEdoCivil().size(); i++) {
				if(respuesta.getListaEdoCivil().get(i).getEstatus().equals("1")){
					this.listEstadoCivil.put(respuesta.getListaEdoCivil().get(i).getId(), respuesta.getListaEdoCivil().get(i).getEstadoCivil());
					log.debug(respuesta.getListaEdoCivil().get(i).getId());
				}
			}
		}
		
		
	}
	
	public void consultaCatalogoNacionalidad() {
		Respuesta respuesta = Call.callURLNacionalidad(); //Llama a la función del Web Service
		List<BeanInvolucradosLista> nacionalidad = respuesta.getListaNacionalidad();
		if(nacionalidad!=null){ /**Valida que no sea nulla la respuesta*/
			for (int i = 0; i < nacionalidad.size(); i++) {
				if(nacionalidad.get(i).getEstatus().equals("ALTA")){
					this.listanacionalidad.put(nacionalidad.get(i).getId(), nacionalidad.get(i).getNacionalidad());
				}
			}
		}
	}
	
	public void consultaCatalogoOcupaciones() {
		Respuesta respuesta = Call.callURLOcupacion(); //Llama a la función del Web Service
		if(listaocupaciones!=null){ /**Valida que no sea nulla la respuesta*/
			for (int i = 0; i < respuesta.getListaOcupacion().size(); i++) {
				if(respuesta.getListaOcupacion().get(i).getEstatus().equals("ALTA")){
					this.listaocupaciones.put(respuesta.getListaOcupacion().get(i).getId(), respuesta.getListaOcupacion().get(i).getOcupacion());
					log.debug(respuesta.getListaOcupacion().get(i).getId());
				}
			}
		}
	}
	public void consultaCatalogoEstadoNac() {
		Respuesta respuesta = Call.callURLEstados(); //Llama a la función del Web Service
		List<BeanInvolucradosLista> EstadoNac = respuesta.getListaNacimiento();
		if(EstadoNac!=null){ /**Valida que no sea nulla la respuesta*/
			for (int i = 0; i < EstadoNac.size(); i++) {
				if(EstadoNac.get(i).getEstatus().equals("ALTA")){
					this.listaestado.put(EstadoNac.get(i).getId(), EstadoNac.get(i).getEstadoNac());
				}
			}
		}
		Utileria.sortByValue(listaestado);
	}


	public void catalogoGradosEstudios () {
		Respuesta respuesta = Call.callUrlGradoEstudios(); //Llama a la función del Web Service
		if(respuesta.getListaGrado() !=null){ /**Valida que no sea nulla la respuesta*/
			for (int i = 0; i < respuesta.getListaGrado().size(); i++) {
				if(respuesta.getListaGrado().get(i).getEstatus().equals("ALTA")){
					this.listaGrado.put(respuesta.getListaGrado().get(i).getId(), respuesta.getListaGrado().get(i).getGrado());
				}
			}
		}
		
	}
	
	public void catalogoLocalidades () {
		Respuesta response =  Call.callURLLocalidades();
		if(response.getListaLocalidades() != null && !response.getListaLocalidades().isEmpty()) {
			this.listaLocalidades.putAll(response.getListaLocalidades());
		}
		
	}
	
	public void catalogoMunicipios () {
		Respuesta response =  Call.callURLMunicipios();
		if(response.getListaMunicipios() != null && !response.getListaMunicipios().isEmpty()) {
			this.listaMunicipios.putAll(response.getListaMunicipios());
		}
		
	}
	
	
	public void catalogoActividad () {
		Respuesta response =  Call.callUrlCatalogoActididad();
		if(response.getListaActividad() != null && !response.getListaActividad().isEmpty()) {
			this.listaActividad.putAll(response.getListaActividad());
		}
		
	}
	
	public void catalogoTipoDocumentosPers () {
		Respuesta response =  Call.callUrlCatalogoTipoDocumentosPers();
		if(response.getListaTipoDocumentosPers() != null && !response.getListaTipoDocumentosPers().isEmpty()) {
			this.listaTipoDocumentoPers.putAll(response.getListaTipoDocumentosPers());
		}
		
	}
	public void catalogoDocumentosPers  () {
		Respuesta response =  Call.callUrlCatalogoDocumentosPers();
		if(response.getListaDocumentosPers() != null && !response.getListaDocumentosPers().isEmpty()) {
			this.listaDocumentosPers.putAll(response.getListaDocumentosPers());
		}
		
	}
	
	public void catalogoSector () {
		Respuesta response =  Call.callUrlCatalogoSector();
		if(response.getListaSector() != null && !response.getListaSector().isEmpty()) {
			this.listaSector.putAll(response.getListaSector());
		}
		
	}
	public void catalogoEstablecimiento () {
		Respuesta response =  Call.callUrlCatalogoEstablecimiento();
		if(response.getListaEstablecimiento() != null && !response.getListaEstablecimiento().isEmpty()) {
			
			
			for(Map.Entry<String, String> object : response.getListaEstablecimiento().entrySet()){
				this.listaEstablecimiento.put(object.getKey(), object.getValue());
			}
			
			
		}
		
	}
	public void catalogoGiro () {
		Respuesta response =  Call.callUrlCatalogoGiro();
		if(response.getListaGiro() != null && !response.getListaGiro().isEmpty()) {
			this.listaGiro.putAll(response.getListaGiro());
		}
		
	}
	public void catalogoPuesto () {
		Respuesta response =  Call.callUrlCatalogoPuesto();
		if(response.getListaPuesto() != null && !response.getListaPuesto().isEmpty()) {
			this.listaPuesto.putAll(response.getListaPuesto());
		}
		
	}
	public void catalogoFacultad () {
		Respuesta response =  Call.callUrlCatalogoFacultad();
		if(response.getListaFacultad() != null && !response.getListaFacultad().isEmpty()) {
			this.listaFacultad.putAll(response.getListaFacultad());
		}
		
	}
	public void catalogoCorreo () {
		Respuesta response =  Call.callUrlCatalogoCorreo();
		if(response.getListaCorreo() != null && !response.getListaCorreo().isEmpty()) {
			this.listaCorreo.putAll(response.getListaCorreo());
			
		}
		
	}
	public void catalogoSituacion () {
		Respuesta response =  Call.callUrlCatalogoSituacion();
		if(response.getListaSituacion() != null && !response.getListaSituacion().isEmpty()) {
			this.listaSituacion.putAll(response.getListaSituacion());
		}
		
	}
	public void catalogoTiposDeSociedad () {
		Respuesta response =  Call.callUrlCatalogoSituacion();
		if(response.getListaSociedad() != null && !response.getListaSociedad().isEmpty()) {
			this.listaSociedad.putAll(response.getListaSociedad());
		}
		
	}
	
	public void catalogoHijos() {
		for (int i = 1; i <= 20; i++) {
			this.listaHijos.put(""+i, ""+i);
		}
	}
	
	
	public Map<String, String> getListEstadoCivil() {
		return listEstadoCivil;
	}



	public Map<String, String> getListaocupaciones() {
		return listaocupaciones;
	}



	public Map<String, String> getListanacionalidad() {
		return listanacionalidad;
	}



	public Map<String, String> getListaestado() {
		return listaestado;
	}



	public Map<String, String> getListaSector() {
		return listaSector;
	}



	public Map<String, String> getListaEstablecimiento() {
		return listaEstablecimiento;
	}



	public Map<String, String> getListaGiro() {
		return listaGiro;
	}
	
	public Map<String, String> getListaGrado() {
		return listaGrado;
	}



	public Map<String, String> getListaPuesto() {
		return listaPuesto;
	}



	public Map<String, String> getListaFacultad() {
		return listaFacultad;
	}



	public Map<String, String> getListaCorreo() {
		return listaCorreo;
	}



	public Map<String, String> getListaSituacion() {
		return listaSituacion;
	}



	public Map<String, String> getListaSociedad() {
		return listaSociedad;
	}



	public void setListEstadoCivil(Map<String, String> listEstadoCivil) {
		this.listEstadoCivil = listEstadoCivil;
	}



	public void setListaocupaciones(Map<String, String> listaocupaciones) {
		this.listaocupaciones = listaocupaciones;
	}



	public void setListanacionalidad(Map<String, String> listanacionalidad) {
		this.listanacionalidad = listanacionalidad;
	}



	public void setListaGrado(Map<String, String> listaGrado) {
		this.listaGrado = listaGrado;
	}



	public void setListaSector(Map<String, String> listaSector) {
		this.listaSector = listaSector;
	}



	public void setListaEstablecimiento(Map<String, String> listaEstablecimiento) {
		this.listaEstablecimiento = listaEstablecimiento;
	}



	public void setListaGiro(Map<String, String> listaGiro) {
		this.listaGiro = listaGiro;
	}



	public void setListaPuesto(Map<String, String> listaPuesto) {
		this.listaPuesto = listaPuesto;
	}



	public void setListaFacultad(Map<String, String> listaFacultad) {
		this.listaFacultad = listaFacultad;
	}



	public void setListaCorreo(Map<String, String> listaCorreo) {
		this.listaCorreo = listaCorreo;
	}



	public void setListaSituacion(Map<String, String> listaSituacion) {
		this.listaSituacion = listaSituacion;
	}



	public void setListaSociedad(Map<String, String> listaSociedad) {
		this.listaSociedad = listaSociedad;
	}
	
	
	public Map<String, String> getListaHijos() {
		return listaHijos;
	}
	
	
	
	public void setListaHijos(Map<String, String> listaHijos) {
		this.listaHijos = listaHijos;
	}

	public Map<String, String> getListaCompaniaTel() {
		return listaCompaniaTel;
	}



	public void setListaCompaniaTel(Map<String, String> listaCompaniaTel) {
		this.listaCompaniaTel = listaCompaniaTel;
	}



	public Map<String, String> getListaTipoTel() {
		return listaTipoTel;
	}



	public void setListaTipoTel(Map<String, String> listaTipoTel) {
		this.listaTipoTel = listaTipoTel;
	}
	
	public Map<String, String> getListaActividad() {
		return listaActividad;
	}
	public void setListaActividad(Map<String, String> listaActividad) {
		this.listaActividad = listaActividad;
	}
	
	public Map<String, String> getListaColonias() {
		return listaColonias;
	}

	public void setListaColonias(Map<String, String> listaColonias) {
		this.listaColonias = listaColonias;
	}

	public Map<String, String> getListaDocumentosPers() {
		return listaDocumentosPers;
	}

	public void setListaDocumentosPers(Map<String, String> listaDocumentosPers) {
		this.listaDocumentosPers = listaDocumentosPers;
	}

	public Map<String, String> getListaTipoDocumentoPers() {
		return listaTipoDocumentoPers;
	}

	public void setListaTipoDocumentoPers(Map<String, String> listaTipoDocumentoPers) {
		this.listaTipoDocumentoPers = listaTipoDocumentoPers;
	}

	public Map<String, String> getListaLocalidades() {
		return listaLocalidades;
	}

	public void setListaLocalidades(Map<String, String> listaLocalidades) {
		this.listaLocalidades = listaLocalidades;
	}

	public Map<String, String> getListaMunicipios() {
		return listaMunicipios;
	}

	public void setListaMunicipios(Map<String, String> listaMunicipios) {
		this.listaMunicipios = listaMunicipios;
	}

	public void busquedaClienteId(Long id) {
		log.info("::DENTRO DE METODO busquedaClienteId::");
		RestCall2<BeanClienteEntrada, Respuesta> r2 = new RestCall2<>();
		String url = Urls.getIp()+Urls.getPuertoServidor()+Urls.getWebService()+ "/buscaCliente/id";
		r2.setUrl(url);
		log.info("url busqueda: "+url);
		this.getBeanClienteEntrada().setNoCliente(id);
		r2.setEntrada(this.getBeanClienteEntrada());
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		if (null != respuesta && null != respuesta.getError() && null!= respuesta.getError().getClave() && respuesta.getError().getClave().equals("0")) {
			log.info("::llamado exitoso::");
			log.info("salida respuesta "+respuesta.getBeanCatalogoDatos());
			setBeanCliente(respuesta.getBeanCatalogoDatos().getBeanCliente());
			setVip(respuesta.getBeanCatalogoDatos().getVip());
			
			//mostrarResultado = true;
		} else {
			log.info("::problemas en el llamado::");
			log.info("Mensaje: "+respuesta.getError().getMessage()+ "error"+respuesta.getError().getClave());
			setMensaje(respuesta.getError().getMessage());
		}		
	}
	
	public void insertaCliente() {
		log.info("::DENTRO DE METODO InsertaCliente::");
		RestCall2<BeanCatalogosDatos, Respuesta> r2 = new RestCall2<>();
		String url = Urls.getIp()+Urls.getPuertoServidor()+Urls.getWebService()+ UrlService.BUSQUEDA_CLIENTE+UrlService.INSERTA_CLIENTE;
		r2.setUrl(url);
		log.info("url busqueda: "+url);
		r2.setEntrada(this);
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		if (null != respuesta && null != respuesta.getError() && null!= respuesta.getError().getClave() && respuesta.getError().getClave().equals("0")) {
			log.info("::llamado exitoso::");
			log.info("salida respuesta "+respuesta.getBeanCatalogoDatos().getBeanCliente().getBeanInformacion().getNoCliente());
//			setBeanCliente(respuesta.getBeanCatalogoDatos().getBeanCliente());
//			setVip(respuesta.getBeanCatalogoDatos().getVip());
			
			//mostrarResultado = true;
			setExisteCliente(true);
			addMessage("Datos Insertados");
		} else {
			log.info("::problemas en el llamado::");
			log.info("Mensaje: "+respuesta.getError().getMessage()+ "error"+respuesta.getError().getClave());
			setMensaje(respuesta.getError().getMessage());
		}		
	}
	
	public void addMessage(String summary) {
		  FacesMessage message = new FacesMessage(FacesMessage.SEVERITY_INFO, summary,  null);
		  FacesContext.getCurrentInstance().addMessage(null, message);
		}
	  
	public boolean isExisteCliente() {
		return existeCliente;
	}

	public void setExisteCliente(boolean existeCliente) {
		this.existeCliente = existeCliente;
	}
	
	
}
