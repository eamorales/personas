package com.mx.promesi.presentation;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.springframework.stereotype.Component;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;
import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.primefaces.event.map.GeocodeEvent;
import org.primefaces.event.map.ReverseGeocodeEvent;
import org.primefaces.model.map.DefaultMapModel;
import org.primefaces.model.map.GeocodeResult;
import org.primefaces.model.map.LatLng;
import org.primefaces.model.map.MapModel;
import org.primefaces.model.map.Marker;
@Component
@ManagedBean
@SessionScoped
public class GeocodeView implements Serializable{
	    /**
	 * 
	 */
	private static final long serialVersionUID = -6038408661100522297L;
	
	private static final Logger LOG = Logger.getLogger(GeocodeView.class); // Implementación del LOG
		private MapModel geoModel;
	    private MapModel revGeoModel;
	    private String centerGeoMap = "23.7214652, -99.1595868";
	    private String centerRevGeoMap = "41.850033, -87.6500523";
	    private String codigoPostal="87000";
	    private String calle="nicolas bravo";
	    private String colonia="centro";
	     
	    @PostConstruct
	    public void init() {
	        geoModel = new DefaultMapModel();
	        revGeoModel = new DefaultMapModel();
	        RestCall(); //manda llamar funcion que consulta ws google 
	    }
	    
	    //consultamos la el ws de google con los parametros de envio
	    public void RestCall(){
	    	 try {
	    		 	//consume el ws de google y se envian los parametris en addres 87000, victoria, tamaulipas
//	    			URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?address=87000+victoria+tamaulipas&key=AIzaSyAyBviIXn-gDl_4Sy7AVCFO1Gb9Jl8xAWs");
	    		 	
	    		 	//se ingresan los valores calle CP colonia como variable
	    		 	URL url = new URL("https://maps.googleapis.com/maps/api/geocode/json?address="+codigoPostal+"+"+calle+"+"+colonia+"&key=AIzaSyAyBviIXn-gDl_4Sy7AVCFO1Gb9Jl8xAWs");
	    			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
	    			conn.setRequestMethod("GET");
	    			conn.setRequestProperty("Accept", "application/json");
	    			
	    			LOG.info("::RESPUESTA DE CONEXION ::"+conn.getResponseCode());
	    			if (conn.getResponseCode() != 200) {
	    				throw new RuntimeException("Failed : HTTP error code : "
	    						+ conn.getResponseCode());
	    			}else{
	    				//si la respuesta es 200 (EXITO) hace la extraccion de la respuesta
	    				BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
	        			String output;
	        			String trama="";
	        			System.out.println("Output from Server .... \n");
	        			while ((output = br.readLine()) != null) {
	        				System.out.println(output);
	        				//se concatena la trama de respuesta
	        				trama = trama+output;
	        			}
	        			LOG.info("::TRAMA GOOGLE::"+trama);
//	        			jsonTransform(trama);
	    			}
	    			conn.disconnect();
	    		  } catch (MalformedURLException e) {
	    			e.printStackTrace();
	    		  } catch (IOException e) {
	    			e.printStackTrace();
	    		  }
	    }
	     
	    public void onGeocode(GeocodeEvent event) {
	    	LOG.info("Cargando.. onGeoCode");
	        List<GeocodeResult> results = event.getResults();
	         
	        if (results != null && !results.isEmpty()) {
	            LatLng center = results.get(0).getLatLng();
	            centerGeoMap = center.getLat() + "," + center.getLng();
	             
	            for (int i = 0; i < results.size(); i++) {
	                GeocodeResult result = results.get(i);
	                geoModel.addOverlay(new Marker(result.getLatLng(), result.getAddress()));
	            }
	        }
	    }
	     
	    public void onReverseGeocode(ReverseGeocodeEvent event) {
	        List<String> addresses = event.getAddresses();
	        LatLng coord = event.getLatlng();
	         
	        if (addresses != null && !addresses.isEmpty()) {
	            centerRevGeoMap = coord.getLat() + "," + coord.getLng();
	            revGeoModel.addOverlay(new Marker(coord, addresses.get(0)));
	        }
	    }
	 
	    public MapModel getGeoModel() {
	        return geoModel;
	    }
	 
	    public MapModel getRevGeoModel() {
	        return revGeoModel;
	    }
	 
	    public String getCenterGeoMap() {
	        return centerGeoMap;
	    }
	 
	    public String getCenterRevGeoMap() {
	        return centerRevGeoMap;
	    }
}
