package com.mx.promesi.presentation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mx.beans.BeanConsultasCrediticias;
/**
 * @author M CD
 * @fecha 28/05/2017
 **/
@Component
@ManagedBean(name="consultaCrediticiaView")
@SessionScoped
public class ConsultaCrediticiaView  extends BeanConsultasCrediticias implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 7342040880678494962L;
	private static final Logger LOG = Logger.getLogger(ConsultaCrediticiaView.class); // Implementación	// del	// LOG

	public void init(){
		setEntidadFinanciera("Buro de Credito5");
		tablaConsulta();
		tablaConsultaAnteriores();
		LOG.info("::numero::"+getEntidadFinanciera());
		setPDF("archivo.pdf");
	}
/**	
	public void consultaTablaTelefonos() {
		RestCall2<TelefonosReq, Respuesta> r2=new RestCall2<>();
		TelefonosReq request = new TelefonosReq();
		r2.setUrl(Constantes.IP+Constantes.PUERTO+Constantes.WEBSERVICE+UrlService.TELEFONO+UrlService.CONSULTA_DIRECTORIO_TELEFONO);
		r2.setEntrada(request);
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		this.setPantalla(Constantes.telefonos_Directoriotelefonico_pantalla);
		List<BeanTelefonos> x = respuesta.getListaTelefonos();
		this.setListaDirectorio(x);
	}
	**/
	public void tablaConsulta(){
//		RestCall2<TelefonosReq, Respuesta> r2=new RestCall2<>();
//		ConsCredReq request = new ConsCredReq();
//		r2.setUrl(Constantes.IP+Constantes.PUERTO+Constantes.WEBSERVICE+UrlService.TELEFONO+UrlService.CONSULTA_DIRECTORIO_TELEFONO);
////		r2.setEntrada();
//		r2.setClase(Respuesta.class);
//		Respuesta respuesta;
//		respuesta =r2.call();
//		List<BeanConsultasCrediticias> x = respuesta.get
		
		 List<String> entidadFinanciera = new ArrayList<>();
		 List<String> antecedentes = new ArrayList<>();
		 List<BeanConsultasCrediticias> tipo = new ArrayList<>();
		 
		 antecedentes.add("Antecedentes 1");
		 antecedentes.add("Antecedentes 2");
		 antecedentes.add("Antecedentes 3");
		 entidadFinanciera.add( "Buro de Credito1");
		 entidadFinanciera.add("Buro de Credito2");
		 entidadFinanciera.add("Buro de Credito3");
		 for(int i  = 0;  i <antecedentes.size();i++){
			 BeanConsultasCrediticias x = new BeanConsultasCrediticias();
			 x.setEntidadFinanciera(entidadFinanciera.get(i));
			 tipo.add(x);
		 }
       setListaRealizaConsulta(tipo);
		
	}
	
	public void tablaConsultaAnteriores(){
		
		 List<String> folio = new ArrayList<>();
		 List<String> autoriza = new ArrayList<>();
		 List<String> fecha = new ArrayList<>();
		 List<String> entidadFinanciera = new ArrayList<>();
		 List<String> observaciones = new ArrayList<>();
				 
		 List<BeanConsultasCrediticias> tipo = new ArrayList<>();
		 
		 autoriza.add("Mario1 Solares");
		 autoriza.add("Mario2 Solares");
		 autoriza.add("Mario3 Solares");
		 folio.add( "123456789");
		 folio.add("9632587144");
		 folio.add("852147963");
		 fecha.add("29/05/2017");
		 fecha.add("30/05/2017");
		 fecha.add("31/05/2017");
		 entidadFinanciera.add( "Buro de Credito9");
	     entidadFinanciera.add("Buro de Credito7");
	     entidadFinanciera.add("Buro de Credito6");
	     observaciones.add("NA");
	     observaciones.add("NA");
	     observaciones.add("NA");
      for(int i  = 0;  i <folio.size();i++){
      	BeanConsultasCrediticias x = new BeanConsultasCrediticias();
      	x.setFolio(folio.get(i));
    	x.setEntidadFinanciera(entidadFinanciera.get(i));
      	x.setAutoriza(autoriza.get(i));
      	x.setFecha(fecha.get(i));
      	x.setObservaciones(observaciones.get(i));
      	tipo.add(x);
      }
      setListaConsultaAnterior(tipo);
		
	}

}
