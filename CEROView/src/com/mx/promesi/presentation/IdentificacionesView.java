package com.mx.promesi.presentation;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;
import com.mx.Req.IdentificacionesReq;
import com.mx.Res.Respuesta;
import com.mx.beans.BeanIdentificacion;
import com.mx.constantes.Constantes;
import com.mx.constantes.HardCode;
import com.mx.promesi.conf.Urls;
import com.mx.promesi.util.Call;
import com.mx.promesi.util.RestCall2;
import com.mx.url.UrlService;
/**
 * @Autor M C.D.
 * @Fecha 27/05/2017
 **/
@Component
@ManagedBean(name="identificaciones")
@SessionScoped
public class IdentificacionesView extends BeanIdentificacion implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5871724097034588650L;
	private static final Logger LOG = Logger.getLogger(IdentificacionesView.class); // Implementación	// del	// LOG
    private Map<String,String> listTipoIdentificacion = new LinkedHashMap<>();/*ComboBox tipo Identificaciones*/
    private Map<String,String> listTipoIdentificacionExpresiones = new LinkedHashMap<>();/*ComboBox tipo Identificaciones*/    

	/**Funcion que carga los datos de la pantalla principal de Identificaciones*/
	public void init() {
		LOG.info("Cargando Datos identificaciones");
		this.setPrincipal(false);
	
		this.consultarCatalogoIdentificacionExpresiones();
		this.setIdIdeSeleccionado(Constantes.REGISTRODEFAULT);
		this.consultaTablaIdentificacion();
		this.consultaDetalleIdentificacion(Constantes.REGISTRODEFAULT);
	} 
	/**Carga el catalogo de identificación*/
	public void consultarCatalogoIdentificacion(){
		Respuesta respuesta = Call.consultarCatalogoIdentificacion();
		FacesContext context = FacesContext.getCurrentInstance();
	   if(respuesta.getCatalogoTipoIdentificacion().isEmpty())
	   	   context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.ERROR, HardCode.ERROR_CARGANDO_CATALOGOS));
	   else{
		   listTipoIdentificacion.putAll(respuesta.getCatalogoTipoIdentificacion());
	   }
	}
	/**se utiliza para el filtrado del comboBox*/
	public void consultarCatalogoIdentificacion(Map<String,String> listado){
		Respuesta respuesta = Call.consultarCatalogoIdentificacion();
		FacesContext context = FacesContext.getCurrentInstance();
	   if(respuesta.getCatalogoTipoIdentificacion().isEmpty())
	   	   context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.ERROR, HardCode.ERROR_CARGANDO_CATALOGOS));
	   else{
		   listTipoIdentificacion.putAll(respuesta.getCatalogoTipoIdentificacion());
		   for(Map.Entry<String, String> entry : listado.entrySet()) {
			   listTipoIdentificacion.remove(entry.getKey());			   
			}	   
	   }
	}
	/**Carga el catalogo de identificación expresiones*/
	public void consultarCatalogoIdentificacionExpresiones(){
		Respuesta respuesta = Call.consultarCatalogoIdentificacionExpresiones();
		FacesContext context = FacesContext.getCurrentInstance();
	   if(respuesta.getCatalogoTipoIdentificacionExpresiones().isEmpty())
	   	   context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.ERROR, HardCode.ERROR_CARGANDO_CATALOGOS));
	   else{
		   listTipoIdentificacionExpresiones = respuesta.getCatalogoTipoIdentificacionExpresiones();
	   }
	}
	
	/**Consulta el detalle de la identificación*/
	public void consultaDetalleIdentificacion(String id){
		this.limpiarCamposDatos();
		this.consultarCatalogoIdentificacion();
		IdentificacionesReq request = new IdentificacionesReq();
		BeanIdentificacion beanidentificacion = new BeanIdentificacion();
		beanidentificacion.setIdIdeSeleccionado(id);
		request.setIdentificacion(beanidentificacion);
		RestCall2<IdentificacionesReq, Respuesta> r2=new RestCall2<>();
		
		LOG.info("URL::: "+Urls.getUrlBase()+UrlService.IDENTIFICACIONES+UrlService.CONSULTA_DATOS_IDENTIFICACIONES);
		  r2.setUrl(Urls.getUrlBase()+UrlService.IDENTIFICACIONES+UrlService.CONSULTA_DATOS_IDENTIFICACIONES);
		  r2.setEntrada(request);
		  r2.setClase(Respuesta.class);
		  Respuesta respuesta;
		  respuesta = r2.call();
		  if (respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			  BeanIdentificacion identificacion = respuesta.getIdentificaciones();
			  if(identificacion !=null){
					this.setIdIdeSeleccionado(identificacion.getIdIdeSeleccionado());
					this.setPrincipal(identificacion.isPrincipal());
					this.setTipoIdentificacionSelected(identificacion.getTipoIdentificacionSelected());
					this.setValor(identificacion.getValor());
					this.setDisabledcomboBox("true");
			}	
		  }else{
			  this.limpiarCamposDatos();
		  }
	}
	public void limpiarCamposDatos(){
		setTipoIdentificacionSelected("");
		setValor("");
		setPrincipal(false);
		if(getIdIdeSeleccionado()!= null &&!getIdIdeSeleccionado().equals(Constantes.REGISTRODEFAULT)){
			setIdIdeSeleccionado(getIdIdeSeleccionado());
			this.setDisabledcomboBox("true");
		}else{
			setIdIdeSeleccionado(Constantes.REGISTRODEFAULT);
			this.setDisabledcomboBox("false");
		}
		consultarCatalogoIdentificacion(this.getCatalogoUsado());/**se utiliza para el filtrado del comboBox*/
	}
	//Funcion Guardar (valida campos)
	public void save(){
		LOG.info("Guardando..:"+getTipoIdentificacionSelected());
		FacesContext context = FacesContext.getCurrentInstance();
		if ( getTipoIdentificacionSelected().equals(HardCode.SELECCIONE))	
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.ERROR, HardCode.ERROR_COMBO_BOX));
		else if(getValor().isEmpty())
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.ERROR,HardCode.ERROR_VALOR));
		else if(!validarExpresion(getTipoIdentificacionSelected(),getValor()))
			context.addMessage(null,  new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.ERROR,HardCode.ERROR_EXPRESION));
		else{
			boolean update = false;
				/**Mandar llamar al Service*/
				IdentificacionesReq request = new IdentificacionesReq();
				RestCall2<IdentificacionesReq, Respuesta> r2 = new RestCall2<>();
				LOG.info("ID "+getIdIdeSeleccionado());
				if(getIdIdeSeleccionado().equals(Constantes.REGISTRODEFAULT)){ /** Crear registro nuevo*/
					LOG.info("Guardar Datos de Identificaciones::: "+Urls.getUrlBase()+UrlService.IDENTIFICACIONES+UrlService.INSERTAR_IDENTIFICAICONES);
					r2.setUrl(Urls.getUrlBase()+UrlService.IDENTIFICACIONES+UrlService.INSERTAR_IDENTIFICAICONES);
					update = false;
				}else{ /** Actualiza registro nuevo*/
					
					LOG.info("Actualizar Datos de Identificaciones::: "+Urls.getUrlBase()+UrlService.IDENTIFICACIONES+UrlService.ACTUALIZAR_IDENTIFICACIONES);
					r2.setUrl(Urls.getUrlBase()+UrlService.IDENTIFICACIONES+UrlService.ACTUALIZAR_IDENTIFICACIONES);
					update = true;
				}
				BeanIdentificacion identificacionBean = new BeanIdentificacion();
				identificacionBean.setIdIdeSeleccionado(getIdIdeSeleccionado());
				identificacionBean.setTipoIdentificacionSelected(getTipoIdentificacionSelected());
				identificacionBean.setValor(getValor());
				identificacionBean.setEstatus(Constantes.ESTATUS_ACTIVO);
				identificacionBean.setPrincipal(isPrincipal());
				
				request.setIdentificacion(identificacionBean);
				r2.setEntrada(request);
				r2.setClase(Respuesta.class);
				Respuesta respuesta;
				respuesta = r2.call();
				if(respuesta!=null){
					if(respuesta.getError().getMessage().equals(Constantes.EXITO)){
						if(update) /** se ejecuto una actualización*/
							context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,HardCode.SUCCESS,HardCode.GUARDAR_IDENTIFICACION));
						else{ /** se ejecuto una inserción*/
							context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,HardCode.SUCCESS,HardCode.GUARDAR_IDENTIFICACION));
							this.consultaDetalleIdentificacion(Constantes.REGISTRODEFAULT);
						}
					this.consultaTablaIdentificacion();
					}else
						context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, HardCode.ERROR, respuesta.getError().getMessage()));
				}else{ /**Respuesta es null*/
					context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, HardCode.ERROR, HardCode.ERROR_GENERICO));
					limpiarCamposDatos();
				}
		}
	}
	
	private boolean validarExpresion(String tipoIdentificacionSelected, String valor) {
		String regexp = listTipoIdentificacionExpresiones.get(tipoIdentificacionSelected);
		if(regexp.trim().length()>0)
			return Pattern.matches(regexp, valor);
		else
			return true;
	}
	public void delete(){
		LOG.info("Eliminando registros");
		FacesContext context = FacesContext.getCurrentInstance();
		IdentificacionesReq request = new IdentificacionesReq();
		RestCall2<IdentificacionesReq, Respuesta> r2 = new RestCall2<>();
		BeanIdentificacion identificacionBean = new BeanIdentificacion();
		identificacionBean.setIdIdeSeleccionado(getIdIdeSeleccionado());
		identificacionBean.setEstatus(Constantes.ESTATUS_BAJA);
		identificacionBean.setPrincipal(isPrincipal());
		try {
			LOG.info("Elimina Datos de Identificacion::: "+Urls.getUrlBase()+UrlService.IDENTIFICACIONES+UrlService.BAJA_IDENTIFICACIONES);
			r2.setUrl(Urls.getUrlBase()+UrlService.IDENTIFICACIONES+UrlService.BAJA_IDENTIFICACIONES);
			request.setIdentificacion(identificacionBean);
			r2.setEntrada(request);
			r2.setClase(Respuesta.class);
			Respuesta respuesta;
			respuesta = r2.call();
			if(respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)){
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,HardCode.SUCCESS, HardCode.ELIMINAR_IDENTIFICACION));
				this.consultaTablaIdentificacion();
				this.consultaDetalleIdentificacion(Constantes.REGISTRODEFAULT);
			}else{
				LOG.info("::NO SE PUEDE REALIZAR LA OPERACION::");
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.ERROR,respuesta.getError().getMessage()));
			}
		} catch (Exception e) {
				LOG.info("::NO SE PUEDE REALIZAR LA OPERACION::");
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO,HardCode.ERROR, HardCode.ELIMINAR_IDENTIFICACION));
		}
	}
	
	public void add(){		
		setTipoIdentificacionSelected(Constantes.REGISTRODEFAULT);
		setIdIdeSeleccionado(Constantes.REGISTRODEFAULT);		
		setValor("");
		setPrincipal(false);
		limpiarCamposDatos();
	}
	
	/*Funciones de Tabla*/
	public void consultaTablaIdentificacion() {
	  this.consultarCatalogoIdentificacion();
	  this.setPantalla(Constantes.identificacion_Directorio_pantalla);
	  RestCall2<IdentificacionesReq, Respuesta> r2=new RestCall2<>();
	   r2.setUrl(Urls.getUrlBase()+UrlService.IDENTIFICACIONES+UrlService.CONSULTAR_TABLA_IDENTIFICACIONES);
	   r2.setEntrada(new IdentificacionesReq());
	   r2.setClase(Respuesta.class);
	   Respuesta respuesta;
	   respuesta = r2.call();
	   this.setCatalogoUsado(new LinkedHashMap<String,String>());  /**se utiliza para el filtrado del comboBox*/
	   if(respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)){
		   List<BeanIdentificacion> listIdentificaciones = respuesta.getListaIdentificacion();
		   for( BeanIdentificacion obj : listIdentificaciones){
			   LOG.info(obj.getTipoIdentificacionSelected());
			   LOG.info(listTipoIdentificacion.get(obj.getTipoIdentificacionSelected()));
			   String id = obj.getTipoIdentificacionSelected();
			   obj.setTipoIdentificacionSelected(listTipoIdentificacion.get(obj.getTipoIdentificacionSelected()));
			   if(obj.getEstatus().equals(Constantes.ESTATUS_ACTIVO))
				   obj.setEstatus("ACTIVO");
			   this.getCatalogoUsado().put(id, obj.getTipoIdentificacionSelected());
		   }
		   this.setListaDirectorio(listIdentificaciones);
	   }else{
		   setMensaje(respuesta.getError().getMessage());
	   }
	}
	
	/* Permite obtener el Id de la tabla seleccionada */
	public void onRowSelect() {
		LOG.info("onRowSelect");
		consultaDetalleIdentificacion(getListaDirectorioSelected().getIdide());
	
	}
	
	public void cancel(){
		LOG.info("Cancelar la modificación");
		setIdIdeSeleccionado(Constantes.REGISTRODEFAULT); /*Selecciona el registro principal*/
		this.consultaDetalleIdentificacion(getIdIdeSeleccionado());/*Consulta Los datos de la identificacion principal*/
	}
	
	/**
	 * GETTERS AND SETTERS
	 */
	
	public Map<String, String> getListTipoIdentificacion() {
		return listTipoIdentificacion;
	}
	public void setListTipoIdentificacion(Map<String, String> listTipoIdentificacion) {
		this.listTipoIdentificacion = listTipoIdentificacion;
	}
	public Map<String, String> getListTipoIdentificacionExpresiones() {
		return listTipoIdentificacionExpresiones;
	}
	public void setListTipoIdentificacionExpresiones(Map<String, String> listTipoIdentificacionExpresiones) {
		this.listTipoIdentificacionExpresiones = listTipoIdentificacionExpresiones;
	}

	

}
