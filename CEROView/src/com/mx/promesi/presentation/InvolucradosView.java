package com.mx.promesi.presentation;

import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import com.mx.Req.InvolucradosReq;
import com.mx.Res.Respuesta;
import com.mx.beans.BeanDomicilios;
import com.mx.beans.BeanInvolucradosLista;
import com.mx.beans.BeanInvolucradosPrincipal;
import com.mx.beans.BeanTelefonos;
import com.mx.beans.BeanTipoTelefono;
import com.mx.constantes.Constantes;
import com.mx.promesi.conf.Urls;
import com.mx.promesi.util.Call;
import com.mx.promesi.util.RestCall2;
import com.mx.url.UrlService;

@Component
@SessionScoped
@ManagedBean(name="involucrados")
public class InvolucradosView extends BeanInvolucradosPrincipal implements Serializable {

	/**
	 * @author JMata
	 * @date 11/05/2017
	 * 
	 */
	private static final long serialVersionUID = 1456872483498858949L;
	private static final Logger LOG = Logger.getLogger(InvolucradosView.class); //Implementación del LOG
	private Map<String,String> listEstadoCivil= new LinkedHashMap<>();
	private Map<String,String> ocupaciones= new LinkedHashMap<>();
	private Map<String,String> companiaTel= new LinkedHashMap<>();
	private Map<String,String> nacionalidad= new LinkedHashMap<>();
	private Map<String,String> estado= new LinkedHashMap<>();
	private Map<String,Integer> comb06= new LinkedHashMap<>();
	private Map<String,String> tipoTel= new LinkedHashMap<String,String>();
	/**-------------------------DOMICILIOS*/
	private String colIdCmbo;
	private Map<String,String> ubic1= new LinkedHashMap<>(); /**Combo Box ubicación 1*/
	private String ubicIdCmbo1;
	private Map<String,String> ubic2= new LinkedHashMap<>(); /**Combo Box ubicación 2*/
	private String ubicIdCmbo2;
	private Map<String,String> ubic3= new LinkedHashMap<>(); /**Combo Box ubicación 3*/
	private String ubicIdCmbo3;
	private Map<String,String> num1= new LinkedHashMap<>(); /**Combo Box numero 1*/
	private String numIdCmbo1;
	private Map<String,String> num2= new LinkedHashMap<>();/**Combo Box numero 2*/ 
	private String numIdCmbo2;
	private Map<String,String> listColonias = new LinkedHashMap<>();/**ComboBox para colonias*/
	private Map<String,String> listColoniasFiltro = new LinkedHashMap<>();/**ComboBox para colonias*/
	private Map<String,String> listCP = new LinkedHashMap<>(); /**Arreglo de Codigo Postal*/


	public void init(){
		tablaInvolucrados();
		consultaCatalogoEdoCivil();
		consultaCatalogoOcupaciones();
		consultaCatalogoNacionalidad();
		consultaCatalogoCompaniasTelefonos();
		consultaCatalogoEstadoNac();
		consultaCatalogoColonias();
	}
	/**
	 * Consulta Catalogo Edo Civil
	 * */
	public void consultaCatalogoEdoCivil() {
		Respuesta respuesta = Call.callURLEstadoCivil(); //Llama a la función del Web Service
		if(respuesta.getListaEdoCivil()!=null){ /**Valida que no sea nulla la respuesta*/
			for (int i = 0; i < respuesta.getListaEdoCivil().size(); i++) {
				if(respuesta.getListaEdoCivil().get(i).getEstatus().equals("ALTA")){
					this.listEstadoCivil.put(respuesta.getListaEdoCivil().get(i).getId(), respuesta.getListaEdoCivil().get(i).getEstadoCivil());
				}
			}
		}
	}
	
	public void consultaCatalogoOcupaciones() {
		Respuesta respuesta = Call.callURLOcupacion(); //Llama a la función del Web Service
		if(ocupaciones!=null){ /**Valida que no sea nulla la respuesta*/
			for (int i = 0; i < respuesta.getListaOcupacion().size(); i++) {
				if(respuesta.getListaOcupacion().get(i).getEstatus().equals("ALTA")){
					this.ocupaciones.put(respuesta.getListaOcupacion().get(i).getId(), respuesta.getListaOcupacion().get(i).getOcupacion());
				}
			}
		}
	}
	
	public void consultaCatalogoCompaniasTelefonos() {
		LOG.info("ConsultaCatalogoCompaniasTelefono");
		List<BeanTipoTelefono> listtel  = null;
		Respuesta respuesta = Call.callURLCompaniaTelefonos();
		listtel= respuesta.getListaCompTelefonos();
		companiaTel.clear();
	    if(listtel!=null){
			for (int i = 0; i < listtel.size(); i++) {
				if(listtel.get(i).getEstatus().equals("ALTA"))
					companiaTel.put( listtel.get(i).getIdTipoCompania(),listtel.get(i).getTipoCompania());
			}
		}  
	}
	
	public void consultaCatalogoNacionalidad() {
		Respuesta respuesta = Call.callURLNacionalidad(); //Llama a la función del Web Service
		List<BeanInvolucradosLista> nacionalidad = respuesta.getListaNacionalidad();
		if(nacionalidad!=null){ /**Valida que no sea nulla la respuesta*/
			for (int i = 0; i < nacionalidad.size(); i++) {
				if(nacionalidad.get(i).getEstatus().equals("ALTA")){
					this.nacionalidad.put(nacionalidad.get(i).getId(), nacionalidad.get(i).getNacionalidad());
				}
			}
		}
	}

	public void consultaCatalogoEstadoNac() {
		Respuesta respuesta = Call.callURLEstados(); //Llama a la función del Web Service
		List<BeanInvolucradosLista> EstadoNac = respuesta.getListaNacimiento();
		if(EstadoNac!=null){ /**Valida que no sea nulla la respuesta*/
			for (int i = 0; i < EstadoNac.size(); i++) {
				if(EstadoNac.get(i).getEstatus().equals("ALTA")){
					this.estado.put(EstadoNac.get(i).getId(), EstadoNac.get(i).getEstadoNac());
				}
			}
		}
	}
	public void consultaCatalogoTipoTelefonos() {
		BeanTelefonos bean = new BeanTelefonos();
		bean.setIdTelSeleccionado(Constantes.REGISTRODEFAULT);
	    List<BeanTipoTelefono> listtel  = null;
	    Respuesta respuesta = Call.callURLTipoTelefonos();
	    listtel = respuesta.getListatipoTelefonos();
	    tipoTel.clear();
	    if(listtel!=null){
			for (int i = 0; i < listtel.size(); i++) {
				if(listtel.get(i).getEstatus().equals("1"))
					tipoTel.put( listtel.get(i).getIdTipoTelefono(),listtel.get(i).getTipoTelefono());
			}
		}
	}
	
	
	public void consultaCatalogoColonias() {
		LOG.info("Consultando Catalogo de Colonias por WebService");
		Respuesta respuesta = Call.callURLColonias(); //Llama a la función del Web Service
		List<BeanDomicilios> listCol = respuesta.getListaColonias(); 
		if(listCol!=null){ /**Valida que no sea nulla la respuesta*/
			for (int i = 0; i < listCol.size(); i++) {
				if(listCol.get(i).getStatus().equals("ALTA")){ /**Valida el estatus del catalogo*/
					listColonias.put(listCol.get(i).getIdColonia(),listCol.get(i).getColonias().get(i) ); /**Agregar la información al Combobox*/
					listCP.put(listCol.get(i).getIdColonia(),listCol.get(i).getCodigoPostal()); /**Agregamos la información del Codigo Pastal*/
				}
			}
		}
	}
	
//	public void validarColonia(){
//		listColoniasFiltro.clear();
//		LOG.info("ValidarColonia");
//		if(getCodigoPostal()!=null)
//		if(getCodigoPostal().length()==5){
//			for (Map.Entry<String, String> entry : listCP.entrySet()) {
//				if(entry.getValue().equals(getCodigoPostal())){
//					LOG.info("Colonias:"+entry.getKey()+"," +entry.getValue());
//					for (Map.Entry<String, String> entry2 : listColonias.entrySet()) {
//						if(entry.getKey().equals(entry2.getKey())){
//							LOG.info("Colonias:"+entry2.getKey()+"," +entry2.getValue());
//							listColoniasFiltro.put(entry2.getKey(), entry2.getValue());
//						}
//					}
//				}
//			}
//		}else{
//			listColoniasFiltro.clear();
//		}
//	}
	/** Metodo para obtener todos los registros de tipo 
	 * ubicacion para llenar los combos de domicilios 
	 * */
	private void consultaTipoubicaciones() {
//		LOG.info("Consultado Catalogo tipo Ubicación::");
//		TipoUbicacionReq tipoUbicReq = new TipoUbicacionReq();
//		RestCall2<TipoUbicacionReq, Respuesta> r2=new RestCall2<>();
//		LOG.info("URL del catalogo de Ubicación::: "+Urls.getWebServiceConsultaTipoUbicaciones());
//		r2.setUrl(Urls.getWebServiceConsultaTipoUbicaciones());
//		r2.setEntrada(tipoUbicReq);
//		r2.setClase(Respuesta.class);
//		Respuesta respuesta;
//		respuesta = r2.call();
//		
//		if (respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
//			List<BeanTipoUbicacion> listTipUb = respuesta.getTipoUbicaciones();
//			if(listTipUb !=null){
//				for(int i = 0; i < listTipUb.size(); i++)
//				{
//					ubic1.put(listTipUb.get(i).getId().toString(), listTipUb.get(i).getTipoUbicacion());
//					ubic2.put(listTipUb.get(i).getId().toString(), listTipUb.get(i).getTipoUbicacion());
//					ubic3.put(listTipUb.get(i).getId().toString(), listTipUb.get(i).getTipoUbicacion());
//				}
//			} else {
//				LOG.info("Ha ocurrido un error al consultar los catalogo de tipo ubicación");
//			}
//		}else{
//			LOG.info(" ha ocurrido un error en el Servicio");
//		}		
	}
	/** Metodo para obtener todos los registros de tipo 
	 * numeracion para llenar los combos de domicilios 
	 * */
	private void consultaTipoNumeraciones() {
//		LOG.info("Consultado Catalogo tipo Numeración::");
//		TipoUbicacionReq tipoUbicReq = new TipoUbicacionReq();
//		RestCall2<TipoUbicacionReq, Respuesta> r2=new RestCall2<>();
//		LOG.info("URL del catalogo de Numeración::: "+Urls.getUrlBase()+UrlService.DOMICILIO+UrlService.CONSULTA_TIPONUMERACIONES);
//		r2.setUrl(Urls.getUrlBase()+UrlService.DOMICILIO+UrlService.CONSULTA_TIPONUMERACIONES);
//		r2.setEntrada(tipoUbicReq);
//		r2.setClase(Respuesta.class);
//		Respuesta respuesta;
//		respuesta = r2.call();
//		if (respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
//			List<BeanTipoNumeracion> listTipNum = respuesta.getTipoNumeraciones();
//			if(listTipNum !=null){
//				for(int i = 0; i < listTipNum.size(); i++)
//				{
//					num1.put(listTipNum.get(i).getId().toString(), listTipNum.get(i).getTipoNumeracion());
//					num2.put(listTipNum.get(i).getId().toString(), listTipNum.get(i).getTipoNumeracion());
//				}
//			} else {
//				LOG.info("Ha ocurrido un error al consultar los datos");
//			}
//		}else{
//			LOG.info(" ha ocurrido un error en el Servicio");
//		}	
	}

	public void tablaInvolucrados(){
		LOG.info("DENTRO DE INVOLUCRADOS VIEW tabla inv");
		RestCall2<InvolucradosReq, Respuesta> r2=new RestCall2<>();
		InvolucradosReq request = new InvolucradosReq(); 
		r2.setUrl(Urls.getUrlBase()+UrlService.INVOLUCRADOS+UrlService.CONSULTAINVLUCRADOSLISTA);
		LOG.info(Urls.getUrlBase()+UrlService.INVOLUCRADOS+UrlService.CONSULTAINVLUCRADOSLISTA);
		r2.setEntrada(request);
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		LOG.info("::RESPUESTA CLAVE ERROR VIEW::"+respuesta.getError().getClave());
		if(respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)){
			this.setListaInv(respuesta.getBeanInvolucradosPrincipal().getListaInv());
		}else{
		}
	}
	
	
	
	public void consultacatalogoCompaniaTel(){
		
	}
	
	public void consultacatalogoNacionalidad(){
			
		}
	
	public void consultacatalogoEstadoNAc(){
		
	}
	
	public void consultacatalogoCorreo(){
		
	}
	
	
	public Map<String, String> getListEstadoCivil() {
		return listEstadoCivil;
	}

	public void setListEstadoCivil(Map<String, String> listEstadoCivil) {
		this.listEstadoCivil = listEstadoCivil;
	}

	
	public Map<String, String> getCompaniaTel() {
		return companiaTel;
	}

	public void setCompaniaTel(Map<String, String> companiaTel) {
		this.companiaTel = companiaTel;
	}

	public Map<String, Integer> getComb06() {
		return comb06;
	}

	public void setComb06(Map<String, Integer> comb06) {
		this.comb06 = comb06;
	}
	
	public Map<String, String> getOcupaciones() {
		return ocupaciones;
	}

	public void setOcupaciones(Map<String, String> ocupaciones) {
		this.ocupaciones = ocupaciones;
	}

	public Map<String,String> getNacionalidad() {
		return nacionalidad;
	}

	public void setNacionalidad(Map<String,String> nacionalidad) {
		this.nacionalidad = nacionalidad;
	}

	public Map<String,String> getEstado() {
		return estado;
	}

	public void setEstado(Map<String,String> estado) {
		this.estado = estado; 
	}
	
}
