package com.mx.promesi.presentation;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

@Component
@ManagedBean(name = "autenticacionView")
@SessionScoped
public class AutenticacionContrasena implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2931884591917684309L;
	private static final Logger LOG = Logger.getLogger(AutenticacionContrasena.class);

	public void init() {
		LOG.info("Autenticacion init()");
	}

	public void submit() {
		LOG.info("submit");
		FacesMessage msg = new FacesMessage(FacesMessage.SEVERITY_INFO, "Correct", "Correct");
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	public void submitModal(){
		LOG.info("submitModal");
	}
	/*Atributo numero de tiempo regreseivo**/
	private int number = 60;

	public int getNumber() {
		return number;
	}

	public void decrementa() {
		number--;
		if (number == 0)
			number = 60;
	}

}
