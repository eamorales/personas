package com.mx.promesi.presentation;

import java.io.Serializable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import org.apache.log4j.Logger;
import org.primefaces.component.accordionpanel.AccordionPanel;
import org.primefaces.event.TabChangeEvent;
import org.springframework.stereotype.Component;
import com.mx.Req.TelefonosReq;
import com.mx.Res.Respuesta;
import com.mx.Utilerias.Utileria;
import com.mx.beans.BeanTelefonos;
import com.mx.beans.BeanTipoTelefono;
import com.mx.constantes.Constantes;
import com.mx.constantes.HardCode;
import com.mx.promesi.conf.Urls;
import com.mx.promesi.util.Call;
import com.mx.promesi.util.RestCall2;
import com.mx.url.UrlService;

@Component
@SessionScoped
@ManagedBean(name = "telefonos")
public class TelefonosView extends BeanTelefonos implements Serializable {
	/**
	 * @author JonaMata
	 * @date 28/05/2017
	 * @category Telefonos View
	 */
	private static final long serialVersionUID = 7231146230185384856L;
	private static final Logger LOG = Logger.getLogger(TelefonosView.class); // Implementación del LOG
    private Map<String,String> listtipotel = new LinkedHashMap<>();/* ComboBox tipo telefono*/
    private Map<String,String> validaExtension = new LinkedHashMap<>();/* ComboBox tipo telefono*/
    private Map<String,String> listcompaniatel = new LinkedHashMap<>();/*ComboBox tipo Compañias */
    private Integer extensionPermitida = 0;
    private int activeIndex = 0;

	private String ladaLength ="3"; /*max length lada*/
	private String numeroLength ="7"; /*max length numero*/
	private String observLength ="1000"; /*max length Observaciones*/
	private String extensionLength="3"; /*max length extensión*/
	private String disabledTipoTelefono;
	/**
	 * PostConstruct
	 * 
	 * */
	@PostConstruct
	public void iniciando() {
		LOG.info("PostConstruct");
		this.setMaxchar(HardCode.NUMERO_MEXICO);
	}
	/**
	 *Funcion que carga los datos de la pantalla principal de telefonos 
	 * */
	public void init() {
		limpiarCamposDatos();
		setIdTelSeleccionado(Constantes.REGISTRODEFAULT);	/*Selecciona el registro principal*/
		this.setPantalla(Constantes.telefonos_Datostelefono_pantalla);
		
		this.consultaTablaTelefonos();/*Consulta la tabla de telefonos*/
		this.consultarDatosTelefono(getIdTelSeleccionado());/*Consulta Los datos del telefono principal*/
	}
	/**
	 * -------------------------------CODIGO PARA DATOS TELEFONOS ----------------------------
	 */
	
	/**
	 *  Consulta los catalgos de telefonos 
	 *  
	 *  */
	public void consultaCatalogoTipoTelefonos() {
		LOG.info("ConsultaCatalogoTipoTelefono");
	    List<BeanTipoTelefono> listtel  = null;
	    Respuesta respuesta = Call.callURLTipoTelefonos();
	    listtel = respuesta.getListatipoTelefonos();
	    listtipotel.clear();
	    validaExtension.clear();
	    if(listtel!=null){
			for (int i = 0; i < listtel.size(); i++) {
				if(listtel.get(i).getEstatus().equals("ALTA")) {
					listtipotel.put( listtel.get(i).getIdTipoTelefono(),listtel.get(i).getTipoTelefono());
					validaExtension.put(listtel.get(i).getIdTipoTelefono(), listtel.get(i).getValidaExtension());
				}
			}
		
			listtipotel= Utileria.sortByValue(listtipotel);
			validaExtension=Utileria.sortByValue(validaExtension);
			
		}
		this.setDisabled("true");
	}
	/**se utiliza para el filtrado del comboBox*/
	public void consultaCatalogoTipoTelefonos(Map<String,String> listado) {
		LOG.info("ConsultaCatalogoTipoTelefono");
	    List<BeanTipoTelefono> listtel  = null;
	    Respuesta respuesta = Call.callURLTipoTelefonos();
	    listtel = respuesta.getListatipoTelefonos();
	    listtipotel.clear();
	    validaExtension.clear();
	    if(listtel!=null){
			for (int i = 0; i < listtel.size(); i++) {
				if(listtel.get(i).getEstatus().equals("ALTA") && !listado.containsKey(listtel.get(i).getTipoTelefono())) {
					listtipotel.put( listtel.get(i).getIdTipoTelefono(),listtel.get(i).getTipoTelefono());
					validaExtension.put(listtel.get(i).getIdTipoTelefono(), listtel.get(i).getValidaExtension());
				}
			}
		
			listtipotel= Utileria.sortByValue(listtipotel);
			validaExtension=Utileria.sortByValue(validaExtension);
			
		}
		this.setDisabled("true");
	}
		
	/**
	 * Consulta Catalogo de Companias
	 * */
	
	public void consultaCatalogoCompaniasTelefonos() {
		LOG.info("ConsultaCatalogoCompaniasTelefono");
		this.setPantalla(Constantes.telefonos_Datostelefono_pantalla);
		List<BeanTipoTelefono> listtel  = null;
		Respuesta respuesta = Call.callURLCompaniaTelefonos();
		listtel= respuesta.getListaCompTelefonos();
		listcompaniatel.clear();
	    if(listtel!=null){
			for (int i = 0; i < listtel.size(); i++) {
				if(listtel.get(i).getEstatus().equals("ALTA"))
					listcompaniatel.put( listtel.get(i).getIdTipoCompania(),listtel.get(i).getTipoCompania());
			}
			listcompaniatel =Utileria.sortByValue(listcompaniatel);
		}  
	}
	/**se utiliza para el filtrado del comboBox*/
	public void consultaCatalogoCompaniasTelefonos(Map<String,String> listado) {
		LOG.info("ConsultaCatalogoCompaniasTelefono");
		this.setPantalla(Constantes.telefonos_Datostelefono_pantalla);
		List<BeanTipoTelefono> listtel  = null;
		Respuesta respuesta = Call.callURLCompaniaTelefonos();
		listtel= respuesta.getListaCompTelefonos();
		listcompaniatel.clear();
	    if(listtel!=null){
			for (int i = 0; i < listtel.size(); i++) {
				if(listtel.get(i).getEstatus().equals("ALTA") && !listado.containsKey(listtel.get(i).getTipoCompania()))
					listcompaniatel.put( listtel.get(i).getIdTipoCompania(),listtel.get(i).getTipoCompania());
			}
			listcompaniatel =Utileria.sortByValue(listcompaniatel);
		}  
	}
	
	/**
	 * Consulta datos telefonos detalle 
	 * */
	public void consultarDatosTelefono(String idTel) {
		this.limpiarCamposDatos();
		this.consultaCatalogoTipoTelefonos(); /*Consulta la lista de catalogos de Tipo telefono*/
		this.consultaCatalogoCompaniasTelefonos();  /*Consulta la lista de catalogos de companias de telefono*/		
		LOG.info("Consultado Datos telefono::" + idTel);
		TelefonosReq telefonosReq = new TelefonosReq();
		BeanTelefonos bean = new BeanTelefonos();
		bean.setIdTelSeleccionado(idTel);
		telefonosReq.setTelefonos(bean);
		RestCall2<TelefonosReq, Respuesta> r2=new RestCall2<>();
		LOG.info("Datos principales del Telefono::: "+Urls.getUrlBase()+UrlService.TELEFONO+UrlService.CONSULTA_TELEFONO);
		  r2.setUrl(Urls.getUrlBase()+UrlService.TELEFONO+UrlService.CONSULTA_TELEFONO);
		  r2.setEntrada(telefonosReq);
		  r2.setClase(Respuesta.class);
		  Respuesta respuesta;
		  respuesta = r2.call();
		  if (respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)) {
			  List<BeanTelefonos> listdetalleTelefono = respuesta.getListaTelefonos();
			  if(!listdetalleTelefono.isEmpty()){
				  BeanTelefonos detalleTelefono = listdetalleTelefono.get(0); 
				  this.setLada(detalleTelefono.getNumero().substring(0, 3));
				  this.setNumero(detalleTelefono.getNumero().substring(3));
				  this.setPrincipal(detalleTelefono.isPrincipal());
				  this.setExtension(detalleTelefono.getExtension());
				  this.setObservacion(detalleTelefono.getObservacion());
				  this.setIdTelSeleccionado(detalleTelefono.getIdTelSeleccionado());
				  this.setTipoTelefono(detalleTelefono.getTipoTelefono());
				  this.setCompania(detalleTelefono.getCompania());
				  this.validaExtension();
				  this.validarLada();
				  this.setDisabledTipoTelefono("true");
				} else {
					LOG.info("Ha ocurrido un error al consultar los datos");
					this.limpiarCamposDatos();
					  
				}
			}		   
	}


	/**
	 * 
	 *  Valida Lada 
	 *  
	 *  */
	public void validarLada() {
		LOG.info("Lada::"+getLada());
		int contador =0;
		if(getLada().equals(HardCode.LADA_MEXICO)){
			setMaxchar(HardCode.NUMERO_MEXICO);
			if(this.getNumero()!=null)
				contador = this.getNumero().length();
			if(HardCode.NUMERO_MEXICO<contador)
				setNumero("");
			LOG.info(HardCode.NUMERO_MEXICO+"::NUM MEXICO");
		}else{
			setMaxchar(HardCode.NUMERO_NO_MEXICO);
			LOG.info(HardCode.NUMERO_NO_MEXICO+"::NUMERO_NO_MEXICO");
		}
	}
	
	/**
	 * 
	 *  Limpia los datos cada vez que se accede a la pantalla 
	 *  
	 *  */
	public void limpiarCamposDatos() {
		setTipoTelefono("");
		setNumero("");
		setCompania("");
		setObservacion("");
		this.setExtension("");
		this.setLada("");
		this.setPrincipal(false);
		
		if(getIdTelSeleccionado()!= null &&!getIdTelSeleccionado().equals(Constantes.REGISTRODEFAULT)){
			setIdTelSeleccionado(getIdTelSeleccionado());
			this.setDisabledTipoTelefono("true");
		}else{
			setIdTelSeleccionado(Constantes.REGISTRODEFAULT);
			this.setDisabledTipoTelefono("false");
			this.setDisabled("true");
		}
		consultaCatalogoCompaniasTelefonos(this.getCatalogoUsado()); /**se utiliza para el filtrado del comboBox*/
		consultaCatalogoTipoTelefonos(this.getCatalogoUsado()); /**se utiliza para el filtrado del comboBox*/
	}

	
	/**
	 * Cancela la modificación
	 * 
	 * */
	public void cancel(){
		LOG.info("Cancelar la modificación");
		setIdTelSeleccionado(Constantes.REGISTRODEFAULT); /*Selecciona el registro principal*/
		this.consultarDatosTelefono(getIdTelSeleccionado());/*Consulta Los datos del telefono principal*/
	}
	/**
	 * Se crea nuevo telefono
	 * */
	public void add(){
		LOG.info("Nuevo telefono");
		setIdTelSeleccionado(Constantes.REGISTRODEFAULT);
		limpiarCamposDatos();
	}
	
	/**
	 * @author JonaMata
	 * Elimina Campos
	 * @date 1-JUL-2017
	 * */
	public void delete(){
		LOG.info("Eliminando Registro");
		FacesContext context = FacesContext.getCurrentInstance();
		TelefonosReq reqTel = new TelefonosReq();
		BeanTelefonos tel = new BeanTelefonos();
		RestCall2<TelefonosReq, Respuesta> r2=new RestCall2<>();
		setEstatus(Constantes.ESTATUS_BAJA);
		tel.setEstatus(getEstatus());
		tel.setIdTelSeleccionado(getIdTelSeleccionado());
		reqTel.setTelefonos(tel); 
		r2.setEntrada(reqTel);
		
		
		LOG.info("::ID_TEL::"+getIdTelSeleccionado()+"::\nESTATUS::"+getEstatus());
		r2.setUrl(Urls.getUrlBase()+UrlService.TELEFONO+UrlService.ELIMINAR_TELEFONO);
		LOG.info("::URL ELIMINAR::"+Urls.getUrlBase()+UrlService.TELEFONO+UrlService.ELIMINAR_TELEFONO);
		Respuesta respuesta;
		r2.setClase(Respuesta.class);
		respuesta = r2.call();
		  LOG.info(".::RESPUESTA ERROR::."+respuesta.getError());
			if(respuesta.getError().getMessage().equals(Constantes.EXITO)){
					context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, HardCode.SUCCESS, HardCode.ELIMINAR_TELEFONO));
					this.consultarDatosTelefono(Constantes.REGISTRODEFAULT);
					this.consultaTablaTelefonos();
			}else{
				LOG.info("Ha ocurrio un error al eliminar");
				context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR,HardCode.ERROR,respuesta.getError().getMessage()));
			}
	}
	
	
	/**
	 * 
	 *  Guarda Campos registro
	 *   
	 **/
	public void save() {
		FacesContext context = FacesContext.getCurrentInstance();
		if(validaCamposVacios()){
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, HardCode.ERROR, HardCode.ERROR_LLENE_CAMPOS_REQUERIDOS));			
		}else{		
			boolean update = false;
			if(validaLadaVSNumero(context)){
				/**Mandar llamar al Service*/
				TelefonosReq request = new TelefonosReq();
				RestCall2<TelefonosReq, Respuesta> r2=new RestCall2<>();
				if(getIdTelSeleccionado().equals(Constantes.REGISTRODEFAULT)){ /** Crear registro nuevo*/
					LOG.info("Guardar Datos del Telefono::: "+Urls.getUrlBase()+UrlService.TELEFONO+UrlService.INSERTAR_TELEFONO);
					r2.setUrl(Urls.getUrlBase()+UrlService.TELEFONO+UrlService.INSERTAR_TELEFONO);
					update = false;
				}else{ /** Actualiza registro nuevo*/
					LOG.info("Actualizar Datos del Telefono::: "+Urls.getUrlBase()+UrlService.TELEFONO+UrlService.ACTUALIZAR_TELEFONO);
					r2.setUrl(Urls.getUrlBase()+UrlService.TELEFONO+UrlService.ACTUALIZAR_TELEFONO);
					update = true;
				}
				BeanTelefonos tel = new BeanTelefonos();
				tel.setIdTelSeleccionado(getIdTelSeleccionado());
				tel.setLada(getLada());
				tel.setNumero(getNumero());
				tel.setObservacion(getObservacion());
				tel.setPrincipal(isPrincipal());
				tel.setTipoTelefono(getTipoTelefono());
				tel.setCompania(getCompania());
				tel.setExtension(getExtension());
				tel.setEstatus(Constantes.ESTATUS_ACTIVO); 
				request.setTelefonos(tel); 
				r2.setEntrada(request);
				r2.setClase(Respuesta.class);
				Respuesta respuesta;
				respuesta = r2.call();
				if(respuesta.getError().getMessage().equals(Constantes.EXITO)){
						if(update) /** se ejecuto una actualización*/
							context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, HardCode.SUCCESS, HardCode.SUCCESS_MESSAGE));
						else{ /** se ejecuto una inserción*/
							context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, HardCode.SUCCESS, HardCode.SUCCESS_MESSAGE));
							this.consultarDatosTelefono(Constantes.REGISTRODEFAULT);
						}
					this.consultaTablaTelefonos(); /**Se vuelve a consulta la información de directorio de telefonos*/
				}else{
					context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, HardCode.ERROR, respuesta.getError().getMessage()));
					limpiarCamposDatos();
				}
			}
		}
	}
	/**
	 * Valida si existen campos vacios
	 * */
	public boolean validaCamposVacios(){
		boolean respuesta = false;
		if (getLada().isEmpty() || getNumero().isEmpty() || getTipoTelefono().equals(HardCode.SELECCIONE) ||getCompania().equals(HardCode.SELECCIONE)){
			respuesta = true;
		}
		return respuesta;
		
	}

	public void changeListener(final TabChangeEvent event) {
		AccordionPanel tv = (AccordionPanel) event.getComponent();
		LOG.info("------Cambiando TAB------" + tv.getActiveIndex());
	}
	public void onTabChange(TabChangeEvent event) {
		LOG.info("Tab ChangedActive Tab: "+ event.getTab().getTitle());
		FacesMessage msg = new FacesMessage("Tab Changed", "Active Tab: " + event.getTab().getTitle());
		FacesContext.getCurrentInstance().addMessage(null, msg);
	}
	/**
	 * Valida se se habilita o deshabilita la extensión
	 * */
	

	
	/**
	 * Valida que la lada coincida con el numero
	 * */
	public boolean validaLadaVSNumero(FacesContext context){
		boolean respuesta = false;
		if(getNumero().length()==HardCode.NUMERO_MEXICO && getLada().equals(HardCode.LADA_MEXICO) || getNumero().length()==HardCode.NUMERO_NO_MEXICO && getLada().length()==3 )/*Valida que el telefono sea de mexio u otro estado y que se encuentre correcto*/
			respuesta = true;
		else if(getNumero().length()!=HardCode.NUMERO_MEXICO && getLada().equals(HardCode.LADA_MEXICO)) /*Lada de Mexico pero longitud numero equivocado*/
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, HardCode.ERROR, HardCode.ERROR_NUMERO_MEXICO));		
		else if(getNumero().length()!=HardCode.NUMERO_NO_MEXICO || getLada().length()!=3) /*Lada de otro Cualquier estado pero longitud numero equivocado*/
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, HardCode.ERROR, HardCode.ERROR_NUMERO_NO_MEXICO));
		else
			context.addMessage(null, new FacesMessage(FacesMessage.SEVERITY_ERROR, HardCode.ERROR, HardCode.ERROR_GENERICO));
		return respuesta;
	}
	/**
	 * Valida el estatus de la extensión
	 * */
	public void validaExtension(){
		
		if(this.getTipoTelefono()!=null){
			String valor = validaExtension.get(this.getTipoTelefono());
			if(valor == null||valor.equals("A"))
				valor = "0";
			if(Integer.parseInt(valor) == HardCode.REQUIERE_EXTENSION)
				this.setDisabled("false");
			else
				this.setDisabled("true");
		}else{
			this.setDisabled("true");
		}
	}
	
	
	/**
	 * ------------------CODIGO PARA DIRECTORIO TELEFONOS ----------------
	 */
	/**
	 * Consulta los datos de la tabla
	 * */
	public void consultaTablaTelefonos() {
		this.consultaCatalogoTipoTelefonos(); /*Consulta la lista de catalogos de Tipo telefono*/
		this.consultaCatalogoCompaniasTelefonos();  /*Consulta la lista de catalogos de companias de telefono*/
//		this.limpiarCamposDatos();
		RestCall2<TelefonosReq, Respuesta> r2=new RestCall2<>();
		TelefonosReq request = new TelefonosReq();
		r2.setUrl(Urls.getUrlBase()+UrlService.TELEFONO+UrlService.CONSULTA_DIRECTORIO_TELEFONO);
		r2.setEntrada(request);
		r2.setClase(Respuesta.class);
		Respuesta respuesta;
		respuesta = r2.call();
		this.setPantalla(Constantes.telefonos_Directoriotelefonico_pantalla);
		this.setCatalogoUsado(new LinkedHashMap<String,String>());  /**se utiliza para el filtrado del comboBox*/
		if(respuesta.getError().getClave().equals(Constantes.CLAVE_CORRECTA)){
			List<BeanTelefonos> listtel = respuesta.getListaTelefonos();
			for(BeanTelefonos bean : listtel){
				bean.setTipoTelefono(listtipotel.get(bean.getTipoTelefono()));
				bean.setCompania(listcompaniatel.get(bean.getCompania()));
				this.getCatalogoUsado().put(bean.getCompania(), bean.getCompania()); /**se utiliza para el filtrado del comboBox*/
				this.getCatalogoUsado().put(bean.getTipoTelefono(), bean.getTipoTelefono()); /**se utiliza para el filtrado del comboBox*/
			}
			this.setListaDirectorio(listtel);
		}else{			
			setMensaje(respuesta.getError().getMessage());
		}
	}

	/* Permite obtener el Id de la tabla seleccionada */
	public void onRowSelect() {
		setActiveIndex(0);
		consultarDatosTelefono(getListaDirectorioSelected().getIdTelSeleccionado());
	}

	
	
	/**
	 * 
	 * GETTERS AND SETTERS
	 * 
	 * 
	 */

	public String getExtensionLength() {
		return extensionLength;
	}
	public void setExtensionLength(String extensionLength) {
		this.extensionLength = extensionLength;
	}
	
	public String getLadaLength() {
		return ladaLength;
	}
	public void setLadaLength(String ladaLength) {
		this.ladaLength = ladaLength;
	}
	public String getNumeroLength() {
		return numeroLength;
	}
	public void setNumeroLength(String numeroLength) {
		this.numeroLength = numeroLength;
		
	}
	public String getObservLength() {
		return observLength;
	}
	public void setObservLength(String observLength) {
		this.observLength = observLength;
	}
	
	/**
	 * 
	 *Listado de Tipo de telefonos 
	 * */
	public Map<String, String> getListtipotel() {
		return listtipotel;
	}
	public void setListtipotel(Map<String, String> listtipotel) {
		this.listtipotel = listtipotel;
	}
	/**
	 * 
	 *Listado de Companias de telefonos 
	 * */
	public Map<String, String> getListcompaniatel() {
		return listcompaniatel;
	}
	public void setListcompaniatel(Map<String, String> listcompaniatel) {
		this.listcompaniatel = listcompaniatel;
	}
	public int getActiveIndex() {
		return activeIndex;
	}
	public void setActiveIndex(int activeIndex) {
		this.activeIndex = activeIndex;
	}
	public Map<String, String> getValidaExtension() {
		return validaExtension;
	}
	public void setValidaExtension(Map<String, String> validaExtension) {
		this.validaExtension = validaExtension;
	}
	public Integer getExtensionPermitida() {
		return extensionPermitida;
	}
	public void setExtensionPermitida(Integer extensionPermitida) {
		this.extensionPermitida = extensionPermitida;
	}
	public String getDisabledTipoTelefono() {
		return disabledTipoTelefono;
	}
	public void setDisabledTipoTelefono(String disabledTipoTelefono) {
		this.disabledTipoTelefono = disabledTipoTelefono;
	}
	
	
	
	

}
