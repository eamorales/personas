package com.mx.promesi.conf;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;

import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.mx.constantes.Constantes;
import com.mx.url.UrlService;


@Service
public class Urls {
	private static final Logger LOG = Logger.getLogger(Urls.class);
	Urls(){
		Properties propiedades = new Properties();
		try {
			/**Se crear el properties*/
			/**Cargamos el archivo desde la ruta especificada*/
			propiedades.load(getClass().getResourceAsStream("personas.properties"));
			
			setIp(propiedades.getProperty("ip"));
			setIpServidorServicios(propiedades.getProperty("ipServicios"));
		
			setIpFront(propiedades.getProperty("ipFront"));
			
			setPuerto(propiedades.getProperty("puerto"));
			setWebService(propiedades.getProperty("webService"));
			setWebServicePersonas(propiedades.getProperty("webServicePersonas"));
			setWebServiceLogin(propiedades.getProperty("webServiceLogin"));
						
			setPuertoServidor(propiedades.getProperty("puertoServidor"));
			setWebServiceServidor(propiedades.getProperty("webServiceServidor"));
			
			setWebServiceAlfresco(propiedades.getProperty("webServiceAlfresco"));
			
			setUrlConsultaById(propiedades.getProperty("urlConsultaById"));
			
			setUrlBase(getIp()+getPuerto()+getWebService());
			setUrlBaseLogin(getIpFront()+getPuerto()+getWebServiceLogin());
			setUrlBasePersonas(getIpFront()+getPuerto()+getWebServicePersonas());
			
			setWebSerciceConsultaTipoTelefonos(getIpServidorServicios()+getPuertoServidor()+getWebServiceServidor()+Constantes.PATH_TIPO_TELEFONO+getUrlConsultaById());
			setWebServiceConsultaColoniaDomicilios(getIpServidorServicios()+getPuertoServidor()+getWebServiceServidor()+Constantes.PATH_COLONIA_DOMICILIO+getUrlConsultaById());
			setWebServiceConsultaCompaniaTelefonos(getIpServidorServicios()+getPuertoServidor()+getWebServiceServidor()+Constantes.PATH_COMPANIA_TELEFONO+getUrlConsultaById());
			setWebServiceConsultaEstadoCivil(getIpServidorServicios()+getPuertoServidor()+getWebServiceServidor()+Constantes.PATH_EDOCIVIL+getUrlConsultaById());
			setWebServiceConsultaOcupaciones(getIpServidorServicios()+getPuertoServidor()+getWebServiceServidor()+Constantes.PATH_OCUPACIONES+getUrlConsultaById());
			setWebServiceConsultaPaises(getIpServidorServicios()+getPuertoServidor()+getWebServiceServidor()+Constantes.PATH_PAISES+getUrlConsultaById());
			
			setWebServiceConsultaEstadoNac(getIpServidorServicios()+getPuertoServidor()+getWebServiceServidor()+Constantes.PATH_ESTADONAC+getUrlConsultaById());
			setWebServiceConsultaMunicipios(getIpServidorServicios()+getPuertoServidor()+getWebServiceServidor()+Constantes.PATH_MUNICIPIOS+getUrlConsultaById());
			setWebServiceConsultaLocalidades(getIpServidorServicios()+getPuertoServidor()+getWebServiceServidor()+Constantes.PATH_LOCALIDADES+getUrlConsultaById());
			setWebServiceConsultaGrado(getIpServidorServicios()+getPuertoServidor()+getWebServiceServidor()+Constantes.PATH_GRADOESTUDIOS+getUrlConsultaById());
			setWebServiceConsultaDomicilioById(getUrlBase()+UrlService.DOMICILIO+UrlService.CONSULTA_DIRECION_ID);
			setWebServiceConsultaTipoUbicaciones(getUrlBase()+UrlService.DOMICILIO+UrlService.CONSULTA_TIPOUBICACIONES); /**Consulta el tipo de ubicación en la sección de domicilios*/
			
			setWebServiceAlertas(getIpServidorServicios()+getPuertoServidor()+getWebServiceServidor()+Constantes.PATH_ALERTAS+getUrlConsultaById());
			/**Obtenemos los parametros definidos en el archivo*/
		} catch (FileNotFoundException e) {
			LOG.info("Error, El archivo no exite");
		} catch (IOException e) {
			LOG.info("ha ocurrido un error al leer el archivo");
		}
	}
		
	/**
	 * 
	 *  Datos de conexion al servicio REST
	 *  
	 *  */
	private static String ip;
	private static String ipFront;
	private static String puerto;
	private static String webService;
	private static String webServicePersonas;
	private static String webServiceLogin;
	private static String urlBase;
	private static String urlBaseLogin;
	private static String urlBasePersonas;
	private static String webServiceAlfresco;
//	
	/**
	 * 
	 * Datos de Conexion al Servidor del Nucleo
	 * */
	private static String ipServidorServicios;
	private static String puertoServidor;
	private static String webServiceServidor;

	/**
	 * 
	 * 
	 * Servicios Web
	 * */

	private static String urlConsultaById;
	private static String webSerciceConsultaTipoTelefonos;
	private static String webServiceConsultaColoniaDomicilios;
	private static String webServiceConsultaCompaniaTelefonos;
	private static String webServiceConsultaDomicilioById;
	private static String webServiceConsultaTipoUbicaciones;
	private static String webServiceConsultaEstadoCivil;
	private static String webServiceConsultaOcupaciones;
	private static String webServiceConsultaPaises;
	private static String webServiceConsultaEstadoNac;
	private static String webServiceConsultaCorreos;
	private static String webServiceConsultaGrado;
	private static String webServiceConsultaMunicipios;
	private static String webServiceConsultaLocalidades;
	private static String webServiceAlertas;


	

	public static String getIp() {
		return ip;
	}
	public static void setIp(String ip) {
		Urls.ip = ip;
	}
	public static String getUrlBasePersonas() {
		return urlBasePersonas;
	}
	public static void setUrlBasePersonas(String urlBasePersonas) {
		Urls.urlBasePersonas = urlBasePersonas;
	}
	
	
	public static String getWebServicePersonas() {
		return webServicePersonas;
	}
	public static void setWebServicePersonas(String webServicePersonas) {
		Urls.webServicePersonas = webServicePersonas;
	}
	public static String getIpFront() {
		return ipFront;
	}
	public static void setIpFront(String ipFront) {
		Urls.ipFront = ipFront;
	}
	public static String getUrlBaseLogin() {
		return urlBaseLogin;
	}
	public static void setUrlBaseLogin(String urlBaseLogin) {
		Urls.urlBaseLogin = urlBaseLogin;
	}
	public static String getWebServiceLogin() {
		return webServiceLogin;
	}
	public static void setWebServiceLogin(String webServiceLogin) {
		Urls.webServiceLogin = webServiceLogin;
	}
	public static String getPuerto() {
		return puerto;
	}
	public static void setPuerto(String puerto) {
		Urls.puerto = puerto;
	}
	public static String getWebService() {
		return webService;
	}
	public static void setWebService(String webService) {
		Urls.webService = webService;
	}
	public static String getUrlBase() {
		return urlBase;
	}
	public static void setUrlBase(String urlBase) {
		Urls.urlBase = urlBase;
	}
	
	public static String getIpServidorServicios() {
		return ipServidorServicios;
	}
	public static void setIpServidorServicios(String ipServidorServicios) {
		Urls.ipServidorServicios = ipServidorServicios;
	}
	public static String getPuertoServidor() {
		return puertoServidor;
	}
	public static void setPuertoServidor(String puertoServidor) {
		Urls.puertoServidor = puertoServidor;
	}
	public static String getWebServiceServidor() {
		return webServiceServidor;
	}
	public static void setWebServiceServidor(String webServiceServidor) {
		Urls.webServiceServidor = webServiceServidor;
	}
	
	public static String getWebServiceAlfresco() {
		return webServiceAlfresco;
	}
	public static void setWebServiceAlfresco(String webServiceAlfresco) {
		Urls.webServiceAlfresco = webServiceAlfresco;
	}
	public static String getUrlConsultaById() {
		return urlConsultaById;
	}
	public static void setUrlConsultaById(String urlConsultaById) {
		Urls.urlConsultaById = urlConsultaById;
	}
	public static String getWebServiceConsultaColoniaDomicilios() {
		return webServiceConsultaColoniaDomicilios;
	}
	public static String getWebServiceConsultaDomicilioById() {
		return webServiceConsultaDomicilioById;
	}
	public static void setWebServiceConsultaDomicilioById(String webServiceConsultaDomicilioById) {
		Urls.webServiceConsultaDomicilioById = webServiceConsultaDomicilioById;
	}
	public static String getWebServiceConsultaTipoUbicaciones() {
		return webServiceConsultaTipoUbicaciones;
	}
	public static void setWebServiceConsultaTipoUbicaciones(String webServiceConsultaTipoUbicaciones) {
		Urls.webServiceConsultaTipoUbicaciones = webServiceConsultaTipoUbicaciones;
	}
	public static void setWebServiceConsultaColoniaDomicilios(String webServiceConsultaColoniaDomicilios) {
		Urls.webServiceConsultaColoniaDomicilios = webServiceConsultaColoniaDomicilios;
	}
	public static String getWebSerciceConsultaTipoTelefonos() {
		return webSerciceConsultaTipoTelefonos;
	}
	public static void setWebSerciceConsultaTipoTelefonos(String webSerciceConsultaTipoTelefonos) {
		Urls.webSerciceConsultaTipoTelefonos = webSerciceConsultaTipoTelefonos;
	}
	public static String getWebServiceConsultaCompaniaTelefonos() {
		return webServiceConsultaCompaniaTelefonos;
	}
	public static void setWebServiceConsultaCompaniaTelefonos(String webServiceConsultaCompaniaTelefonos) {
		Urls.webServiceConsultaCompaniaTelefonos = webServiceConsultaCompaniaTelefonos;
	}
	public static String getWebServiceConsultaEstadoCivil() {
		return webServiceConsultaEstadoCivil;
	}
	public static void setWebServiceConsultaEstadoCivil(String webServiceConsultaEstadoCivil) {
		Urls.webServiceConsultaEstadoCivil = webServiceConsultaEstadoCivil;
	}
	public static String getWebServiceConsultaOcupaciones() {
		return webServiceConsultaOcupaciones;
	}
	public static void setWebServiceConsultaOcupaciones(String webServiceConsultaOcupaciones) {
		Urls.webServiceConsultaOcupaciones = webServiceConsultaOcupaciones;
	}
	public static String getWebServiceConsultaPaises() {
		return webServiceConsultaPaises;
	}
	public static void setWebServiceConsultaPaises(String webServiceConsultaPaises) {
		Urls.webServiceConsultaPaises = webServiceConsultaPaises;
	}
	public static String getWebServiceConsultaEstadoNac() {
		return webServiceConsultaEstadoNac;
	}
	public static void setWebServiceConsultaEstadoNac(String webServiceConsultaEstadoNac) {
		Urls.webServiceConsultaEstadoNac = webServiceConsultaEstadoNac;
	}
	public static String getWebServiceConsultaCorreos() {
		return webServiceConsultaCorreos;
	}
	public static void setWebServiceConsultaCorreos(String webServiceConsultaCorreos) {
		Urls.webServiceConsultaCorreos = webServiceConsultaCorreos;
	}
	public static String getWebServiceConsultaGrado() {
		return webServiceConsultaGrado;
	}
	public static void setWebServiceConsultaGrado(String webServiceConsultaGrado) {
		Urls.webServiceConsultaGrado = webServiceConsultaGrado;
	}
	public static String getWebServiceConsultaMunicipios() {
		return webServiceConsultaMunicipios;
	}
	public static void setWebServiceConsultaMunicipios(String webServiceConsultaMunicipios) {
		Urls.webServiceConsultaMunicipios = webServiceConsultaMunicipios;
	}
	public static String getWebServiceConsultaLocalidades() {
		return webServiceConsultaLocalidades;
	}
	public static void setWebServiceConsultaLocalidades(String webServiceConsultaLocalidades) {
		Urls.webServiceConsultaLocalidades = webServiceConsultaLocalidades;
	}
	public static String getWebServiceAlertas() {
		return webServiceAlertas;
	}
	public static void setWebServiceAlertas(String webServiceAlertas) {
		Urls.webServiceAlertas = webServiceAlertas;
	}
	
	

}
