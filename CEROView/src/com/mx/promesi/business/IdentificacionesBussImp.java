package com.mx.promesi.business;

import org.apache.log4j.Logger;

import com.mx.beans.BeanIdentificacion;
import com.mx.promesi.service.IdentificacionesService;
import com.mx.promesi.service.IdentificacionesServiceImp;

public class IdentificacionesBussImp implements IdentificacionesBuss {
	 private static final Logger LOG = Logger.getLogger(IdentificacionesBussImp.class);
	 
	 IdentificacionesService ideTest;
	/**
	 * -----METODOS PARA DIRECTORIO IDENTIFICACIONES
	 **/
	/*Consulta catalogo de datos identificaciones*/
	@Override
	public BeanIdentificacion consultaTablaIdentificaciones() {
		LOG.info("ConsultaTablaIdentificaciones");
		ideTest = new IdentificacionesServiceImp();
        return ideTest.consultaTabla_identificaciones();
	}

}
