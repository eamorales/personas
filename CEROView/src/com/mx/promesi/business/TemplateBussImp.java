package com.mx.promesi.business;

import javax.annotation.PostConstruct;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.mx.beans.BeanTemplate;
import com.mx.promesi.service.TemplateService;
import com.mx.promesi.service.TemplateServiceImp;


@Component
public class TemplateBussImp implements TemplateBuss {
	  private static final Logger LOG = Logger.getLogger(TemplateBussImp.class);
	  
	  @Autowired 
	  TemplateService templateser;
	  
	 @PostConstruct
    public void init() {
        LOG.info("EL BEAN TemplateBussImp se ha inicializado.");
    }
	@Override
	public BeanTemplate ConsultarInformacion() {
        LOG.info("Consultando Información.");
        if(templateser == null)
        	templateser = new TemplateServiceImp();
        return templateser.ConsultarInformacion();
	}
	
}
