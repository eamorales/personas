package com.mx.promesi.business;

import com.mx.beans.BeanIdentificacion;

public interface IdentificacionesBuss {
	
	/**
	 * -----METODOS PARA DIRECTORIO IDENTIFICACIONES
	 **/
	/*Consulta catalogo de datos identificaciones*/
	BeanIdentificacion consultaTablaIdentificaciones();
}
