//package com.mx.promesi.business;
//
//import java.util.List;
//
//import javax.annotation.PostConstruct;
//
//import org.apache.log4j.Logger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import com.mx.beans.BeanPersona;
//import com.mx.beans.BeanTelefonos;
//import com.mx.beans.BeanTipoTelefono;
//import com.mx.promesi.service.TelefonosService;
//import com.mx.promesi.service.TelefonosServiceImp;
///**
// * @author JonaMata
// * @date 28/05/2017
// * @category Telefonos Bussines Impl
// */
//@Service
//public class TelefonosBussImpl implements TelefonosBuss {
//	
//	  private static final Logger LOG = Logger.getLogger(TemplateBussImp.class);
//	  
//	  TelefonosService telTest;
//	 @PostConstruct
//    public void init() {
//        LOG.info("EL BEAN TemplateBussImp se ha inicializado.");
//    }
//	 
//	 /**
//		 * -----METODOS PARA DATOS TELEFONOS
//	  **/
//	 
//	 /*Consulta catalogo de datos telefonos*/
//		@Override
//		public List<BeanTipoTelefono> consultaCatalogo() {
////			if(telTest==null)
////				telTest = new TelefonosServiceImp();
//			return telTest.consultaCatalogo(); 
//		}
//		/*Consulta Datos telefono*/
//		@Override
//		public BeanTelefonos consultarDatosTelefono(BeanPersona persona, String idTel) {
//			LOG.info("ConsultandoDatoTelefono -- Buss");
//			if(telTest==null)
//				telTest = new TelefonosServiceImp();
//			return telTest.ConsultarDatosTelefono(persona, idTel); 
//		}
//		
//		
//	/**
//	 * -----METODOS PARA DIRECTORIO TELEFONOS
//	 **/
//		
//	 /*Consulta catalogo de datos telefonos*/
//	@Override
//	public BeanTelefonos consultaTabla_telefonos() {
//		telTest = new TelefonosServiceImp();
//        return telTest.consultaTabla_telefonos();
//	}
//
//	
//}
