package com.mx.promesi.business;

import java.util.List;

import com.mx.beans.BeanPersona;
import com.mx.beans.BeanTelefonos;
import com.mx.beans.BeanTipoTelefono;


public interface TelefonosBuss {
	/**
	 * -----METODOS PARA DATOS TELEFONOS
	 **/
	
	/*Consulta catalogo de datos telefonos*/
	public List<BeanTipoTelefono> consultaCatalogo();
	
	/*Consulta Datos telefono*/
	public BeanTelefonos consultarDatosTelefono(BeanPersona persona, String idTel);
	
	/**
	 * -----METODOS PARA DIRECTORIO TELEFONOS
	 **/
	/*Consulta catalogo de datos telefonos*/
	public BeanTelefonos consultaTabla_telefonos();

	
}
