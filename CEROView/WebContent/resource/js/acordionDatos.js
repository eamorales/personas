function init() {
//$('#myBtn').click(function() {
//	$('#myModal').show();			
//});
//	/**TEST BSUQUEDA**/
//	$("#ModalMain").load("XHTML/Modal/modal.xhtml");
//	/*****************/
	
	$(".acordeonDatosPersonalidad").hide();
	$(".acordeonDatosGenerales").show();
};

function ActividadProductiva(actividad){
	if(actividad==1){
		$("#actprod1").prop('checked', true);
		$("#informacionGeneralContent").load("XHTML/Datos/InformacionLaboral_Asalariado.xhtml");
		
	}else if(actividad==2){
		$("#actprod2").prop('checked', true);
		$("#informacionGeneralContent").load("XHTML/Datos/InformacionLaboral_ActividadProductiva.xhtml");
	}
};
function TipoPersonas(tipo){
	/*Valida si la persona es Fisica o Moral*/
	if(tipo==1){
		$("#tipPersonas1").prop('checked', true);
		$(".LabelAcordeon02").html("Datos Adicionales");
		$(".LabelAcordeon03").html("Informaci&oacute;n Laboral");
		
		$("#main").empty();
		$("#main").load("XHTML/Datos/acordeonContent.xhtml",function(){
		$(".menuRadioInfoLaboral").show();
		});
	}else if (tipo==2){
		$(".LabelAcordeon02").html("Datos Personalidad");
		$(".LabelAcordeon03").html("Datos Adicionales");
		
		$(".menuRadioInfoLaboral").hide();
		$("#tipPersonas2").prop('checked', true);
		$("#datosGeneralesContent").load("XHTML/Datos/datosGeneralesPM.xhtml");
		$("#datosAdicionalesContent").load("XHTML/Datos/datosPersonalidad.xhtml");
		$("#informacionGeneralContent").load("XHTML/Datos/datosAdicionalesPM.xhtml");
	} 
};

init(); 