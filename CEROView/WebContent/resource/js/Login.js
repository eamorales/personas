
/*Validación de solo numeros*/
function justNumbers(e) {
	var keynum = window.event ? window.event.keyCode : e.which;
	if ((keynum == 8) || (keynum == 45))
		return true;

	return /\d/.test(String.fromCharCode(keynum));
}

function cargalogtoken(){
	var passwd = $("#login\\:txtContrasena").val();
	var txtUser = $("#login\\:txtUser").val();
	if (passwd.length<=5 || txtUser==""){
		$("#login\\:txtContrasena").css( 'border-color','red');
		$("#login\\:txtUser").css( 'border-color','red');
	} else {
		$("#login\\:txtContrasena").css( 'border-color','gray');		
	}
	
}
function colorGris(){
	$("#txtContrasena").css( 'border-color','gray');
	
}
function cargaLogBusqueda(){
	var passwd = $("#log-token-form\\:txtPasswd").val();
	if (passwd.length<=5){
		$("#log-token-form\\:txtPasswd").css( 'border-color','red');
	} 
}