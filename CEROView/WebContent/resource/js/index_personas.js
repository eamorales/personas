function init() {
	$("#main").load("XHTML/Datos/acordeonContent.xhtml");
	/* Menu-toggle */
	$("#menu-toggle").click(function(e) {
		e.preventDefault();
		$("#nav").toggleClass("active");
	});

}
/*Validación de solo numeros*/
function justNumbers(e) {
	var keynum = window.event ? window.event.keyCode : e.which;
	if ((keynum == 8) || (keynum == 45))
		return true;

	return /\d/.test(String.fromCharCode(keynum));
}

function LetrasYNumeros(e) {
	var key = window.event ? e.keyCode : e.which;
	if (key == 13 || ((key >= 33) && (key <= 64))
			|| ((key >= 91) && (key <= 96)) || (key >= 123)) {
		var keychar = String.fromCharCode(key);
		reg = /\d/;
		return reg.test(keychar);
	}
	else {
		var keychar = String.fromCharCode(key);
		reg = /\d/;
		return !reg.test(keychar);
	}
}
init();



/*CONSTANTES*/
				/*General*/
var Constante_SELECCIONAR = "--SELECCIONAR--";
				/*Telefonos*/
var Constante_requiereExtension = "1";
var CONS_PAG_DEFAULT = 1; 
var CONS_TAB_UNO = 1; 
