function init() {
	/*************************VALIDACIONES JMATA*****************************/
	
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:cp").keypress(function(){
		$( "#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:cp" ).trigger( "click" );
	});
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:subirArchivo").click(function(){
		alert("click");
		$(".ui-corner-all ui-button-text-icon-left ui-fileupload-upload").trigger("click");		
	})
	/**-------------------------------------------------------------------**/
	
	//Cambia a color gris el campo de texto Codigo Postal
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:cp").keypress(function(){
		colorGris($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:cp"));
	});
	
	//Cambia a color gris el campo de texto Descripcion 1
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescrip1").keypress(function(event){
		colorGris($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescrip1"));
		return LetrasYNumeros(event);
	});
	//Cambia a color gris el campo de texto Descripcion 2
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescrip2").keypress(function(event){
		colorGris($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescrip2"));
		return LetrasYNumeros(event);
	});
	//Cambia a color gris el campo de texto Descripcion 3
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescrip3").keypress(function(event){
		colorGris($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescrip3"));
		return LetrasYNumeros(event);
	});
	//Cambia a color gris el campo de texto Descripcion Numero 1
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescripN1").keypress(function(event){
		colorGris($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescripN1"));
		return LetrasYNumeros(event);
	});

}
function colorGris(datos){
	datos.css( 'border-color','gray');
}
function colorRojo(datos){
	datos.css( 'border-color','red');
}
function validarCampos(){
	/**Valida campo Codigo Postal**/
	if($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:cp").val().length==0||$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:cp").val().length<=4){
		var x = $("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:cp");
		colorRojo(x);
	}
	//Valida campo texto Descripcion 1
	if($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescrip1").val().length==0){
		colorRojo($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescrip1"));
	}
	//Valida campo texto Descripcion 2
	if($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescrip2").val().length==0){
		colorRojo($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescrip2"));
		
	}
	//Valida campo texto Descripcion 3
	if($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescrip3").val().length==0){
		colorRojo($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescrip3"));
		
	}
	//Valida campo texto  Descripcion Numero 1
	if($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescripN1").val().length==0){
		colorRojo($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:txtDescripN1"));
		
	}
	/**Valida combos **/
	if($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:cmbColonia").find('option:selected').text() == "--SELECCIONAR--")
		colorRojo($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:cmbColonia"));
	else
		colorGris($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:cmbColonia"));
	
	if($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos02").find('option:selected').text() == "--SELECCIONAR--")
		colorRojo($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos02"));
	else
		colorGris($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos02"));
	
	if($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos03").find('option:selected').text() == "--SELECCIONAR--")
		colorRojo($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos03"));
	else
		colorGris($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos03"));
	
	if($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos04").find('option:selected').text() == "--SELECCIONAR--")
		colorRojo($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos04"));
	else
		colorGris($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos04"));

	if($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos05").find('option:selected').text() == "--SELECCIONAR--")
		colorRojo($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos05"));
	else
		colorGris($("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos05"));
}
function cambiaColorcmboColonia(){
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:cmbColonia").css( 'border-color','gray');
}
function cambiaColorCmboUbic1(){
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:cmbColonia").css( 'border-color','gray');
}
function cambiaColorCmboUbic1(){
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos02").css( 'border-color','gray');
}
function cambiaColorCmboUbic2(){
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos03").css( 'border-color','gray');
}
function cambiaColorCmboUbic3(){
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos04").css( 'border-color','gray');
}
function cambiaColorCmbonum1(){
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos05").css( 'border-color','gray');
}
function cambiaColorCmbonum2(){
	$("#acordeonGeneral\\:domicilios_form\\:form-domDatos\\:CmbdomiciliosDatos06").css( 'border-color','gray');
}
init(); 